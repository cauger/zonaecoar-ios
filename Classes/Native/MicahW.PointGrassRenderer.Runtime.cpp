﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>


struct VirtualActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
struct GenericVirtualActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
struct InterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
struct GenericInterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct InvokerActionInvoker1;
template <typename T1>
struct InvokerActionInvoker1<T1*>
{
	static inline void Invoke (Il2CppMethodPointer methodPtr, const RuntimeMethod* method, void* obj, T1* p1)
	{
		void* params[1] = { p1 };
		method->invoker_method(methodPtr, method, obj, params, NULL);
	}
};
template <typename T1, typename T2>
struct InvokerActionInvoker2;
template <typename T1, typename T2>
struct InvokerActionInvoker2<T1*, T2*>
{
	static inline void Invoke (Il2CppMethodPointer methodPtr, const RuntimeMethod* method, void* obj, T1* p1, T2* p2)
	{
		void* params[2] = { p1, p2 };
		method->invoker_method(methodPtr, method, obj, params, NULL);
	}
};

// System.Func`2<System.Object,MicahW.PointGrass.PointGrassCommon/ObjectData>
struct Func_2_tC0C5B5765EBF104D9C013A26717BC3C3AC47157C;
// System.Func`2<MicahW.PointGrass.PointGrassDisplacer,MicahW.PointGrass.PointGrassCommon/ObjectData>
struct Func_2_t868371C41B8A357BB81D47C6157D60859AD0411F;
// System.Collections.Generic.IEnumerable`1<UnityEngine.ComputeBuffer>
struct IEnumerable_1_t967BA6D97F13DEB72F9F10E2CDCE3BA414052931;
// System.Collections.Generic.IEnumerable`1<UnityEngine.MaterialPropertyBlock>
struct IEnumerable_1_t5EBD50CB174298656EBBE5E42CC24981CA2FB2F5;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_tF95C9E01A913DD50575531C8305932628663D9E9;
// System.Collections.Generic.IEnumerable`1<MicahW.PointGrass.PointGrassDisplacer>
struct IEnumerable_1_tDEFF903946DCF12B67DA695266A03EE819DD0110;
// System.Collections.Generic.IEnumerable`1<UnityEngine.TerrainLayer>
struct IEnumerable_1_tFF061A9026619AE9CDDB0428E268D9D243611A8F;
// System.Collections.Generic.IEnumerable`1<MicahW.PointGrass.PointGrassCommon/ObjectData>
struct IEnumerable_1_t6AE8C55D594C13D201E5B611CE2AC10073CA20ED;
// System.Collections.Generic.List`1<UnityEngine.Bounds>
struct List_1_t7D73463EA17F4D29A37E8110AB60DAD5C80EBF65;
// System.Collections.Generic.List`1<UnityEngine.ComputeBuffer>
struct List_1_t7DA451437393B82A39CEE1076115D7FD8B970B3B;
// System.Collections.Generic.List`1<UnityEngine.MaterialPropertyBlock>
struct List_1_tAFCDB9CCBAF11013AA7B0EC7BB51B10127AB467C;
// System.Collections.Generic.List`1<System.Object>
struct List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D;
// System.Collections.Generic.List`1<MicahW.PointGrass.PointGrassDisplacer>
struct List_1_tDEBBED5B3545B06B3FD16E4ACE9B2C7AB62A1B3E;
// System.Boolean[]
struct BooleanU5BU5D_tD317D27C31DB892BE79FAE3AEBC0B3FFB73DE9B4;
// UnityEngine.Bounds[]
struct BoundsU5BU5D_t45563ED55B62FA0536E8117454C56C0CFA5B25F5;
// UnityEngine.Color[]
struct ColorU5BU5D_t612261CF293F6FFC3D80AB52259FF0DC2B2CC389;
// UnityEngine.ComputeBuffer[]
struct ComputeBufferU5BU5D_t7832804740B13E96807A836AD90ADF1477D7FE27;
// System.Delegate[]
struct DelegateU5BU5D_tC5AB7E8F745616680F337909D3A8E6C722CDF771;
// System.Int32[]
struct Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C;
// System.IntPtr[]
struct IntPtrU5BU5D_tFD177F8C806A6921AD7150264CCC62FA00CAD832;
// UnityEngine.Material[]
struct MaterialU5BU5D_t2B1D11C42DB07A4400C0535F92DBB87A2E346D3D;
// UnityEngine.MaterialPropertyBlock[]
struct MaterialPropertyBlockU5BU5D_t6911DDC471FC23F2AF58FF0140ED595529EC5F5B;
// UnityEngine.Mesh[]
struct MeshU5BU5D_t178CA36422FC397211E68FB7E39C5B2F95619689;
// UnityEngine.MeshFilter[]
struct MeshFilterU5BU5D_tCE3B457E6F7ECE5ECEE9E09150642150448685BA;
// System.Object[]
struct ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918;
// MicahW.PointGrass.PointGrassDisplacer[]
struct PointGrassDisplacerU5BU5D_tCF7DC1B216819C23C858D699519E75C0F1A53C7A;
// System.Single[]
struct SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF;
// UnityEngine.TerrainLayer[]
struct TerrainLayerU5BU5D_t259E391D6115F121FCD284E79F62012D70956EB0;
// UnityEngine.Texture2D[]
struct Texture2DU5BU5D_t05332F1E3F7D4493E304C702201F9BE4F9236191;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C;
// MicahW.PointGrass.PointGrassCommon/MeshPoint[]
struct MeshPointU5BU5D_tBCEA150F23C3861E840A87B994115F06D7490DD8;
// MicahW.PointGrass.PointGrassCommon/ObjectData[]
struct ObjectDataU5BU5D_tFCDCD60E1D45526EC9B864EF342ACB7834D38930;
// System.Single[,,]
struct SingleU5BU2CU2CU5D_tE902E5192C7283A470AAADB477117789A9682488;
// System.ArgumentException
struct ArgumentException_tAD90411542A20A9C72D5CDA3A84181D8B947A263;
// System.AsyncCallback
struct AsyncCallback_t7FEF460CBDCFB9C5FA2EF776984778B9A4145F4C;
// UnityEngine.Behaviour
struct Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA;
// UnityEngine.Camera
struct Camera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184;
// UnityEngine.Component
struct Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3;
// UnityEngine.ComputeBuffer
struct ComputeBuffer_t51EADA9015EBCC1B982C5584E9AB2734415A8233;
// System.Delegate
struct Delegate_t;
// System.DelegateData
struct DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E;
// MicahW.PointGrass.FreeCam
struct FreeCam_tCB0B2B68E5483040D01032D18F6046E3733C844B;
// UnityEngine.GameObject
struct GameObject_t76FEDD663AB33C991A9C9A23129337651094216F;
// System.IAsyncResult
struct IAsyncResult_t7B9B5A0ECB35DCEC31B8A8122C37D687369253B5;
// System.Collections.IDictionary
struct IDictionary_t6D03155AF1FA9083817AA5B6AD7DEEACC26AB220;
// UnityEngine.LightProbeProxyVolume
struct LightProbeProxyVolume_t431001CA94D2BB5DB419E2A89E7D8116E4E1B658;
// UnityEngine.Material
struct Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3;
// UnityEngine.MaterialPropertyBlock
struct MaterialPropertyBlock_t2308669579033A857EFE6E4831909F638B27411D;
// UnityEngine.Mesh
struct Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4;
// UnityEngine.MeshFilter
struct MeshFilter_t6D1CE2473A1E45AC73013400585A1163BF66B2F5;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71;
// UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C;
// MicahW.PointGrass.ObjectOscillator
struct ObjectOscillator_tE8A6DB7BEA8C02735F3219A068F4BC2FFF0CF1ED;
// MicahW.PointGrass.PointGrassDisplacementManager
struct PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900;
// MicahW.PointGrass.PointGrassDisplacer
struct PointGrassDisplacer_tB9B22DA401FDA294990903A742F3ACA3351A82FF;
// MicahW.PointGrass.PointGrassRenderer
struct PointGrassRenderer_tCAB9C932B5AEF5960C04E43451965E31C6A471C2;
// MicahW.PointGrass.PointGrassWind
struct PointGrassWind_t2002E1D6C3245367514FF9A0270205781DEB2C67;
// UnityEngine.RenderTexture
struct RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6;
// System.String
struct String_t;
// UnityEngine.TerrainData
struct TerrainData_t615A68EAC648066681875D47FC641496D12F2E24;
// UnityEngine.TerrainLayer
struct TerrainLayer_t52E14A94A0CF76B0B5509B7FDFDE64FF8A9FEFF9;
// UnityEngine.Texture
struct Texture_t791CBB51219779964E0E8A2ED7C1AA5F92A4A700;
// UnityEngine.Texture2D
struct Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4;
// UnityEngine.Transform
struct Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1;
// System.Void
struct Void_t4861ACF8F4594C3437BB48B6E56783494B843915;
// UnityEngine.Camera/CameraCallback
struct CameraCallback_t844E527BFE37BC0495E7F67993E43C07642DA9DD;
// MicahW.PointGrass.PointGrassCommon/MeshData
struct MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F;
// MicahW.PointGrass.PointGrassDisplacementManager/<>c
struct U3CU3Ec_tC71A09B989D2FDC7808DBE99B3025974C73DE7C5;
// MicahW.PointGrass.PointGrassDisplacementManager/DisplacementDelegate
struct DisplacementDelegate_t4C836C2D37D9FABEC9514A574F9425C4B15B981A;

IL2CPP_EXTERN_C RuntimeClass* ArgumentException_tAD90411542A20A9C72D5CDA3A84181D8B947A263_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* BooleanU5BU5D_tD317D27C31DB892BE79FAE3AEBC0B3FFB73DE9B4_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ColorU5BU5D_t612261CF293F6FFC3D80AB52259FF0DC2B2CC389_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ComputeBufferU5BU5D_t7832804740B13E96807A836AD90ADF1477D7FE27_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ComputeBuffer_t51EADA9015EBCC1B982C5584E9AB2734415A8233_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* DisplacementDelegate_t4C836C2D37D9FABEC9514A574F9425C4B15B981A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_t868371C41B8A357BB81D47C6157D60859AD0411F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Graphics_t99CD970FFEA58171C70F54DF0C06D315BD452F2C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t7D73463EA17F4D29A37E8110AB60DAD5C80EBF65_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t7DA451437393B82A39CEE1076115D7FD8B970B3B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_tAFCDB9CCBAF11013AA7B0EC7BB51B10127AB467C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_tDEBBED5B3545B06B3FD16E4ACE9B2C7AB62A1B3E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* MaterialPropertyBlockU5BU5D_t6911DDC471FC23F2AF58FF0140ED595529EC5F5B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* MaterialPropertyBlock_t2308669579033A857EFE6E4831909F638B27411D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* MaterialU5BU5D_t2B1D11C42DB07A4400C0535F92DBB87A2E346D3D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Math_tEB65DE7CA8B083C412C969C92981C030865486CE_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* MeshFilterU5BU5D_tCE3B457E6F7ECE5ECEE9E09150642150448685BA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* MeshPointU5BU5D_tBCEA150F23C3861E840A87B994115F06D7490DD8_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* MeshU5BU5D_t178CA36422FC397211E68FB7E39C5B2F95619689_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* PointGrassWind_t2002E1D6C3245367514FF9A0270205781DEB2C67_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Texture2DU5BU5D_t05332F1E3F7D4493E304C702201F9BE4F9236191_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec_tC71A09B989D2FDC7808DBE99B3025974C73DE7C5_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral0C0964978D1C92B07D383BD488C886667A5B2474;
IL2CPP_EXTERN_C String_t* _stringLiteral0DE7A6FC33D54DC5C50D28AE72F63A8EF3D50C0E;
IL2CPP_EXTERN_C String_t* _stringLiteral0EFE6FA17D252C5C2E805ED3D4A3CD84F1AA816C;
IL2CPP_EXTERN_C String_t* _stringLiteral16DD21BE77B115D392226EB71A2D3A9FDC29E3F0;
IL2CPP_EXTERN_C String_t* _stringLiteral265E15F1F86F1C766555899D5771CF29055DE75A;
IL2CPP_EXTERN_C String_t* _stringLiteral28CD797346D19735D91A839AA66F72C5C932CF79;
IL2CPP_EXTERN_C String_t* _stringLiteral37C137AFA042CC6217D39FFF28A6DC2896EC7CC6;
IL2CPP_EXTERN_C String_t* _stringLiteral4970116458FF1090C3095CD0661522B9014F1D2E;
IL2CPP_EXTERN_C String_t* _stringLiteral4AFB7467DBFB075C643B3F66B3583BE23188AF8B;
IL2CPP_EXTERN_C String_t* _stringLiteral4BCFADE7ADD10F910A8CCCCFCD42193E98F4132B;
IL2CPP_EXTERN_C String_t* _stringLiteral527CF25F73BD2E1EEA838CA0E9911C736700293E;
IL2CPP_EXTERN_C String_t* _stringLiteral666C1ECD84E7610A200C08C24AF2A55B343400F8;
IL2CPP_EXTERN_C String_t* _stringLiteral68F6AFD1C49DB84D0049EF8E3B88393FB2EF1B4E;
IL2CPP_EXTERN_C String_t* _stringLiteral6A5A4AFA6CE99EA3CE9AA1DC42E26C493F5B85D9;
IL2CPP_EXTERN_C String_t* _stringLiteral70A3E84D94AAB3B2B54557DF4CDB0569C20B19BA;
IL2CPP_EXTERN_C String_t* _stringLiteral7F8C014BD4810CC276D0F9F81A1E759C7B098B1E;
IL2CPP_EXTERN_C String_t* _stringLiteral8118A337F70EBA77C08A715F57383297414C016F;
IL2CPP_EXTERN_C String_t* _stringLiteral88169464D968B27B7856C0A38FD386DE5FDAFF49;
IL2CPP_EXTERN_C String_t* _stringLiteral88BEE283254D7094E258B3A88730F4CC4F1E4AC7;
IL2CPP_EXTERN_C String_t* _stringLiteral9D7439C5F97ECD9F4F649172D7F9D7165E71ECC3;
IL2CPP_EXTERN_C String_t* _stringLiteralB3E5B2D8F6FE296B992C16F74EE1A5F2DE2F9CDD;
IL2CPP_EXTERN_C String_t* _stringLiteralC3C49CF9E0D06A1BABDDB923B010206544D63157;
IL2CPP_EXTERN_C String_t* _stringLiteralCF0C3727D9DD1CD08C91EB1B8399286422952900;
IL2CPP_EXTERN_C String_t* _stringLiteralD65D851CDF4779B341333708570AA533D90FABFC;
IL2CPP_EXTERN_C String_t* _stringLiteralDC2F51E5A87BA3DD01A3464780589AFB8E1761AA;
IL2CPP_EXTERN_C String_t* _stringLiteralE066450B3C698B7EEB9191269100E6ADAB08BEA6;
IL2CPP_EXTERN_C String_t* _stringLiteralE4CF68ABCF7518481A531AEFC2E0812182AD4226;
IL2CPP_EXTERN_C String_t* _stringLiteralE7DC65FC74777E1C0F193ECF8DD2F17B43E4AEED;
IL2CPP_EXTERN_C String_t* _stringLiteralEC5CDEAF8345F709CD3D3E03D811C6FE724AD794;
IL2CPP_EXTERN_C String_t* _stringLiteralF11EAC0A40707F9A5D0649BA624E242ECCE752E5;
IL2CPP_EXTERN_C String_t* _stringLiteralFF915760C44B3E7D7328B47C84AD434F656A679E;
IL2CPP_EXTERN_C const RuntimeMethod* Array_Resize_TisMaterial_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3_m79C251DDDBE015E6DB85FE487F8748D4613BCC7C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Array_Resize_TisSingle_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_m879C2A54DAFE78F46D1185B50ED527EE182BFB04_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisMeshFilter_t6D1CE2473A1E45AC73013400585A1163BF66B2F5_mB82F66059DFB5715DD85BDED1D90BC03A6C9E623_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerable_Contains_TisTerrainLayer_t52E14A94A0CF76B0B5509B7FDFDE64FF8A9FEFF9_m6D7B0AFE1EC7DB5185D1BB1A733E959511E0A6D1_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerable_Select_TisPointGrassDisplacer_tB9B22DA401FDA294990903A742F3ACA3351A82FF_TisObjectData_t24D94D36E8D49DEB90BBFEE7BA833C910F0AF6C4_m8A5429699E824BA5B507C2A87B19D7F34D84CC1D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerable_ToArray_TisObjectData_t24D94D36E8D49DEB90BBFEE7BA833C910F0AF6C4_mBA811FFD832E9653BA91207907FC396E52CB0DDF_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_AddRange_m17F664FC05537795FFF2C58E6DF293488A469EA4_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_AddRange_m77FEC24D7D0DA5225D0ECC76F11E793A228F30CD_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_m03B29C69F9974D985EFCE1B1D91AC7AC4F578723_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_m28265F4F8593F134812477A243BBEF0513421160_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_m930CA3F1A016B820967E8D53B5D8B2EEB9F10DAC_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_mB3A89627A69CB0296DA00911471E9FD7DDDD58F5_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Clear_mA21FF0362F71CC4E90FB3BC2BD3AA7B955DE93FC_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Remove_m5C0188F84A17A37FED29306A626DF5015F87088D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_ToArray_m18ACE74090BCEB952354342ADF397C950A01321A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_ToArray_mACDC8BDC8BB93E18220068239BC4D9F8065DAF41_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_ToArray_mAD5B129F08D52D3E82AE1915C038DBD06D0AADF3_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m6495AB7C6BDDC4D06EE93E691A9DC2C6FC791911_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m776C5CB754FECF0324C22F3651FA444D287BFBDC_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m9FDBD09FEFDB8C3F98E74C51E846806B1523EE85_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_mF6D991AAA0C3CF45DA3EC80A854889E3DD9268CB_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Count_m7CF0642F7F16E3F98C217C5CDEAB8C0554FEF85E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Count_mB13C1EB08F28C5DEA82F64C1E42E75932F697955_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Nullable_1__ctor_m75F3ABB694E26670F021136BD3B9E71A65948BC2_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Nullable_1_get_HasValue_m6B76086B0E863AB1D634FD03E30154F230070435_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Nullable_1_get_Value_m6A74FA440FE386A9905C61B41B5C261CD9DC4792_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PointGrassDisplacer_Initialize_m45044718CDB24241F017732A234C381653A0650A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PointGrassRenderer_GetGrassMesh_mE60BB12C500D3FF1E47167072BC6943666771124_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec_U3CUpdateBufferU3Eb__13_0_m28DE49B834780F799169B47BA3327FE3FCFF9CFA_RuntimeMethod_var;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;

struct BooleanU5BU5D_tD317D27C31DB892BE79FAE3AEBC0B3FFB73DE9B4;
struct BoundsU5BU5D_t45563ED55B62FA0536E8117454C56C0CFA5B25F5;
struct ColorU5BU5D_t612261CF293F6FFC3D80AB52259FF0DC2B2CC389;
struct ComputeBufferU5BU5D_t7832804740B13E96807A836AD90ADF1477D7FE27;
struct DelegateU5BU5D_tC5AB7E8F745616680F337909D3A8E6C722CDF771;
struct Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C;
struct MaterialU5BU5D_t2B1D11C42DB07A4400C0535F92DBB87A2E346D3D;
struct MaterialPropertyBlockU5BU5D_t6911DDC471FC23F2AF58FF0140ED595529EC5F5B;
struct MeshU5BU5D_t178CA36422FC397211E68FB7E39C5B2F95619689;
struct MeshFilterU5BU5D_tCE3B457E6F7ECE5ECEE9E09150642150448685BA;
struct ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918;
struct SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C;
struct TerrainLayerU5BU5D_t259E391D6115F121FCD284E79F62012D70956EB0;
struct Texture2DU5BU5D_t05332F1E3F7D4493E304C702201F9BE4F9236191;
struct Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA;
struct Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C;
struct MeshPointU5BU5D_tBCEA150F23C3861E840A87B994115F06D7490DD8;
struct ObjectDataU5BU5D_tFCDCD60E1D45526EC9B864EF342ACB7834D38930;
struct SingleU5BU2CU2CU5D_tE902E5192C7283A470AAADB477117789A9682488;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct U3CModuleU3E_tF8564FF9F069B9002D4A18870D49C1F9F219B46C 
{
};

// System.Collections.Generic.List`1<UnityEngine.Bounds>
struct List_1_t7D73463EA17F4D29A37E8110AB60DAD5C80EBF65  : public RuntimeObject
{
	// T[] System.Collections.Generic.List`1::_items
	BoundsU5BU5D_t45563ED55B62FA0536E8117454C56C0CFA5B25F5* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject* ____syncRoot_4;
};

struct List_1_t7D73463EA17F4D29A37E8110AB60DAD5C80EBF65_StaticFields
{
	// T[] System.Collections.Generic.List`1::s_emptyArray
	BoundsU5BU5D_t45563ED55B62FA0536E8117454C56C0CFA5B25F5* ___s_emptyArray_5;
};

// System.Collections.Generic.List`1<UnityEngine.ComputeBuffer>
struct List_1_t7DA451437393B82A39CEE1076115D7FD8B970B3B  : public RuntimeObject
{
	// T[] System.Collections.Generic.List`1::_items
	ComputeBufferU5BU5D_t7832804740B13E96807A836AD90ADF1477D7FE27* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject* ____syncRoot_4;
};

struct List_1_t7DA451437393B82A39CEE1076115D7FD8B970B3B_StaticFields
{
	// T[] System.Collections.Generic.List`1::s_emptyArray
	ComputeBufferU5BU5D_t7832804740B13E96807A836AD90ADF1477D7FE27* ___s_emptyArray_5;
};

// System.Collections.Generic.List`1<UnityEngine.MaterialPropertyBlock>
struct List_1_tAFCDB9CCBAF11013AA7B0EC7BB51B10127AB467C  : public RuntimeObject
{
	// T[] System.Collections.Generic.List`1::_items
	MaterialPropertyBlockU5BU5D_t6911DDC471FC23F2AF58FF0140ED595529EC5F5B* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject* ____syncRoot_4;
};

struct List_1_tAFCDB9CCBAF11013AA7B0EC7BB51B10127AB467C_StaticFields
{
	// T[] System.Collections.Generic.List`1::s_emptyArray
	MaterialPropertyBlockU5BU5D_t6911DDC471FC23F2AF58FF0140ED595529EC5F5B* ___s_emptyArray_5;
};

// System.Collections.Generic.List`1<System.Object>
struct List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D  : public RuntimeObject
{
	// T[] System.Collections.Generic.List`1::_items
	ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject* ____syncRoot_4;
};

struct List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D_StaticFields
{
	// T[] System.Collections.Generic.List`1::s_emptyArray
	ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* ___s_emptyArray_5;
};

// System.Collections.Generic.List`1<MicahW.PointGrass.PointGrassDisplacer>
struct List_1_tDEBBED5B3545B06B3FD16E4ACE9B2C7AB62A1B3E  : public RuntimeObject
{
	// T[] System.Collections.Generic.List`1::_items
	PointGrassDisplacerU5BU5D_tCF7DC1B216819C23C858D699519E75C0F1A53C7A* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject* ____syncRoot_4;
};

struct List_1_tDEBBED5B3545B06B3FD16E4ACE9B2C7AB62A1B3E_StaticFields
{
	// T[] System.Collections.Generic.List`1::s_emptyArray
	PointGrassDisplacerU5BU5D_tCF7DC1B216819C23C858D699519E75C0F1A53C7A* ___s_emptyArray_5;
};
struct Il2CppArrayBounds;

// MicahW.PointGrass.DistributePointsAlongMesh
struct DistributePointsAlongMesh_t0A7219E58B6F964C6A9DD10091B6A8E49D459216  : public RuntimeObject
{
};

// System.String
struct String_t  : public RuntimeObject
{
	// System.Int32 System.String::_stringLength
	int32_t ____stringLength_4;
	// System.Char System.String::_firstChar
	Il2CppChar ____firstChar_5;
};

struct String_t_StaticFields
{
	// System.String System.String::Empty
	String_t* ___Empty_6;
};

// System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F  : public RuntimeObject
{
};
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F_marshaled_com
{
};

// MicahW.PointGrass.PointGrassDisplacementManager/<>c
struct U3CU3Ec_tC71A09B989D2FDC7808DBE99B3025974C73DE7C5  : public RuntimeObject
{
};

struct U3CU3Ec_tC71A09B989D2FDC7808DBE99B3025974C73DE7C5_StaticFields
{
	// MicahW.PointGrass.PointGrassDisplacementManager/<>c MicahW.PointGrass.PointGrassDisplacementManager/<>c::<>9
	U3CU3Ec_tC71A09B989D2FDC7808DBE99B3025974C73DE7C5* ___U3CU3E9_0;
	// System.Func`2<MicahW.PointGrass.PointGrassDisplacer,MicahW.PointGrass.PointGrassCommon/ObjectData> MicahW.PointGrass.PointGrassDisplacementManager/<>c::<>9__13_0
	Func_2_t868371C41B8A357BB81D47C6157D60859AD0411F* ___U3CU3E9__13_0_1;
};

// System.Boolean
struct Boolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22 
{
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;
};

struct Boolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22_StaticFields
{
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;
};

// UnityEngine.Color
struct Color_tD001788D726C3A7F1379BEED0260B9591F440C1F 
{
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;
};

// System.Double
struct Double_tE150EF3D1D43DEE85D533810AB4C742307EEDE5F 
{
	// System.Double System.Double::m_value
	double ___m_value_0;
};

// System.Int32
struct Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C 
{
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;
};

// System.IntPtr
struct IntPtr_t 
{
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;
};

struct IntPtr_t_StaticFields
{
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;
};

// UnityEngine.LayerMask
struct LayerMask_t97CB6BDADEDC3D6423C7BCFEA7F86DA2EC6241DB 
{
	// System.Int32 UnityEngine.LayerMask::m_Mask
	int32_t ___m_Mask_0;
};

// UnityEngine.Matrix4x4
struct Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 
{
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;
};

struct Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6_StaticFields
{
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 ___identityMatrix_17;
};

// UnityEngine.Quaternion
struct Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 
{
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;
};

struct Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974_StaticFields
{
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___identityQuaternion_4;
};

// UnityEngine.Rect
struct Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D 
{
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;
};

// System.Single
struct Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C 
{
	// System.Single System.Single::m_value
	float ___m_value_0;
};

// MicahW.PointGrass.SingleLayer
struct SingleLayer_tA74696727907F9D2CD336CD4C7AA5C5C8C8FB4ED 
{
	// System.Int32 MicahW.PointGrass.SingleLayer::m_LayerIndex
	int32_t ___m_LayerIndex_0;
};

// UnityEngine.Vector2
struct Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 
{
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;
};

struct Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_StaticFields
{
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___negativeInfinityVector_9;
};

// UnityEngine.Vector2Int
struct Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A 
{
	// System.Int32 UnityEngine.Vector2Int::m_X
	int32_t ___m_X_0;
	// System.Int32 UnityEngine.Vector2Int::m_Y
	int32_t ___m_Y_1;
};

struct Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A_StaticFields
{
	// UnityEngine.Vector2Int UnityEngine.Vector2Int::s_Zero
	Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A ___s_Zero_2;
	// UnityEngine.Vector2Int UnityEngine.Vector2Int::s_One
	Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A ___s_One_3;
	// UnityEngine.Vector2Int UnityEngine.Vector2Int::s_Up
	Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A ___s_Up_4;
	// UnityEngine.Vector2Int UnityEngine.Vector2Int::s_Down
	Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A ___s_Down_5;
	// UnityEngine.Vector2Int UnityEngine.Vector2Int::s_Left
	Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A ___s_Left_6;
	// UnityEngine.Vector2Int UnityEngine.Vector2Int::s_Right
	Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A ___s_Right_7;
};

// UnityEngine.Vector3
struct Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 
{
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;
};

struct Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_StaticFields
{
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___negativeInfinityVector_14;
};

// UnityEngine.Vector4
struct Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 
{
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;
};

struct Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3_StaticFields
{
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___negativeInfinityVector_8;
};

// System.Void
struct Void_t4861ACF8F4594C3437BB48B6E56783494B843915 
{
	union
	{
		struct
		{
		};
		uint8_t Void_t4861ACF8F4594C3437BB48B6E56783494B843915__padding[1];
	};
};

// MicahW.PointGrass.PointGrassCommon/<>c__DisplayClass17_0
struct U3CU3Ec__DisplayClass17_0_t3B43FCEDB710544F3D8D366A3758A6D2CEF70412 
{
	// UnityEngine.Vector3[] MicahW.PointGrass.PointGrassCommon/<>c__DisplayClass17_0::verts
	Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* ___verts_0;
	// UnityEngine.Vector2[] MicahW.PointGrass.PointGrassCommon/<>c__DisplayClass17_0::uvs
	Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* ___uvs_1;
	// UnityEngine.Color[] MicahW.PointGrass.PointGrassCommon/<>c__DisplayClass17_0::cols
	ColorU5BU5D_t612261CF293F6FFC3D80AB52259FF0DC2B2CC389* ___cols_2;
};

// MicahW.PointGrass.PointGrassRenderer/DebugInformation
struct DebugInformation_tEF4E30A924E4C1CAD82F63774AD5738CD5B726C5 
{
	// System.Int32 MicahW.PointGrass.PointGrassRenderer/DebugInformation::totalPointCount
	int32_t ___totalPointCount_0;
	// System.Boolean MicahW.PointGrass.PointGrassRenderer/DebugInformation::usingMultipleBuffers
	bool ___usingMultipleBuffers_1;
	// System.Int32 MicahW.PointGrass.PointGrassRenderer/DebugInformation::bufferCount
	int32_t ___bufferCount_2;
	// System.Int32 MicahW.PointGrass.PointGrassRenderer/DebugInformation::smallestBuffer
	int32_t ___smallestBuffer_3;
	// System.Int32 MicahW.PointGrass.PointGrassRenderer/DebugInformation::largestBuffer
	int32_t ___largestBuffer_4;
};
// Native definition for P/Invoke marshalling of MicahW.PointGrass.PointGrassRenderer/DebugInformation
struct DebugInformation_tEF4E30A924E4C1CAD82F63774AD5738CD5B726C5_marshaled_pinvoke
{
	int32_t ___totalPointCount_0;
	int32_t ___usingMultipleBuffers_1;
	int32_t ___bufferCount_2;
	int32_t ___smallestBuffer_3;
	int32_t ___largestBuffer_4;
};
// Native definition for COM marshalling of MicahW.PointGrass.PointGrassRenderer/DebugInformation
struct DebugInformation_tEF4E30A924E4C1CAD82F63774AD5738CD5B726C5_marshaled_com
{
	int32_t ___totalPointCount_0;
	int32_t ___usingMultipleBuffers_1;
	int32_t ___bufferCount_2;
	int32_t ___smallestBuffer_3;
	int32_t ___largestBuffer_4;
};

// UnityEngine.Random/State
struct State_tA37EB68FE687D41D4B228462D4C7427FAC5BF9C1 
{
	// System.Int32 UnityEngine.Random/State::s0
	int32_t ___s0_0;
	// System.Int32 UnityEngine.Random/State::s1
	int32_t ___s1_1;
	// System.Int32 UnityEngine.Random/State::s2
	int32_t ___s2_2;
	// System.Int32 UnityEngine.Random/State::s3
	int32_t ___s3_3;
};

// System.Nullable`1<UnityEngine.Vector3>
struct Nullable_1_t9C51B084784B716FFF4ED4575C63CFD8A71A86FE 
{
	// System.Boolean System.Nullable`1::hasValue
	bool ___hasValue_0;
	// T System.Nullable`1::value
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___value_1;
};

// UnityEngine.Bounds
struct Bounds_t367E830C64BBF235ED8C3B2F8CF6254FDCAD39C3 
{
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Center
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Center_0;
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Extents
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Extents_1;
};

// UnityEngine.ComputeBuffer
struct ComputeBuffer_t51EADA9015EBCC1B982C5584E9AB2734415A8233  : public RuntimeObject
{
	// System.IntPtr UnityEngine.ComputeBuffer::m_Ptr
	intptr_t ___m_Ptr_0;
};

// System.Delegate
struct Delegate_t  : public RuntimeObject
{
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject* ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.IntPtr System.Delegate::interp_method
	intptr_t ___interp_method_7;
	// System.IntPtr System.Delegate::interp_invoke_impl
	intptr_t ___interp_invoke_impl_8;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t* ___method_info_9;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t* ___original_method_info_10;
	// System.DelegateData System.Delegate::data
	DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E* ___data_11;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_12;
};
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	intptr_t ___interp_method_7;
	intptr_t ___interp_invoke_impl_8;
	MethodInfo_t* ___method_info_9;
	MethodInfo_t* ___original_method_info_10;
	DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E* ___data_11;
	int32_t ___method_is_virtual_12;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	intptr_t ___interp_method_7;
	intptr_t ___interp_invoke_impl_8;
	MethodInfo_t* ___method_info_9;
	MethodInfo_t* ___original_method_info_10;
	DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E* ___data_11;
	int32_t ___method_is_virtual_12;
};

// System.Exception
struct Exception_t  : public RuntimeObject
{
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t* ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject* ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject* ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6* ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_tFD177F8C806A6921AD7150264CCC62FA00CAD832* ___native_trace_ips_15;
	// System.Int32 System.Exception::caught_in_unmanaged
	int32_t ___caught_in_unmanaged_16;
};

struct Exception_t_StaticFields
{
	// System.Object System.Exception::s_EDILock
	RuntimeObject* ___s_EDILock_0;
};
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6* ____safeSerializationManager_13;
	StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
	int32_t ___caught_in_unmanaged_16;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6* ____safeSerializationManager_13;
	StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
	int32_t ___caught_in_unmanaged_16;
};

// UnityEngine.MaterialPropertyBlock
struct MaterialPropertyBlock_t2308669579033A857EFE6E4831909F638B27411D  : public RuntimeObject
{
	// System.IntPtr UnityEngine.MaterialPropertyBlock::m_Ptr
	intptr_t ___m_Ptr_0;
};

// UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C  : public RuntimeObject
{
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;
};

struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_StaticFields
{
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;
};
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// MicahW.PointGrass.PointGrassCommon
struct PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A  : public RuntimeObject
{
};

struct PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_StaticFields
{
	// UnityEngine.Mesh MicahW.PointGrass.PointGrassCommon::grassMeshFlat
	Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* ___grassMeshFlat_0;
	// UnityEngine.Mesh MicahW.PointGrass.PointGrassCommon::grassMeshCyl
	Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* ___grassMeshCyl_1;
	// System.Int32 MicahW.PointGrass.PointGrassCommon::ID_PointBuff
	int32_t ___ID_PointBuff_2;
	// System.Int32 MicahW.PointGrass.PointGrassCommon::ID_ObjBuff
	int32_t ___ID_ObjBuff_3;
	// System.Int32 MicahW.PointGrass.PointGrassCommon::ID_ObjCount
	int32_t ___ID_ObjCount_4;
	// System.Int32 MicahW.PointGrass.PointGrassCommon::ID_MatrixL2W
	int32_t ___ID_MatrixL2W_5;
	// System.Int32 MicahW.PointGrass.PointGrassCommon::ID_MatrixW2L
	int32_t ___ID_MatrixW2L_6;
	// System.Boolean MicahW.PointGrass.PointGrassCommon::<PropertyIDsInitialized>k__BackingField
	bool ___U3CPropertyIDsInitializedU3Ek__BackingField_7;
	// System.Int32 MicahW.PointGrass.PointGrassCommon::heightmapSize
	int32_t ___heightmapSize_8;
	// System.Int32 MicahW.PointGrass.PointGrassCommon::alphamapWidth
	int32_t ___alphamapWidth_9;
	// System.Int32 MicahW.PointGrass.PointGrassCommon::alphamapHeight
	int32_t ___alphamapHeight_10;
	// System.Int32 MicahW.PointGrass.PointGrassCommon::numLayers
	int32_t ___numLayers_11;
	// UnityEngine.Vector3 MicahW.PointGrass.PointGrassCommon::terrainSize
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___terrainSize_12;
	// System.Single[,,] MicahW.PointGrass.PointGrassCommon::alphamaps
	SingleU5BU2CU2CU5D_tE902E5192C7283A470AAADB477117789A9682488* ___alphamaps_13;
	// System.Boolean[] MicahW.PointGrass.PointGrassCommon::grassLayerMask
	BooleanU5BU5D_tD317D27C31DB892BE79FAE3AEBC0B3FFB73DE9B4* ___grassLayerMask_14;
	// UnityEngine.Texture2D[] MicahW.PointGrass.PointGrassCommon::layerTextures
	Texture2DU5BU5D_t05332F1E3F7D4493E304C702201F9BE4F9236191* ___layerTextures_15;
	// UnityEngine.Vector2[] MicahW.PointGrass.PointGrassCommon::layerOffsets
	Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* ___layerOffsets_16;
	// UnityEngine.Vector2[] MicahW.PointGrass.PointGrassCommon::layerSizes
	Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* ___layerSizes_17;
};

// UnityEngine.RaycastHit
struct RaycastHit_t6F30BD0B38B56401CA833A1B87BD74F2ACD2F2B5 
{
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Point
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Point_0;
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Normal
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Normal_1;
	// System.UInt32 UnityEngine.RaycastHit::m_FaceID
	uint32_t ___m_FaceID_2;
	// System.Single UnityEngine.RaycastHit::m_Distance
	float ___m_Distance_3;
	// UnityEngine.Vector2 UnityEngine.RaycastHit::m_UV
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___m_UV_4;
	// System.Int32 UnityEngine.RaycastHit::m_Collider
	int32_t ___m_Collider_5;
};

// MicahW.PointGrass.PointGrassCommon/MeshPoint
struct MeshPoint_t033BD3FDC9D80AE452F7B3D583D17CDADF299152 
{
	// UnityEngine.Vector3 MicahW.PointGrass.PointGrassCommon/MeshPoint::position
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___position_0;
	// UnityEngine.Vector3 MicahW.PointGrass.PointGrassCommon/MeshPoint::normal
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___normal_1;
	// UnityEngine.Color MicahW.PointGrass.PointGrassCommon/MeshPoint::color
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___color_2;
	// UnityEngine.Vector4 MicahW.PointGrass.PointGrassCommon/MeshPoint::extraData
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___extraData_3;
};

// MicahW.PointGrass.PointGrassCommon/ObjectData
struct ObjectData_t24D94D36E8D49DEB90BBFEE7BA833C910F0AF6C4 
{
	// UnityEngine.Vector3 MicahW.PointGrass.PointGrassCommon/ObjectData::position
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___position_0;
	// System.Single MicahW.PointGrass.PointGrassCommon/ObjectData::radius
	float ___radius_1;
	// System.Single MicahW.PointGrass.PointGrassCommon/ObjectData::strength
	float ___strength_2;
};

// MicahW.PointGrass.PointGrassWind/PackedProperties
struct PackedProperties_tAE6E15A41456BA8070C1C0B4F6D3DA9E28624A32 
{
	// UnityEngine.Vector4 MicahW.PointGrass.PointGrassWind/PackedProperties::vecA
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___vecA_0;
	// UnityEngine.Vector4 MicahW.PointGrass.PointGrassWind/PackedProperties::vecB
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___vecB_1;
	// System.Single MicahW.PointGrass.PointGrassWind/PackedProperties::valA
	float ___valA_2;
};

// UnityEngine.Component
struct Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};

// UnityEngine.GameObject
struct GameObject_t76FEDD663AB33C991A9C9A23129337651094216F  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};

// UnityEngine.Material
struct Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};

// UnityEngine.Mesh
struct Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};

// System.MulticastDelegate
struct MulticastDelegate_t  : public Delegate_t
{
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tC5AB7E8F745616680F337909D3A8E6C722CDF771* ___delegates_13;
};
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_13;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_13;
};

// System.SystemException
struct SystemException_tCC48D868298F4C0705279823E34B00F4FBDB7295  : public Exception_t
{
};

// UnityEngine.TerrainData
struct TerrainData_t615A68EAC648066681875D47FC641496D12F2E24  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};

struct TerrainData_t615A68EAC648066681875D47FC641496D12F2E24_StaticFields
{
	// System.Int32 UnityEngine.TerrainData::k_MaximumResolution
	int32_t ___k_MaximumResolution_10;
	// System.Int32 UnityEngine.TerrainData::k_MinimumDetailResolutionPerPatch
	int32_t ___k_MinimumDetailResolutionPerPatch_11;
	// System.Int32 UnityEngine.TerrainData::k_MaximumDetailResolutionPerPatch
	int32_t ___k_MaximumDetailResolutionPerPatch_12;
	// System.Int32 UnityEngine.TerrainData::k_MaximumDetailPatchCount
	int32_t ___k_MaximumDetailPatchCount_13;
	// System.Int32 UnityEngine.TerrainData::k_MaximumDetailsPerRes
	int32_t ___k_MaximumDetailsPerRes_14;
	// System.Int32 UnityEngine.TerrainData::k_MinimumAlphamapResolution
	int32_t ___k_MinimumAlphamapResolution_15;
	// System.Int32 UnityEngine.TerrainData::k_MaximumAlphamapResolution
	int32_t ___k_MaximumAlphamapResolution_16;
	// System.Int32 UnityEngine.TerrainData::k_MinimumBaseMapResolution
	int32_t ___k_MinimumBaseMapResolution_17;
	// System.Int32 UnityEngine.TerrainData::k_MaximumBaseMapResolution
	int32_t ___k_MaximumBaseMapResolution_18;
};

// UnityEngine.TerrainLayer
struct TerrainLayer_t52E14A94A0CF76B0B5509B7FDFDE64FF8A9FEFF9  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};
// Native definition for P/Invoke marshalling of UnityEngine.TerrainLayer
struct TerrainLayer_t52E14A94A0CF76B0B5509B7FDFDE64FF8A9FEFF9_marshaled_pinvoke : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.TerrainLayer
struct TerrainLayer_t52E14A94A0CF76B0B5509B7FDFDE64FF8A9FEFF9_marshaled_com : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_com
{
};

// UnityEngine.Texture
struct Texture_t791CBB51219779964E0E8A2ED7C1AA5F92A4A700  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};

struct Texture_t791CBB51219779964E0E8A2ED7C1AA5F92A4A700_StaticFields
{
	// System.Int32 UnityEngine.Texture::GenerateAllMips
	int32_t ___GenerateAllMips_4;
};

// MicahW.PointGrass.PointGrassCommon/MeshData
struct MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F  : public RuntimeObject
{
	// UnityEngine.Vector3[] MicahW.PointGrass.PointGrassCommon/MeshData::verts
	Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* ___verts_0;
	// UnityEngine.Vector3[] MicahW.PointGrass.PointGrassCommon/MeshData::normals
	Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* ___normals_1;
	// UnityEngine.Vector2[] MicahW.PointGrass.PointGrassCommon/MeshData::UVs
	Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* ___UVs_2;
	// UnityEngine.Color[] MicahW.PointGrass.PointGrassCommon/MeshData::colours
	ColorU5BU5D_t612261CF293F6FFC3D80AB52259FF0DC2B2CC389* ___colours_3;
	// System.Int32[] MicahW.PointGrass.PointGrassCommon/MeshData::tris
	Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* ___tris_4;
	// UnityEngine.Vector2[] MicahW.PointGrass.PointGrassCommon/MeshData::attributes
	Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* ___attributes_5;
	// UnityEngine.Bounds MicahW.PointGrass.PointGrassCommon/MeshData::bounds
	Bounds_t367E830C64BBF235ED8C3B2F8CF6254FDCAD39C3 ___bounds_6;
};

struct MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F_StaticFields
{
	// MicahW.PointGrass.PointGrassCommon/MeshData MicahW.PointGrass.PointGrassCommon/MeshData::Empty
	MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F* ___Empty_7;
};

// System.Func`2<MicahW.PointGrass.PointGrassDisplacer,MicahW.PointGrass.PointGrassCommon/ObjectData>
struct Func_2_t868371C41B8A357BB81D47C6157D60859AD0411F  : public MulticastDelegate_t
{
};

// System.ArgumentException
struct ArgumentException_tAD90411542A20A9C72D5CDA3A84181D8B947A263  : public SystemException_tCC48D868298F4C0705279823E34B00F4FBDB7295
{
	// System.String System.ArgumentException::_paramName
	String_t* ____paramName_18;
};

// System.AsyncCallback
struct AsyncCallback_t7FEF460CBDCFB9C5FA2EF776984778B9A4145F4C  : public MulticastDelegate_t
{
};

// UnityEngine.Behaviour
struct Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA  : public Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3
{
};

// UnityEngine.MeshFilter
struct MeshFilter_t6D1CE2473A1E45AC73013400585A1163BF66B2F5  : public Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3
{
};

// UnityEngine.RenderTexture
struct RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27  : public Texture_t791CBB51219779964E0E8A2ED7C1AA5F92A4A700
{
};

// UnityEngine.Texture2D
struct Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4  : public Texture_t791CBB51219779964E0E8A2ED7C1AA5F92A4A700
{
};

// UnityEngine.Transform
struct Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1  : public Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3
{
};

// MicahW.PointGrass.PointGrassDisplacementManager/DisplacementDelegate
struct DisplacementDelegate_t4C836C2D37D9FABEC9514A574F9425C4B15B981A  : public MulticastDelegate_t
{
};

// UnityEngine.Camera
struct Camera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184  : public Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA
{
};

struct Camera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184_StaticFields
{
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreCull
	CameraCallback_t844E527BFE37BC0495E7F67993E43C07642DA9DD* ___onPreCull_4;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreRender
	CameraCallback_t844E527BFE37BC0495E7F67993E43C07642DA9DD* ___onPreRender_5;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPostRender
	CameraCallback_t844E527BFE37BC0495E7F67993E43C07642DA9DD* ___onPostRender_6;
};

// UnityEngine.LightProbeProxyVolume
struct LightProbeProxyVolume_t431001CA94D2BB5DB419E2A89E7D8116E4E1B658  : public Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA
{
};

// UnityEngine.MonoBehaviour
struct MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71  : public Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA
{
};

// MicahW.PointGrass.FreeCam
struct FreeCam_tCB0B2B68E5483040D01032D18F6046E3733C844B  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// System.Single MicahW.PointGrass.FreeCam::lookSpeed
	float ___lookSpeed_4;
	// System.Single MicahW.PointGrass.FreeCam::moveSpeed
	float ___moveSpeed_5;
	// System.Single MicahW.PointGrass.FreeCam::moveAccel
	float ___moveAccel_6;
	// System.Single MicahW.PointGrass.FreeCam::fastSpeed
	float ___fastSpeed_7;
	// System.Single MicahW.PointGrass.FreeCam::currentPitch
	float ___currentPitch_8;
	// System.Single MicahW.PointGrass.FreeCam::currentYaw
	float ___currentYaw_9;
	// UnityEngine.Vector3 MicahW.PointGrass.FreeCam::currentVelocity
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___currentVelocity_10;
};

// MicahW.PointGrass.ObjectOscillator
struct ObjectOscillator_tE8A6DB7BEA8C02735F3219A068F4BC2FFF0CF1ED  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// UnityEngine.Vector3 MicahW.PointGrass.ObjectOscillator::oscStrength
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___oscStrength_4;
	// UnityEngine.Vector3 MicahW.PointGrass.ObjectOscillator::oscFrequency
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___oscFrequency_5;
	// UnityEngine.Vector3 MicahW.PointGrass.ObjectOscillator::oscPhase
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___oscPhase_6;
	// UnityEngine.Vector3 MicahW.PointGrass.ObjectOscillator::oscOffset
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___oscOffset_7;
	// UnityEngine.Vector3 MicahW.PointGrass.ObjectOscillator::origin
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___origin_8;
};

// MicahW.PointGrass.PointGrassDisplacementManager
struct PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// UnityEngine.ComputeBuffer MicahW.PointGrass.PointGrassDisplacementManager::objectsBuffer
	ComputeBuffer_t51EADA9015EBCC1B982C5584E9AB2734415A8233* ___objectsBuffer_5;
	// System.Collections.Generic.List`1<MicahW.PointGrass.PointGrassDisplacer> MicahW.PointGrass.PointGrassDisplacementManager::displacers
	List_1_tDEBBED5B3545B06B3FD16E4ACE9B2C7AB62A1B3E* ___displacers_6;
};

struct PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900_StaticFields
{
	// MicahW.PointGrass.PointGrassDisplacementManager MicahW.PointGrass.PointGrassDisplacementManager::instance
	PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900* ___instance_4;
	// System.Int32 MicahW.PointGrass.PointGrassDisplacementManager::maxDisplacerCount
	int32_t ___maxDisplacerCount_7;
	// MicahW.PointGrass.PointGrassDisplacementManager/DisplacementDelegate MicahW.PointGrass.PointGrassDisplacementManager::OnInitialize
	DisplacementDelegate_t4C836C2D37D9FABEC9514A574F9425C4B15B981A* ___OnInitialize_8;
};

// MicahW.PointGrass.PointGrassDisplacer
struct PointGrassDisplacer_tB9B22DA401FDA294990903A742F3ACA3351A82FF  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// UnityEngine.Vector3 MicahW.PointGrass.PointGrassDisplacer::localPosition
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___localPosition_4;
	// System.Single MicahW.PointGrass.PointGrassDisplacer::radius
	float ___radius_5;
	// System.Single MicahW.PointGrass.PointGrassDisplacer::strength
	float ___strength_6;
};

// MicahW.PointGrass.PointGrassRenderer
struct PointGrassRenderer_tCAB9C932B5AEF5960C04E43451965E31C6A471C2  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// MicahW.PointGrass.PointGrassCommon/DistributionSource MicahW.PointGrass.PointGrassRenderer::distSource
	int32_t ___distSource_4;
	// UnityEngine.Mesh MicahW.PointGrass.PointGrassRenderer::baseMesh
	Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* ___baseMesh_5;
	// UnityEngine.TerrainData MicahW.PointGrass.PointGrassRenderer::terrain
	TerrainData_t615A68EAC648066681875D47FC641496D12F2E24* ___terrain_6;
	// UnityEngine.TerrainLayer[] MicahW.PointGrass.PointGrassRenderer::terrainLayers
	TerrainLayerU5BU5D_t259E391D6115F121FCD284E79F62012D70956EB0* ___terrainLayers_7;
	// UnityEngine.Vector2Int MicahW.PointGrass.PointGrassRenderer::chunkCount
	Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A ___chunkCount_8;
	// UnityEngine.MeshFilter[] MicahW.PointGrass.PointGrassRenderer::sceneFilters
	MeshFilterU5BU5D_tCE3B457E6F7ECE5ECEE9E09150642150448685BA* ___sceneFilters_9;
	// UnityEngine.MeshFilter MicahW.PointGrass.PointGrassRenderer::filter
	MeshFilter_t6D1CE2473A1E45AC73013400585A1163BF66B2F5* ___filter_10;
	// MicahW.PointGrass.PointGrassCommon/BladeType MicahW.PointGrass.PointGrassRenderer::bladeType
	int32_t ___bladeType_11;
	// System.Boolean MicahW.PointGrass.PointGrassRenderer::multipleMeshes
	bool ___multipleMeshes_12;
	// UnityEngine.Mesh MicahW.PointGrass.PointGrassRenderer::grassBladeMesh
	Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* ___grassBladeMesh_13;
	// UnityEngine.Mesh[] MicahW.PointGrass.PointGrassRenderer::grassBladeMeshes
	MeshU5BU5D_t178CA36422FC397211E68FB7E39C5B2F95619689* ___grassBladeMeshes_14;
	// System.Single[] MicahW.PointGrass.PointGrassRenderer::meshDensityValues
	SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* ___meshDensityValues_15;
	// System.Boolean MicahW.PointGrass.PointGrassRenderer::multipleMaterials
	bool ___multipleMaterials_16;
	// UnityEngine.Material MicahW.PointGrass.PointGrassRenderer::material
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___material_17;
	// UnityEngine.Material[] MicahW.PointGrass.PointGrassRenderer::materials
	MaterialU5BU5D_t2B1D11C42DB07A4400C0535F92DBB87A2E346D3D* ___materials_18;
	// UnityEngine.Rendering.ShadowCastingMode MicahW.PointGrass.PointGrassRenderer::shadowMode
	int32_t ___shadowMode_19;
	// MicahW.PointGrass.SingleLayer MicahW.PointGrass.PointGrassRenderer::renderLayer
	SingleLayer_tA74696727907F9D2CD336CD4C7AA5C5C8C8FB4ED ___renderLayer_20;
	// System.Single MicahW.PointGrass.PointGrassRenderer::pointCount
	float ___pointCount_21;
	// System.Boolean MicahW.PointGrass.PointGrassRenderer::multiplyByArea
	bool ___multiplyByArea_22;
	// System.Single MicahW.PointGrass.PointGrassRenderer::pointLODFactor
	float ___pointLODFactor_23;
	// System.Boolean MicahW.PointGrass.PointGrassRenderer::randomiseSeed
	bool ___randomiseSeed_24;
	// System.Int32 MicahW.PointGrass.PointGrassRenderer::seed
	int32_t ___seed_25;
	// System.Boolean MicahW.PointGrass.PointGrassRenderer::overwriteNormalDirection
	bool ___overwriteNormalDirection_26;
	// UnityEngine.Vector3 MicahW.PointGrass.PointGrassRenderer::forcedNormal
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___forcedNormal_27;
	// System.Boolean MicahW.PointGrass.PointGrassRenderer::useDensity
	bool ___useDensity_28;
	// System.Single MicahW.PointGrass.PointGrassRenderer::densityCutoff
	float ___densityCutoff_29;
	// System.Boolean MicahW.PointGrass.PointGrassRenderer::useLength
	bool ___useLength_30;
	// UnityEngine.Vector2 MicahW.PointGrass.PointGrassRenderer::lengthMapping
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___lengthMapping_31;
	// UnityEngine.ComputeBuffer MicahW.PointGrass.PointGrassRenderer::pointBuffer
	ComputeBuffer_t51EADA9015EBCC1B982C5584E9AB2734415A8233* ___pointBuffer_32;
	// UnityEngine.MaterialPropertyBlock MicahW.PointGrass.PointGrassRenderer::materialBlock
	MaterialPropertyBlock_t2308669579033A857EFE6E4831909F638B27411D* ___materialBlock_33;
	// UnityEngine.Bounds MicahW.PointGrass.PointGrassRenderer::boundingBox
	Bounds_t367E830C64BBF235ED8C3B2F8CF6254FDCAD39C3 ___boundingBox_34;
	// UnityEngine.ComputeBuffer[] MicahW.PointGrass.PointGrassRenderer::pointBuffers
	ComputeBufferU5BU5D_t7832804740B13E96807A836AD90ADF1477D7FE27* ___pointBuffers_35;
	// UnityEngine.MaterialPropertyBlock[] MicahW.PointGrass.PointGrassRenderer::materialBlocks
	MaterialPropertyBlockU5BU5D_t6911DDC471FC23F2AF58FF0140ED595529EC5F5B* ___materialBlocks_36;
	// UnityEngine.Bounds[] MicahW.PointGrass.PointGrassRenderer::boundingBoxes
	BoundsU5BU5D_t45563ED55B62FA0536E8117454C56C0CFA5B25F5* ___boundingBoxes_37;
	// MicahW.PointGrass.PointGrassCommon/ProjectionType MicahW.PointGrass.PointGrassRenderer::projectType
	int32_t ___projectType_38;
	// UnityEngine.LayerMask MicahW.PointGrass.PointGrassRenderer::projectMask
	LayerMask_t97CB6BDADEDC3D6423C7BCFEA7F86DA2EC6241DB ___projectMask_39;
	// UnityEngine.Bounds MicahW.PointGrass.PointGrassRenderer::boundingBoxOffset
	Bounds_t367E830C64BBF235ED8C3B2F8CF6254FDCAD39C3 ___boundingBoxOffset_40;
};

// MicahW.PointGrass.PointGrassWind
struct PointGrassWind_t2002E1D6C3245367514FF9A0270205781DEB2C67  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// System.Single MicahW.PointGrass.PointGrassWind::windScale
	float ___windScale_4;
	// UnityEngine.Vector2 MicahW.PointGrass.PointGrassWind::noiseRange
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___noiseRange_5;
	// UnityEngine.Vector3 MicahW.PointGrass.PointGrassWind::windDirection
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___windDirection_6;
	// UnityEngine.Vector3 MicahW.PointGrass.PointGrassWind::windScroll
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___windScroll_7;
	// UnityEngine.Vector3 MicahW.PointGrass.PointGrassWind::currentNoisePosition
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___currentNoisePosition_8;
};

struct PointGrassWind_t2002E1D6C3245367514FF9A0270205781DEB2C67_StaticFields
{
	// System.Int32 MicahW.PointGrass.PointGrassWind::ID_vecA
	int32_t ___ID_vecA_9;
	// System.Int32 MicahW.PointGrass.PointGrassWind::ID_vecB
	int32_t ___ID_vecB_10;
	// System.Int32 MicahW.PointGrass.PointGrassWind::ID_valA
	int32_t ___ID_valA_11;
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
// MicahW.PointGrass.PointGrassCommon/MeshPoint[]
struct MeshPointU5BU5D_tBCEA150F23C3861E840A87B994115F06D7490DD8  : public RuntimeArray
{
	ALIGN_FIELD (8) MeshPoint_t033BD3FDC9D80AE452F7B3D583D17CDADF299152 m_Items[1];

	inline MeshPoint_t033BD3FDC9D80AE452F7B3D583D17CDADF299152 GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline MeshPoint_t033BD3FDC9D80AE452F7B3D583D17CDADF299152* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, MeshPoint_t033BD3FDC9D80AE452F7B3D583D17CDADF299152 value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline MeshPoint_t033BD3FDC9D80AE452F7B3D583D17CDADF299152 GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline MeshPoint_t033BD3FDC9D80AE452F7B3D583D17CDADF299152* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, MeshPoint_t033BD3FDC9D80AE452F7B3D583D17CDADF299152 value)
	{
		m_Items[index] = value;
	}
};
// System.Single[]
struct SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C  : public RuntimeArray
{
	ALIGN_FIELD (8) float m_Items[1];

	inline float GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline float* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, float value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline float GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline float* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, float value)
	{
		m_Items[index] = value;
	}
};
// System.Int32[]
struct Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C  : public RuntimeArray
{
	ALIGN_FIELD (8) int32_t m_Items[1];

	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Vector3[]
struct Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C  : public RuntimeArray
{
	ALIGN_FIELD (8) Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 m_Items[1];

	inline Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA  : public RuntimeArray
{
	ALIGN_FIELD (8) Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 m_Items[1];

	inline Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Color[]
struct ColorU5BU5D_t612261CF293F6FFC3D80AB52259FF0DC2B2CC389  : public RuntimeArray
{
	ALIGN_FIELD (8) Color_tD001788D726C3A7F1379BEED0260B9591F440C1F m_Items[1];

	inline Color_tD001788D726C3A7F1379BEED0260B9591F440C1F GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Color_tD001788D726C3A7F1379BEED0260B9591F440C1F* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Color_tD001788D726C3A7F1379BEED0260B9591F440C1F value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Color_tD001788D726C3A7F1379BEED0260B9591F440C1F GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Color_tD001788D726C3A7F1379BEED0260B9591F440C1F* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Color_tD001788D726C3A7F1379BEED0260B9591F440C1F value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.TerrainLayer[]
struct TerrainLayerU5BU5D_t259E391D6115F121FCD284E79F62012D70956EB0  : public RuntimeArray
{
	ALIGN_FIELD (8) TerrainLayer_t52E14A94A0CF76B0B5509B7FDFDE64FF8A9FEFF9* m_Items[1];

	inline TerrainLayer_t52E14A94A0CF76B0B5509B7FDFDE64FF8A9FEFF9* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline TerrainLayer_t52E14A94A0CF76B0B5509B7FDFDE64FF8A9FEFF9** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, TerrainLayer_t52E14A94A0CF76B0B5509B7FDFDE64FF8A9FEFF9* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline TerrainLayer_t52E14A94A0CF76B0B5509B7FDFDE64FF8A9FEFF9* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline TerrainLayer_t52E14A94A0CF76B0B5509B7FDFDE64FF8A9FEFF9** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, TerrainLayer_t52E14A94A0CF76B0B5509B7FDFDE64FF8A9FEFF9* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Single[,,]
struct SingleU5BU2CU2CU5D_tE902E5192C7283A470AAADB477117789A9682488  : public RuntimeArray
{
	ALIGN_FIELD (8) float m_Items[1];

	inline float GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline float* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, float value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline float GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline float* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, float value)
	{
		m_Items[index] = value;
	}
	inline float GetAt(il2cpp_array_size_t i, il2cpp_array_size_t j, il2cpp_array_size_t k) const
	{
		il2cpp_array_size_t iBound = bounds[0].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(i, iBound);
		il2cpp_array_size_t jBound = bounds[1].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(j, jBound);
		il2cpp_array_size_t kBound = bounds[2].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(k, kBound);

		il2cpp_array_size_t index = (i * jBound + j) * kBound + k;
		return m_Items[index];
	}
	inline float* GetAddressAt(il2cpp_array_size_t i, il2cpp_array_size_t j, il2cpp_array_size_t k)
	{
		il2cpp_array_size_t iBound = bounds[0].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(i, iBound);
		il2cpp_array_size_t jBound = bounds[1].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(j, jBound);
		il2cpp_array_size_t kBound = bounds[2].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(k, kBound);

		il2cpp_array_size_t index = (i * jBound + j) * kBound + k;
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t i, il2cpp_array_size_t j, il2cpp_array_size_t k, float value)
	{
		il2cpp_array_size_t iBound = bounds[0].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(i, iBound);
		il2cpp_array_size_t jBound = bounds[1].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(j, jBound);
		il2cpp_array_size_t kBound = bounds[2].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(k, kBound);

		il2cpp_array_size_t index = (i * jBound + j) * kBound + k;
		m_Items[index] = value;
	}
	inline float GetAtUnchecked(il2cpp_array_size_t i, il2cpp_array_size_t j, il2cpp_array_size_t k) const
	{
		il2cpp_array_size_t jBound = bounds[1].length;
		il2cpp_array_size_t kBound = bounds[2].length;

		il2cpp_array_size_t index = (i * jBound + j) * kBound + k;
		return m_Items[index];
	}
	inline float* GetAddressAtUnchecked(il2cpp_array_size_t i, il2cpp_array_size_t j, il2cpp_array_size_t k)
	{
		il2cpp_array_size_t jBound = bounds[1].length;
		il2cpp_array_size_t kBound = bounds[2].length;

		il2cpp_array_size_t index = (i * jBound + j) * kBound + k;
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t i, il2cpp_array_size_t j, il2cpp_array_size_t k, float value)
	{
		il2cpp_array_size_t jBound = bounds[1].length;
		il2cpp_array_size_t kBound = bounds[2].length;

		il2cpp_array_size_t index = (i * jBound + j) * kBound + k;
		m_Items[index] = value;
	}
};
// System.Boolean[]
struct BooleanU5BU5D_tD317D27C31DB892BE79FAE3AEBC0B3FFB73DE9B4  : public RuntimeArray
{
	ALIGN_FIELD (8) bool m_Items[1];

	inline bool GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline bool* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, bool value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline bool GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline bool* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, bool value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Texture2D[]
struct Texture2DU5BU5D_t05332F1E3F7D4493E304C702201F9BE4F9236191  : public RuntimeArray
{
	ALIGN_FIELD (8) Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* m_Items[1];

	inline Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// UnityEngine.MeshFilter[]
struct MeshFilterU5BU5D_tCE3B457E6F7ECE5ECEE9E09150642150448685BA  : public RuntimeArray
{
	ALIGN_FIELD (8) MeshFilter_t6D1CE2473A1E45AC73013400585A1163BF66B2F5* m_Items[1];

	inline MeshFilter_t6D1CE2473A1E45AC73013400585A1163BF66B2F5* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline MeshFilter_t6D1CE2473A1E45AC73013400585A1163BF66B2F5** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, MeshFilter_t6D1CE2473A1E45AC73013400585A1163BF66B2F5* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline MeshFilter_t6D1CE2473A1E45AC73013400585A1163BF66B2F5* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline MeshFilter_t6D1CE2473A1E45AC73013400585A1163BF66B2F5** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, MeshFilter_t6D1CE2473A1E45AC73013400585A1163BF66B2F5* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// MicahW.PointGrass.PointGrassCommon/ObjectData[]
struct ObjectDataU5BU5D_tFCDCD60E1D45526EC9B864EF342ACB7834D38930  : public RuntimeArray
{
	ALIGN_FIELD (8) ObjectData_t24D94D36E8D49DEB90BBFEE7BA833C910F0AF6C4 m_Items[1];

	inline ObjectData_t24D94D36E8D49DEB90BBFEE7BA833C910F0AF6C4 GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline ObjectData_t24D94D36E8D49DEB90BBFEE7BA833C910F0AF6C4* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, ObjectData_t24D94D36E8D49DEB90BBFEE7BA833C910F0AF6C4 value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline ObjectData_t24D94D36E8D49DEB90BBFEE7BA833C910F0AF6C4 GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline ObjectData_t24D94D36E8D49DEB90BBFEE7BA833C910F0AF6C4* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, ObjectData_t24D94D36E8D49DEB90BBFEE7BA833C910F0AF6C4 value)
	{
		m_Items[index] = value;
	}
};
// System.Delegate[]
struct DelegateU5BU5D_tC5AB7E8F745616680F337909D3A8E6C722CDF771  : public RuntimeArray
{
	ALIGN_FIELD (8) Delegate_t* m_Items[1];

	inline Delegate_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Delegate_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Delegate_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Delegate_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Delegate_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Delegate_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// UnityEngine.Mesh[]
struct MeshU5BU5D_t178CA36422FC397211E68FB7E39C5B2F95619689  : public RuntimeArray
{
	ALIGN_FIELD (8) Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* m_Items[1];

	inline Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// UnityEngine.Material[]
struct MaterialU5BU5D_t2B1D11C42DB07A4400C0535F92DBB87A2E346D3D  : public RuntimeArray
{
	ALIGN_FIELD (8) Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* m_Items[1];

	inline Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// UnityEngine.ComputeBuffer[]
struct ComputeBufferU5BU5D_t7832804740B13E96807A836AD90ADF1477D7FE27  : public RuntimeArray
{
	ALIGN_FIELD (8) ComputeBuffer_t51EADA9015EBCC1B982C5584E9AB2734415A8233* m_Items[1];

	inline ComputeBuffer_t51EADA9015EBCC1B982C5584E9AB2734415A8233* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline ComputeBuffer_t51EADA9015EBCC1B982C5584E9AB2734415A8233** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, ComputeBuffer_t51EADA9015EBCC1B982C5584E9AB2734415A8233* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline ComputeBuffer_t51EADA9015EBCC1B982C5584E9AB2734415A8233* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline ComputeBuffer_t51EADA9015EBCC1B982C5584E9AB2734415A8233** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, ComputeBuffer_t51EADA9015EBCC1B982C5584E9AB2734415A8233* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// UnityEngine.MaterialPropertyBlock[]
struct MaterialPropertyBlockU5BU5D_t6911DDC471FC23F2AF58FF0140ED595529EC5F5B  : public RuntimeArray
{
	ALIGN_FIELD (8) MaterialPropertyBlock_t2308669579033A857EFE6E4831909F638B27411D* m_Items[1];

	inline MaterialPropertyBlock_t2308669579033A857EFE6E4831909F638B27411D* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline MaterialPropertyBlock_t2308669579033A857EFE6E4831909F638B27411D** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, MaterialPropertyBlock_t2308669579033A857EFE6E4831909F638B27411D* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline MaterialPropertyBlock_t2308669579033A857EFE6E4831909F638B27411D* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline MaterialPropertyBlock_t2308669579033A857EFE6E4831909F638B27411D** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, MaterialPropertyBlock_t2308669579033A857EFE6E4831909F638B27411D* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// UnityEngine.Bounds[]
struct BoundsU5BU5D_t45563ED55B62FA0536E8117454C56C0CFA5B25F5  : public RuntimeArray
{
	ALIGN_FIELD (8) Bounds_t367E830C64BBF235ED8C3B2F8CF6254FDCAD39C3 m_Items[1];

	inline Bounds_t367E830C64BBF235ED8C3B2F8CF6254FDCAD39C3 GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Bounds_t367E830C64BBF235ED8C3B2F8CF6254FDCAD39C3* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Bounds_t367E830C64BBF235ED8C3B2F8CF6254FDCAD39C3 value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Bounds_t367E830C64BBF235ED8C3B2F8CF6254FDCAD39C3 GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Bounds_t367E830C64BBF235ED8C3B2F8CF6254FDCAD39C3* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Bounds_t367E830C64BBF235ED8C3B2F8CF6254FDCAD39C3 value)
	{
		m_Items[index] = value;
	}
};
// System.Object[]
struct ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918  : public RuntimeArray
{
	ALIGN_FIELD (8) RuntimeObject* m_Items[1];

	inline RuntimeObject* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline RuntimeObject* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};


// System.Boolean System.Nullable`1<UnityEngine.Vector3>::get_HasValue()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool Nullable_1_get_HasValue_m6B76086B0E863AB1D634FD03E30154F230070435_gshared_inline (Nullable_1_t9C51B084784B716FFF4ED4575C63CFD8A71A86FE* __this, const RuntimeMethod* method) ;
// T System.Nullable`1<UnityEngine.Vector3>::get_Value()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Nullable_1_get_Value_m6A74FA440FE386A9905C61B41B5C261CD9DC4792_gshared (Nullable_1_t9C51B084784B716FFF4ED4575C63CFD8A71A86FE* __this, const RuntimeMethod* method) ;
// System.Boolean System.Linq.Enumerable::Contains<System.Object>(System.Collections.Generic.IEnumerable`1<TSource>,TSource)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerable_Contains_TisRuntimeObject_mBCDB5870C52FC5BD2B6AE472A749FC03B9CF8958_gshared (RuntimeObject* ___source0, RuntimeObject* ___value1, const RuntimeMethod* method) ;
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m4407E4C389F22B8CEC282C15D56516658746C383_gshared_inline (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_m7F078BB342729BDF11327FD89D7872265328F690_gshared (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<System.Object>::Clear()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void List_1_Clear_m16C1F2C61FED5955F10EB36BC1CB2DF34B128994_gshared_inline (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, const RuntimeMethod* method) ;
// System.Void System.Func`2<System.Object,MicahW.PointGrass.PointGrassCommon/ObjectData>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_m1DE76DEA42858761846B8A55B60FBEBF0EB931DB_gshared (Func_2_tC0C5B5765EBF104D9C013A26717BC3C3AC47157C* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Select<System.Object,MicahW.PointGrass.PointGrassCommon/ObjectData>(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Enumerable_Select_TisRuntimeObject_TisObjectData_t24D94D36E8D49DEB90BBFEE7BA833C910F0AF6C4_m30D3D8A59446A496CFE4C9A719F7ABD8C94A8DF1_gshared (RuntimeObject* ___source0, Func_2_tC0C5B5765EBF104D9C013A26717BC3C3AC47157C* ___selector1, const RuntimeMethod* method) ;
// TSource[] System.Linq.Enumerable::ToArray<MicahW.PointGrass.PointGrassCommon/ObjectData>(System.Collections.Generic.IEnumerable`1<TSource>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ObjectDataU5BU5D_tFCDCD60E1D45526EC9B864EF342ACB7834D38930* Enumerable_ToArray_TisObjectData_t24D94D36E8D49DEB90BBFEE7BA833C910F0AF6C4_mBA811FFD832E9653BA91207907FC396E52CB0DDF_gshared (RuntimeObject* ___source0, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<System.Object>::Add(T)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void List_1_Add_mEBCF994CC3814631017F46A387B1A192ED6C85C7_gshared_inline (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, RuntimeObject* ___item0, const RuntimeMethod* method) ;
// System.Boolean System.Collections.Generic.List`1<System.Object>::Remove(T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool List_1_Remove_m4DFA48F4CEB9169601E75FC28517C5C06EFA5AD7_gshared (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, RuntimeObject* ___item0, const RuntimeMethod* method) ;
// System.Void System.Nullable`1<UnityEngine.Vector3>::.ctor(T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Nullable_1__ctor_m75F3ABB694E26670F021136BD3B9E71A65948BC2_gshared (Nullable_1_t9C51B084784B716FFF4ED4575C63CFD8A71A86FE* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___value0, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<UnityEngine.Bounds>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_m6495AB7C6BDDC4D06EE93E691A9DC2C6FC791911_gshared (List_1_t7D73463EA17F4D29A37E8110AB60DAD5C80EBF65* __this, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<System.Object>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_AddRange_m1F76B300133150E6046C5FED00E88B5DE0A02E17_gshared (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, RuntimeObject* ___collection0, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<UnityEngine.Bounds>::Add(T)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void List_1_Add_m930CA3F1A016B820967E8D53B5D8B2EEB9F10DAC_gshared_inline (List_1_t7D73463EA17F4D29A37E8110AB60DAD5C80EBF65* __this, Bounds_t367E830C64BBF235ED8C3B2F8CF6254FDCAD39C3 ___item0, const RuntimeMethod* method) ;
// T[] System.Collections.Generic.List`1<System.Object>::ToArray()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* List_1_ToArray_mD7E4F8E7C11C3C67CB5739FCC0A6E86106A6291F_gshared (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, const RuntimeMethod* method) ;
// T[] System.Collections.Generic.List`1<UnityEngine.Bounds>::ToArray()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR BoundsU5BU5D_t45563ED55B62FA0536E8117454C56C0CFA5B25F5* List_1_ToArray_m18ACE74090BCEB952354342ADF397C950A01321A_gshared (List_1_t7D73463EA17F4D29A37E8110AB60DAD5C80EBF65* __this, const RuntimeMethod* method) ;
// T UnityEngine.Component::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Component_GetComponent_TisRuntimeObject_m7181F81CAEC2CF53F5D2BC79B7425C16E1F80D33_gshared (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3* __this, const RuntimeMethod* method) ;
// System.Void System.Array::Resize<System.Single>(T[]&,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Array_Resize_TisSingle_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_m879C2A54DAFE78F46D1185B50ED527EE182BFB04_gshared (SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C** ___array0, int32_t ___newSize1, const RuntimeMethod* method) ;
// System.Void System.Array::Resize<System.Object>(T[]&,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Array_Resize_TisRuntimeObject_mE8D92C287251BAF8256D85E5829F749359EC334E_gshared (ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918** ___array0, int32_t ___newSize1, const RuntimeMethod* method) ;

// UnityEngine.Transform UnityEngine.Component::get_transform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371 (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3* __this, const RuntimeMethod* method) ;
// UnityEngine.Quaternion UnityEngine.Transform::get_rotation()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 Transform_get_rotation_m32AF40CA0D50C797DA639A696F8EAEC7524C179C (Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* __this, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Quaternion::get_eulerAngles()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Quaternion_get_eulerAngles_m2DB5158B5C3A71FD60FC8A6EE43D3AAA1CFED122_inline (Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974* __this, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Vector3::get_zero()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_get_zero_m0C1249C3F25B1C70EAD3CC8B31259975A457AE39_inline (const RuntimeMethod* method) ;
// System.Single UnityEngine.Input::GetAxis(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Input_GetAxis_m10372E6C5FF591668D2DC5F58C58D213CC598A62 (String_t* ___axisName0, const RuntimeMethod* method) ;
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* __this, float ___x0, float ___y1, const RuntimeMethod* method) ;
// System.Boolean UnityEngine.Input::GetKey(UnityEngine.KeyCode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Input_GetKey_mE5681EF775F3CEBA7EAD7C63984F7B34C8E8D434 (int32_t ___key0, const RuntimeMethod* method) ;
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Vector3::get_up()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_get_up_m128AF3FDC820BF59D5DE86D973E7DE3F20C3AEBA_inline (const RuntimeMethod* method) ;
// UnityEngine.Quaternion UnityEngine.Quaternion::AngleAxis(System.Single,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 Quaternion_AngleAxis_mF37022977B297E63AA70D69EA1C4C922FF22CC80 (float ___angle0, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___axis1, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Vector3::get_right()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_get_right_mFF573AFBBB2186E7AFA1BA7CA271A78DF67E4EA0_inline (const RuntimeMethod* method) ;
// UnityEngine.Quaternion UnityEngine.Quaternion::op_Multiply(UnityEngine.Quaternion,UnityEngine.Quaternion)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 Quaternion_op_Multiply_mCB375FCCC12A2EC8F9EB824A1BFB4453B58C2012_inline (Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___lhs0, Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___rhs1, const RuntimeMethod* method) ;
// System.Void UnityEngine.Transform::set_rotation(UnityEngine.Quaternion)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_rotation_m61340DE74726CF0F9946743A727C4D444397331D (Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* __this, Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___value0, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Transform::TransformDirection(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Transform_TransformDirection_m9BE1261DF2D48B7A4A27D31EE24D2D97F89E7757 (Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___direction0, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(UnityEngine.Vector3,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_op_Multiply_m87BA7C578F96C8E49BB07088DAAC4649F83B0353_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___a0, float ___d1, const RuntimeMethod* method) ;
// System.Single UnityEngine.Time::get_deltaTime()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Time_get_deltaTime_mC3195000401F0FD167DD2F948FD2BC58330D0865 (const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Vector3::MoveTowards(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_MoveTowards_m0363264647799F3173AC37F8E819F98298249B08_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___current0, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___target1, float ___maxDistanceDelta2, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1 (Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* __this, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Addition(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_op_Addition_m78C0EC70CB66E8DCAC225743D82B268DAEE92067_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___a0, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___b1, const RuntimeMethod* method) ;
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_position_mA1A817124BB41B685043DED2A9BA48CDF37C4156 (Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___value0, const RuntimeMethod* method) ;
// System.Void UnityEngine.MonoBehaviour::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E (MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71* __this, const RuntimeMethod* method) ;
// System.Single UnityEngine.Time::get_time()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Time_get_time_m3A271BB1B20041144AC5B7863B71AB1F0150374B (const RuntimeMethod* method) ;
// System.Void UnityEngine.Vector3::Scale(UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3_Scale_mE0DC2C1B7902271788591F17DBE7F7F72EC37283_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___scale0, const RuntimeMethod* method) ;
// System.Boolean MicahW.PointGrass.PointGrassCommon/MeshData::get_HasAttributes()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool MeshData_get_HasAttributes_m4F13A5EE919CA43A34BBE872AC66756434B30060 (MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F* __this, const RuntimeMethod* method) ;
// System.Boolean MicahW.PointGrass.PointGrassCommon/MeshData::get_HasColours()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool MeshData_get_HasColours_m67226DD3676EBC3004861960060933A3EFB23FA2 (MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F* __this, const RuntimeMethod* method) ;
// System.Void MicahW.PointGrass.PointGrassCommon/MeshData::ApplyDensityCutoff(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MeshData_ApplyDensityCutoff_m8F76018CA26B3621D4B38ED51A0EF5101D35B1E4 (MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F* __this, float ___cutoff0, const RuntimeMethod* method) ;
// System.Void MicahW.PointGrass.PointGrassCommon/MeshData::ApplyLengthMapping(UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MeshData_ApplyLengthMapping_mCD709AEF3230ECE96D914FBDF433B3D1DF97C183 (MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F* __this, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___mapping0, const RuntimeMethod* method) ;
// System.Single[] MicahW.PointGrass.DistributePointsAlongMesh::GetCumulativeTriSizes(System.Int32[],UnityEngine.Vector3[],UnityEngine.Vector2[],UnityEngine.Vector3,System.Boolean,System.Single&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* DistributePointsAlongMesh_GetCumulativeTriSizes_m64D783A59608A96F2FABF19CA7D0BB72F399B18C (Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* ___tris0, Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* ___verts1, Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* ___attributes2, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___scale3, bool ___useDensity4, float* ___totalArea5, const RuntimeMethod* method) ;
// System.Int32 UnityEngine.Mathf::FloorToInt(System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t Mathf_FloorToInt_m2A39AE881CAEE6B6A4B3BFEF9CA1ED40625F5AB7_inline (float ___f0, const RuntimeMethod* method) ;
// MicahW.PointGrass.PointGrassCommon/MeshPoint[] MicahW.PointGrass.DistributePointsAlongMesh::DistributePoints_CPU(MicahW.PointGrass.PointGrassCommon/MeshData,System.Single[],System.Int32,System.Int32,System.Single,System.Nullable`1<UnityEngine.Vector3>,System.Boolean,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR MeshPointU5BU5D_tBCEA150F23C3861E840A87B994115F06D7490DD8* DistributePointsAlongMesh_DistributePoints_CPU_m7EDF68975B721F8590DB32548BFCA21B9593805D (MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F* ___mesh0, SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* ___cumulativeSizes1, int32_t ___pointCount2, int32_t ___seed3, float ___totalArea4, Nullable_1_t9C51B084784B716FFF4ED4575C63CFD8A71A86FE ___forcedNormal5, bool ___useColours6, bool ___useLength7, const RuntimeMethod* method) ;
// UnityEngine.Random/State UnityEngine.Random::get_state()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR State_tA37EB68FE687D41D4B228462D4C7427FAC5BF9C1 Random_get_state_m84FD78A44C27EA34F1CD9833545DAB9B541B98AA (const RuntimeMethod* method) ;
// System.Void UnityEngine.Random::InitState(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Random_InitState_mE70961834F42FFEEB06CB9C68175354E0C255664 (int32_t ___seed0, const RuntimeMethod* method) ;
// System.Single UnityEngine.Random::Range(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Random_Range_m5236C99A7D8AE6AC9190592DC66016652A2D2494 (float ___minInclusive0, float ___maxInclusive1, const RuntimeMethod* method) ;
// System.Int32 MicahW.PointGrass.DistributePointsAlongMesh::FindTriangleIndex(System.Single[],System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t DistributePointsAlongMesh_FindTriangleIndex_mFA277A2DD05D747D627B4504C44E8BED5F0F8DFA (SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* ___cumulativeTriSizes0, float ___randomSample1, const RuntimeMethod* method) ;
// System.Single UnityEngine.Random::get_value()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Random_get_value_m2CEA87FADF5222EF9E13D32695F15E2BA282E24B (const RuntimeMethod* method) ;
// System.Boolean System.Nullable`1<UnityEngine.Vector3>::get_HasValue()
inline bool Nullable_1_get_HasValue_m6B76086B0E863AB1D634FD03E30154F230070435_inline (Nullable_1_t9C51B084784B716FFF4ED4575C63CFD8A71A86FE* __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Nullable_1_t9C51B084784B716FFF4ED4575C63CFD8A71A86FE*, const RuntimeMethod*))Nullable_1_get_HasValue_m6B76086B0E863AB1D634FD03E30154F230070435_gshared_inline)(__this, method);
}
// T System.Nullable`1<UnityEngine.Vector3>::get_Value()
inline Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Nullable_1_get_Value_m6A74FA440FE386A9905C61B41B5C261CD9DC4792 (Nullable_1_t9C51B084784B716FFF4ED4575C63CFD8A71A86FE* __this, const RuntimeMethod* method)
{
	return ((  Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 (*) (Nullable_1_t9C51B084784B716FFF4ED4575C63CFD8A71A86FE*, const RuntimeMethod*))Nullable_1_get_Value_m6A74FA440FE386A9905C61B41B5C261CD9DC4792_gshared)(__this, method);
}
// UnityEngine.Vector2 UnityEngine.Vector2::op_Multiply(UnityEngine.Vector2,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 Vector2_op_Multiply_m2D984B613020089BF5165BA4CA10988E2DC771FE_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___a0, float ___d1, const RuntimeMethod* method) ;
// UnityEngine.Vector2 UnityEngine.Vector2::op_Addition(UnityEngine.Vector2,UnityEngine.Vector2)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 Vector2_op_Addition_m8136742CE6EE33BA4EB81C5F584678455917D2AE_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___a0, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___b1, const RuntimeMethod* method) ;
// UnityEngine.Vector4 UnityEngine.Vector4::op_Implicit(UnityEngine.Vector2)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 Vector4_op_Implicit_mB193CD8DA20DEB9E9F95CFEB5A2B1B9B3B7ECFEB_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___v0, const RuntimeMethod* method) ;
// UnityEngine.Color UnityEngine.Color::op_Multiply(UnityEngine.Color,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Color_tD001788D726C3A7F1379BEED0260B9591F440C1F Color_op_Multiply_m379B20A820266ACF82A21425B9CAE8DCD773CFBB_inline (Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___a0, float ___b1, const RuntimeMethod* method) ;
// UnityEngine.Color UnityEngine.Color::op_Addition(UnityEngine.Color,UnityEngine.Color)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Color_tD001788D726C3A7F1379BEED0260B9591F440C1F Color_op_Addition_mA7A51CACA49ED8D23D3D9CA3A0092D32F657E053_inline (Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___a0, Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___b1, const RuntimeMethod* method) ;
// UnityEngine.Color UnityEngine.Color::get_white()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Color_tD001788D726C3A7F1379BEED0260B9591F440C1F Color_get_white_m068F5AF879B0FCA584E3693F762EA41BB65532C6_inline (const RuntimeMethod* method) ;
// System.Void UnityEngine.Random::set_state(UnityEngine.Random/State)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Random_set_state_m335B3E4E30A9A85E8794BC0A84013F1F365BA179 (State_tA37EB68FE687D41D4B228462D4C7427FAC5BF9C1 ___value0, const RuntimeMethod* method) ;
// System.Single[] MicahW.PointGrass.DistributePointsAlongMesh::GetTriSizes(System.Int32[],UnityEngine.Vector3[],UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* DistributePointsAlongMesh_GetTriSizes_m8FCA660CA122706DD76F206211626C4421F8439B (Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* ___tris0, Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* ___verts1, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___scale2, const RuntimeMethod* method) ;
// System.Single[] MicahW.PointGrass.DistributePointsAlongMesh::GetWeightedTriSizes(System.Int32[],UnityEngine.Vector3[],UnityEngine.Vector2[],UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* DistributePointsAlongMesh_GetWeightedTriSizes_m7C97E84C6C9894F39FBE0A9E10D6581E58EF2C97 (Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* ___tris0, Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* ___verts1, Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* ___attributes2, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___scale3, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Subtraction(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_op_Subtraction_mE42023FF80067CB44A1D4A27EB7CF2B24CABB828_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___a0, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___b1, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Vector3::Cross(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_Cross_mF93A280558BCE756D13B6CC5DCD7DE8A43148987_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___lhs0, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___rhs1, const RuntimeMethod* method) ;
// System.Single UnityEngine.Vector3::get_magnitude()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Vector3_get_magnitude_mF0D6017E90B345F1F52D1CC564C640F1A847AF2D_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* __this, const RuntimeMethod* method) ;
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Inequality_mD0BE578448EAA61948F25C32F8DD55AB1F778602 (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* ___x0, Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* ___y1, const RuntimeMethod* method) ;
// System.Boolean MicahW.PointGrass.PointGrassCommon::get_PropertyIDsInitialized()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool PointGrassCommon_get_PropertyIDsInitialized_m9958FC8398CDF68DA0FFF72DAA3C35469B07D1A1_inline (const RuntimeMethod* method) ;
// System.Int32 UnityEngine.Shader::PropertyToID(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Shader_PropertyToID_mE98523D50F5656CAE89B30695C458253EB8956CA (String_t* ___name0, const RuntimeMethod* method) ;
// System.Void MicahW.PointGrass.PointGrassCommon::set_PropertyIDsInitialized(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void PointGrassCommon_set_PropertyIDsInitialized_mAC9F656EF7F0EF2F47B26CD89EC6BE6CD235245D_inline (bool ___value0, const RuntimeMethod* method) ;
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Equality_mB6120F782D83091EF56A198FCEBCF066DB4A9605 (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* ___x0, Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* ___y1, const RuntimeMethod* method) ;
// UnityEngine.Matrix4x4 UnityEngine.Transform::get_localToWorldMatrix()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 Transform_get_localToWorldMatrix_m5D35188766856338DD21DE756F42277C21719E6D (Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* __this, const RuntimeMethod* method) ;
// System.Void UnityEngine.MaterialPropertyBlock::SetMatrix(System.Int32,UnityEngine.Matrix4x4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MaterialPropertyBlock_SetMatrix_mA86792A03023DC1F6B46B06C72D61F3CCE4177AC (MaterialPropertyBlock_t2308669579033A857EFE6E4831909F638B27411D* __this, int32_t ___nameID0, Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 ___value1, const RuntimeMethod* method) ;
// UnityEngine.Matrix4x4 UnityEngine.Transform::get_worldToLocalMatrix()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 Transform_get_worldToLocalMatrix_mB633C122A01BCE8E51B10B8B8CB95F580750B3F1 (Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* __this, const RuntimeMethod* method) ;
// System.Void MicahW.PointGrass.PointGrassDisplacementManager::UpdatePropertyBlock(UnityEngine.MaterialPropertyBlock&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointGrassDisplacementManager_UpdatePropertyBlock_mE705BEB14C825F65DBF87F0DA864B492DFBAC365 (PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900* __this, MaterialPropertyBlock_t2308669579033A857EFE6E4831909F638B27411D** ___block0, const RuntimeMethod* method) ;
// UnityEngine.Mesh MicahW.PointGrass.PointGrassCommon::GenerateGrassMesh_Flat(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* PointGrassCommon_GenerateGrassMesh_Flat_m0B8138A7DAEFC9028E47E9D68ACCCC39A3B2E23B (int32_t ___divisions0, const RuntimeMethod* method) ;
// UnityEngine.Mesh MicahW.PointGrass.PointGrassCommon::GenerateGrassMesh_Cylinder(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* PointGrassCommon_GenerateGrassMesh_Cylinder_m3290C77744AA3E4E2C99868E8D082E81EC4A9D0F (int32_t ___divisions0, int32_t ___loops1, const RuntimeMethod* method) ;
// System.Int32 UnityEngine.Mathf::Max(System.Int32,System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t Mathf_Max_m7FA442918DE37E3A00106D1F2E789D65829792B8_inline (int32_t ___a0, int32_t ___b1, const RuntimeMethod* method) ;
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline (Color_tD001788D726C3A7F1379BEED0260B9591F440C1F* __this, float ___r0, float ___g1, float ___b2, float ___a3, const RuntimeMethod* method) ;
// System.Void UnityEngine.Mesh::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Mesh__ctor_m5A9AECEDDAFFD84811ED8928012BDE97A9CEBD00 (Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* __this, const RuntimeMethod* method) ;
// System.Void UnityEngine.Object::set_name(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_set_name_mC79E6DC8FFD72479C90F0C4CC7F42A0FEAF5AE47 (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* __this, String_t* ___value0, const RuntimeMethod* method) ;
// System.Void UnityEngine.Mesh::set_vertices(UnityEngine.Vector3[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Mesh_set_vertices_m5BB814D89E9ACA00DBF19F7D8E22CB73AC73FE5C (Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* __this, Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* ___value0, const RuntimeMethod* method) ;
// System.Void UnityEngine.Mesh::set_triangles(System.Int32[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Mesh_set_triangles_m124405320579A8D92711BB5A124644963A26F60B (Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* __this, Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* ___value0, const RuntimeMethod* method) ;
// System.Void UnityEngine.Mesh::set_colors(UnityEngine.Color[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Mesh_set_colors_m5558BAAA60676427B7954F1694A1765B000EB0FE (Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* __this, ColorU5BU5D_t612261CF293F6FFC3D80AB52259FF0DC2B2CC389* ___value0, const RuntimeMethod* method) ;
// System.Void UnityEngine.Mesh::SetUVs(System.Int32,UnityEngine.Vector2[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Mesh_SetUVs_m6AFD5BFC4D7FB9EE57D8F19AB1BECD0675771D48 (Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* __this, int32_t ___channel0, Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* ___uvs1, const RuntimeMethod* method) ;
// System.Void UnityEngine.Mesh::RecalculateNormals()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Mesh_RecalculateNormals_m3AA2788914611444E030CA310E03E3CFE683902B (Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* __this, const RuntimeMethod* method) ;
// System.Void MicahW.PointGrass.PointGrassCommon::<GenerateGrassMesh_Cylinder>g__SetVert|17_0(System.Int32,UnityEngine.Vector3,MicahW.PointGrass.PointGrassCommon/<>c__DisplayClass17_0&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointGrassCommon_U3CGenerateGrassMesh_CylinderU3Eg__SetVertU7C17_0_m9E44405726796B377C797C8B52794EF850E59ABD (int32_t ___index0, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___pos1, U3CU3Ec__DisplayClass17_0_t3B43FCEDB710544F3D8D366A3758A6D2CEF70412* p2, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Matrix4x4::MultiplyPoint(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Matrix4x4_MultiplyPoint_m20E910B65693559BFDE99382472D8DD02C862E7E (Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___point0, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Matrix4x4::MultiplyVector(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Matrix4x4_MultiplyVector_mFD12F86A473E90BBB0002149ABA3917B2A518937 (Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___vector0, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Vector3::op_UnaryNegation(UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_op_UnaryNegation_m5450829F333BD2A88AF9A592C4EE331661225915_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___a0, const RuntimeMethod* method) ;
// System.Int32 UnityEngine.LayerMask::op_Implicit(UnityEngine.LayerMask)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t LayerMask_op_Implicit_m7F5A5B9D079281AC445ED39DEE1FCFA9D795810D (LayerMask_t97CB6BDADEDC3D6423C7BCFEA7F86DA2EC6241DB ___mask0, const RuntimeMethod* method) ;
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Physics_Raycast_m56120FFEF0D4F0A44CCA505B5C946E6FB8742F12 (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___origin0, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___direction1, RaycastHit_t6F30BD0B38B56401CA833A1B87BD74F2ACD2F2B5* ___hitInfo2, float ___maxDistance3, int32_t ___layerMask4, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.RaycastHit::get_point()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 RaycastHit_get_point_m02B764612562AFE0F998CC7CFB2EEDE41BA47F39 (RaycastHit_t6F30BD0B38B56401CA833A1B87BD74F2ACD2F2B5* __this, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.RaycastHit::get_normal()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 RaycastHit_get_normal_mD8741B70D2039C5CAFC4368D4CE59D89562040B5 (RaycastHit_t6F30BD0B38B56401CA833A1B87BD74F2ACD2F2B5* __this, const RuntimeMethod* method) ;
// System.Void MicahW.PointGrass.PointGrassCommon/MeshData::RecalculateBounds()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MeshData_RecalculateBounds_m23CC32EAA976F74CAD691ACF55DEDE635C504655 (MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F* __this, const RuntimeMethod* method) ;
// UnityEngine.RenderTexture UnityEngine.RenderTexture::GetTemporary(System.Int32,System.Int32,System.Int32,UnityEngine.RenderTextureFormat,UnityEngine.RenderTextureReadWrite)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* RenderTexture_GetTemporary_mA6619EA324AAE80B6892107C6968092F6F1B4C45 (int32_t ___width0, int32_t ___height1, int32_t ___depthBuffer2, int32_t ___format3, int32_t ___readWrite4, const RuntimeMethod* method) ;
// UnityEngine.RenderTexture UnityEngine.RenderTexture::get_active()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* RenderTexture_get_active_mA4434B3E79DEF2C01CAE0A53061598B16443C9E7 (const RuntimeMethod* method) ;
// System.Void UnityEngine.Graphics::Blit(UnityEngine.Texture,UnityEngine.RenderTexture)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Graphics_Blit_m8D99E16B74C7D3C8F79F4F142C59DB6B38114504 (Texture_t791CBB51219779964E0E8A2ED7C1AA5F92A4A700* ___source0, RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* ___dest1, const RuntimeMethod* method) ;
// System.Void UnityEngine.Texture2D::.ctor(System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Texture2D__ctor_mECF60A9EC0638EC353C02C8E99B6B465D23BE917 (Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* __this, int32_t ___width0, int32_t ___height1, int32_t ___textureFormat2, bool ___mipChain3, const RuntimeMethod* method) ;
// System.Void UnityEngine.Rect::.ctor(System.Single,System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rect__ctor_m18C3033D135097BEE424AAA68D91C706D2647F23 (Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D* __this, float ___x0, float ___y1, float ___width2, float ___height3, const RuntimeMethod* method) ;
// System.Void UnityEngine.Texture2D::ReadPixels(UnityEngine.Rect,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Texture2D_ReadPixels_m6B45DF7C051BF599C72ED09691F21A6C769EEBD9 (Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* __this, Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D ___source0, int32_t ___destX1, int32_t ___destY2, const RuntimeMethod* method) ;
// System.Void UnityEngine.Texture2D::Apply()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Texture2D_Apply_mA014182C9EE0BBF6EEE3B286854F29E50EB972DC (Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* __this, const RuntimeMethod* method) ;
// System.Void UnityEngine.RenderTexture::set_active(UnityEngine.RenderTexture)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RenderTexture_set_active_m5EE8E2327EF9B306C1425014CC34C41A8384E7AB (RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* ___value0, const RuntimeMethod* method) ;
// System.Void UnityEngine.RenderTexture::ReleaseTemporary(UnityEngine.RenderTexture)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RenderTexture_ReleaseTemporary_mEEF2C1990196FF06FDD0DC190928AD3A023EBDD2 (RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* ___temp0, const RuntimeMethod* method) ;
// System.Int32 UnityEngine.TerrainData::get_heightmapResolution()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t TerrainData_get_heightmapResolution_m39FE9A5C31A80B28021F8E2484EF5F2664798836 (TerrainData_t615A68EAC648066681875D47FC641496D12F2E24* __this, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.TerrainData::get_size()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 TerrainData_get_size_mCD3977F344B9DEBFF61DD537D03FEB9473838DA5 (TerrainData_t615A68EAC648066681875D47FC641496D12F2E24* __this, const RuntimeMethod* method) ;
// System.Int32 UnityEngine.TerrainData::get_alphamapWidth()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t TerrainData_get_alphamapWidth_m07E5B04B08E87AC9F66D766B363000F94C8612D4 (TerrainData_t615A68EAC648066681875D47FC641496D12F2E24* __this, const RuntimeMethod* method) ;
// System.Int32 UnityEngine.TerrainData::get_alphamapHeight()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t TerrainData_get_alphamapHeight_m4A8273D6E0E3526A31E2669FBAB240353C086AED (TerrainData_t615A68EAC648066681875D47FC641496D12F2E24* __this, const RuntimeMethod* method) ;
// System.Int32 UnityEngine.TerrainData::get_alphamapLayers()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t TerrainData_get_alphamapLayers_mF8A0A4F157F7C56354C5A6E3FABF9F230F410F69 (TerrainData_t615A68EAC648066681875D47FC641496D12F2E24* __this, const RuntimeMethod* method) ;
// System.Single[,,] UnityEngine.TerrainData::GetAlphamaps(System.Int32,System.Int32,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR SingleU5BU2CU2CU5D_tE902E5192C7283A470AAADB477117789A9682488* TerrainData_GetAlphamaps_m2DEF5D2068D54BDAE78661483C1FC4936B06EA01 (TerrainData_t615A68EAC648066681875D47FC641496D12F2E24* __this, int32_t ___x0, int32_t ___y1, int32_t ___width2, int32_t ___height3, const RuntimeMethod* method) ;
// UnityEngine.TerrainLayer[] UnityEngine.TerrainData::get_terrainLayers()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR TerrainLayerU5BU5D_t259E391D6115F121FCD284E79F62012D70956EB0* TerrainData_get_terrainLayers_m3B436DF37DDD9F18A46DD6BF112925AD5B8857C8 (TerrainData_t615A68EAC648066681875D47FC641496D12F2E24* __this, const RuntimeMethod* method) ;
// System.Boolean System.Linq.Enumerable::Contains<UnityEngine.TerrainLayer>(System.Collections.Generic.IEnumerable`1<TSource>,TSource)
inline bool Enumerable_Contains_TisTerrainLayer_t52E14A94A0CF76B0B5509B7FDFDE64FF8A9FEFF9_m6D7B0AFE1EC7DB5185D1BB1A733E959511E0A6D1 (RuntimeObject* ___source0, TerrainLayer_t52E14A94A0CF76B0B5509B7FDFDE64FF8A9FEFF9* ___value1, const RuntimeMethod* method)
{
	return ((  bool (*) (RuntimeObject*, TerrainLayer_t52E14A94A0CF76B0B5509B7FDFDE64FF8A9FEFF9*, const RuntimeMethod*))Enumerable_Contains_TisRuntimeObject_mBCDB5870C52FC5BD2B6AE472A749FC03B9CF8958_gshared)(___source0, ___value1, method);
}
// UnityEngine.Texture2D UnityEngine.TerrainLayer::get_diffuseTexture()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* TerrainLayer_get_diffuseTexture_mAF75D09F08293C26B26D7D422B4A0ACC9732DD31 (TerrainLayer_t52E14A94A0CF76B0B5509B7FDFDE64FF8A9FEFF9* __this, const RuntimeMethod* method) ;
// UnityEngine.Texture2D MicahW.PointGrass.PointGrassCommon::CreateTextureCopy(UnityEngine.Texture2D,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* PointGrassCommon_CreateTextureCopy_mADE8309152AFE527D5756F91C7491A9B0C2A8C1F (Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* ___source0, int32_t ___width1, int32_t ___height2, const RuntimeMethod* method) ;
// UnityEngine.Vector2 UnityEngine.TerrainLayer::get_tileOffset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 TerrainLayer_get_tileOffset_m1E81B6D002E91BE4EF402337C595227221E8163D (TerrainLayer_t52E14A94A0CF76B0B5509B7FDFDE64FF8A9FEFF9* __this, const RuntimeMethod* method) ;
// UnityEngine.Vector2 UnityEngine.TerrainLayer::get_tileSize()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 TerrainLayer_get_tileSize_mF6313141BE22D6AC7362DA9A40FB41236458E0A3 (TerrainLayer_t52E14A94A0CF76B0B5509B7FDFDE64FF8A9FEFF9* __this, const RuntimeMethod* method) ;
// UnityEngine.Vector2 UnityEngine.Vector2::get_zero()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 Vector2_get_zero_m32506C40EC2EE7D5D4410BF40D3EE683A3D5F32C_inline (const RuntimeMethod* method) ;
// System.Single UnityEngine.Mathf::Clamp01(System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Mathf_Clamp01_mA7E048DBDA832D399A581BE4D6DED9FA44CE0F14_inline (float ___value0, const RuntimeMethod* method) ;
// System.Single UnityEngine.TerrainData::GetHeight(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float TerrainData_GetHeight_mC0FAC93E5B8128D5B918EDDDFEE55F6E3E23E8D5 (TerrainData_t615A68EAC648066681875D47FC641496D12F2E24* __this, int32_t ___x0, int32_t ___y1, const RuntimeMethod* method) ;
// UnityEngine.Color UnityEngine.Color::get_black()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Color_tD001788D726C3A7F1379BEED0260B9591F440C1F Color_get_black_mB50217951591A045844C61E7FF31EEE3FEF16737_inline (const RuntimeMethod* method) ;
// UnityEngine.Color UnityEngine.Texture2D::GetPixelBilinear(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_tD001788D726C3A7F1379BEED0260B9591F440C1F Texture2D_GetPixelBilinear_m6AE4AF4FD181C478DF0F2C5C329F22A263ABFF5C (Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* __this, float ___u0, float ___v1, const RuntimeMethod* method) ;
// System.Void MicahW.PointGrass.PointGrassCommon/MeshData::.ctor(UnityEngine.Vector3[],UnityEngine.Vector3[],UnityEngine.Vector2[],UnityEngine.Color[],System.Int32[],UnityEngine.Vector2[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MeshData__ctor_m70ACE57271D2DB0D80D16027646899AFC5035A50 (MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F* __this, Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* ___verts0, Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* ___normals1, Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* ___UVs2, ColorU5BU5D_t612261CF293F6FFC3D80AB52259FF0DC2B2CC389* ___colours3, Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* ___tris4, Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* ___attributes5, const RuntimeMethod* method) ;
// System.Void MicahW.PointGrass.PointGrassCommon/MeshData::RecalculateNormals()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MeshData_RecalculateNormals_m3DB8055C317DE19049DCC6647BCCB199FAF40917 (MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F* __this, const RuntimeMethod* method) ;
// UnityEngine.Mesh UnityEngine.MeshFilter::get_sharedMesh()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* MeshFilter_get_sharedMesh_mE4ED3E7E31C1DE5097E4980DA996E620F7D7CB8C (MeshFilter_t6D1CE2473A1E45AC73013400585A1163BF66B2F5* __this, const RuntimeMethod* method) ;
// System.Int32 UnityEngine.Mesh::get_vertexCount()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Mesh_get_vertexCount_mB7BE0340AAF272933068D830C8E711FC8978E12C (Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* __this, const RuntimeMethod* method) ;
// System.Int32[] UnityEngine.Mesh::get_triangles()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* Mesh_get_triangles_m33E39B4A383CC613C760FA7E297AC417A433F24B (Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* __this, const RuntimeMethod* method) ;
// UnityEngine.Vector3[] UnityEngine.Mesh::get_vertices()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* Mesh_get_vertices_mA3577F1B08EDDD54E26AEB3F8FFE4EC247D2ABB9 (Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* __this, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Transform::TransformPoint(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Transform_TransformPoint_m05BFF013DB830D7BFE44A007703694AE1062EE44 (Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___position0, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Transform::InverseTransformPoint(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Transform_InverseTransformPoint_m18CD395144D9C78F30E15A5B82B6670E792DBA5D (Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___position0, const RuntimeMethod* method) ;
// UnityEngine.Vector3[] UnityEngine.Mesh::get_normals()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* Mesh_get_normals_m2B6B159B799E6E235EA651FCAB2E18EE5B18ED62 (Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* __this, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Transform::InverseTransformVector(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Transform_InverseTransformVector_mBBA687CE32C0394FC9AB4F273D4E4A11F53FB044 (Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___vector0, const RuntimeMethod* method) ;
// UnityEngine.Color[] UnityEngine.Mesh::get_colors()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ColorU5BU5D_t612261CF293F6FFC3D80AB52259FF0DC2B2CC389* Mesh_get_colors_m3A38944EBA064B0E55A24C95C3706193F45B313D (Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* __this, const RuntimeMethod* method) ;
// UnityEngine.Vector2 UnityEngine.Vector2::get_one()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 Vector2_get_one_m9097EB8DC23C26118A591AF16702796C3EF51DFB_inline (const RuntimeMethod* method) ;
// UnityEngine.Vector2[] UnityEngine.Mesh::get_uv()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* Mesh_get_uv_mA47805C48AB3493FF3727922C43E77880E73519F (Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* __this, const RuntimeMethod* method) ;
// System.Void System.Array::Copy(System.Array,System.Int32,System.Array,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Array_Copy_mB4904E17BD92E320613A3251C0205E0786B3BF41 (RuntimeArray* ___sourceArray0, int32_t ___sourceIndex1, RuntimeArray* ___destinationArray2, int32_t ___destinationIndex3, int32_t ___length4, const RuntimeMethod* method) ;
// System.Void MicahW.PointGrass.PointGrassCommon/MeshData::.ctor(UnityEngine.Vector3[],UnityEngine.Vector3[],UnityEngine.Vector2[],System.Int32[],UnityEngine.Vector2[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MeshData__ctor_m4DDE83D4B889F4A8E8CB34522845BB26A776D752 (MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F* __this, Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* ___verts0, Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* ___normals1, Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* ___UVs2, Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* ___tris3, Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* ___attributes4, const RuntimeMethod* method) ;
// System.Void MicahW.PointGrass.PointGrassCommon/MeshPoint::.ctor(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Color,UnityEngine.Vector4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MeshPoint__ctor_m6C2D6745D2F6CAC65B54433FBFAB990C649DC31E (MeshPoint_t033BD3FDC9D80AE452F7B3D583D17CDADF299152* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___position0, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___normal1, Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___color2, Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___extraData3, const RuntimeMethod* method) ;
// System.Void MicahW.PointGrass.PointGrassCommon/ObjectData::.ctor(UnityEngine.Vector3,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ObjectData__ctor_mB2378318DDD47DCB6AEC9D788C27A66A94AE84CC (ObjectData_t24D94D36E8D49DEB90BBFEE7BA833C910F0AF6C4* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___position0, float ___radius1, float ___strength2, const RuntimeMethod* method) ;
// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2 (RuntimeObject* __this, const RuntimeMethod* method) ;
// System.Void UnityEngine.Bounds::.ctor(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Bounds__ctor_mAF7B238B9FBF90C495E5D7951760085A93119C5A (Bounds_t367E830C64BBF235ED8C3B2F8CF6254FDCAD39C3* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___center0, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___size1, const RuntimeMethod* method) ;
// System.Void UnityEngine.Bounds::Encapsulate(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Bounds_Encapsulate_m1FCA57C58536ADB67B85A703470C6F5BFB837C2F (Bounds_t367E830C64BBF235ED8C3B2F8CF6254FDCAD39C3* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___point0, const RuntimeMethod* method) ;
// System.Void System.Array::Clear(System.Array,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Array_Clear_m50BAA3751899858B097D3FF2ED31F284703FE5CB (RuntimeArray* ___array0, int32_t ___index1, int32_t ___length2, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Vector3::Normalize(UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_Normalize_mEF8349CC39674236CFC694189AFD36E31F89AC8F_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___value0, const RuntimeMethod* method) ;
// System.Void UnityEngine.Vector3::Normalize()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3_Normalize_mC749B887A4C74BA0A2E13E6377F17CCAEB0AADA8_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* __this, const RuntimeMethod* method) ;
// System.Void UnityEngine.Debug::LogError(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_LogError_mB00B2B4468EF3CAF041B038D840820FB84C924B2 (RuntimeObject* ___message0, const RuntimeMethod* method) ;
// System.Single UnityEngine.Mathf::Lerp(System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Mathf_Lerp_m47EF2FFB7647BD0A1FDC26DC03E28B19812139B5_inline (float ___a0, float ___b1, float ___t2, const RuntimeMethod* method) ;
// System.Int32 MicahW.PointGrass.SingleLayer::get_LayerIndex()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t SingleLayer_get_LayerIndex_mB5C8A986682EAAA66CD1371500373D28A93B727E_inline (SingleLayer_tA74696727907F9D2CD336CD4C7AA5C5C8C8FB4ED* __this, const RuntimeMethod* method) ;
// System.Int32 MicahW.PointGrass.SingleLayer::get_Mask()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t SingleLayer_get_Mask_m46E3E593C1213BB974ABBCDD3A0FD09791DC1473 (SingleLayer_tA74696727907F9D2CD336CD4C7AA5C5C8C8FB4ED* __this, const RuntimeMethod* method) ;
// System.Void MicahW.PointGrass.SingleLayer::.ctor(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void SingleLayer__ctor_mB56173673F914A84F0A215F602C28EC44017CE31_inline (SingleLayer_tA74696727907F9D2CD336CD4C7AA5C5C8C8FB4ED* __this, int32_t ___layerIndex0, const RuntimeMethod* method) ;
// System.Void MicahW.PointGrass.SingleLayer::Set(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SingleLayer_Set_mBBB701BA6BC74136FD35A221A1341CFECEB93BEB (SingleLayer_tA74696727907F9D2CD336CD4C7AA5C5C8C8FB4ED* __this, int32_t ____layerIndex0, const RuntimeMethod* method) ;
// System.Int32 System.Collections.Generic.List`1<MicahW.PointGrass.PointGrassDisplacer>::get_Count()
inline int32_t List_1_get_Count_mB13C1EB08F28C5DEA82F64C1E42E75932F697955_inline (List_1_tDEBBED5B3545B06B3FD16E4ACE9B2C7AB62A1B3E* __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_tDEBBED5B3545B06B3FD16E4ACE9B2C7AB62A1B3E*, const RuntimeMethod*))List_1_get_Count_m4407E4C389F22B8CEC282C15D56516658746C383_gshared_inline)(__this, method);
}
// System.Int32 UnityEngine.Mathf::Min(System.Int32,System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t Mathf_Min_m888083F74FF5655778F0403BB5E9608BEFDEA8CB_inline (int32_t ___a0, int32_t ___b1, const RuntimeMethod* method) ;
// System.Delegate System.Delegate::Combine(System.Delegate,System.Delegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Delegate_t* Delegate_Combine_m1F725AEF318BE6F0426863490691A6F4606E7D00 (Delegate_t* ___a0, Delegate_t* ___b1, const RuntimeMethod* method) ;
// System.Delegate System.Delegate::Remove(System.Delegate,System.Delegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Delegate_t* Delegate_Remove_m8B7DD5661308FA972E23CA1CC3FC9CEB355504E3 (Delegate_t* ___source0, Delegate_t* ___value1, const RuntimeMethod* method) ;
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_Destroy_mE97D0A766419A81296E8D4E5C23D01D3FE91ACBB (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* ___obj0, const RuntimeMethod* method) ;
// System.Void UnityEngine.ComputeBuffer::.ctor(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ComputeBuffer__ctor_mE40DE5EF5ADAC29B6B4DECBD1EE33E8526202617 (ComputeBuffer_t51EADA9015EBCC1B982C5584E9AB2734415A8233* __this, int32_t ___count0, int32_t ___stride1, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<MicahW.PointGrass.PointGrassDisplacer>::.ctor()
inline void List_1__ctor_mF6D991AAA0C3CF45DA3EC80A854889E3DD9268CB (List_1_tDEBBED5B3545B06B3FD16E4ACE9B2C7AB62A1B3E* __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_tDEBBED5B3545B06B3FD16E4ACE9B2C7AB62A1B3E*, const RuntimeMethod*))List_1__ctor_m7F078BB342729BDF11327FD89D7872265328F690_gshared)(__this, method);
}
// System.Void MicahW.PointGrass.PointGrassDisplacementManager/DisplacementDelegate::Invoke(MicahW.PointGrass.PointGrassDisplacementManager)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void DisplacementDelegate_Invoke_m5D544DE149EA8535E783D20BD994198770497B8D_inline (DisplacementDelegate_t4C836C2D37D9FABEC9514A574F9425C4B15B981A* __this, PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900* ___manager0, const RuntimeMethod* method) ;
// System.Void UnityEngine.ComputeBuffer::Release()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ComputeBuffer_Release_mF1F157C929A0A5B2FDCD703A286EE09723450B72 (ComputeBuffer_t51EADA9015EBCC1B982C5584E9AB2734415A8233* __this, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<MicahW.PointGrass.PointGrassDisplacer>::Clear()
inline void List_1_Clear_mA21FF0362F71CC4E90FB3BC2BD3AA7B955DE93FC_inline (List_1_tDEBBED5B3545B06B3FD16E4ACE9B2C7AB62A1B3E* __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_tDEBBED5B3545B06B3FD16E4ACE9B2C7AB62A1B3E*, const RuntimeMethod*))List_1_Clear_m16C1F2C61FED5955F10EB36BC1CB2DF34B128994_gshared_inline)(__this, method);
}
// System.Void MicahW.PointGrass.PointGrassDisplacementManager::UpdateBuffer()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointGrassDisplacementManager_UpdateBuffer_m852C3B19D6D618139E8B3913E875A1DA1A48996F (PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900* __this, const RuntimeMethod* method) ;
// System.Void System.Func`2<MicahW.PointGrass.PointGrassDisplacer,MicahW.PointGrass.PointGrassCommon/ObjectData>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_mF8D09AC6B6F3894CB2E7089BC68605790564651B (Func_2_t868371C41B8A357BB81D47C6157D60859AD0411F* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_t868371C41B8A357BB81D47C6157D60859AD0411F*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_m1DE76DEA42858761846B8A55B60FBEBF0EB931DB_gshared)(__this, ___object0, ___method1, method);
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Select<MicahW.PointGrass.PointGrassDisplacer,MicahW.PointGrass.PointGrassCommon/ObjectData>(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TResult>)
inline RuntimeObject* Enumerable_Select_TisPointGrassDisplacer_tB9B22DA401FDA294990903A742F3ACA3351A82FF_TisObjectData_t24D94D36E8D49DEB90BBFEE7BA833C910F0AF6C4_m8A5429699E824BA5B507C2A87B19D7F34D84CC1D (RuntimeObject* ___source0, Func_2_t868371C41B8A357BB81D47C6157D60859AD0411F* ___selector1, const RuntimeMethod* method)
{
	return ((  RuntimeObject* (*) (RuntimeObject*, Func_2_t868371C41B8A357BB81D47C6157D60859AD0411F*, const RuntimeMethod*))Enumerable_Select_TisRuntimeObject_TisObjectData_t24D94D36E8D49DEB90BBFEE7BA833C910F0AF6C4_m30D3D8A59446A496CFE4C9A719F7ABD8C94A8DF1_gshared)(___source0, ___selector1, method);
}
// TSource[] System.Linq.Enumerable::ToArray<MicahW.PointGrass.PointGrassCommon/ObjectData>(System.Collections.Generic.IEnumerable`1<TSource>)
inline ObjectDataU5BU5D_tFCDCD60E1D45526EC9B864EF342ACB7834D38930* Enumerable_ToArray_TisObjectData_t24D94D36E8D49DEB90BBFEE7BA833C910F0AF6C4_mBA811FFD832E9653BA91207907FC396E52CB0DDF (RuntimeObject* ___source0, const RuntimeMethod* method)
{
	return ((  ObjectDataU5BU5D_tFCDCD60E1D45526EC9B864EF342ACB7834D38930* (*) (RuntimeObject*, const RuntimeMethod*))Enumerable_ToArray_TisObjectData_t24D94D36E8D49DEB90BBFEE7BA833C910F0AF6C4_mBA811FFD832E9653BA91207907FC396E52CB0DDF_gshared)(___source0, method);
}
// System.Int32 MicahW.PointGrass.PointGrassDisplacementManager::get_DisplacerCount()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t PointGrassDisplacementManager_get_DisplacerCount_m0526FE46A67C96DC0238F4E9281DB7DAEFF83E66 (PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900* __this, const RuntimeMethod* method) ;
// System.Void UnityEngine.ComputeBuffer::SetData(System.Array,System.Int32,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ComputeBuffer_SetData_m5D75D81304937FD04C820B1015AFA066DF36DC0E (ComputeBuffer_t51EADA9015EBCC1B982C5584E9AB2734415A8233* __this, RuntimeArray* ___data0, int32_t ___managedBufferStartIndex1, int32_t ___computeBufferStartIndex2, int32_t ___count3, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<MicahW.PointGrass.PointGrassDisplacer>::Add(T)
inline void List_1_Add_mB3A89627A69CB0296DA00911471E9FD7DDDD58F5_inline (List_1_tDEBBED5B3545B06B3FD16E4ACE9B2C7AB62A1B3E* __this, PointGrassDisplacer_tB9B22DA401FDA294990903A742F3ACA3351A82FF* ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_tDEBBED5B3545B06B3FD16E4ACE9B2C7AB62A1B3E*, PointGrassDisplacer_tB9B22DA401FDA294990903A742F3ACA3351A82FF*, const RuntimeMethod*))List_1_Add_mEBCF994CC3814631017F46A387B1A192ED6C85C7_gshared_inline)(__this, ___item0, method);
}
// System.Boolean System.Collections.Generic.List`1<MicahW.PointGrass.PointGrassDisplacer>::Remove(T)
inline bool List_1_Remove_m5C0188F84A17A37FED29306A626DF5015F87088D (List_1_tDEBBED5B3545B06B3FD16E4ACE9B2C7AB62A1B3E* __this, PointGrassDisplacer_tB9B22DA401FDA294990903A742F3ACA3351A82FF* ___item0, const RuntimeMethod* method)
{
	return ((  bool (*) (List_1_tDEBBED5B3545B06B3FD16E4ACE9B2C7AB62A1B3E*, PointGrassDisplacer_tB9B22DA401FDA294990903A742F3ACA3351A82FF*, const RuntimeMethod*))List_1_Remove_m4DFA48F4CEB9169601E75FC28517C5C06EFA5AD7_gshared)(__this, ___item0, method);
}
// System.Void UnityEngine.MaterialPropertyBlock::SetBuffer(System.Int32,UnityEngine.ComputeBuffer)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MaterialPropertyBlock_SetBuffer_m9FF172AF2C41F7CBCC50F4AC4C6109B15289BF73 (MaterialPropertyBlock_t2308669579033A857EFE6E4831909F638B27411D* __this, int32_t ___nameID0, ComputeBuffer_t51EADA9015EBCC1B982C5584E9AB2734415A8233* ___value1, const RuntimeMethod* method) ;
// System.Void UnityEngine.MaterialPropertyBlock::SetInt(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MaterialPropertyBlock_SetInt_mB07015BD8FF1A7D2FF70D7FB89C0FDFAE06B86F8 (MaterialPropertyBlock_t2308669579033A857EFE6E4831909F638B27411D* __this, int32_t ___nameID0, int32_t ___value1, const RuntimeMethod* method) ;
// System.Void MicahW.PointGrass.PointGrassDisplacementManager/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m81B88999DD9498F0E72DF82C81657C2AEB967FE3 (U3CU3Ec_tC71A09B989D2FDC7808DBE99B3025974C73DE7C5* __this, const RuntimeMethod* method) ;
// MicahW.PointGrass.PointGrassCommon/ObjectData MicahW.PointGrass.PointGrassDisplacer::GetObjectData()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ObjectData_t24D94D36E8D49DEB90BBFEE7BA833C910F0AF6C4 PointGrassDisplacer_GetObjectData_m910E224560C96B24B45954527158E492A9CF4859 (PointGrassDisplacer_tB9B22DA401FDA294990903A742F3ACA3351A82FF* __this, const RuntimeMethod* method) ;
// System.Void MicahW.PointGrass.PointGrassDisplacementManager::AddDisplacer(MicahW.PointGrass.PointGrassDisplacer)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointGrassDisplacementManager_AddDisplacer_m34B41A7480D16398EC5BF58ECFB2A73F92EFEE6F (PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900* __this, PointGrassDisplacer_tB9B22DA401FDA294990903A742F3ACA3351A82FF* ___displacer0, const RuntimeMethod* method) ;
// System.Void MicahW.PointGrass.PointGrassDisplacementManager/DisplacementDelegate::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DisplacementDelegate__ctor_m5FC18C77D43CFAAC46030EE3782AB012C1852A2B (DisplacementDelegate_t4C836C2D37D9FABEC9514A574F9425C4B15B981A* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void MicahW.PointGrass.PointGrassDisplacementManager::add_OnInitialize(MicahW.PointGrass.PointGrassDisplacementManager/DisplacementDelegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointGrassDisplacementManager_add_OnInitialize_mD1CDC92403A311CF3BBA4C8913EF1BE50C48DD54 (DisplacementDelegate_t4C836C2D37D9FABEC9514A574F9425C4B15B981A* ___value0, const RuntimeMethod* method) ;
// System.Void MicahW.PointGrass.PointGrassDisplacementManager::RemoveDisplacer(MicahW.PointGrass.PointGrassDisplacer)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointGrassDisplacementManager_RemoveDisplacer_m610EACCC6E76F1C21D580178793521035C34B32A (PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900* __this, PointGrassDisplacer_tB9B22DA401FDA294990903A742F3ACA3351A82FF* ___displacer0, const RuntimeMethod* method) ;
// System.Void MicahW.PointGrass.PointGrassDisplacementManager::remove_OnInitialize(MicahW.PointGrass.PointGrassDisplacementManager/DisplacementDelegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointGrassDisplacementManager_remove_OnInitialize_m5F94865C071988D7AF7EF522E3ABFAFFABF7E6B0 (DisplacementDelegate_t4C836C2D37D9FABEC9514A574F9425C4B15B981A* ___value0, const RuntimeMethod* method) ;
// System.Void MicahW.PointGrass.PointGrassWind::GetShaderIDs()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointGrassWind_GetShaderIDs_mF3766A5521D8AFCA5B72DD11BA5EA10B02FCC8C9 (const RuntimeMethod* method) ;
// System.Void MicahW.PointGrass.PointGrassWind::RefreshValues()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointGrassWind_RefreshValues_m9AF23283B96B07EF4B748964644941DD789CB8EA (PointGrassWind_t2002E1D6C3245367514FF9A0270205781DEB2C67* __this, const RuntimeMethod* method) ;
// MicahW.PointGrass.PointGrassWind/PackedProperties MicahW.PointGrass.PointGrassWind::PackProperties()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR PackedProperties_tAE6E15A41456BA8070C1C0B4F6D3DA9E28624A32 PointGrassWind_PackProperties_mFEB1D1032F4CBADF707C35634FA8BE9169FF526B (PointGrassWind_t2002E1D6C3245367514FF9A0270205781DEB2C67* __this, const RuntimeMethod* method) ;
// System.Void UnityEngine.Shader::SetGlobalVector(System.Int32,UnityEngine.Vector4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Shader_SetGlobalVector_mDC5F45B008D44A2C8BF6D450CFE8B58B847C8190 (int32_t ___nameID0, Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___value1, const RuntimeMethod* method) ;
// System.Void UnityEngine.Shader::SetGlobalFloat(System.Int32,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Shader_SetGlobalFloat_mE7D0DA2B0A62925E093B318785AF82A173794AFC (int32_t ___nameID0, float ___value1, const RuntimeMethod* method) ;
// System.Void MicahW.PointGrass.PointGrassWind/PackedProperties::.ctor(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector2,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PackedProperties__ctor_m1ADC969557DD708678FAFC6C730181CDAFEAC8A2 (PackedProperties_tAE6E15A41456BA8070C1C0B4F6D3DA9E28624A32* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___windDirection0, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___windScroll1, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___noiseRange2, float ___windScale3, const RuntimeMethod* method) ;
// UnityEngine.Vector4 UnityEngine.Vector4::op_Implicit(UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 Vector4_op_Implicit_m2ECA73F345A7AD84144133E9E51657204002B12D_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___v0, const RuntimeMethod* method) ;
// System.Void MicahW.PointGrass.PointGrassRenderer::ClearBuffers()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointGrassRenderer_ClearBuffers_mE07D764621EE41CB4ED709B578227440F728B60A (PointGrassRenderer_tCAB9C932B5AEF5960C04E43451965E31C6A471C2* __this, const RuntimeMethod* method) ;
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3* __this, const RuntimeMethod* method) ;
// System.Int32 UnityEngine.GameObject::get_layer()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t GameObject_get_layer_m108902B9C89E9F837CE06B9942AA42307450FEAF (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* __this, const RuntimeMethod* method) ;
// UnityEngine.LayerMask UnityEngine.LayerMask::op_Implicit(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR LayerMask_t97CB6BDADEDC3D6423C7BCFEA7F86DA2EC6241DB LayerMask_op_Implicit_m01C8996A2CB2085328B9C33539C43139660D8222 (int32_t ___intVal0, const RuntimeMethod* method) ;
// System.Boolean MicahW.PointGrass.PointGrassRenderer::CompatibilityCheck()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PointGrassRenderer_CompatibilityCheck_mF24B3CB5752F3B46635B710E9399DC0169EDC298 (PointGrassRenderer_tCAB9C932B5AEF5960C04E43451965E31C6A471C2* __this, const RuntimeMethod* method) ;
// System.Void MicahW.PointGrass.PointGrassCommon::FindPropertyIDs()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointGrassCommon_FindPropertyIDs_m4B83779C3F568CC8BFEEFEC4FB0A50B525997711 (const RuntimeMethod* method) ;
// System.Void MicahW.PointGrass.PointGrassCommon::GenerateGrassMeshes()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointGrassCommon_GenerateGrassMeshes_m1187A0027530C7B935B788DF2975503346224C28 (const RuntimeMethod* method) ;
// System.Void MicahW.PointGrass.PointGrassRenderer::BuildPoints()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointGrassRenderer_BuildPoints_m22ADAC358BA12DDC83B6F4E9AAB9317E4A53FA13 (PointGrassRenderer_tCAB9C932B5AEF5960C04E43451965E31C6A471C2* __this, const RuntimeMethod* method) ;
// System.Void MicahW.PointGrass.PointGrassRenderer::DrawGrass()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointGrassRenderer_DrawGrass_m3930D3BDAD2C3E7C6EB7DB2B1206DE93B1095288 (PointGrassRenderer_tCAB9C932B5AEF5960C04E43451965E31C6A471C2* __this, const RuntimeMethod* method) ;
// System.Boolean UnityEngine.SystemInfo::get_supportsInstancing()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool SystemInfo_get_supportsInstancing_m8EF067060BFA3D50A266342D26A392747DA4FF3E (const RuntimeMethod* method) ;
// System.String UnityEngine.Object::get_name()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Object_get_name_mAC2F6B897CF1303BA4249B4CB55271AFACBB6392 (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* __this, const RuntimeMethod* method) ;
// System.String System.String::Concat(System.String,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_m8855A6DE10F84DA7F4EC113CADDB59873A25573B (String_t* ___str00, String_t* ___str11, String_t* ___str22, const RuntimeMethod* method) ;
// System.Void MicahW.PointGrass.PointGrassRenderer::<CompatibilityCheck>g__Disable|43_0()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointGrassRenderer_U3CCompatibilityCheckU3Eg__DisableU7C43_0_mAD8E4C2F34F3754FDA2660945EF8B86F40B9AF3C (PointGrassRenderer_tCAB9C932B5AEF5960C04E43451965E31C6A471C2* __this, const RuntimeMethod* method) ;
// System.Int32 UnityEngine.SystemInfo::get_graphicsShaderLevel()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t SystemInfo_get_graphicsShaderLevel_m9E6B001FA80EFBFC92EF4E7440AE64828B15070F (const RuntimeMethod* method) ;
// System.Int32 UnityEngine.Random::Range(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Random_Range_m6763D9767F033357F88B6637F048F4ACA4123B68 (int32_t ___minInclusive0, int32_t ___maxExclusive1, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Vector3::get_normalized()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_get_normalized_m736BBF65D5CDA7A18414370D15B4DFCC1E466F07_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* __this, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Transform::InverseTransformDirection(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Transform_InverseTransformDirection_m69C077B881A98B08C7F231EFC49429C906FBC575 (Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___direction0, const RuntimeMethod* method) ;
// System.Void System.Nullable`1<UnityEngine.Vector3>::.ctor(T)
inline void Nullable_1__ctor_m75F3ABB694E26670F021136BD3B9E71A65948BC2 (Nullable_1_t9C51B084784B716FFF4ED4575C63CFD8A71A86FE* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___value0, const RuntimeMethod* method)
{
	((  void (*) (Nullable_1_t9C51B084784B716FFF4ED4575C63CFD8A71A86FE*, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2, const RuntimeMethod*))Nullable_1__ctor_m75F3ABB694E26670F021136BD3B9E71A65948BC2_gshared)(__this, ___value0, method);
}
// System.Int32 UnityEngine.Vector2Int::get_x()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t Vector2Int_get_x_mA2CACB1B6E6B5AD0CCC32B2CD2EDCE3ECEB50576_inline (Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A* __this, const RuntimeMethod* method) ;
// System.Int32 UnityEngine.Vector2Int::get_y()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t Vector2Int_get_y_m48454163ECF0B463FB5A16A0C4FC4B14DB0768B3_inline (Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A* __this, const RuntimeMethod* method) ;
// System.Void MicahW.PointGrass.PointGrassRenderer::BuildPoints_Terrain(System.Int32,System.Nullable`1<UnityEngine.Vector3>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointGrassRenderer_BuildPoints_Terrain_mE2B10018E53754D361B63EDF70C17C8A1C706CE2 (PointGrassRenderer_tCAB9C932B5AEF5960C04E43451965E31C6A471C2* __this, int32_t ___seed0, Nullable_1_t9C51B084784B716FFF4ED4575C63CFD8A71A86FE ___overwriteNormal1, const RuntimeMethod* method) ;
// System.Void MicahW.PointGrass.PointGrassRenderer::BuildPoints_Mesh(System.Int32,System.Nullable`1<UnityEngine.Vector3>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointGrassRenderer_BuildPoints_Mesh_m68283F3A64997B2E73127B91105D2501F74225BB (PointGrassRenderer_tCAB9C932B5AEF5960C04E43451965E31C6A471C2* __this, int32_t ___seed0, Nullable_1_t9C51B084784B716FFF4ED4575C63CFD8A71A86FE ___overwriteNormal1, const RuntimeMethod* method) ;
// System.Boolean MicahW.PointGrass.PointGrassRenderer::GetMeshData(MicahW.PointGrass.PointGrassCommon/MeshData&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PointGrassRenderer_GetMeshData_mCF6800F541A60360ACDF687BB51CA1DD1B7B173D (PointGrassRenderer_tCAB9C932B5AEF5960C04E43451965E31C6A471C2* __this, MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F** ___meshData0, const RuntimeMethod* method) ;
// System.Void MicahW.PointGrass.PointGrassCommon::ProjectBaseMesh(MicahW.PointGrass.PointGrassCommon/MeshData&,UnityEngine.LayerMask,UnityEngine.Transform)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointGrassCommon_ProjectBaseMesh_m5562159B69B1FD110DEC077043E7DE5D4020F74B (MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F** ___mesh0, LayerMask_t97CB6BDADEDC3D6423C7BCFEA7F86DA2EC6241DB ___mask1, Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___transform2, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Transform::get_localScale()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Transform_get_localScale_m804A002A53A645CDFCD15BB0F37209162720363F (Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* __this, const RuntimeMethod* method) ;
// MicahW.PointGrass.PointGrassCommon/MeshPoint[] MicahW.PointGrass.DistributePointsAlongMesh::DistributePoints(MicahW.PointGrass.PointGrassCommon/MeshData,UnityEngine.Vector3,System.Single,System.Int32,System.Boolean,System.Nullable`1<UnityEngine.Vector3>,System.Boolean,System.Boolean,System.Boolean,System.Single,UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR MeshPointU5BU5D_tBCEA150F23C3861E840A87B994115F06D7490DD8* DistributePointsAlongMesh_DistributePoints_m697A346959B6378FF0BF569C2DD2C8189E81AAF4 (MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F* ___mesh0, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___scale1, float ___pointCount2, int32_t ___seed3, bool ___multiplyPointsByArea4, Nullable_1_t9C51B084784B716FFF4ED4575C63CFD8A71A86FE ___forcedNormal5, bool ___useColours6, bool ___useDensity7, bool ___useLength8, float ___densityCutoff9, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___lengthMapping10, const RuntimeMethod* method) ;
// System.Void MicahW.PointGrass.PointGrassRenderer::CreateBuffers(MicahW.PointGrass.PointGrassCommon/MeshPoint[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointGrassRenderer_CreateBuffers_mFC53982D8596D1E77FE4711A0D9FEC742A925FAB (PointGrassRenderer_tCAB9C932B5AEF5960C04E43451965E31C6A471C2* __this, MeshPointU5BU5D_tBCEA150F23C3861E840A87B994115F06D7490DD8* ___points0, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<UnityEngine.ComputeBuffer>::.ctor()
inline void List_1__ctor_m776C5CB754FECF0324C22F3651FA444D287BFBDC (List_1_t7DA451437393B82A39CEE1076115D7FD8B970B3B* __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t7DA451437393B82A39CEE1076115D7FD8B970B3B*, const RuntimeMethod*))List_1__ctor_m7F078BB342729BDF11327FD89D7872265328F690_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<UnityEngine.MaterialPropertyBlock>::.ctor()
inline void List_1__ctor_m9FDBD09FEFDB8C3F98E74C51E846806B1523EE85 (List_1_tAFCDB9CCBAF11013AA7B0EC7BB51B10127AB467C* __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_tAFCDB9CCBAF11013AA7B0EC7BB51B10127AB467C*, const RuntimeMethod*))List_1__ctor_m7F078BB342729BDF11327FD89D7872265328F690_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<UnityEngine.Bounds>::.ctor()
inline void List_1__ctor_m6495AB7C6BDDC4D06EE93E691A9DC2C6FC791911 (List_1_t7D73463EA17F4D29A37E8110AB60DAD5C80EBF65* __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t7D73463EA17F4D29A37E8110AB60DAD5C80EBF65*, const RuntimeMethod*))List_1__ctor_m6495AB7C6BDDC4D06EE93E691A9DC2C6FC791911_gshared)(__this, method);
}
// System.Void MicahW.PointGrass.PointGrassCommon::CacheTerrainData(UnityEngine.TerrainData,UnityEngine.TerrainLayer[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointGrassCommon_CacheTerrainData_mE54FEFBA4451B4B69F63A2617DA43149225A3EAE (TerrainData_t615A68EAC648066681875D47FC641496D12F2E24* ___terrain0, TerrainLayerU5BU5D_t259E391D6115F121FCD284E79F62012D70956EB0* ___grassLayers1, const RuntimeMethod* method) ;
// System.Void UnityEngine.Vector2Int::.ctor(System.Int32,System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector2Int__ctor_mC20D1312133EB8CB63EC11067088B043660F11CE_inline (Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A* __this, int32_t ___x0, int32_t ___y1, const RuntimeMethod* method) ;
// System.Boolean MicahW.PointGrass.PointGrassRenderer::GetTerrainMeshData(MicahW.PointGrass.PointGrassCommon/MeshData&,UnityEngine.Vector2Int)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PointGrassRenderer_GetTerrainMeshData_mE1FA1BE05C1FCB35601AD79D3C1B0219ECA2BEF8 (PointGrassRenderer_tCAB9C932B5AEF5960C04E43451965E31C6A471C2* __this, MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F** ___meshData0, Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A ___chunkCoord1, const RuntimeMethod* method) ;
// System.Boolean MicahW.PointGrass.PointGrassRenderer::get_UsingMultipleMeshes()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PointGrassRenderer_get_UsingMultipleMeshes_m61A92AF3BC219534CD301983C34CECB07E6A3E59 (PointGrassRenderer_tCAB9C932B5AEF5960C04E43451965E31C6A471C2* __this, const RuntimeMethod* method) ;
// System.Void MicahW.PointGrass.PointGrassRenderer::CreateBuffersFromPoints_Multi(MicahW.PointGrass.PointGrassCommon/MeshPoint[],UnityEngine.ComputeBuffer[]&,UnityEngine.MaterialPropertyBlock[]&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointGrassRenderer_CreateBuffersFromPoints_Multi_m0FB6D3F6742650ACA37DF48E03DE221ABC7EF599 (PointGrassRenderer_tCAB9C932B5AEF5960C04E43451965E31C6A471C2* __this, MeshPointU5BU5D_tBCEA150F23C3861E840A87B994115F06D7490DD8* ___points0, ComputeBufferU5BU5D_t7832804740B13E96807A836AD90ADF1477D7FE27** ___buffers1, MaterialPropertyBlockU5BU5D_t6911DDC471FC23F2AF58FF0140ED595529EC5F5B** ___blocks2, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<UnityEngine.ComputeBuffer>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
inline void List_1_AddRange_m17F664FC05537795FFF2C58E6DF293488A469EA4 (List_1_t7DA451437393B82A39CEE1076115D7FD8B970B3B* __this, RuntimeObject* ___collection0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t7DA451437393B82A39CEE1076115D7FD8B970B3B*, RuntimeObject*, const RuntimeMethod*))List_1_AddRange_m1F76B300133150E6046C5FED00E88B5DE0A02E17_gshared)(__this, ___collection0, method);
}
// System.Void System.Collections.Generic.List`1<UnityEngine.MaterialPropertyBlock>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
inline void List_1_AddRange_m77FEC24D7D0DA5225D0ECC76F11E793A228F30CD (List_1_tAFCDB9CCBAF11013AA7B0EC7BB51B10127AB467C* __this, RuntimeObject* ___collection0, const RuntimeMethod* method)
{
	((  void (*) (List_1_tAFCDB9CCBAF11013AA7B0EC7BB51B10127AB467C*, RuntimeObject*, const RuntimeMethod*))List_1_AddRange_m1F76B300133150E6046C5FED00E88B5DE0A02E17_gshared)(__this, ___collection0, method);
}
// System.Void MicahW.PointGrass.PointGrassRenderer::CreateBufferFromPoints(MicahW.PointGrass.PointGrassCommon/MeshPoint[],UnityEngine.ComputeBuffer&,UnityEngine.MaterialPropertyBlock&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointGrassRenderer_CreateBufferFromPoints_mFC5340A5C557D32EAEDECCC71E3D48114E5DCF07 (PointGrassRenderer_tCAB9C932B5AEF5960C04E43451965E31C6A471C2* __this, MeshPointU5BU5D_tBCEA150F23C3861E840A87B994115F06D7490DD8* ___points0, ComputeBuffer_t51EADA9015EBCC1B982C5584E9AB2734415A8233** ___buffer1, MaterialPropertyBlock_t2308669579033A857EFE6E4831909F638B27411D** ___block2, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<UnityEngine.ComputeBuffer>::Add(T)
inline void List_1_Add_m28265F4F8593F134812477A243BBEF0513421160_inline (List_1_t7DA451437393B82A39CEE1076115D7FD8B970B3B* __this, ComputeBuffer_t51EADA9015EBCC1B982C5584E9AB2734415A8233* ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t7DA451437393B82A39CEE1076115D7FD8B970B3B*, ComputeBuffer_t51EADA9015EBCC1B982C5584E9AB2734415A8233*, const RuntimeMethod*))List_1_Add_mEBCF994CC3814631017F46A387B1A192ED6C85C7_gshared_inline)(__this, ___item0, method);
}
// System.Void System.Collections.Generic.List`1<UnityEngine.MaterialPropertyBlock>::Add(T)
inline void List_1_Add_m03B29C69F9974D985EFCE1B1D91AC7AC4F578723_inline (List_1_tAFCDB9CCBAF11013AA7B0EC7BB51B10127AB467C* __this, MaterialPropertyBlock_t2308669579033A857EFE6E4831909F638B27411D* ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_tAFCDB9CCBAF11013AA7B0EC7BB51B10127AB467C*, MaterialPropertyBlock_t2308669579033A857EFE6E4831909F638B27411D*, const RuntimeMethod*))List_1_Add_mEBCF994CC3814631017F46A387B1A192ED6C85C7_gshared_inline)(__this, ___item0, method);
}
// System.Void System.Collections.Generic.List`1<UnityEngine.Bounds>::Add(T)
inline void List_1_Add_m930CA3F1A016B820967E8D53B5D8B2EEB9F10DAC_inline (List_1_t7D73463EA17F4D29A37E8110AB60DAD5C80EBF65* __this, Bounds_t367E830C64BBF235ED8C3B2F8CF6254FDCAD39C3 ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t7D73463EA17F4D29A37E8110AB60DAD5C80EBF65*, Bounds_t367E830C64BBF235ED8C3B2F8CF6254FDCAD39C3, const RuntimeMethod*))List_1_Add_m930CA3F1A016B820967E8D53B5D8B2EEB9F10DAC_gshared_inline)(__this, ___item0, method);
}
// System.Int32 System.Collections.Generic.List`1<UnityEngine.ComputeBuffer>::get_Count()
inline int32_t List_1_get_Count_m7CF0642F7F16E3F98C217C5CDEAB8C0554FEF85E_inline (List_1_t7DA451437393B82A39CEE1076115D7FD8B970B3B* __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t7DA451437393B82A39CEE1076115D7FD8B970B3B*, const RuntimeMethod*))List_1_get_Count_m4407E4C389F22B8CEC282C15D56516658746C383_gshared_inline)(__this, method);
}
// T[] System.Collections.Generic.List`1<UnityEngine.ComputeBuffer>::ToArray()
inline ComputeBufferU5BU5D_t7832804740B13E96807A836AD90ADF1477D7FE27* List_1_ToArray_mACDC8BDC8BB93E18220068239BC4D9F8065DAF41 (List_1_t7DA451437393B82A39CEE1076115D7FD8B970B3B* __this, const RuntimeMethod* method)
{
	return ((  ComputeBufferU5BU5D_t7832804740B13E96807A836AD90ADF1477D7FE27* (*) (List_1_t7DA451437393B82A39CEE1076115D7FD8B970B3B*, const RuntimeMethod*))List_1_ToArray_mD7E4F8E7C11C3C67CB5739FCC0A6E86106A6291F_gshared)(__this, method);
}
// T[] System.Collections.Generic.List`1<UnityEngine.MaterialPropertyBlock>::ToArray()
inline MaterialPropertyBlockU5BU5D_t6911DDC471FC23F2AF58FF0140ED595529EC5F5B* List_1_ToArray_mAD5B129F08D52D3E82AE1915C038DBD06D0AADF3 (List_1_tAFCDB9CCBAF11013AA7B0EC7BB51B10127AB467C* __this, const RuntimeMethod* method)
{
	return ((  MaterialPropertyBlockU5BU5D_t6911DDC471FC23F2AF58FF0140ED595529EC5F5B* (*) (List_1_tAFCDB9CCBAF11013AA7B0EC7BB51B10127AB467C*, const RuntimeMethod*))List_1_ToArray_mD7E4F8E7C11C3C67CB5739FCC0A6E86106A6291F_gshared)(__this, method);
}
// T[] System.Collections.Generic.List`1<UnityEngine.Bounds>::ToArray()
inline BoundsU5BU5D_t45563ED55B62FA0536E8117454C56C0CFA5B25F5* List_1_ToArray_m18ACE74090BCEB952354342ADF397C950A01321A (List_1_t7D73463EA17F4D29A37E8110AB60DAD5C80EBF65* __this, const RuntimeMethod* method)
{
	return ((  BoundsU5BU5D_t45563ED55B62FA0536E8117454C56C0CFA5B25F5* (*) (List_1_t7D73463EA17F4D29A37E8110AB60DAD5C80EBF65*, const RuntimeMethod*))List_1_ToArray_m18ACE74090BCEB952354342ADF397C950A01321A_gshared)(__this, method);
}
// T UnityEngine.Component::GetComponent<UnityEngine.MeshFilter>()
inline MeshFilter_t6D1CE2473A1E45AC73013400585A1163BF66B2F5* Component_GetComponent_TisMeshFilter_t6D1CE2473A1E45AC73013400585A1163BF66B2F5_mB82F66059DFB5715DD85BDED1D90BC03A6C9E623 (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3* __this, const RuntimeMethod* method)
{
	return ((  MeshFilter_t6D1CE2473A1E45AC73013400585A1163BF66B2F5* (*) (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3*, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m7181F81CAEC2CF53F5D2BC79B7425C16E1F80D33_gshared)(__this, method);
}
// UnityEngine.Vector2Int UnityEngine.Vector2Int::get_zero()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A Vector2Int_get_zero_mF92C338E9CB9434105090E675E04D20A29649553_inline (const RuntimeMethod* method) ;
// MicahW.PointGrass.PointGrassCommon/MeshData MicahW.PointGrass.PointGrassCommon::CreateMeshFromFilters(UnityEngine.Transform,UnityEngine.MeshFilter[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F* PointGrassCommon_CreateMeshFromFilters_mD660AD50FC9DD73F259A0D447B2C689EF1A437B3 (Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___parent0, MeshFilterU5BU5D_tCE3B457E6F7ECE5ECEE9E09150642150448685BA* ___filters1, const RuntimeMethod* method) ;
// System.Int32 UnityEngine.Mathf::CeilToInt(System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t Mathf_CeilToInt_mF2BF9F4261B3431DC20E10A46CFEEED103C48963_inline (float ___f0, const RuntimeMethod* method) ;
// MicahW.PointGrass.PointGrassCommon/MeshData MicahW.PointGrass.PointGrassCommon::CreateMeshFromTerrainData(UnityEngine.TerrainData,System.Single,System.Int32,System.Int32,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F* PointGrassCommon_CreateMeshFromTerrainData_mA5C0F6BD781BEB33C68AF5BF697E1FAE23002E55 (TerrainData_t615A68EAC648066681875D47FC641496D12F2E24* ___terrain0, float ___densityCutoff1, int32_t ___startX2, int32_t ___startY3, int32_t ___sizeX4, int32_t ___sizeY5, const RuntimeMethod* method) ;
// System.Void UnityEngine.ComputeBuffer::SetData(System.Array)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ComputeBuffer_SetData_m9F845E6B347CE028FA9A987D740FC642D828013A (ComputeBuffer_t51EADA9015EBCC1B982C5584E9AB2734415A8233* __this, RuntimeArray* ___data0, const RuntimeMethod* method) ;
// UnityEngine.MaterialPropertyBlock MicahW.PointGrass.PointGrassRenderer::CreateMaterialPropertyBlock(UnityEngine.ComputeBuffer)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR MaterialPropertyBlock_t2308669579033A857EFE6E4831909F638B27411D* PointGrassRenderer_CreateMaterialPropertyBlock_m0FC42DFE135544DE358968620AEAD8A957FC46B0 (PointGrassRenderer_tCAB9C932B5AEF5960C04E43451965E31C6A471C2* __this, ComputeBuffer_t51EADA9015EBCC1B982C5584E9AB2734415A8233* ___pointBuffer0, const RuntimeMethod* method) ;
// System.Int32 UnityEngine.Mathf::RoundToInt(System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t Mathf_RoundToInt_m60F8B66CF27F1FA75AA219342BD184B75771EB4B_inline (float ___f0, const RuntimeMethod* method) ;
// UnityEngine.Mesh MicahW.PointGrass.PointGrassRenderer::GetGrassMesh()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* PointGrassRenderer_GetGrassMesh_mE60BB12C500D3FF1E47167072BC6943666771124 (PointGrassRenderer_tCAB9C932B5AEF5960C04E43451965E31C6A471C2* __this, const RuntimeMethod* method) ;
// UnityEngine.Bounds MicahW.PointGrass.PointGrassRenderer::GetLocalBounds()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Bounds_t367E830C64BBF235ED8C3B2F8CF6254FDCAD39C3 PointGrassRenderer_GetLocalBounds_mD32C528489677A59FBD7360789D895D7361C5E4D (PointGrassRenderer_tCAB9C932B5AEF5960C04E43451965E31C6A471C2* __this, const RuntimeMethod* method) ;
// UnityEngine.Bounds MicahW.PointGrass.PointGrassRenderer::TransformBounds(UnityEngine.Bounds)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Bounds_t367E830C64BBF235ED8C3B2F8CF6254FDCAD39C3 PointGrassRenderer_TransformBounds_m4DDAF13C40FA22C3FD9D439ECE335DF21345A5A1 (PointGrassRenderer_tCAB9C932B5AEF5960C04E43451965E31C6A471C2* __this, Bounds_t367E830C64BBF235ED8C3B2F8CF6254FDCAD39C3 ___localBounds0, const RuntimeMethod* method) ;
// System.Void MicahW.PointGrass.PointGrassRenderer::DrawGrassBuffer(UnityEngine.ComputeBuffer,UnityEngine.MaterialPropertyBlock&,UnityEngine.Mesh,UnityEngine.Material,UnityEngine.Bounds)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointGrassRenderer_DrawGrassBuffer_m97B71865FE29413A6BA689202968F9B24C097B88 (PointGrassRenderer_tCAB9C932B5AEF5960C04E43451965E31C6A471C2* __this, ComputeBuffer_t51EADA9015EBCC1B982C5584E9AB2734415A8233* ___buffer0, MaterialPropertyBlock_t2308669579033A857EFE6E4831909F638B27411D** ___block1, Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* ___mesh2, Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___mat3, Bounds_t367E830C64BBF235ED8C3B2F8CF6254FDCAD39C3 ___bounds4, const RuntimeMethod* method) ;
// System.Boolean UnityEngine.ComputeBuffer::IsValid()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ComputeBuffer_IsValid_mC64E2E1D72BA70F04D177F82C3E258494B929B5D (ComputeBuffer_t51EADA9015EBCC1B982C5584E9AB2734415A8233* __this, const RuntimeMethod* method) ;
// System.Int32 UnityEngine.ComputeBuffer::get_count()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ComputeBuffer_get_count_m4DAA2D2714BA7A46F007697F601E4446F1049506 (ComputeBuffer_t51EADA9015EBCC1B982C5584E9AB2734415A8233* __this, const RuntimeMethod* method) ;
// UnityEngine.Bounds MicahW.PointGrass.PointGrassRenderer::AddBoundsExtrusion(UnityEngine.Bounds)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Bounds_t367E830C64BBF235ED8C3B2F8CF6254FDCAD39C3 PointGrassRenderer_AddBoundsExtrusion_m2D5303CFC7A6FFCC3A0F3DDD2D8AE89AD7D55305 (PointGrassRenderer_tCAB9C932B5AEF5960C04E43451965E31C6A471C2* __this, Bounds_t367E830C64BBF235ED8C3B2F8CF6254FDCAD39C3 ___worldSpaceBounds0, const RuntimeMethod* method) ;
// System.Void MicahW.PointGrass.PointGrassCommon::UpdateMaterialPropertyBlock(UnityEngine.MaterialPropertyBlock&,UnityEngine.Transform)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointGrassCommon_UpdateMaterialPropertyBlock_mC29A761CEB43323C5150BA2BBFDB228ABA7427C6 (MaterialPropertyBlock_t2308669579033A857EFE6E4831909F638B27411D** ___block0, Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___trans1, const RuntimeMethod* method) ;
// System.Void UnityEngine.Graphics::DrawMeshInstancedProcedural(UnityEngine.Mesh,System.Int32,UnityEngine.Material,UnityEngine.Bounds,System.Int32,UnityEngine.MaterialPropertyBlock,UnityEngine.Rendering.ShadowCastingMode,System.Boolean,System.Int32,UnityEngine.Camera,UnityEngine.Rendering.LightProbeUsage,UnityEngine.LightProbeProxyVolume)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Graphics_DrawMeshInstancedProcedural_m177E33D3685441D2943AD08303050E28D85CB3F2 (Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* ___mesh0, int32_t ___submeshIndex1, Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___material2, Bounds_t367E830C64BBF235ED8C3B2F8CF6254FDCAD39C3 ___bounds3, int32_t ___count4, MaterialPropertyBlock_t2308669579033A857EFE6E4831909F638B27411D* ___properties5, int32_t ___castShadows6, bool ___receiveShadows7, int32_t ___layer8, Camera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184* ___camera9, int32_t ___lightProbeUsage10, LightProbeProxyVolume_t431001CA94D2BB5DB419E2A89E7D8116E4E1B658* ___lightProbeProxyVolume11, const RuntimeMethod* method) ;
// System.Void UnityEngine.MaterialPropertyBlock::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MaterialPropertyBlock__ctor_m14C3432585F7BB65028BCD64A0FD6607A1B490FB (MaterialPropertyBlock_t2308669579033A857EFE6E4831909F638B27411D* __this, const RuntimeMethod* method) ;
// System.Void System.ArgumentException::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArgumentException__ctor_m026938A67AF9D36BB7ED27F80425D7194B514465 (ArgumentException_tAD90411542A20A9C72D5CDA3A84181D8B947A263* __this, String_t* ___message0, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Vector3::get_one()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_get_one_mC9B289F1E15C42C597180C9FE6FB492495B51D02_inline (const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Bounds::get_min()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Bounds_get_min_m465AC9BBE1DE5D8E8AD95AC19B9899068FEEBB13 (Bounds_t367E830C64BBF235ED8C3B2F8CF6254FDCAD39C3* __this, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Bounds::get_max()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Bounds_get_max_m6446F2AB97C1E57CA89467B9DE52D4EB61F1CB09 (Bounds_t367E830C64BBF235ED8C3B2F8CF6254FDCAD39C3* __this, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Bounds::get_center()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Bounds_get_center_m5B05F81CB835EB6DD8628FDA24B638F477984DC3 (Bounds_t367E830C64BBF235ED8C3B2F8CF6254FDCAD39C3* __this, const RuntimeMethod* method) ;
// System.Void UnityEngine.Bounds::set_center(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Bounds_set_center_m891869DD5B1BEEE2D17907BBFB7EB79AAE44884B (Bounds_t367E830C64BBF235ED8C3B2F8CF6254FDCAD39C3* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___value0, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Bounds::get_size()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Bounds_get_size_m0699A53A55A78B3201D7270D6F338DFA91B6FAD4 (Bounds_t367E830C64BBF235ED8C3B2F8CF6254FDCAD39C3* __this, const RuntimeMethod* method) ;
// System.Void UnityEngine.Bounds::set_size(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Bounds_set_size_m950CFB68CDD1BF409E770509A38B958E1AE68128 (Bounds_t367E830C64BBF235ED8C3B2F8CF6254FDCAD39C3* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___value0, const RuntimeMethod* method) ;
// System.Void MicahW.PointGrass.PointGrassRenderer::SetDistributionSource(UnityEngine.MeshFilter[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointGrassRenderer_SetDistributionSource_m88344D60FD8578ECD8AE1B7A3A596E06C514B978 (PointGrassRenderer_tCAB9C932B5AEF5960C04E43451965E31C6A471C2* __this, MeshFilterU5BU5D_tCE3B457E6F7ECE5ECEE9E09150642150448685BA* ___sceneFilters0, const RuntimeMethod* method) ;
// System.Void MicahW.PointGrass.PointGrassRenderer::SetBladeType(MicahW.PointGrass.PointGrassCommon/BladeType)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void PointGrassRenderer_SetBladeType_mF4BC8DC77A9FC34419A19830C292A4C415A6C805_inline (PointGrassRenderer_tCAB9C932B5AEF5960C04E43451965E31C6A471C2* __this, int32_t ___type0, const RuntimeMethod* method) ;
// System.Void MicahW.PointGrass.PointGrassRenderer::SetBladeMesh(UnityEngine.Mesh)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointGrassRenderer_SetBladeMesh_mE55D9726DCDFCAC335303BB80BDC47A5A172CC98 (PointGrassRenderer_tCAB9C932B5AEF5960C04E43451965E31C6A471C2* __this, Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* ___mesh0, const RuntimeMethod* method) ;
// System.Void System.Array::Resize<System.Single>(T[]&,System.Int32)
inline void Array_Resize_TisSingle_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_m879C2A54DAFE78F46D1185B50ED527EE182BFB04 (SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C** ___array0, int32_t ___newSize1, const RuntimeMethod* method)
{
	((  void (*) (SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C**, int32_t, const RuntimeMethod*))Array_Resize_TisSingle_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_m879C2A54DAFE78F46D1185B50ED527EE182BFB04_gshared)(___array0, ___newSize1, method);
}
// System.Void MicahW.PointGrass.PointGrassRenderer::SetMaterials(UnityEngine.Material[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointGrassRenderer_SetMaterials_mF165DA9F1096738EBA94283B8AF3C7BDA4F02DAC (PointGrassRenderer_tCAB9C932B5AEF5960C04E43451965E31C6A471C2* __this, MaterialU5BU5D_t2B1D11C42DB07A4400C0535F92DBB87A2E346D3D* ___materials0, const RuntimeMethod* method) ;
// System.Void MicahW.PointGrass.PointGrassRenderer::SetMaterial(UnityEngine.Material)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointGrassRenderer_SetMaterial_mF9A26D70C3BC4E8BDE7F1E2D8BDF0E796EDB6BCF (PointGrassRenderer_tCAB9C932B5AEF5960C04E43451965E31C6A471C2* __this, Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___mat0, const RuntimeMethod* method) ;
// System.Void System.Array::Resize<UnityEngine.Material>(T[]&,System.Int32)
inline void Array_Resize_TisMaterial_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3_m79C251DDDBE015E6DB85FE487F8748D4613BCC7C (MaterialU5BU5D_t2B1D11C42DB07A4400C0535F92DBB87A2E346D3D** ___array0, int32_t ___newSize1, const RuntimeMethod* method)
{
	((  void (*) (MaterialU5BU5D_t2B1D11C42DB07A4400C0535F92DBB87A2E346D3D**, int32_t, const RuntimeMethod*))Array_Resize_TisRuntimeObject_mE8D92C287251BAF8256D85E5829F749359EC334E_gshared)(___array0, ___newSize1, method);
}
// System.Boolean UnityEngine.Behaviour::get_enabled()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Behaviour_get_enabled_mAAC9F15E9EBF552217A5AE2681589CC0BFA300C1 (Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA* __this, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Quaternion::Internal_ToEulerRad(UnityEngine.Quaternion)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Quaternion_Internal_ToEulerRad_m5BD0EEC543120C320DC77FCCDFD2CE2E6BD3F1A8 (Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___rotation0, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Quaternion::Internal_MakePositive(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Quaternion_Internal_MakePositive_m73E2D01920CB0DFE661A55022C129E8617F0C9A8 (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___euler0, const RuntimeMethod* method) ;
// System.Void UnityEngine.Quaternion::.ctor(System.Single,System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Quaternion__ctor_m868FD60AA65DD5A8AC0C5DEB0608381A8D85FCD8_inline (Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974* __this, float ___x0, float ___y1, float ___z2, float ___w3, const RuntimeMethod* method) ;
// System.Void UnityEngine.Vector4::.ctor(System.Single,System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector4__ctor_m96B2CD8B862B271F513AF0BDC2EABD58E4DBC813_inline (Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3* __this, float ___x0, float ___y1, float ___z2, float ___w3, const RuntimeMethod* method) ;
// System.Single UnityEngine.Vector3::Magnitude(UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Vector3_Magnitude_m21652D951393A3D7CE92CE40049A0E7F76544D1B_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___vector0, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Division(UnityEngine.Vector3,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_op_Division_mCC6BB24E372AB96B8380D1678446EF6A8BAE13BB_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___a0, float ___d1, const RuntimeMethod* method) ;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MicahW.PointGrass.FreeCam::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FreeCam_Start_m3F115537F67F9ECAEDFC485B8C64E5984ED93348 (FreeCam_tCB0B2B68E5483040D01032D18F6046E3733C844B* __this, const RuntimeMethod* method) 
{
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// currentPitch = transform.rotation.eulerAngles.x;
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_0;
		L_0 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(__this, NULL);
		NullCheck(L_0);
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_1;
		L_1 = Transform_get_rotation_m32AF40CA0D50C797DA639A696F8EAEC7524C179C(L_0, NULL);
		V_0 = L_1;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_2;
		L_2 = Quaternion_get_eulerAngles_m2DB5158B5C3A71FD60FC8A6EE43D3AAA1CFED122_inline((&V_0), NULL);
		float L_3 = L_2.___x_2;
		__this->___currentPitch_8 = L_3;
		// currentYaw = transform.rotation.eulerAngles.y;
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_4;
		L_4 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(__this, NULL);
		NullCheck(L_4);
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_5;
		L_5 = Transform_get_rotation_m32AF40CA0D50C797DA639A696F8EAEC7524C179C(L_4, NULL);
		V_0 = L_5;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_6;
		L_6 = Quaternion_get_eulerAngles_m2DB5158B5C3A71FD60FC8A6EE43D3AAA1CFED122_inline((&V_0), NULL);
		float L_7 = L_6.___y_3;
		__this->___currentYaw_9 = L_7;
		// currentVelocity = Vector3.zero;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_8;
		L_8 = Vector3_get_zero_m0C1249C3F25B1C70EAD3CC8B31259975A457AE39_inline(NULL);
		__this->___currentVelocity_10 = L_8;
		// }
		return;
	}
}
// System.Void MicahW.PointGrass.FreeCam::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FreeCam_Update_m87790A0FA44364BB417773C383A66444BF47B72C (FreeCam_tCB0B2B68E5483040D01032D18F6046E3733C844B* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral16DD21BE77B115D392226EB71A2D3A9FDC29E3F0);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral265E15F1F86F1C766555899D5771CF29055DE75A);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7F8C014BD4810CC276D0F9F81A1E759C7B098B1E);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral88BEE283254D7094E258B3A88730F4CC4F1E4AC7);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 V_0;
	memset((&V_0), 0, sizeof(V_0));
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_1;
	memset((&V_1), 0, sizeof(V_1));
	float V_2 = 0.0f;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_3;
	memset((&V_3), 0, sizeof(V_3));
	float G_B2_0 = 0.0f;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* G_B2_1 = NULL;
	float G_B1_0 = 0.0f;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* G_B1_1 = NULL;
	float G_B3_0 = 0.0f;
	float G_B3_1 = 0.0f;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* G_B3_2 = NULL;
	float G_B5_0 = 0.0f;
	float G_B5_1 = 0.0f;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* G_B5_2 = NULL;
	float G_B4_0 = 0.0f;
	float G_B4_1 = 0.0f;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* G_B4_2 = NULL;
	float G_B6_0 = 0.0f;
	float G_B6_1 = 0.0f;
	float G_B6_2 = 0.0f;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* G_B6_3 = NULL;
	float G_B9_0 = 0.0f;
	{
		// Vector2 mouseInput = new Vector2(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));
		float L_0;
		L_0 = Input_GetAxis_m10372E6C5FF591668D2DC5F58C58D213CC598A62(_stringLiteral88BEE283254D7094E258B3A88730F4CC4F1E4AC7, NULL);
		float L_1;
		L_1 = Input_GetAxis_m10372E6C5FF591668D2DC5F58C58D213CC598A62(_stringLiteral16DD21BE77B115D392226EB71A2D3A9FDC29E3F0, NULL);
		Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline((&V_0), L_0, L_1, NULL);
		// Vector3 moveInput = new Vector3(
		//     Input.GetAxis("Horizontal"),
		//     (Input.GetKey(KeyCode.E) ? 1f : 0f) + (Input.GetKey(KeyCode.Q) ? -1f : 0f),
		//     Input.GetAxis("Vertical")
		// );
		float L_2;
		L_2 = Input_GetAxis_m10372E6C5FF591668D2DC5F58C58D213CC598A62(_stringLiteral7F8C014BD4810CC276D0F9F81A1E759C7B098B1E, NULL);
		bool L_3;
		L_3 = Input_GetKey_mE5681EF775F3CEBA7EAD7C63984F7B34C8E8D434(((int32_t)101), NULL);
		G_B1_0 = L_2;
		G_B1_1 = (&V_1);
		if (L_3)
		{
			G_B2_0 = L_2;
			G_B2_1 = (&V_1);
			goto IL_0037;
		}
	}
	{
		G_B3_0 = (0.0f);
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		goto IL_003c;
	}

IL_0037:
	{
		G_B3_0 = (1.0f);
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
	}

IL_003c:
	{
		bool L_4;
		L_4 = Input_GetKey_mE5681EF775F3CEBA7EAD7C63984F7B34C8E8D434(((int32_t)113), NULL);
		G_B4_0 = G_B3_0;
		G_B4_1 = G_B3_1;
		G_B4_2 = G_B3_2;
		if (L_4)
		{
			G_B5_0 = G_B3_0;
			G_B5_1 = G_B3_1;
			G_B5_2 = G_B3_2;
			goto IL_004c;
		}
	}
	{
		G_B6_0 = (0.0f);
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0051;
	}

IL_004c:
	{
		G_B6_0 = (-1.0f);
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0051:
	{
		float L_5;
		L_5 = Input_GetAxis_m10372E6C5FF591668D2DC5F58C58D213CC598A62(_stringLiteral265E15F1F86F1C766555899D5771CF29055DE75A, NULL);
		Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline(G_B6_3, G_B6_2, ((float)il2cpp_codegen_add(G_B6_1, G_B6_0)), L_5, NULL);
		// float targetSpeed = Input.GetKey(KeyCode.LeftShift) ? fastSpeed : moveSpeed;
		bool L_6;
		L_6 = Input_GetKey_mE5681EF775F3CEBA7EAD7C63984F7B34C8E8D434(((int32_t)304), NULL);
		if (L_6)
		{
			goto IL_0075;
		}
	}
	{
		float L_7 = __this->___moveSpeed_5;
		G_B9_0 = L_7;
		goto IL_007b;
	}

IL_0075:
	{
		float L_8 = __this->___fastSpeed_7;
		G_B9_0 = L_8;
	}

IL_007b:
	{
		V_2 = G_B9_0;
		// currentPitch = (currentPitch - lookSpeed * mouseInput.y) % 360f;
		float L_9 = __this->___currentPitch_8;
		float L_10 = __this->___lookSpeed_4;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_11 = V_0;
		float L_12 = L_11.___y_1;
		__this->___currentPitch_8 = (fmodf(((float)il2cpp_codegen_subtract(L_9, ((float)il2cpp_codegen_multiply(L_10, L_12)))), (360.0f)));
		// currentYaw = (currentYaw + lookSpeed * mouseInput.x) % 360f;
		float L_13 = __this->___currentYaw_9;
		float L_14 = __this->___lookSpeed_4;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_15 = V_0;
		float L_16 = L_15.___x_0;
		__this->___currentYaw_9 = (fmodf(((float)il2cpp_codegen_add(L_13, ((float)il2cpp_codegen_multiply(L_14, L_16)))), (360.0f)));
		// transform.rotation = Quaternion.AngleAxis(currentYaw, Vector3.up) * Quaternion.AngleAxis(currentPitch, Vector3.right);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_17;
		L_17 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(__this, NULL);
		float L_18 = __this->___currentYaw_9;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_19;
		L_19 = Vector3_get_up_m128AF3FDC820BF59D5DE86D973E7DE3F20C3AEBA_inline(NULL);
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_20;
		L_20 = Quaternion_AngleAxis_mF37022977B297E63AA70D69EA1C4C922FF22CC80(L_18, L_19, NULL);
		float L_21 = __this->___currentPitch_8;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_22;
		L_22 = Vector3_get_right_mFF573AFBBB2186E7AFA1BA7CA271A78DF67E4EA0_inline(NULL);
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_23;
		L_23 = Quaternion_AngleAxis_mF37022977B297E63AA70D69EA1C4C922FF22CC80(L_21, L_22, NULL);
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_24;
		L_24 = Quaternion_op_Multiply_mCB375FCCC12A2EC8F9EB824A1BFB4453B58C2012_inline(L_20, L_23, NULL);
		NullCheck(L_17);
		Transform_set_rotation_m61340DE74726CF0F9946743A727C4D444397331D(L_17, L_24, NULL);
		// Vector3 targetVelocity = transform.TransformDirection(moveInput) * targetSpeed;
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_25;
		L_25 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(__this, NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_26 = V_1;
		NullCheck(L_25);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_27;
		L_27 = Transform_TransformDirection_m9BE1261DF2D48B7A4A27D31EE24D2D97F89E7757(L_25, L_26, NULL);
		float L_28 = V_2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_29;
		L_29 = Vector3_op_Multiply_m87BA7C578F96C8E49BB07088DAAC4649F83B0353_inline(L_27, L_28, NULL);
		V_3 = L_29;
		// currentVelocity = Vector3.MoveTowards(currentVelocity, targetVelocity, moveAccel * Time.deltaTime);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_30 = __this->___currentVelocity_10;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_31 = V_3;
		float L_32 = __this->___moveAccel_6;
		float L_33;
		L_33 = Time_get_deltaTime_mC3195000401F0FD167DD2F948FD2BC58330D0865(NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_34;
		L_34 = Vector3_MoveTowards_m0363264647799F3173AC37F8E819F98298249B08_inline(L_30, L_31, ((float)il2cpp_codegen_multiply(L_32, L_33)), NULL);
		__this->___currentVelocity_10 = L_34;
		// transform.position += currentVelocity * Time.deltaTime;
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_35;
		L_35 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(__this, NULL);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_36 = L_35;
		NullCheck(L_36);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_37;
		L_37 = Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1(L_36, NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_38 = __this->___currentVelocity_10;
		float L_39;
		L_39 = Time_get_deltaTime_mC3195000401F0FD167DD2F948FD2BC58330D0865(NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_40;
		L_40 = Vector3_op_Multiply_m87BA7C578F96C8E49BB07088DAAC4649F83B0353_inline(L_38, L_39, NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_41;
		L_41 = Vector3_op_Addition_m78C0EC70CB66E8DCAC225743D82B268DAEE92067_inline(L_37, L_40, NULL);
		NullCheck(L_36);
		Transform_set_position_mA1A817124BB41B685043DED2A9BA48CDF37C4156(L_36, L_41, NULL);
		// }
		return;
	}
}
// System.Void MicahW.PointGrass.FreeCam::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FreeCam__ctor_m48EBAA195DB00803549D380950ABCDB9CC9433B2 (FreeCam_tCB0B2B68E5483040D01032D18F6046E3733C844B* __this, const RuntimeMethod* method) 
{
	{
		// public float lookSpeed = 7.5f;
		__this->___lookSpeed_4 = (7.5f);
		// public float moveSpeed = 5f;
		__this->___moveSpeed_5 = (5.0f);
		// public float moveAccel = 30f;
		__this->___moveAccel_6 = (30.0f);
		// public float fastSpeed = 15f;
		__this->___fastSpeed_7 = (15.0f);
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MicahW.PointGrass.ObjectOscillator::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ObjectOscillator_Start_mF5D4EF734128FE6DCBDA83C5AC82865A1523F859 (ObjectOscillator_tE8A6DB7BEA8C02735F3219A068F4BC2FFF0CF1ED* __this, const RuntimeMethod* method) 
{
	{
		// origin = transform.position;
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_0;
		L_0 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(__this, NULL);
		NullCheck(L_0);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_1;
		L_1 = Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1(L_0, NULL);
		__this->___origin_8 = L_1;
		// }
		return;
	}
}
// System.Void MicahW.PointGrass.ObjectOscillator::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ObjectOscillator_Update_mC3721C9C8D8241120B5FFB7A287C6FD3F13DC815 (ObjectOscillator_tE8A6DB7BEA8C02735F3219A068F4BC2FFF0CF1ED* __this, const RuntimeMethod* method) 
{
	float V_0 = 0.0f;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_1;
	memset((&V_1), 0, sizeof(V_1));
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_2;
	memset((&V_2), 0, sizeof(V_2));
	{
		// float frequencyMul = Time.time * Mathf.PI * 2f;
		float L_0;
		L_0 = Time_get_time_m3A271BB1B20041144AC5B7863B71AB1F0150374B(NULL);
		V_0 = ((float)il2cpp_codegen_multiply(((float)il2cpp_codegen_multiply(L_0, (3.14159274f))), (2.0f)));
		// Vector3 phase = oscPhase * Mathf.PI * 2f;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_1 = __this->___oscPhase_6;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_2;
		L_2 = Vector3_op_Multiply_m87BA7C578F96C8E49BB07088DAAC4649F83B0353_inline(L_1, (3.14159274f), NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_3;
		L_3 = Vector3_op_Multiply_m87BA7C578F96C8E49BB07088DAAC4649F83B0353_inline(L_2, (2.0f), NULL);
		V_1 = L_3;
		// Vector3 offset = new Vector3(
		//     Mathf.Sin(oscFrequency.x * frequencyMul + phase.x),
		//     Mathf.Sin(oscFrequency.y * frequencyMul + phase.y),
		//     Mathf.Sin(oscFrequency.z * frequencyMul + phase.z)
		// );
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* L_4 = (&__this->___oscFrequency_5);
		float L_5 = L_4->___x_2;
		float L_6 = V_0;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_7 = V_1;
		float L_8 = L_7.___x_2;
		float L_9;
		L_9 = sinf(((float)il2cpp_codegen_add(((float)il2cpp_codegen_multiply(L_5, L_6)), L_8)));
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* L_10 = (&__this->___oscFrequency_5);
		float L_11 = L_10->___y_3;
		float L_12 = V_0;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_13 = V_1;
		float L_14 = L_13.___y_3;
		float L_15;
		L_15 = sinf(((float)il2cpp_codegen_add(((float)il2cpp_codegen_multiply(L_11, L_12)), L_14)));
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* L_16 = (&__this->___oscFrequency_5);
		float L_17 = L_16->___z_4;
		float L_18 = V_0;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_19 = V_1;
		float L_20 = L_19.___z_4;
		float L_21;
		L_21 = sinf(((float)il2cpp_codegen_add(((float)il2cpp_codegen_multiply(L_17, L_18)), L_20)));
		Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline((&V_2), L_9, L_15, L_21, NULL);
		// offset.Scale(oscStrength);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_22 = __this->___oscStrength_4;
		Vector3_Scale_mE0DC2C1B7902271788591F17DBE7F7F72EC37283_inline((&V_2), L_22, NULL);
		// offset += oscOffset;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_23 = V_2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_24 = __this->___oscOffset_7;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_25;
		L_25 = Vector3_op_Addition_m78C0EC70CB66E8DCAC225743D82B268DAEE92067_inline(L_23, L_24, NULL);
		V_2 = L_25;
		// transform.position = origin + offset;
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_26;
		L_26 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(__this, NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_27 = __this->___origin_8;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_28 = V_2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_29;
		L_29 = Vector3_op_Addition_m78C0EC70CB66E8DCAC225743D82B268DAEE92067_inline(L_27, L_28, NULL);
		NullCheck(L_26);
		Transform_set_position_mA1A817124BB41B685043DED2A9BA48CDF37C4156(L_26, L_29, NULL);
		// }
		return;
	}
}
// System.Void MicahW.PointGrass.ObjectOscillator::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ObjectOscillator__ctor_m5E852F90A3D1DBDD07D28F92624934F31CDF9AD7 (ObjectOscillator_tE8A6DB7BEA8C02735F3219A068F4BC2FFF0CF1ED* __this, const RuntimeMethod* method) 
{
	{
		// public Vector3 oscStrength = Vector3.right;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0;
		L_0 = Vector3_get_right_mFF573AFBBB2186E7AFA1BA7CA271A78DF67E4EA0_inline(NULL);
		__this->___oscStrength_4 = L_0;
		// public Vector3 oscFrequency = Vector3.right;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_1;
		L_1 = Vector3_get_right_mFF573AFBBB2186E7AFA1BA7CA271A78DF67E4EA0_inline(NULL);
		__this->___oscFrequency_5 = L_1;
		// public Vector3 oscPhase = Vector3.zero;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_2;
		L_2 = Vector3_get_zero_m0C1249C3F25B1C70EAD3CC8B31259975A457AE39_inline(NULL);
		__this->___oscPhase_6 = L_2;
		// public Vector3 oscOffset = Vector3.zero;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_3;
		L_3 = Vector3_get_zero_m0C1249C3F25B1C70EAD3CC8B31259975A457AE39_inline(NULL);
		__this->___oscOffset_7 = L_3;
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// MicahW.PointGrass.PointGrassCommon/MeshPoint[] MicahW.PointGrass.DistributePointsAlongMesh::DistributePoints(MicahW.PointGrass.PointGrassCommon/MeshData,UnityEngine.Vector3,System.Single,System.Int32,System.Boolean,System.Nullable`1<UnityEngine.Vector3>,System.Boolean,System.Boolean,System.Boolean,System.Single,UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR MeshPointU5BU5D_tBCEA150F23C3861E840A87B994115F06D7490DD8* DistributePointsAlongMesh_DistributePoints_m697A346959B6378FF0BF569C2DD2C8189E81AAF4 (MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F* ___mesh0, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___scale1, float ___pointCount2, int32_t ___seed3, bool ___multiplyPointsByArea4, Nullable_1_t9C51B084784B716FFF4ED4575C63CFD8A71A86FE ___forcedNormal5, bool ___useColours6, bool ___useDensity7, bool ___useLength8, float ___densityCutoff9, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___lengthMapping10, const RuntimeMethod* method) 
{
	SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* V_0 = NULL;
	float V_1 = 0.0f;
	int32_t V_2 = 0;
	int32_t G_B11_0 = 0;
	{
		// if (pointCount <= 0) { return null; } // Since the point count is too low, return nothing
		float L_0 = ___pointCount2;
		if ((!(((float)L_0) <= ((float)(0.0f)))))
		{
			goto IL_000a;
		}
	}
	{
		// if (pointCount <= 0) { return null; } // Since the point count is too low, return nothing
		return (MeshPointU5BU5D_tBCEA150F23C3861E840A87B994115F06D7490DD8*)NULL;
	}

IL_000a:
	{
		// if (densityCutoff >= 1f) { return null; } // Since the density cutoff is too high (And may cause errors) return nothing
		float L_1 = ___densityCutoff9;
		if ((!(((float)L_1) >= ((float)(1.0f)))))
		{
			goto IL_0015;
		}
	}
	{
		// if (densityCutoff >= 1f) { return null; } // Since the density cutoff is too high (And may cause errors) return nothing
		return (MeshPointU5BU5D_tBCEA150F23C3861E840A87B994115F06D7490DD8*)NULL;
	}

IL_0015:
	{
		// useDensity &= mesh.HasAttributes;
		bool L_2 = ___useDensity7;
		MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F* L_3 = ___mesh0;
		NullCheck(L_3);
		bool L_4;
		L_4 = MeshData_get_HasAttributes_m4F13A5EE919CA43A34BBE872AC66756434B30060(L_3, NULL);
		___useDensity7 = (bool)((int32_t)((int32_t)L_2&(int32_t)L_4));
		// useLength &= mesh.HasAttributes;
		bool L_5 = ___useLength8;
		MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F* L_6 = ___mesh0;
		NullCheck(L_6);
		bool L_7;
		L_7 = MeshData_get_HasAttributes_m4F13A5EE919CA43A34BBE872AC66756434B30060(L_6, NULL);
		___useLength8 = (bool)((int32_t)((int32_t)L_5&(int32_t)L_7));
		// useColours &= mesh.HasColours;
		bool L_8 = ___useColours6;
		MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F* L_9 = ___mesh0;
		NullCheck(L_9);
		bool L_10;
		L_10 = MeshData_get_HasColours_m67226DD3676EBC3004861960060933A3EFB23FA2(L_9, NULL);
		___useColours6 = (bool)((int32_t)((int32_t)L_8&(int32_t)L_10));
		// if (useDensity) { mesh.ApplyDensityCutoff(densityCutoff); }
		bool L_11 = ___useDensity7;
		if (!L_11)
		{
			goto IL_0042;
		}
	}
	{
		// if (useDensity) { mesh.ApplyDensityCutoff(densityCutoff); }
		MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F* L_12 = ___mesh0;
		float L_13 = ___densityCutoff9;
		NullCheck(L_12);
		MeshData_ApplyDensityCutoff_m8F76018CA26B3621D4B38ED51A0EF5101D35B1E4(L_12, L_13, NULL);
	}

IL_0042:
	{
		// if (useLength) { mesh.ApplyLengthMapping(lengthMapping); }
		bool L_14 = ___useLength8;
		if (!L_14)
		{
			goto IL_004e;
		}
	}
	{
		// if (useLength) { mesh.ApplyLengthMapping(lengthMapping); }
		MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F* L_15 = ___mesh0;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_16 = ___lengthMapping10;
		NullCheck(L_15);
		MeshData_ApplyLengthMapping_mCD709AEF3230ECE96D914FBDF433B3D1DF97C183(L_15, L_16, NULL);
	}

IL_004e:
	{
		// float[] cumulativeSizes = GetCumulativeTriSizes(mesh.tris, mesh.verts, mesh.attributes, scale, useDensity, out float totalArea);
		MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F* L_17 = ___mesh0;
		NullCheck(L_17);
		Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_18 = L_17->___tris_4;
		MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F* L_19 = ___mesh0;
		NullCheck(L_19);
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_20 = L_19->___verts_0;
		MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F* L_21 = ___mesh0;
		NullCheck(L_21);
		Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* L_22 = L_21->___attributes_5;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_23 = ___scale1;
		bool L_24 = ___useDensity7;
		SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* L_25;
		L_25 = DistributePointsAlongMesh_GetCumulativeTriSizes_m64D783A59608A96F2FABF19CA7D0BB72F399B18C(L_18, L_20, L_22, L_23, L_24, (&V_1), NULL);
		V_0 = L_25;
		// int pointTotal = multiplyPointsByArea ? Mathf.FloorToInt(pointCount * totalArea) : Mathf.FloorToInt(pointCount);
		bool L_26 = ___multiplyPointsByArea4;
		if (L_26)
		{
			goto IL_0077;
		}
	}
	{
		float L_27 = ___pointCount2;
		int32_t L_28;
		L_28 = Mathf_FloorToInt_m2A39AE881CAEE6B6A4B3BFEF9CA1ED40625F5AB7_inline(L_27, NULL);
		G_B11_0 = L_28;
		goto IL_007f;
	}

IL_0077:
	{
		float L_29 = ___pointCount2;
		float L_30 = V_1;
		int32_t L_31;
		L_31 = Mathf_FloorToInt_m2A39AE881CAEE6B6A4B3BFEF9CA1ED40625F5AB7_inline(((float)il2cpp_codegen_multiply(L_29, L_30)), NULL);
		G_B11_0 = L_31;
	}

IL_007f:
	{
		V_2 = G_B11_0;
		// if (pointTotal <= 0 || totalArea <= 0) { return null; }
		int32_t L_32 = V_2;
		if ((((int32_t)L_32) <= ((int32_t)0)))
		{
			goto IL_008c;
		}
	}
	{
		float L_33 = V_1;
		if ((!(((float)L_33) <= ((float)(0.0f)))))
		{
			goto IL_008e;
		}
	}

IL_008c:
	{
		// if (pointTotal <= 0 || totalArea <= 0) { return null; }
		return (MeshPointU5BU5D_tBCEA150F23C3861E840A87B994115F06D7490DD8*)NULL;
	}

IL_008e:
	{
		// return DistributePoints_CPU(mesh, cumulativeSizes, pointTotal, seed, totalArea, forcedNormal, useColours, useLength);
		MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F* L_34 = ___mesh0;
		SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* L_35 = V_0;
		int32_t L_36 = V_2;
		int32_t L_37 = ___seed3;
		float L_38 = V_1;
		Nullable_1_t9C51B084784B716FFF4ED4575C63CFD8A71A86FE L_39 = ___forcedNormal5;
		bool L_40 = ___useColours6;
		bool L_41 = ___useLength8;
		MeshPointU5BU5D_tBCEA150F23C3861E840A87B994115F06D7490DD8* L_42;
		L_42 = DistributePointsAlongMesh_DistributePoints_CPU_m7EDF68975B721F8590DB32548BFCA21B9593805D(L_34, L_35, L_36, L_37, L_38, L_39, L_40, L_41, NULL);
		return L_42;
	}
}
// MicahW.PointGrass.PointGrassCommon/MeshPoint[] MicahW.PointGrass.DistributePointsAlongMesh::DistributePoints_CPU(MicahW.PointGrass.PointGrassCommon/MeshData,System.Single[],System.Int32,System.Int32,System.Single,System.Nullable`1<UnityEngine.Vector3>,System.Boolean,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR MeshPointU5BU5D_tBCEA150F23C3861E840A87B994115F06D7490DD8* DistributePointsAlongMesh_DistributePoints_CPU_m7EDF68975B721F8590DB32548BFCA21B9593805D (MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F* ___mesh0, SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* ___cumulativeSizes1, int32_t ___pointCount2, int32_t ___seed3, float ___totalArea4, Nullable_1_t9C51B084784B716FFF4ED4575C63CFD8A71A86FE ___forcedNormal5, bool ___useColours6, bool ___useLength7, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MeshPointU5BU5D_tBCEA150F23C3861E840A87B994115F06D7490DD8_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Nullable_1_get_HasValue_m6B76086B0E863AB1D634FD03E30154F230070435_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Nullable_1_get_Value_m6A74FA440FE386A9905C61B41B5C261CD9DC4792_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	State_tA37EB68FE687D41D4B228462D4C7427FAC5BF9C1 V_0;
	memset((&V_0), 0, sizeof(V_0));
	MeshPointU5BU5D_tBCEA150F23C3861E840A87B994115F06D7490DD8* V_1 = NULL;
	int32_t V_2 = 0;
	float V_3 = 0.0f;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_7;
	memset((&V_7), 0, sizeof(V_7));
	MeshPoint_t033BD3FDC9D80AE452F7B3D583D17CDADF299152 V_8;
	memset((&V_8), 0, sizeof(V_8));
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_9;
	memset((&V_9), 0, sizeof(V_9));
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_10;
	memset((&V_10), 0, sizeof(V_10));
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_11;
	memset((&V_11), 0, sizeof(V_11));
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 V_12;
	memset((&V_12), 0, sizeof(V_12));
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 V_13;
	memset((&V_13), 0, sizeof(V_13));
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 V_14;
	memset((&V_14), 0, sizeof(V_14));
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_15;
	memset((&V_15), 0, sizeof(V_15));
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_16;
	memset((&V_16), 0, sizeof(V_16));
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_17;
	memset((&V_17), 0, sizeof(V_17));
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F V_18;
	memset((&V_18), 0, sizeof(V_18));
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F V_19;
	memset((&V_19), 0, sizeof(V_19));
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F V_20;
	memset((&V_20), 0, sizeof(V_20));
	float V_21 = 0.0f;
	float V_22 = 0.0f;
	float V_23 = 0.0f;
	{
		// Random.State state = Random.state;
		State_tA37EB68FE687D41D4B228462D4C7427FAC5BF9C1 L_0;
		L_0 = Random_get_state_m84FD78A44C27EA34F1CD9833545DAB9B541B98AA(NULL);
		V_0 = L_0;
		// Random.InitState(seed);
		int32_t L_1 = ___seed3;
		Random_InitState_mE70961834F42FFEEB06CB9C68175354E0C255664(L_1, NULL);
		// MeshPoint[] points = new MeshPoint[pointCount];
		int32_t L_2 = ___pointCount2;
		MeshPointU5BU5D_tBCEA150F23C3861E840A87B994115F06D7490DD8* L_3 = (MeshPointU5BU5D_tBCEA150F23C3861E840A87B994115F06D7490DD8*)(MeshPointU5BU5D_tBCEA150F23C3861E840A87B994115F06D7490DD8*)SZArrayNew(MeshPointU5BU5D_tBCEA150F23C3861E840A87B994115F06D7490DD8_il2cpp_TypeInfo_var, (uint32_t)L_2);
		V_1 = L_3;
		// for (int i = 0; i < pointCount; i++) {
		V_2 = 0;
		goto IL_0342;
	}

IL_001a:
	{
		// float randomSample = Random.Range(0f, totalArea);
		float L_4 = ___totalArea4;
		float L_5;
		L_5 = Random_Range_m5236C99A7D8AE6AC9190592DC66016652A2D2494((0.0f), L_4, NULL);
		V_3 = L_5;
		// int triIndexA = FindTriangleIndex(cumulativeSizes, randomSample);
		SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* L_6 = ___cumulativeSizes1;
		float L_7 = V_3;
		int32_t L_8;
		L_8 = DistributePointsAlongMesh_FindTriangleIndex_mFA277A2DD05D747D627B4504C44E8BED5F0F8DFA(L_6, L_7, NULL);
		V_4 = L_8;
		// int triIndexB = triIndexA + 1;
		int32_t L_9 = V_4;
		V_5 = ((int32_t)il2cpp_codegen_add(L_9, 1));
		// int triIndexC = triIndexA + 2;
		int32_t L_10 = V_4;
		V_6 = ((int32_t)il2cpp_codegen_add(L_10, 2));
		// triIndexA = mesh.tris[triIndexA];
		MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F* L_11 = ___mesh0;
		NullCheck(L_11);
		Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_12 = L_11->___tris_4;
		int32_t L_13 = V_4;
		NullCheck(L_12);
		int32_t L_14 = L_13;
		int32_t L_15 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_14));
		V_4 = L_15;
		// triIndexB = mesh.tris[triIndexB];
		MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F* L_16 = ___mesh0;
		NullCheck(L_16);
		Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_17 = L_16->___tris_4;
		int32_t L_18 = V_5;
		NullCheck(L_17);
		int32_t L_19 = L_18;
		int32_t L_20 = (L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
		V_5 = L_20;
		// triIndexC = mesh.tris[triIndexC];
		MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F* L_21 = ___mesh0;
		NullCheck(L_21);
		Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_22 = L_21->___tris_4;
		int32_t L_23 = V_6;
		NullCheck(L_22);
		int32_t L_24 = L_23;
		int32_t L_25 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		V_6 = L_25;
		// Vector3 BC = new Vector3(Random.value, Random.value, 0f);
		float L_26;
		L_26 = Random_get_value_m2CEA87FADF5222EF9E13D32695F15E2BA282E24B(NULL);
		float L_27;
		L_27 = Random_get_value_m2CEA87FADF5222EF9E13D32695F15E2BA282E24B(NULL);
		Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline((&V_7), L_26, L_27, (0.0f), NULL);
		// if (BC.x + BC.y >= 1f) {
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_28 = V_7;
		float L_29 = L_28.___x_2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_30 = V_7;
		float L_31 = L_30.___y_3;
		if ((!(((float)((float)il2cpp_codegen_add(L_29, L_31))) >= ((float)(1.0f)))))
		{
			goto IL_00b1;
		}
	}
	{
		// BC.x = 1 - BC.x;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_32 = V_7;
		float L_33 = L_32.___x_2;
		(&V_7)->___x_2 = ((float)il2cpp_codegen_subtract((1.0f), L_33));
		// BC.y = 1 - BC.y;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_34 = V_7;
		float L_35 = L_34.___y_3;
		(&V_7)->___y_3 = ((float)il2cpp_codegen_subtract((1.0f), L_35));
	}

IL_00b1:
	{
		// BC.z = 1f - BC.x - BC.y;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_36 = V_7;
		float L_37 = L_36.___x_2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_38 = V_7;
		float L_39 = L_38.___y_3;
		(&V_7)->___z_4 = ((float)il2cpp_codegen_subtract(((float)il2cpp_codegen_subtract((1.0f), L_37)), L_39));
		// MeshPoint point = new MeshPoint();
		il2cpp_codegen_initobj((&V_8), sizeof(MeshPoint_t033BD3FDC9D80AE452F7B3D583D17CDADF299152));
		// Vector3 P1 = mesh.verts[triIndexA];
		MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F* L_40 = ___mesh0;
		NullCheck(L_40);
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_41 = L_40->___verts_0;
		int32_t L_42 = V_4;
		NullCheck(L_41);
		int32_t L_43 = L_42;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_44 = (L_41)->GetAt(static_cast<il2cpp_array_size_t>(L_43));
		V_9 = L_44;
		// Vector3 P2 = mesh.verts[triIndexB];
		MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F* L_45 = ___mesh0;
		NullCheck(L_45);
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_46 = L_45->___verts_0;
		int32_t L_47 = V_5;
		NullCheck(L_46);
		int32_t L_48 = L_47;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_49 = (L_46)->GetAt(static_cast<il2cpp_array_size_t>(L_48));
		V_10 = L_49;
		// Vector3 P3 = mesh.verts[triIndexC];
		MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F* L_50 = ___mesh0;
		NullCheck(L_50);
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_51 = L_50->___verts_0;
		int32_t L_52 = V_6;
		NullCheck(L_51);
		int32_t L_53 = L_52;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_54 = (L_51)->GetAt(static_cast<il2cpp_array_size_t>(L_53));
		V_11 = L_54;
		// point.position = (P1 * BC.x) + (P2 * BC.y) + (P3 * BC.z);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_55 = V_9;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_56 = V_7;
		float L_57 = L_56.___x_2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_58;
		L_58 = Vector3_op_Multiply_m87BA7C578F96C8E49BB07088DAAC4649F83B0353_inline(L_55, L_57, NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_59 = V_10;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_60 = V_7;
		float L_61 = L_60.___y_3;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_62;
		L_62 = Vector3_op_Multiply_m87BA7C578F96C8E49BB07088DAAC4649F83B0353_inline(L_59, L_61, NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_63;
		L_63 = Vector3_op_Addition_m78C0EC70CB66E8DCAC225743D82B268DAEE92067_inline(L_58, L_62, NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_64 = V_11;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_65 = V_7;
		float L_66 = L_65.___z_4;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_67;
		L_67 = Vector3_op_Multiply_m87BA7C578F96C8E49BB07088DAAC4649F83B0353_inline(L_64, L_66, NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_68;
		L_68 = Vector3_op_Addition_m78C0EC70CB66E8DCAC225743D82B268DAEE92067_inline(L_63, L_67, NULL);
		(&V_8)->___position_0 = L_68;
		// if (!forcedNormal.HasValue) {
		bool L_69;
		L_69 = Nullable_1_get_HasValue_m6B76086B0E863AB1D634FD03E30154F230070435_inline((&___forcedNormal5), Nullable_1_get_HasValue_m6B76086B0E863AB1D634FD03E30154F230070435_RuntimeMethod_var);
		if (L_69)
		{
			goto IL_01b0;
		}
	}
	{
		// Vector3 N1 = mesh.normals[triIndexA];
		MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F* L_70 = ___mesh0;
		NullCheck(L_70);
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_71 = L_70->___normals_1;
		int32_t L_72 = V_4;
		NullCheck(L_71);
		int32_t L_73 = L_72;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_74 = (L_71)->GetAt(static_cast<il2cpp_array_size_t>(L_73));
		V_15 = L_74;
		// Vector3 N2 = mesh.normals[triIndexB];
		MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F* L_75 = ___mesh0;
		NullCheck(L_75);
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_76 = L_75->___normals_1;
		int32_t L_77 = V_5;
		NullCheck(L_76);
		int32_t L_78 = L_77;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_79 = (L_76)->GetAt(static_cast<il2cpp_array_size_t>(L_78));
		V_16 = L_79;
		// Vector3 N3 = mesh.normals[triIndexC];
		MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F* L_80 = ___mesh0;
		NullCheck(L_80);
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_81 = L_80->___normals_1;
		int32_t L_82 = V_6;
		NullCheck(L_81);
		int32_t L_83 = L_82;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_84 = (L_81)->GetAt(static_cast<il2cpp_array_size_t>(L_83));
		V_17 = L_84;
		// point.normal = (N1 * BC.x) + (N2 * BC.y) + (N3 * BC.z);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_85 = V_15;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_86 = V_7;
		float L_87 = L_86.___x_2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_88;
		L_88 = Vector3_op_Multiply_m87BA7C578F96C8E49BB07088DAAC4649F83B0353_inline(L_85, L_87, NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_89 = V_16;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_90 = V_7;
		float L_91 = L_90.___y_3;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_92;
		L_92 = Vector3_op_Multiply_m87BA7C578F96C8E49BB07088DAAC4649F83B0353_inline(L_89, L_91, NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_93;
		L_93 = Vector3_op_Addition_m78C0EC70CB66E8DCAC225743D82B268DAEE92067_inline(L_88, L_92, NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_94 = V_17;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_95 = V_7;
		float L_96 = L_95.___z_4;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_97;
		L_97 = Vector3_op_Multiply_m87BA7C578F96C8E49BB07088DAAC4649F83B0353_inline(L_94, L_96, NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_98;
		L_98 = Vector3_op_Addition_m78C0EC70CB66E8DCAC225743D82B268DAEE92067_inline(L_93, L_97, NULL);
		(&V_8)->___normal_1 = L_98;
		goto IL_01be;
	}

IL_01b0:
	{
		// else { point.normal = forcedNormal.Value; }
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_99;
		L_99 = Nullable_1_get_Value_m6A74FA440FE386A9905C61B41B5C261CD9DC4792((&___forcedNormal5), Nullable_1_get_Value_m6A74FA440FE386A9905C61B41B5C261CD9DC4792_RuntimeMethod_var);
		(&V_8)->___normal_1 = L_99;
	}

IL_01be:
	{
		// Vector2 U1 = mesh.UVs[triIndexA];
		MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F* L_100 = ___mesh0;
		NullCheck(L_100);
		Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* L_101 = L_100->___UVs_2;
		int32_t L_102 = V_4;
		NullCheck(L_101);
		int32_t L_103 = L_102;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_104 = (L_101)->GetAt(static_cast<il2cpp_array_size_t>(L_103));
		V_12 = L_104;
		// Vector2 U2 = mesh.UVs[triIndexB];
		MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F* L_105 = ___mesh0;
		NullCheck(L_105);
		Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* L_106 = L_105->___UVs_2;
		int32_t L_107 = V_5;
		NullCheck(L_106);
		int32_t L_108 = L_107;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_109 = (L_106)->GetAt(static_cast<il2cpp_array_size_t>(L_108));
		V_13 = L_109;
		// Vector2 U3 = mesh.UVs[triIndexC];
		MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F* L_110 = ___mesh0;
		NullCheck(L_110);
		Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* L_111 = L_110->___UVs_2;
		int32_t L_112 = V_6;
		NullCheck(L_111);
		int32_t L_113 = L_112;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_114 = (L_111)->GetAt(static_cast<il2cpp_array_size_t>(L_113));
		V_14 = L_114;
		// point.extraData = (U1 * BC.x) + (U2 * BC.y) + (U3 * BC.z);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_115 = V_12;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_116 = V_7;
		float L_117 = L_116.___x_2;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_118;
		L_118 = Vector2_op_Multiply_m2D984B613020089BF5165BA4CA10988E2DC771FE_inline(L_115, L_117, NULL);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_119 = V_13;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_120 = V_7;
		float L_121 = L_120.___y_3;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_122;
		L_122 = Vector2_op_Multiply_m2D984B613020089BF5165BA4CA10988E2DC771FE_inline(L_119, L_121, NULL);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_123;
		L_123 = Vector2_op_Addition_m8136742CE6EE33BA4EB81C5F584678455917D2AE_inline(L_118, L_122, NULL);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_124 = V_14;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_125 = V_7;
		float L_126 = L_125.___z_4;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_127;
		L_127 = Vector2_op_Multiply_m2D984B613020089BF5165BA4CA10988E2DC771FE_inline(L_124, L_126, NULL);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_128;
		L_128 = Vector2_op_Addition_m8136742CE6EE33BA4EB81C5F584678455917D2AE_inline(L_123, L_127, NULL);
		Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 L_129;
		L_129 = Vector4_op_Implicit_mB193CD8DA20DEB9E9F95CFEB5A2B1B9B3B7ECFEB_inline(L_128, NULL);
		(&V_8)->___extraData_3 = L_129;
		// if (useColours) {
		bool L_130 = ___useColours6;
		if (!L_130)
		{
			goto IL_0299;
		}
	}
	{
		// Color C1 = mesh.colours[triIndexA];
		MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F* L_131 = ___mesh0;
		NullCheck(L_131);
		ColorU5BU5D_t612261CF293F6FFC3D80AB52259FF0DC2B2CC389* L_132 = L_131->___colours_3;
		int32_t L_133 = V_4;
		NullCheck(L_132);
		int32_t L_134 = L_133;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_135 = (L_132)->GetAt(static_cast<il2cpp_array_size_t>(L_134));
		V_18 = L_135;
		// Color C2 = mesh.colours[triIndexB];
		MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F* L_136 = ___mesh0;
		NullCheck(L_136);
		ColorU5BU5D_t612261CF293F6FFC3D80AB52259FF0DC2B2CC389* L_137 = L_136->___colours_3;
		int32_t L_138 = V_5;
		NullCheck(L_137);
		int32_t L_139 = L_138;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_140 = (L_137)->GetAt(static_cast<il2cpp_array_size_t>(L_139));
		V_19 = L_140;
		// Color C3 = mesh.colours[triIndexC];
		MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F* L_141 = ___mesh0;
		NullCheck(L_141);
		ColorU5BU5D_t612261CF293F6FFC3D80AB52259FF0DC2B2CC389* L_142 = L_141->___colours_3;
		int32_t L_143 = V_6;
		NullCheck(L_142);
		int32_t L_144 = L_143;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_145 = (L_142)->GetAt(static_cast<il2cpp_array_size_t>(L_144));
		V_20 = L_145;
		// point.color = (C1 * BC.x) + (C2 * BC.y) + (C3 * BC.z);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_146 = V_18;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_147 = V_7;
		float L_148 = L_147.___x_2;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_149;
		L_149 = Color_op_Multiply_m379B20A820266ACF82A21425B9CAE8DCD773CFBB_inline(L_146, L_148, NULL);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_150 = V_19;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_151 = V_7;
		float L_152 = L_151.___y_3;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_153;
		L_153 = Color_op_Multiply_m379B20A820266ACF82A21425B9CAE8DCD773CFBB_inline(L_150, L_152, NULL);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_154;
		L_154 = Color_op_Addition_mA7A51CACA49ED8D23D3D9CA3A0092D32F657E053_inline(L_149, L_153, NULL);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_155 = V_20;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_156 = V_7;
		float L_157 = L_156.___z_4;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_158;
		L_158 = Color_op_Multiply_m379B20A820266ACF82A21425B9CAE8DCD773CFBB_inline(L_155, L_157, NULL);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_159;
		L_159 = Color_op_Addition_mA7A51CACA49ED8D23D3D9CA3A0092D32F657E053_inline(L_154, L_158, NULL);
		(&V_8)->___color_2 = L_159;
		goto IL_02a5;
	}

IL_0299:
	{
		// else { point.color = Color.white; }
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_160;
		L_160 = Color_get_white_m068F5AF879B0FCA584E3693F762EA41BB65532C6_inline(NULL);
		(&V_8)->___color_2 = L_160;
	}

IL_02a5:
	{
		// if (useLength) {
		bool L_161 = ___useLength7;
		if (!L_161)
		{
			goto IL_0313;
		}
	}
	{
		// float L1 = mesh.attributes[triIndexA].y;
		MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F* L_162 = ___mesh0;
		NullCheck(L_162);
		Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* L_163 = L_162->___attributes_5;
		int32_t L_164 = V_4;
		NullCheck(L_163);
		float L_165 = ((L_163)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_164)))->___y_1;
		V_21 = L_165;
		// float L2 = mesh.attributes[triIndexB].y;
		MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F* L_166 = ___mesh0;
		NullCheck(L_166);
		Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* L_167 = L_166->___attributes_5;
		int32_t L_168 = V_5;
		NullCheck(L_167);
		float L_169 = ((L_167)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_168)))->___y_1;
		V_22 = L_169;
		// float L3 = mesh.attributes[triIndexC].y;
		MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F* L_170 = ___mesh0;
		NullCheck(L_170);
		Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* L_171 = L_170->___attributes_5;
		int32_t L_172 = V_6;
		NullCheck(L_171);
		float L_173 = ((L_171)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_172)))->___y_1;
		V_23 = L_173;
		// point.extraData.z = (L1 * BC.x) + (L2 * BC.y) + (L3 * BC.z);
		Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3* L_174 = (&(&V_8)->___extraData_3);
		float L_175 = V_21;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_176 = V_7;
		float L_177 = L_176.___x_2;
		float L_178 = V_22;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_179 = V_7;
		float L_180 = L_179.___y_3;
		float L_181 = V_23;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_182 = V_7;
		float L_183 = L_182.___z_4;
		L_174->___z_3 = ((float)il2cpp_codegen_add(((float)il2cpp_codegen_add(((float)il2cpp_codegen_multiply(L_175, L_177)), ((float)il2cpp_codegen_multiply(L_178, L_180)))), ((float)il2cpp_codegen_multiply(L_181, L_183))));
		goto IL_0324;
	}

IL_0313:
	{
		// else { point.extraData.z = 1f; }
		Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3* L_184 = (&(&V_8)->___extraData_3);
		L_184->___z_3 = (1.0f);
	}

IL_0324:
	{
		// point.extraData.w = Random.value;
		Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3* L_185 = (&(&V_8)->___extraData_3);
		float L_186;
		L_186 = Random_get_value_m2CEA87FADF5222EF9E13D32695F15E2BA282E24B(NULL);
		L_185->___w_4 = L_186;
		// points[i] = point;
		MeshPointU5BU5D_tBCEA150F23C3861E840A87B994115F06D7490DD8* L_187 = V_1;
		int32_t L_188 = V_2;
		MeshPoint_t033BD3FDC9D80AE452F7B3D583D17CDADF299152 L_189 = V_8;
		NullCheck(L_187);
		(L_187)->SetAt(static_cast<il2cpp_array_size_t>(L_188), (MeshPoint_t033BD3FDC9D80AE452F7B3D583D17CDADF299152)L_189);
		// for (int i = 0; i < pointCount; i++) {
		int32_t L_190 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add(L_190, 1));
	}

IL_0342:
	{
		// for (int i = 0; i < pointCount; i++) {
		int32_t L_191 = V_2;
		int32_t L_192 = ___pointCount2;
		if ((((int32_t)L_191) < ((int32_t)L_192)))
		{
			goto IL_001a;
		}
	}
	{
		// Random.state = state;
		State_tA37EB68FE687D41D4B228462D4C7427FAC5BF9C1 L_193 = V_0;
		Random_set_state_m335B3E4E30A9A85E8794BC0A84013F1F365BA179(L_193, NULL);
		// return points;
		MeshPointU5BU5D_tBCEA150F23C3861E840A87B994115F06D7490DD8* L_194 = V_1;
		return L_194;
	}
}
// System.Single[] MicahW.PointGrass.DistributePointsAlongMesh::GetCumulativeTriSizes(System.Int32[],UnityEngine.Vector3[],UnityEngine.Vector2[],UnityEngine.Vector3,System.Boolean,System.Single&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* DistributePointsAlongMesh_GetCumulativeTriSizes_m64D783A59608A96F2FABF19CA7D0BB72F399B18C (Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* ___tris0, Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* ___verts1, Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* ___attributes2, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___scale3, bool ___useDensity4, float* ___totalArea5, const RuntimeMethod* method) 
{
	SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* V_0 = NULL;
	int32_t V_1 = 0;
	bool G_B2_0 = false;
	bool G_B1_0 = false;
	int32_t G_B3_0 = 0;
	bool G_B3_1 = false;
	SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* G_B6_0 = NULL;
	{
		// useDensity &= attributes != null && attributes.Length == verts.Length;
		bool L_0 = ___useDensity4;
		Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* L_1 = ___attributes2;
		G_B1_0 = L_0;
		if (!L_1)
		{
			G_B2_0 = L_0;
			goto IL_000f;
		}
	}
	{
		Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* L_2 = ___attributes2;
		NullCheck(L_2);
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_3 = ___verts1;
		NullCheck(L_3);
		G_B3_0 = ((((int32_t)((int32_t)(((RuntimeArray*)L_2)->max_length))) == ((int32_t)((int32_t)(((RuntimeArray*)L_3)->max_length))))? 1 : 0);
		G_B3_1 = G_B1_0;
		goto IL_0010;
	}

IL_000f:
	{
		G_B3_0 = 0;
		G_B3_1 = G_B2_0;
	}

IL_0010:
	{
		___useDensity4 = (bool)((int32_t)((int32_t)G_B3_1&G_B3_0));
		// float[] sizes = useDensity ? GetWeightedTriSizes(tris, verts, attributes, scale) : GetTriSizes(tris, verts, scale);
		bool L_4 = ___useDensity4;
		if (L_4)
		{
			goto IL_0021;
		}
	}
	{
		Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_5 = ___tris0;
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_6 = ___verts1;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_7 = ___scale3;
		SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* L_8;
		L_8 = DistributePointsAlongMesh_GetTriSizes_m8FCA660CA122706DD76F206211626C4421F8439B(L_5, L_6, L_7, NULL);
		G_B6_0 = L_8;
		goto IL_002a;
	}

IL_0021:
	{
		Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_9 = ___tris0;
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_10 = ___verts1;
		Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* L_11 = ___attributes2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_12 = ___scale3;
		SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* L_13;
		L_13 = DistributePointsAlongMesh_GetWeightedTriSizes_m7C97E84C6C9894F39FBE0A9E10D6581E58EF2C97(L_9, L_10, L_11, L_12, NULL);
		G_B6_0 = L_13;
	}

IL_002a:
	{
		V_0 = G_B6_0;
		// if (sizes == null || sizes.Length == 0) { totalArea = 0f; return null; }
		SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* L_14 = V_0;
		if (!L_14)
		{
			goto IL_0032;
		}
	}
	{
		SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* L_15 = V_0;
		NullCheck(L_15);
		if ((((RuntimeArray*)L_15)->max_length))
		{
			goto IL_003c;
		}
	}

IL_0032:
	{
		// if (sizes == null || sizes.Length == 0) { totalArea = 0f; return null; }
		float* L_16 = ___totalArea5;
		*((float*)L_16) = (float)(0.0f);
		// if (sizes == null || sizes.Length == 0) { totalArea = 0f; return null; }
		return (SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C*)NULL;
	}

IL_003c:
	{
		// totalArea = sizes[0];
		float* L_17 = ___totalArea5;
		SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* L_18 = V_0;
		NullCheck(L_18);
		int32_t L_19 = 0;
		float L_20 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
		*((float*)L_17) = (float)L_20;
		// for (int i = 1; i < sizes.Length; i++) {
		V_1 = 1;
		goto IL_005a;
	}

IL_0046:
	{
		// totalArea += sizes[i];
		float* L_21 = ___totalArea5;
		float* L_22 = ___totalArea5;
		float L_23 = *((float*)L_22);
		SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* L_24 = V_0;
		int32_t L_25 = V_1;
		NullCheck(L_24);
		int32_t L_26 = L_25;
		float L_27 = (L_24)->GetAt(static_cast<il2cpp_array_size_t>(L_26));
		*((float*)L_21) = (float)((float)il2cpp_codegen_add(L_23, L_27));
		// sizes[i] = totalArea;
		SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* L_28 = V_0;
		int32_t L_29 = V_1;
		float* L_30 = ___totalArea5;
		float L_31 = *((float*)L_30);
		NullCheck(L_28);
		(L_28)->SetAt(static_cast<il2cpp_array_size_t>(L_29), (float)L_31);
		// for (int i = 1; i < sizes.Length; i++) {
		int32_t L_32 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add(L_32, 1));
	}

IL_005a:
	{
		// for (int i = 1; i < sizes.Length; i++) {
		int32_t L_33 = V_1;
		SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* L_34 = V_0;
		NullCheck(L_34);
		if ((((int32_t)L_33) < ((int32_t)((int32_t)(((RuntimeArray*)L_34)->max_length)))))
		{
			goto IL_0046;
		}
	}
	{
		// return sizes;
		SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* L_35 = V_0;
		return L_35;
	}
}
// System.Single[] MicahW.PointGrass.DistributePointsAlongMesh::GetTriSizes(System.Int32[],UnityEngine.Vector3[],UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* DistributePointsAlongMesh_GetTriSizes_m8FCA660CA122706DD76F206211626C4421F8439B (Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* ___tris0, Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* ___verts1, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___scale2, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* V_1 = NULL;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_6;
	memset((&V_6), 0, sizeof(V_6));
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_7;
	memset((&V_7), 0, sizeof(V_7));
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_8;
	memset((&V_8), 0, sizeof(V_8));
	{
		// int triCount = tris.Length / 3;
		Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_0 = ___tris0;
		NullCheck(L_0);
		V_0 = ((int32_t)(((int32_t)(((RuntimeArray*)L_0)->max_length))/3));
		// float[] sizes = new float[triCount];
		int32_t L_1 = V_0;
		SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* L_2 = (SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C*)(SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C*)SZArrayNew(SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C_il2cpp_TypeInfo_var, (uint32_t)L_1);
		V_1 = L_2;
		// for (int i = 0; i < triCount; i++) {
		V_2 = 0;
		goto IL_0085;
	}

IL_0011:
	{
		// int vertA = tris[i * 3 + 1];
		Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_3 = ___tris0;
		int32_t L_4 = V_2;
		NullCheck(L_3);
		int32_t L_5 = ((int32_t)il2cpp_codegen_add(((int32_t)il2cpp_codegen_multiply(L_4, 3)), 1));
		int32_t L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		V_3 = L_6;
		// int vertB = tris[i * 3];
		Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_7 = ___tris0;
		int32_t L_8 = V_2;
		NullCheck(L_7);
		int32_t L_9 = ((int32_t)il2cpp_codegen_multiply(L_8, 3));
		int32_t L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		V_4 = L_10;
		// int vertC = tris[i * 3 + 2];
		Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_11 = ___tris0;
		int32_t L_12 = V_2;
		NullCheck(L_11);
		int32_t L_13 = ((int32_t)il2cpp_codegen_add(((int32_t)il2cpp_codegen_multiply(L_12, 3)), 2));
		int32_t L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		V_5 = L_14;
		// Vector3 vecB = verts[vertA] - verts[vertB]; vecB.Scale(scale);
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_15 = ___verts1;
		int32_t L_16 = V_3;
		NullCheck(L_15);
		int32_t L_17 = L_16;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_18 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_19 = ___verts1;
		int32_t L_20 = V_4;
		NullCheck(L_19);
		int32_t L_21 = L_20;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_22 = (L_19)->GetAt(static_cast<il2cpp_array_size_t>(L_21));
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_23;
		L_23 = Vector3_op_Subtraction_mE42023FF80067CB44A1D4A27EB7CF2B24CABB828_inline(L_18, L_22, NULL);
		V_6 = L_23;
		// Vector3 vecB = verts[vertA] - verts[vertB]; vecB.Scale(scale);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_24 = ___scale2;
		Vector3_Scale_mE0DC2C1B7902271788591F17DBE7F7F72EC37283_inline((&V_6), L_24, NULL);
		// Vector3 vecC = verts[vertC] - verts[vertB]; vecC.Scale(scale);
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_25 = ___verts1;
		int32_t L_26 = V_5;
		NullCheck(L_25);
		int32_t L_27 = L_26;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_28 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_29 = ___verts1;
		int32_t L_30 = V_4;
		NullCheck(L_29);
		int32_t L_31 = L_30;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_32 = (L_29)->GetAt(static_cast<il2cpp_array_size_t>(L_31));
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_33;
		L_33 = Vector3_op_Subtraction_mE42023FF80067CB44A1D4A27EB7CF2B24CABB828_inline(L_28, L_32, NULL);
		V_7 = L_33;
		// Vector3 vecC = verts[vertC] - verts[vertB]; vecC.Scale(scale);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_34 = ___scale2;
		Vector3_Scale_mE0DC2C1B7902271788591F17DBE7F7F72EC37283_inline((&V_7), L_34, NULL);
		// Vector3 cross = Vector3.Cross(vecB, vecC);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_35 = V_6;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_36 = V_7;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_37;
		L_37 = Vector3_Cross_mF93A280558BCE756D13B6CC5DCD7DE8A43148987_inline(L_35, L_36, NULL);
		V_8 = L_37;
		// sizes[i] = .5f * cross.magnitude;
		SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* L_38 = V_1;
		int32_t L_39 = V_2;
		float L_40;
		L_40 = Vector3_get_magnitude_mF0D6017E90B345F1F52D1CC564C640F1A847AF2D_inline((&V_8), NULL);
		NullCheck(L_38);
		(L_38)->SetAt(static_cast<il2cpp_array_size_t>(L_39), (float)((float)il2cpp_codegen_multiply((0.5f), L_40)));
		// for (int i = 0; i < triCount; i++) {
		int32_t L_41 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add(L_41, 1));
	}

IL_0085:
	{
		// for (int i = 0; i < triCount; i++) {
		int32_t L_42 = V_2;
		int32_t L_43 = V_0;
		if ((((int32_t)L_42) < ((int32_t)L_43)))
		{
			goto IL_0011;
		}
	}
	{
		// return sizes;
		SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* L_44 = V_1;
		return L_44;
	}
}
// System.Single[] MicahW.PointGrass.DistributePointsAlongMesh::GetWeightedTriSizes(System.Int32[],UnityEngine.Vector3[],UnityEngine.Vector2[],UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* DistributePointsAlongMesh_GetWeightedTriSizes_m7C97E84C6C9894F39FBE0A9E10D6581E58EF2C97 (Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* ___tris0, Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* ___verts1, Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* ___attributes2, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___scale3, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* V_1 = NULL;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	float V_6 = 0.0f;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_7;
	memset((&V_7), 0, sizeof(V_7));
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_8;
	memset((&V_8), 0, sizeof(V_8));
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_9;
	memset((&V_9), 0, sizeof(V_9));
	{
		// int triCount = tris.Length / 3;
		Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_0 = ___tris0;
		NullCheck(L_0);
		V_0 = ((int32_t)(((int32_t)(((RuntimeArray*)L_0)->max_length))/3));
		// float[] sizes = new float[triCount];
		int32_t L_1 = V_0;
		SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* L_2 = (SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C*)(SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C*)SZArrayNew(SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C_il2cpp_TypeInfo_var, (uint32_t)L_1);
		V_1 = L_2;
		// for (int i = 0; i < triCount; i++) {
		V_2 = 0;
		goto IL_00bb;
	}

IL_0014:
	{
		// int vertA = tris[i * 3 + 1];
		Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_3 = ___tris0;
		int32_t L_4 = V_2;
		NullCheck(L_3);
		int32_t L_5 = ((int32_t)il2cpp_codegen_add(((int32_t)il2cpp_codegen_multiply(L_4, 3)), 1));
		int32_t L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		V_3 = L_6;
		// int vertB = tris[i * 3];
		Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_7 = ___tris0;
		int32_t L_8 = V_2;
		NullCheck(L_7);
		int32_t L_9 = ((int32_t)il2cpp_codegen_multiply(L_8, 3));
		int32_t L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		V_4 = L_10;
		// int vertC = tris[i * 3 + 2];
		Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_11 = ___tris0;
		int32_t L_12 = V_2;
		NullCheck(L_11);
		int32_t L_13 = ((int32_t)il2cpp_codegen_add(((int32_t)il2cpp_codegen_multiply(L_12, 3)), 2));
		int32_t L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		V_5 = L_14;
		// float factor = (attributes[vertA].x + attributes[vertB].x + attributes[vertC].x) / 3f;
		Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* L_15 = ___attributes2;
		int32_t L_16 = V_3;
		NullCheck(L_15);
		float L_17 = ((L_15)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_16)))->___x_0;
		Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* L_18 = ___attributes2;
		int32_t L_19 = V_4;
		NullCheck(L_18);
		float L_20 = ((L_18)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_19)))->___x_0;
		Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* L_21 = ___attributes2;
		int32_t L_22 = V_5;
		NullCheck(L_21);
		float L_23 = ((L_21)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_22)))->___x_0;
		V_6 = ((float)(((float)il2cpp_codegen_add(((float)il2cpp_codegen_add(L_17, L_20)), L_23))/(3.0f)));
		// Vector3 vecB = verts[vertA] - verts[vertB]; vecB.Scale(scale);
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_24 = ___verts1;
		int32_t L_25 = V_3;
		NullCheck(L_24);
		int32_t L_26 = L_25;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_27 = (L_24)->GetAt(static_cast<il2cpp_array_size_t>(L_26));
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_28 = ___verts1;
		int32_t L_29 = V_4;
		NullCheck(L_28);
		int32_t L_30 = L_29;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_31 = (L_28)->GetAt(static_cast<il2cpp_array_size_t>(L_30));
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_32;
		L_32 = Vector3_op_Subtraction_mE42023FF80067CB44A1D4A27EB7CF2B24CABB828_inline(L_27, L_31, NULL);
		V_7 = L_32;
		// Vector3 vecB = verts[vertA] - verts[vertB]; vecB.Scale(scale);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_33 = ___scale3;
		Vector3_Scale_mE0DC2C1B7902271788591F17DBE7F7F72EC37283_inline((&V_7), L_33, NULL);
		// Vector3 vecC = verts[vertC] - verts[vertB]; vecC.Scale(scale);
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_34 = ___verts1;
		int32_t L_35 = V_5;
		NullCheck(L_34);
		int32_t L_36 = L_35;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_37 = (L_34)->GetAt(static_cast<il2cpp_array_size_t>(L_36));
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_38 = ___verts1;
		int32_t L_39 = V_4;
		NullCheck(L_38);
		int32_t L_40 = L_39;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_41 = (L_38)->GetAt(static_cast<il2cpp_array_size_t>(L_40));
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_42;
		L_42 = Vector3_op_Subtraction_mE42023FF80067CB44A1D4A27EB7CF2B24CABB828_inline(L_37, L_41, NULL);
		V_8 = L_42;
		// Vector3 vecC = verts[vertC] - verts[vertB]; vecC.Scale(scale);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_43 = ___scale3;
		Vector3_Scale_mE0DC2C1B7902271788591F17DBE7F7F72EC37283_inline((&V_8), L_43, NULL);
		// sizes[i] = factor * .5f * Vector3.Cross(vecB, vecC).magnitude;
		SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* L_44 = V_1;
		int32_t L_45 = V_2;
		float L_46 = V_6;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_47 = V_7;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_48 = V_8;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_49;
		L_49 = Vector3_Cross_mF93A280558BCE756D13B6CC5DCD7DE8A43148987_inline(L_47, L_48, NULL);
		V_9 = L_49;
		float L_50;
		L_50 = Vector3_get_magnitude_mF0D6017E90B345F1F52D1CC564C640F1A847AF2D_inline((&V_9), NULL);
		NullCheck(L_44);
		(L_44)->SetAt(static_cast<il2cpp_array_size_t>(L_45), (float)((float)il2cpp_codegen_multiply(((float)il2cpp_codegen_multiply(L_46, (0.5f))), L_50)));
		// for (int i = 0; i < triCount; i++) {
		int32_t L_51 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add(L_51, 1));
	}

IL_00bb:
	{
		// for (int i = 0; i < triCount; i++) {
		int32_t L_52 = V_2;
		int32_t L_53 = V_0;
		if ((((int32_t)L_52) < ((int32_t)L_53)))
		{
			goto IL_0014;
		}
	}
	{
		// return sizes;
		SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* L_54 = V_1;
		return L_54;
	}
}
// System.Int32 MicahW.PointGrass.DistributePointsAlongMesh::FindTriangleIndex(System.Single[],System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t DistributePointsAlongMesh_FindTriangleIndex_mFA277A2DD05D747D627B4504C44E8BED5F0F8DFA (SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* ___cumulativeTriSizes0, float ___randomSample1, const RuntimeMethod* method) 
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		// int low = 0;
		V_0 = 0;
		// int high = cumulativeTriSizes.Length - 1;
		SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* L_0 = ___cumulativeTriSizes0;
		NullCheck(L_0);
		V_1 = ((int32_t)il2cpp_codegen_subtract(((int32_t)(((RuntimeArray*)L_0)->max_length)), 1));
		goto IL_001e;
	}

IL_000a:
	{
		// int mid = (low + high) / 2;
		int32_t L_1 = V_0;
		int32_t L_2 = V_1;
		V_2 = ((int32_t)(((int32_t)il2cpp_codegen_add(L_1, L_2))/2));
		// if (cumulativeTriSizes[mid] > randomSample) { high = mid; }
		SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* L_3 = ___cumulativeTriSizes0;
		int32_t L_4 = V_2;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		float L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		float L_7 = ___randomSample1;
		if ((!(((float)L_6) > ((float)L_7))))
		{
			goto IL_001a;
		}
	}
	{
		// if (cumulativeTriSizes[mid] > randomSample) { high = mid; }
		int32_t L_8 = V_2;
		V_1 = L_8;
		goto IL_001e;
	}

IL_001a:
	{
		// else { low = mid + 1; }
		int32_t L_9 = V_2;
		V_0 = ((int32_t)il2cpp_codegen_add(L_9, 1));
	}

IL_001e:
	{
		// while (low < high) {
		int32_t L_10 = V_0;
		int32_t L_11 = V_1;
		if ((((int32_t)L_10) < ((int32_t)L_11)))
		{
			goto IL_000a;
		}
	}
	{
		// int triIndex = low * 3;
		int32_t L_12 = V_0;
		// return triIndex;
		return ((int32_t)il2cpp_codegen_multiply(L_12, 3));
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean MicahW.PointGrass.PointGrassCommon::get_BladeMeshesGenerated()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PointGrassCommon_get_BladeMeshesGenerated_m4E21A4A4AC4828F974670684D93241428FCBD935 (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static bool BladeMeshesGenerated { get { return grassMeshFlat != null && grassMeshCyl != null; } }
		Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* L_0 = ((PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_StaticFields*)il2cpp_codegen_static_fields_for(PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_il2cpp_TypeInfo_var))->___grassMeshFlat_0;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Inequality_mD0BE578448EAA61948F25C32F8DD55AB1F778602(L_0, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* L_2 = ((PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_StaticFields*)il2cpp_codegen_static_fields_for(PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_il2cpp_TypeInfo_var))->___grassMeshCyl_1;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_3;
		L_3 = Object_op_Inequality_mD0BE578448EAA61948F25C32F8DD55AB1F778602(L_2, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		return L_3;
	}

IL_0019:
	{
		return (bool)0;
	}
}
// System.Boolean MicahW.PointGrass.PointGrassCommon::get_PropertyIDsInitialized()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PointGrassCommon_get_PropertyIDsInitialized_m9958FC8398CDF68DA0FFF72DAA3C35469B07D1A1 (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static bool PropertyIDsInitialized { get; private set; }
		bool L_0 = ((PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_StaticFields*)il2cpp_codegen_static_fields_for(PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_il2cpp_TypeInfo_var))->___U3CPropertyIDsInitializedU3Ek__BackingField_7;
		return L_0;
	}
}
// System.Void MicahW.PointGrass.PointGrassCommon::set_PropertyIDsInitialized(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointGrassCommon_set_PropertyIDsInitialized_mAC9F656EF7F0EF2F47B26CD89EC6BE6CD235245D (bool ___value0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static bool PropertyIDsInitialized { get; private set; }
		bool L_0 = ___value0;
		((PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_StaticFields*)il2cpp_codegen_static_fields_for(PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_il2cpp_TypeInfo_var))->___U3CPropertyIDsInitializedU3Ek__BackingField_7 = L_0;
		return;
	}
}
// System.Void MicahW.PointGrass.PointGrassCommon::FindPropertyIDs()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointGrassCommon_FindPropertyIDs_m4B83779C3F568CC8BFEEFEC4FB0A50B525997711 (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral0DE7A6FC33D54DC5C50D28AE72F63A8EF3D50C0E);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral37C137AFA042CC6217D39FFF28A6DC2896EC7CC6);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral4970116458FF1090C3095CD0661522B9014F1D2E);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral527CF25F73BD2E1EEA838CA0E9911C736700293E);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral68F6AFD1C49DB84D0049EF8E3B88393FB2EF1B4E);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (PropertyIDsInitialized) { return; }
		bool L_0;
		L_0 = PointGrassCommon_get_PropertyIDsInitialized_m9958FC8398CDF68DA0FFF72DAA3C35469B07D1A1_inline(NULL);
		if (!L_0)
		{
			goto IL_0008;
		}
	}
	{
		// if (PropertyIDsInitialized) { return; }
		return;
	}

IL_0008:
	{
		// ID_PointBuff = Shader.PropertyToID("_MeshPoints");
		int32_t L_1;
		L_1 = Shader_PropertyToID_mE98523D50F5656CAE89B30695C458253EB8956CA(_stringLiteral0DE7A6FC33D54DC5C50D28AE72F63A8EF3D50C0E, NULL);
		((PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_StaticFields*)il2cpp_codegen_static_fields_for(PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_il2cpp_TypeInfo_var))->___ID_PointBuff_2 = L_1;
		// ID_ObjBuff = Shader.PropertyToID("_DisplacementObjects");
		int32_t L_2;
		L_2 = Shader_PropertyToID_mE98523D50F5656CAE89B30695C458253EB8956CA(_stringLiteral37C137AFA042CC6217D39FFF28A6DC2896EC7CC6, NULL);
		((PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_StaticFields*)il2cpp_codegen_static_fields_for(PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_il2cpp_TypeInfo_var))->___ID_ObjBuff_3 = L_2;
		// ID_ObjCount = Shader.PropertyToID("_DisplacementCount");
		int32_t L_3;
		L_3 = Shader_PropertyToID_mE98523D50F5656CAE89B30695C458253EB8956CA(_stringLiteral527CF25F73BD2E1EEA838CA0E9911C736700293E, NULL);
		((PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_StaticFields*)il2cpp_codegen_static_fields_for(PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_il2cpp_TypeInfo_var))->___ID_ObjCount_4 = L_3;
		// ID_MatrixL2W = Shader.PropertyToID("_ObjMatrix_L2W");
		int32_t L_4;
		L_4 = Shader_PropertyToID_mE98523D50F5656CAE89B30695C458253EB8956CA(_stringLiteral68F6AFD1C49DB84D0049EF8E3B88393FB2EF1B4E, NULL);
		((PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_StaticFields*)il2cpp_codegen_static_fields_for(PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_il2cpp_TypeInfo_var))->___ID_MatrixL2W_5 = L_4;
		// ID_MatrixW2L = Shader.PropertyToID("_ObjMatrix_W2L");
		int32_t L_5;
		L_5 = Shader_PropertyToID_mE98523D50F5656CAE89B30695C458253EB8956CA(_stringLiteral4970116458FF1090C3095CD0661522B9014F1D2E, NULL);
		((PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_StaticFields*)il2cpp_codegen_static_fields_for(PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_il2cpp_TypeInfo_var))->___ID_MatrixW2L_6 = L_5;
		// PropertyIDsInitialized = true;
		PointGrassCommon_set_PropertyIDsInitialized_mAC9F656EF7F0EF2F47B26CD89EC6BE6CD235245D_inline((bool)1, NULL);
		// }
		return;
	}
}
// System.Void MicahW.PointGrass.PointGrassCommon::UpdateMaterialPropertyBlock(UnityEngine.MaterialPropertyBlock&,UnityEngine.Transform)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointGrassCommon_UpdateMaterialPropertyBlock_mC29A761CEB43323C5150BA2BBFDB228ABA7427C6 (MaterialPropertyBlock_t2308669579033A857EFE6E4831909F638B27411D** ___block0, Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___trans1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (block == null || trans == null) { return; }
		MaterialPropertyBlock_t2308669579033A857EFE6E4831909F638B27411D** L_0 = ___block0;
		MaterialPropertyBlock_t2308669579033A857EFE6E4831909F638B27411D* L_1 = *((MaterialPropertyBlock_t2308669579033A857EFE6E4831909F638B27411D**)L_0);
		if (!L_1)
		{
			goto IL_000d;
		}
	}
	{
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_2 = ___trans1;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_3;
		L_3 = Object_op_Equality_mB6120F782D83091EF56A198FCEBCF066DB4A9605(L_2, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_3)
		{
			goto IL_000e;
		}
	}

IL_000d:
	{
		// if (block == null || trans == null) { return; }
		return;
	}

IL_000e:
	{
		// block.SetMatrix(ID_MatrixL2W, trans.localToWorldMatrix);
		MaterialPropertyBlock_t2308669579033A857EFE6E4831909F638B27411D** L_4 = ___block0;
		MaterialPropertyBlock_t2308669579033A857EFE6E4831909F638B27411D* L_5 = *((MaterialPropertyBlock_t2308669579033A857EFE6E4831909F638B27411D**)L_4);
		int32_t L_6 = ((PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_StaticFields*)il2cpp_codegen_static_fields_for(PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_il2cpp_TypeInfo_var))->___ID_MatrixL2W_5;
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_7 = ___trans1;
		NullCheck(L_7);
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_8;
		L_8 = Transform_get_localToWorldMatrix_m5D35188766856338DD21DE756F42277C21719E6D(L_7, NULL);
		NullCheck(L_5);
		MaterialPropertyBlock_SetMatrix_mA86792A03023DC1F6B46B06C72D61F3CCE4177AC(L_5, L_6, L_8, NULL);
		// block.SetMatrix(ID_MatrixW2L, trans.worldToLocalMatrix);
		MaterialPropertyBlock_t2308669579033A857EFE6E4831909F638B27411D** L_9 = ___block0;
		MaterialPropertyBlock_t2308669579033A857EFE6E4831909F638B27411D* L_10 = *((MaterialPropertyBlock_t2308669579033A857EFE6E4831909F638B27411D**)L_9);
		int32_t L_11 = ((PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_StaticFields*)il2cpp_codegen_static_fields_for(PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_il2cpp_TypeInfo_var))->___ID_MatrixW2L_6;
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_12 = ___trans1;
		NullCheck(L_12);
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_13;
		L_13 = Transform_get_worldToLocalMatrix_mB633C122A01BCE8E51B10B8B8CB95F580750B3F1(L_12, NULL);
		NullCheck(L_10);
		MaterialPropertyBlock_SetMatrix_mA86792A03023DC1F6B46B06C72D61F3CCE4177AC(L_10, L_11, L_13, NULL);
		// if (PointGrassDisplacementManager.instance != null) {
		il2cpp_codegen_runtime_class_init_inline(PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900_il2cpp_TypeInfo_var);
		PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900* L_14 = ((PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900_StaticFields*)il2cpp_codegen_static_fields_for(PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900_il2cpp_TypeInfo_var))->___instance_4;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_15;
		L_15 = Object_op_Inequality_mD0BE578448EAA61948F25C32F8DD55AB1F778602(L_14, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_15)
		{
			goto IL_004a;
		}
	}
	{
		// PointGrassDisplacementManager.instance.UpdatePropertyBlock(ref block);
		il2cpp_codegen_runtime_class_init_inline(PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900_il2cpp_TypeInfo_var);
		PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900* L_16 = ((PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900_StaticFields*)il2cpp_codegen_static_fields_for(PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900_il2cpp_TypeInfo_var))->___instance_4;
		MaterialPropertyBlock_t2308669579033A857EFE6E4831909F638B27411D** L_17 = ___block0;
		NullCheck(L_16);
		PointGrassDisplacementManager_UpdatePropertyBlock_mE705BEB14C825F65DBF87F0DA864B492DFBAC365(L_16, L_17, NULL);
	}

IL_004a:
	{
		// }
		return;
	}
}
// System.Void MicahW.PointGrass.PointGrassCommon::GenerateGrassMeshes()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointGrassCommon_GenerateGrassMeshes_m1187A0027530C7B935B788DF2975503346224C28 (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// grassMeshFlat = GenerateGrassMesh_Flat();
		Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* L_0;
		L_0 = PointGrassCommon_GenerateGrassMesh_Flat_m0B8138A7DAEFC9028E47E9D68ACCCC39A3B2E23B(3, NULL);
		((PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_StaticFields*)il2cpp_codegen_static_fields_for(PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_il2cpp_TypeInfo_var))->___grassMeshFlat_0 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&((PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_StaticFields*)il2cpp_codegen_static_fields_for(PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_il2cpp_TypeInfo_var))->___grassMeshFlat_0), (void*)L_0);
		// grassMeshCyl = GenerateGrassMesh_Cylinder();
		Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* L_1;
		L_1 = PointGrassCommon_GenerateGrassMesh_Cylinder_m3290C77744AA3E4E2C99868E8D082E81EC4A9D0F(3, 4, NULL);
		((PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_StaticFields*)il2cpp_codegen_static_fields_for(PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_il2cpp_TypeInfo_var))->___grassMeshCyl_1 = L_1;
		Il2CppCodeGenWriteBarrier((void**)(&((PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_StaticFields*)il2cpp_codegen_static_fields_for(PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_il2cpp_TypeInfo_var))->___grassMeshCyl_1), (void*)L_1);
		// }
		return;
	}
}
// UnityEngine.Mesh MicahW.PointGrass.PointGrassCommon::GenerateGrassMesh_Flat(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* PointGrassCommon_GenerateGrassMesh_Flat_m0B8138A7DAEFC9028E47E9D68ACCCC39A3B2E23B (int32_t ___divisions0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ColorU5BU5D_t612261CF293F6FFC3D80AB52259FF0DC2B2CC389_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral70A3E84D94AAB3B2B54557DF4CDB0569C20B19BA);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* V_2 = NULL;
	Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* V_3 = NULL;
	ColorU5BU5D_t612261CF293F6FFC3D80AB52259FF0DC2B2CC389* V_4 = NULL;
	float V_5 = 0.0f;
	Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* V_6 = NULL;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	int32_t V_9 = 0;
	float V_10 = 0.0f;
	float V_11 = 0.0f;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_12;
	memset((&V_12), 0, sizeof(V_12));
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 V_13;
	memset((&V_13), 0, sizeof(V_13));
	int32_t V_14 = 0;
	int32_t V_15 = 0;
	{
		// divisions = Mathf.Max(divisions, 0);
		int32_t L_0 = ___divisions0;
		int32_t L_1;
		L_1 = Mathf_Max_m7FA442918DE37E3A00106D1F2E789D65829792B8_inline(L_0, 0, NULL);
		___divisions0 = L_1;
		// int vertCount = divisions * 2 + 3;
		int32_t L_2 = ___divisions0;
		V_0 = ((int32_t)il2cpp_codegen_add(((int32_t)il2cpp_codegen_multiply(L_2, 2)), 3));
		// int triCount = divisions * 2 + 1;
		int32_t L_3 = ___divisions0;
		V_1 = ((int32_t)il2cpp_codegen_add(((int32_t)il2cpp_codegen_multiply(L_3, 2)), 1));
		// Vector3[] verts = new Vector3[vertCount];
		int32_t L_4 = V_0;
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_5 = (Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C*)(Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C*)SZArrayNew(Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C_il2cpp_TypeInfo_var, (uint32_t)L_4);
		V_2 = L_5;
		// Vector2[] uvs = new Vector2[vertCount];
		int32_t L_6 = V_0;
		Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* L_7 = (Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA*)(Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA*)SZArrayNew(Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA_il2cpp_TypeInfo_var, (uint32_t)L_6);
		V_3 = L_7;
		// Color[] cols = new Color[vertCount];
		int32_t L_8 = V_0;
		ColorU5BU5D_t612261CF293F6FFC3D80AB52259FF0DC2B2CC389* L_9 = (ColorU5BU5D_t612261CF293F6FFC3D80AB52259FF0DC2B2CC389*)(ColorU5BU5D_t612261CF293F6FFC3D80AB52259FF0DC2B2CC389*)SZArrayNew(ColorU5BU5D_t612261CF293F6FFC3D80AB52259FF0DC2B2CC389_il2cpp_TypeInfo_var, (uint32_t)L_8);
		V_4 = L_9;
		// float divisor = divisions + 1f;
		int32_t L_10 = ___divisions0;
		V_5 = ((float)il2cpp_codegen_add(((float)L_10), (1.0f)));
		// for (int i = 0; i < vertCount; i++) {
		V_9 = 0;
		goto IL_00cd;
	}

IL_003d:
	{
		// float height = (i >> 1) / divisor;
		int32_t L_11 = V_9;
		float L_12 = V_5;
		V_10 = ((float)(((float)((int32_t)(L_11>>1)))/L_12));
		// float radius = Mathf.Cos(height * Mathf.PI * 0.5f);
		float L_13 = V_10;
		float L_14;
		L_14 = cosf(((float)il2cpp_codegen_multiply(((float)il2cpp_codegen_multiply(L_13, (3.14159274f))), (0.5f))));
		V_11 = L_14;
		// if (i % 2 == 1) { radius = -radius; }
		int32_t L_15 = V_9;
		if ((!(((uint32_t)((int32_t)(L_15%2))) == ((uint32_t)1))))
		{
			goto IL_0068;
		}
	}
	{
		// if (i % 2 == 1) { radius = -radius; }
		float L_16 = V_11;
		V_11 = ((-L_16));
	}

IL_0068:
	{
		// Vector3 pos = new Vector3(radius, height, 0f);
		float L_17 = V_11;
		float L_18 = V_10;
		Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline((&V_12), L_17, L_18, (0.0f), NULL);
		// Vector2 uv = new Vector2(radius * 0.5f + 0.5f, height);
		float L_19 = V_11;
		float L_20 = V_10;
		Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline((&V_13), ((float)il2cpp_codegen_add(((float)il2cpp_codegen_multiply(L_19, (0.5f))), (0.5f))), L_20, NULL);
		// verts[i] = pos;
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_21 = V_2;
		int32_t L_22 = V_9;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_23 = V_12;
		NullCheck(L_21);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(L_22), (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2)L_23);
		// uvs[i] = uv;
		Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* L_24 = V_3;
		int32_t L_25 = V_9;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_26 = V_13;
		NullCheck(L_24);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(L_25), (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7)L_26);
		// cols[i] = new Color(1f, 1f, 1f, uv.y);
		ColorU5BU5D_t612261CF293F6FFC3D80AB52259FF0DC2B2CC389* L_27 = V_4;
		int32_t L_28 = V_9;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_29 = V_13;
		float L_30 = L_29.___y_1;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_31;
		memset((&L_31), 0, sizeof(L_31));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_31), (1.0f), (1.0f), (1.0f), L_30, /*hidden argument*/NULL);
		NullCheck(L_27);
		(L_27)->SetAt(static_cast<il2cpp_array_size_t>(L_28), (Color_tD001788D726C3A7F1379BEED0260B9591F440C1F)L_31);
		// for (int i = 0; i < vertCount; i++) {
		int32_t L_32 = V_9;
		V_9 = ((int32_t)il2cpp_codegen_add(L_32, 1));
	}

IL_00cd:
	{
		// for (int i = 0; i < vertCount; i++) {
		int32_t L_33 = V_9;
		int32_t L_34 = V_0;
		if ((((int32_t)L_33) < ((int32_t)L_34)))
		{
			goto IL_003d;
		}
	}
	{
		// int[] tris = new int[triCount * 3];
		int32_t L_35 = V_1;
		Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_36 = (Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C*)(Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C*)SZArrayNew(Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C_il2cpp_TypeInfo_var, (uint32_t)((int32_t)il2cpp_codegen_multiply(L_35, 3)));
		V_6 = L_36;
		// int vertPnt = 0;
		V_7 = 0;
		// for (int i = 0; i < divisions; i++) {
		V_14 = 0;
		goto IL_0135;
	}

IL_00e7:
	{
		// int a = i * 6;
		int32_t L_37 = V_14;
		V_15 = ((int32_t)il2cpp_codegen_multiply(L_37, 6));
		// tris[a] = vertPnt;
		Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_38 = V_6;
		int32_t L_39 = V_15;
		int32_t L_40 = V_7;
		NullCheck(L_38);
		(L_38)->SetAt(static_cast<il2cpp_array_size_t>(L_39), (int32_t)L_40);
		// tris[a + 1] = vertPnt + 1;
		Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_41 = V_6;
		int32_t L_42 = V_15;
		int32_t L_43 = V_7;
		NullCheck(L_41);
		(L_41)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)il2cpp_codegen_add(L_42, 1))), (int32_t)((int32_t)il2cpp_codegen_add(L_43, 1)));
		// tris[a + 2] = vertPnt + 3;
		Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_44 = V_6;
		int32_t L_45 = V_15;
		int32_t L_46 = V_7;
		NullCheck(L_44);
		(L_44)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)il2cpp_codegen_add(L_45, 2))), (int32_t)((int32_t)il2cpp_codegen_add(L_46, 3)));
		// tris[a + 3] = vertPnt;
		Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_47 = V_6;
		int32_t L_48 = V_15;
		int32_t L_49 = V_7;
		NullCheck(L_47);
		(L_47)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)il2cpp_codegen_add(L_48, 3))), (int32_t)L_49);
		// tris[a + 4] = vertPnt + 3;
		Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_50 = V_6;
		int32_t L_51 = V_15;
		int32_t L_52 = V_7;
		NullCheck(L_50);
		(L_50)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)il2cpp_codegen_add(L_51, 4))), (int32_t)((int32_t)il2cpp_codegen_add(L_52, 3)));
		// tris[a + 5] = vertPnt + 2;
		Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_53 = V_6;
		int32_t L_54 = V_15;
		int32_t L_55 = V_7;
		NullCheck(L_53);
		(L_53)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)il2cpp_codegen_add(L_54, 5))), (int32_t)((int32_t)il2cpp_codegen_add(L_55, 2)));
		// vertPnt += 2;
		int32_t L_56 = V_7;
		V_7 = ((int32_t)il2cpp_codegen_add(L_56, 2));
		// for (int i = 0; i < divisions; i++) {
		int32_t L_57 = V_14;
		V_14 = ((int32_t)il2cpp_codegen_add(L_57, 1));
	}

IL_0135:
	{
		// for (int i = 0; i < divisions; i++) {
		int32_t L_58 = V_14;
		int32_t L_59 = ___divisions0;
		if ((((int32_t)L_58) < ((int32_t)L_59)))
		{
			goto IL_00e7;
		}
	}
	{
		// int b = divisions * 6;
		int32_t L_60 = ___divisions0;
		V_8 = ((int32_t)il2cpp_codegen_multiply(L_60, 6));
		// tris[b] = vertPnt;
		Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_61 = V_6;
		int32_t L_62 = V_8;
		int32_t L_63 = V_7;
		NullCheck(L_61);
		(L_61)->SetAt(static_cast<il2cpp_array_size_t>(L_62), (int32_t)L_63);
		// tris[b + 1] = vertPnt + 1;
		Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_64 = V_6;
		int32_t L_65 = V_8;
		int32_t L_66 = V_7;
		NullCheck(L_64);
		(L_64)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)il2cpp_codegen_add(L_65, 1))), (int32_t)((int32_t)il2cpp_codegen_add(L_66, 1)));
		// tris[b + 2] = vertPnt + 2;
		Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_67 = V_6;
		int32_t L_68 = V_8;
		int32_t L_69 = V_7;
		NullCheck(L_67);
		(L_67)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)il2cpp_codegen_add(L_68, 2))), (int32_t)((int32_t)il2cpp_codegen_add(L_69, 2)));
		// Mesh mesh = new Mesh() {
		//     name = "Generated Grass Blade",
		//     vertices = verts,
		//     triangles = tris,
		//     colors = cols
		// };
		Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* L_70 = (Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4*)il2cpp_codegen_object_new(Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4_il2cpp_TypeInfo_var);
		NullCheck(L_70);
		Mesh__ctor_m5A9AECEDDAFFD84811ED8928012BDE97A9CEBD00(L_70, NULL);
		Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* L_71 = L_70;
		NullCheck(L_71);
		Object_set_name_mC79E6DC8FFD72479C90F0C4CC7F42A0FEAF5AE47(L_71, _stringLiteral70A3E84D94AAB3B2B54557DF4CDB0569C20B19BA, NULL);
		Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* L_72 = L_71;
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_73 = V_2;
		NullCheck(L_72);
		Mesh_set_vertices_m5BB814D89E9ACA00DBF19F7D8E22CB73AC73FE5C(L_72, L_73, NULL);
		Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* L_74 = L_72;
		Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_75 = V_6;
		NullCheck(L_74);
		Mesh_set_triangles_m124405320579A8D92711BB5A124644963A26F60B(L_74, L_75, NULL);
		Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* L_76 = L_74;
		ColorU5BU5D_t612261CF293F6FFC3D80AB52259FF0DC2B2CC389* L_77 = V_4;
		NullCheck(L_76);
		Mesh_set_colors_m5558BAAA60676427B7954F1694A1765B000EB0FE(L_76, L_77, NULL);
		// mesh.SetUVs(0, uvs);
		Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* L_78 = L_76;
		Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* L_79 = V_3;
		NullCheck(L_78);
		Mesh_SetUVs_m6AFD5BFC4D7FB9EE57D8F19AB1BECD0675771D48(L_78, 0, L_79, NULL);
		// mesh.RecalculateNormals();
		Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* L_80 = L_78;
		NullCheck(L_80);
		Mesh_RecalculateNormals_m3AA2788914611444E030CA310E03E3CFE683902B(L_80, NULL);
		// return mesh;
		return L_80;
	}
}
// UnityEngine.Mesh MicahW.PointGrass.PointGrassCommon::GenerateGrassMesh_Cylinder(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* PointGrassCommon_GenerateGrassMesh_Cylinder_m3290C77744AA3E4E2C99868E8D082E81EC4A9D0F (int32_t ___divisions0, int32_t ___loops1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ColorU5BU5D_t612261CF293F6FFC3D80AB52259FF0DC2B2CC389_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral666C1ECD84E7610A200C08C24AF2A55B343400F8);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass17_0_t3B43FCEDB710544F3D8D366A3758A6D2CEF70412 V_0;
	memset((&V_0), 0, sizeof(V_0));
	int32_t V_1 = 0;
	Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* V_2 = NULL;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	float V_9 = 0.0f;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_10;
	memset((&V_10), 0, sizeof(V_10));
	int32_t V_11 = 0;
	int32_t V_12 = 0;
	int32_t V_13 = 0;
	int32_t V_14 = 0;
	int32_t V_15 = 0;
	int32_t V_16 = 0;
	int32_t V_17 = 0;
	int32_t V_18 = 0;
	int32_t V_19 = 0;
	int32_t V_20 = 0;
	{
		// int vertCount = divisions * loops + 1;
		int32_t L_0 = ___divisions0;
		int32_t L_1 = ___loops1;
		V_1 = ((int32_t)il2cpp_codegen_add(((int32_t)il2cpp_codegen_multiply(L_0, L_1)), 1));
		// Vector3[] verts = new Vector3[vertCount];
		int32_t L_2 = V_1;
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_3 = (Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C*)(Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C*)SZArrayNew(Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C_il2cpp_TypeInfo_var, (uint32_t)L_2);
		(&V_0)->___verts_0 = L_3;
		Il2CppCodeGenWriteBarrier((void**)(&(&V_0)->___verts_0), (void*)L_3);
		// Vector2[] uvs = new Vector2[vertCount];
		int32_t L_4 = V_1;
		Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* L_5 = (Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA*)(Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA*)SZArrayNew(Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA_il2cpp_TypeInfo_var, (uint32_t)L_4);
		(&V_0)->___uvs_1 = L_5;
		Il2CppCodeGenWriteBarrier((void**)(&(&V_0)->___uvs_1), (void*)L_5);
		// Color[] cols = new Color[vertCount];
		int32_t L_6 = V_1;
		ColorU5BU5D_t612261CF293F6FFC3D80AB52259FF0DC2B2CC389* L_7 = (ColorU5BU5D_t612261CF293F6FFC3D80AB52259FF0DC2B2CC389*)(ColorU5BU5D_t612261CF293F6FFC3D80AB52259FF0DC2B2CC389*)SZArrayNew(ColorU5BU5D_t612261CF293F6FFC3D80AB52259FF0DC2B2CC389_il2cpp_TypeInfo_var, (uint32_t)L_6);
		(&V_0)->___cols_2 = L_7;
		Il2CppCodeGenWriteBarrier((void**)(&(&V_0)->___cols_2), (void*)L_7);
		// for (int i = 0; i < loops; i++) {
		V_4 = 0;
		goto IL_00aa;
	}

IL_0032:
	{
		// float height = i / (float)loops;
		int32_t L_8 = V_4;
		int32_t L_9 = ___loops1;
		V_5 = ((float)(((float)L_8)/((float)L_9)));
		// float radius = Mathf.Cos(height * Mathf.PI * 0.5f);
		float L_10 = V_5;
		float L_11;
		L_11 = cosf(((float)il2cpp_codegen_multiply(((float)il2cpp_codegen_multiply(L_10, (3.14159274f))), (0.5f))));
		V_6 = L_11;
		// int loopOffset = i * divisions;
		int32_t L_12 = V_4;
		int32_t L_13 = ___divisions0;
		V_7 = ((int32_t)il2cpp_codegen_multiply(L_12, L_13));
		// for (int j = 0; j < divisions; j++) {
		V_8 = 0;
		goto IL_009f;
	}

IL_005a:
	{
		// float angle = (j / (float)divisions) * Mathf.PI * 2f;
		int32_t L_14 = V_8;
		int32_t L_15 = ___divisions0;
		V_9 = ((float)il2cpp_codegen_multiply(((float)il2cpp_codegen_multiply(((float)(((float)L_14)/((float)L_15))), (3.14159274f))), (2.0f)));
		// Vector3 pos = new Vector3(
		//     Mathf.Sin(angle) * radius,
		//     height,
		//     Mathf.Cos(angle) * radius
		// );
		float L_16 = V_9;
		float L_17;
		L_17 = sinf(L_16);
		float L_18 = V_6;
		float L_19 = V_5;
		float L_20 = V_9;
		float L_21;
		L_21 = cosf(L_20);
		float L_22 = V_6;
		Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline((&V_10), ((float)il2cpp_codegen_multiply(L_17, L_18)), L_19, ((float)il2cpp_codegen_multiply(L_21, L_22)), NULL);
		// SetVert(loopOffset + j, pos);
		int32_t L_23 = V_7;
		int32_t L_24 = V_8;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_25 = V_10;
		PointGrassCommon_U3CGenerateGrassMesh_CylinderU3Eg__SetVertU7C17_0_m9E44405726796B377C797C8B52794EF850E59ABD(((int32_t)il2cpp_codegen_add(L_23, L_24)), L_25, (&V_0), NULL);
		// for (int j = 0; j < divisions; j++) {
		int32_t L_26 = V_8;
		V_8 = ((int32_t)il2cpp_codegen_add(L_26, 1));
	}

IL_009f:
	{
		// for (int j = 0; j < divisions; j++) {
		int32_t L_27 = V_8;
		int32_t L_28 = ___divisions0;
		if ((((int32_t)L_27) < ((int32_t)L_28)))
		{
			goto IL_005a;
		}
	}
	{
		// for (int i = 0; i < loops; i++) {
		int32_t L_29 = V_4;
		V_4 = ((int32_t)il2cpp_codegen_add(L_29, 1));
	}

IL_00aa:
	{
		// for (int i = 0; i < loops; i++) {
		int32_t L_30 = V_4;
		int32_t L_31 = ___loops1;
		if ((((int32_t)L_30) < ((int32_t)L_31)))
		{
			goto IL_0032;
		}
	}
	{
		// SetVert(vertCount - 1, Vector3.up);
		int32_t L_32 = V_1;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_33;
		L_33 = Vector3_get_up_m128AF3FDC820BF59D5DE86D973E7DE3F20C3AEBA_inline(NULL);
		PointGrassCommon_U3CGenerateGrassMesh_CylinderU3Eg__SetVertU7C17_0_m9E44405726796B377C797C8B52794EF850E59ABD(((int32_t)il2cpp_codegen_subtract(L_32, 1)), L_33, (&V_0), NULL);
		// int triCount = (divisions * (loops - 1) * 2) + divisions;
		int32_t L_34 = ___divisions0;
		int32_t L_35 = ___loops1;
		int32_t L_36 = ___divisions0;
		// int[] tris = new int[triCount * 3];
		Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_37 = (Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C*)(Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C*)SZArrayNew(Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C_il2cpp_TypeInfo_var, (uint32_t)((int32_t)il2cpp_codegen_multiply(((int32_t)il2cpp_codegen_add(((int32_t)il2cpp_codegen_multiply(((int32_t)il2cpp_codegen_multiply(L_34, ((int32_t)il2cpp_codegen_subtract(L_35, 1)))), 2)), L_36)), 3)));
		V_2 = L_37;
		// int triCounter = 0;
		V_3 = 0;
		// for (int i = 0; i < loops - 1; i++) {
		V_11 = 0;
		goto IL_013a;
	}

IL_00d6:
	{
		// for (int j = 0; j < divisions; j++) {
		V_12 = 0;
		goto IL_012f;
	}

IL_00db:
	{
		// int baseVert = i * divisions + j;
		int32_t L_38 = V_11;
		int32_t L_39 = ___divisions0;
		int32_t L_40 = V_12;
		V_13 = ((int32_t)il2cpp_codegen_add(((int32_t)il2cpp_codegen_multiply(L_38, L_39)), L_40));
		// int topVert = baseVert + divisions;
		int32_t L_41 = V_13;
		int32_t L_42 = ___divisions0;
		V_14 = ((int32_t)il2cpp_codegen_add(L_41, L_42));
		// int rightVert = i * divisions + ((j + 1) % divisions);
		int32_t L_43 = V_11;
		int32_t L_44 = ___divisions0;
		int32_t L_45 = V_12;
		int32_t L_46 = ___divisions0;
		V_15 = ((int32_t)il2cpp_codegen_add(((int32_t)il2cpp_codegen_multiply(L_43, L_44)), ((int32_t)(((int32_t)il2cpp_codegen_add(L_45, 1))%L_46))));
		// int diagVert = rightVert + divisions;
		int32_t L_47 = V_15;
		int32_t L_48 = ___divisions0;
		V_16 = ((int32_t)il2cpp_codegen_add(L_47, L_48));
		// tris[triCounter] = baseVert;
		Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_49 = V_2;
		int32_t L_50 = V_3;
		int32_t L_51 = V_13;
		NullCheck(L_49);
		(L_49)->SetAt(static_cast<il2cpp_array_size_t>(L_50), (int32_t)L_51);
		// tris[triCounter + 1] = topVert;
		Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_52 = V_2;
		int32_t L_53 = V_3;
		int32_t L_54 = V_14;
		NullCheck(L_52);
		(L_52)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)il2cpp_codegen_add(L_53, 1))), (int32_t)L_54);
		// tris[triCounter + 2] = rightVert;
		Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_55 = V_2;
		int32_t L_56 = V_3;
		int32_t L_57 = V_15;
		NullCheck(L_55);
		(L_55)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)il2cpp_codegen_add(L_56, 2))), (int32_t)L_57);
		// tris[triCounter + 3] = rightVert;
		Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_58 = V_2;
		int32_t L_59 = V_3;
		int32_t L_60 = V_15;
		NullCheck(L_58);
		(L_58)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)il2cpp_codegen_add(L_59, 3))), (int32_t)L_60);
		// tris[triCounter + 4] = topVert;
		Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_61 = V_2;
		int32_t L_62 = V_3;
		int32_t L_63 = V_14;
		NullCheck(L_61);
		(L_61)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)il2cpp_codegen_add(L_62, 4))), (int32_t)L_63);
		// tris[triCounter + 5] = diagVert;
		Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_64 = V_2;
		int32_t L_65 = V_3;
		int32_t L_66 = V_16;
		NullCheck(L_64);
		(L_64)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)il2cpp_codegen_add(L_65, 5))), (int32_t)L_66);
		// triCounter += 6;
		int32_t L_67 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add(L_67, 6));
		// for (int j = 0; j < divisions; j++) {
		int32_t L_68 = V_12;
		V_12 = ((int32_t)il2cpp_codegen_add(L_68, 1));
	}

IL_012f:
	{
		// for (int j = 0; j < divisions; j++) {
		int32_t L_69 = V_12;
		int32_t L_70 = ___divisions0;
		if ((((int32_t)L_69) < ((int32_t)L_70)))
		{
			goto IL_00db;
		}
	}
	{
		// for (int i = 0; i < loops - 1; i++) {
		int32_t L_71 = V_11;
		V_11 = ((int32_t)il2cpp_codegen_add(L_71, 1));
	}

IL_013a:
	{
		// for (int i = 0; i < loops - 1; i++) {
		int32_t L_72 = V_11;
		int32_t L_73 = ___loops1;
		if ((((int32_t)L_72) < ((int32_t)((int32_t)il2cpp_codegen_subtract(L_73, 1)))))
		{
			goto IL_00d6;
		}
	}
	{
		// for (int i = 0; i < divisions; i++) {
		V_17 = 0;
		goto IL_0180;
	}

IL_0146:
	{
		// int baseVert = (divisions * (loops - 1)) + i;
		int32_t L_74 = ___divisions0;
		int32_t L_75 = ___loops1;
		int32_t L_76 = V_17;
		V_18 = ((int32_t)il2cpp_codegen_add(((int32_t)il2cpp_codegen_multiply(L_74, ((int32_t)il2cpp_codegen_subtract(L_75, 1)))), L_76));
		// int rightVert = (divisions * (loops - 1)) + ((i + 1) % divisions);
		int32_t L_77 = ___divisions0;
		int32_t L_78 = ___loops1;
		int32_t L_79 = V_17;
		int32_t L_80 = ___divisions0;
		V_19 = ((int32_t)il2cpp_codegen_add(((int32_t)il2cpp_codegen_multiply(L_77, ((int32_t)il2cpp_codegen_subtract(L_78, 1)))), ((int32_t)(((int32_t)il2cpp_codegen_add(L_79, 1))%L_80))));
		// int topVert = vertCount - 1;
		int32_t L_81 = V_1;
		V_20 = ((int32_t)il2cpp_codegen_subtract(L_81, 1));
		// tris[triCounter] = baseVert;
		Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_82 = V_2;
		int32_t L_83 = V_3;
		int32_t L_84 = V_18;
		NullCheck(L_82);
		(L_82)->SetAt(static_cast<il2cpp_array_size_t>(L_83), (int32_t)L_84);
		// tris[triCounter + 1] = topVert;
		Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_85 = V_2;
		int32_t L_86 = V_3;
		int32_t L_87 = V_20;
		NullCheck(L_85);
		(L_85)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)il2cpp_codegen_add(L_86, 1))), (int32_t)L_87);
		// tris[triCounter + 2] = rightVert;
		Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_88 = V_2;
		int32_t L_89 = V_3;
		int32_t L_90 = V_19;
		NullCheck(L_88);
		(L_88)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)il2cpp_codegen_add(L_89, 2))), (int32_t)L_90);
		// triCounter += 3;
		int32_t L_91 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add(L_91, 3));
		// for (int i = 0; i < divisions; i++) {
		int32_t L_92 = V_17;
		V_17 = ((int32_t)il2cpp_codegen_add(L_92, 1));
	}

IL_0180:
	{
		// for (int i = 0; i < divisions; i++) {
		int32_t L_93 = V_17;
		int32_t L_94 = ___divisions0;
		if ((((int32_t)L_93) < ((int32_t)L_94)))
		{
			goto IL_0146;
		}
	}
	{
		// Mesh mesh = new Mesh() {
		//     name = "Generated Cylindrical Grass Blade",
		//     vertices = verts,
		//     triangles = tris,
		//     colors = cols
		// };
		Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* L_95 = (Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4*)il2cpp_codegen_object_new(Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4_il2cpp_TypeInfo_var);
		NullCheck(L_95);
		Mesh__ctor_m5A9AECEDDAFFD84811ED8928012BDE97A9CEBD00(L_95, NULL);
		Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* L_96 = L_95;
		NullCheck(L_96);
		Object_set_name_mC79E6DC8FFD72479C90F0C4CC7F42A0FEAF5AE47(L_96, _stringLiteral666C1ECD84E7610A200C08C24AF2A55B343400F8, NULL);
		Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* L_97 = L_96;
		U3CU3Ec__DisplayClass17_0_t3B43FCEDB710544F3D8D366A3758A6D2CEF70412 L_98 = V_0;
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_99 = L_98.___verts_0;
		NullCheck(L_97);
		Mesh_set_vertices_m5BB814D89E9ACA00DBF19F7D8E22CB73AC73FE5C(L_97, L_99, NULL);
		Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* L_100 = L_97;
		Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_101 = V_2;
		NullCheck(L_100);
		Mesh_set_triangles_m124405320579A8D92711BB5A124644963A26F60B(L_100, L_101, NULL);
		Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* L_102 = L_100;
		U3CU3Ec__DisplayClass17_0_t3B43FCEDB710544F3D8D366A3758A6D2CEF70412 L_103 = V_0;
		ColorU5BU5D_t612261CF293F6FFC3D80AB52259FF0DC2B2CC389* L_104 = L_103.___cols_2;
		NullCheck(L_102);
		Mesh_set_colors_m5558BAAA60676427B7954F1694A1765B000EB0FE(L_102, L_104, NULL);
		// mesh.SetUVs(0, uvs);
		Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* L_105 = L_102;
		U3CU3Ec__DisplayClass17_0_t3B43FCEDB710544F3D8D366A3758A6D2CEF70412 L_106 = V_0;
		Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* L_107 = L_106.___uvs_1;
		NullCheck(L_105);
		Mesh_SetUVs_m6AFD5BFC4D7FB9EE57D8F19AB1BECD0675771D48(L_105, 0, L_107, NULL);
		// mesh.RecalculateNormals();
		Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* L_108 = L_105;
		NullCheck(L_108);
		Mesh_RecalculateNormals_m3AA2788914611444E030CA310E03E3CFE683902B(L_108, NULL);
		// return mesh;
		return L_108;
	}
}
// System.Void MicahW.PointGrass.PointGrassCommon::ProjectBaseMesh(MicahW.PointGrass.PointGrassCommon/MeshData&,UnityEngine.LayerMask,UnityEngine.Transform)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointGrassCommon_ProjectBaseMesh_m5562159B69B1FD110DEC077043E7DE5D4020F74B (MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F** ___mesh0, LayerMask_t97CB6BDADEDC3D6423C7BCFEA7F86DA2EC6241DB ___mask1, Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___transform2, const RuntimeMethod* method) 
{
	int32_t V_0 = 0;
	Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 V_1;
	memset((&V_1), 0, sizeof(V_1));
	Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 V_2;
	memset((&V_2), 0, sizeof(V_2));
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_3;
	memset((&V_3), 0, sizeof(V_3));
	RaycastHit_t6F30BD0B38B56401CA833A1B87BD74F2ACD2F2B5 V_4;
	memset((&V_4), 0, sizeof(V_4));
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_5;
	memset((&V_5), 0, sizeof(V_5));
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_6;
	memset((&V_6), 0, sizeof(V_6));
	{
		// for (int i = 0; i < mesh.verts.Length; i++) {
		V_0 = 0;
		goto IL_009a;
	}

IL_0007:
	{
		// Matrix4x4 L2W = transform.localToWorldMatrix;
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_0 = ___transform2;
		NullCheck(L_0);
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_1;
		L_1 = Transform_get_localToWorldMatrix_m5D35188766856338DD21DE756F42277C21719E6D(L_0, NULL);
		V_1 = L_1;
		// Matrix4x4 W2L = transform.worldToLocalMatrix;
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_2 = ___transform2;
		NullCheck(L_2);
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_3;
		L_3 = Transform_get_worldToLocalMatrix_mB633C122A01BCE8E51B10B8B8CB95F580750B3F1(L_2, NULL);
		V_2 = L_3;
		// Vector3 worldPos = L2W.MultiplyPoint(mesh.verts[i]);
		MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F** L_4 = ___mesh0;
		MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F* L_5 = *((MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F**)L_4);
		NullCheck(L_5);
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_6 = L_5->___verts_0;
		int32_t L_7 = V_0;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_10;
		L_10 = Matrix4x4_MultiplyPoint_m20E910B65693559BFDE99382472D8DD02C862E7E((&V_1), L_9, NULL);
		// Vector3 worldNormal = L2W.MultiplyVector(mesh.normals[i]);
		MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F** L_11 = ___mesh0;
		MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F* L_12 = *((MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F**)L_11);
		NullCheck(L_12);
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_13 = L_12->___normals_1;
		int32_t L_14 = V_0;
		NullCheck(L_13);
		int32_t L_15 = L_14;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_17;
		L_17 = Matrix4x4_MultiplyVector_mFD12F86A473E90BBB0002149ABA3917B2A518937((&V_1), L_16, NULL);
		V_3 = L_17;
		// if (Physics.Raycast(worldPos, -worldNormal, out RaycastHit hitInfo, 10f, mask)) {
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_18 = V_3;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_19;
		L_19 = Vector3_op_UnaryNegation_m5450829F333BD2A88AF9A592C4EE331661225915_inline(L_18, NULL);
		LayerMask_t97CB6BDADEDC3D6423C7BCFEA7F86DA2EC6241DB L_20 = ___mask1;
		int32_t L_21;
		L_21 = LayerMask_op_Implicit_m7F5A5B9D079281AC445ED39DEE1FCFA9D795810D(L_20, NULL);
		bool L_22;
		L_22 = Physics_Raycast_m56120FFEF0D4F0A44CCA505B5C946E6FB8742F12(L_10, L_19, (&V_4), (10.0f), L_21, NULL);
		if (!L_22)
		{
			goto IL_0096;
		}
	}
	{
		// Vector3 localPos = W2L.MultiplyPoint(hitInfo.point);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_23;
		L_23 = RaycastHit_get_point_m02B764612562AFE0F998CC7CFB2EEDE41BA47F39((&V_4), NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_24;
		L_24 = Matrix4x4_MultiplyPoint_m20E910B65693559BFDE99382472D8DD02C862E7E((&V_2), L_23, NULL);
		V_5 = L_24;
		// Vector3 localNormal = W2L.MultiplyVector(hitInfo.normal);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_25;
		L_25 = RaycastHit_get_normal_mD8741B70D2039C5CAFC4368D4CE59D89562040B5((&V_4), NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_26;
		L_26 = Matrix4x4_MultiplyVector_mFD12F86A473E90BBB0002149ABA3917B2A518937((&V_2), L_25, NULL);
		V_6 = L_26;
		// mesh.verts[i] = localPos;
		MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F** L_27 = ___mesh0;
		MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F* L_28 = *((MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F**)L_27);
		NullCheck(L_28);
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_29 = L_28->___verts_0;
		int32_t L_30 = V_0;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_31 = V_5;
		NullCheck(L_29);
		(L_29)->SetAt(static_cast<il2cpp_array_size_t>(L_30), (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2)L_31);
		// mesh.normals[i] = localNormal;
		MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F** L_32 = ___mesh0;
		MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F* L_33 = *((MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F**)L_32);
		NullCheck(L_33);
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_34 = L_33->___normals_1;
		int32_t L_35 = V_0;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_36 = V_6;
		NullCheck(L_34);
		(L_34)->SetAt(static_cast<il2cpp_array_size_t>(L_35), (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2)L_36);
	}

IL_0096:
	{
		// for (int i = 0; i < mesh.verts.Length; i++) {
		int32_t L_37 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add(L_37, 1));
	}

IL_009a:
	{
		// for (int i = 0; i < mesh.verts.Length; i++) {
		int32_t L_38 = V_0;
		MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F** L_39 = ___mesh0;
		MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F* L_40 = *((MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F**)L_39);
		NullCheck(L_40);
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_41 = L_40->___verts_0;
		NullCheck(L_41);
		if ((((int32_t)L_38) < ((int32_t)((int32_t)(((RuntimeArray*)L_41)->max_length)))))
		{
			goto IL_0007;
		}
	}
	{
		// mesh.RecalculateBounds();
		MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F** L_42 = ___mesh0;
		MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F* L_43 = *((MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F**)L_42);
		NullCheck(L_43);
		MeshData_RecalculateBounds_m23CC32EAA976F74CAD691ACF55DEDE635C504655(L_43, NULL);
		// }
		return;
	}
}
// UnityEngine.Texture2D MicahW.PointGrass.PointGrassCommon::CreateTextureCopy(UnityEngine.Texture2D,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* PointGrassCommon_CreateTextureCopy_mADE8309152AFE527D5756F91C7491A9B0C2A8C1F (Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* ___source0, int32_t ___width1, int32_t ___height2, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Graphics_t99CD970FFEA58171C70F54DF0C06D315BD452F2C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* V_0 = NULL;
	RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* V_1 = NULL;
	{
		// width = Mathf.Max(width, 16); height = Mathf.Max(height, 16);
		int32_t L_0 = ___width1;
		int32_t L_1;
		L_1 = Mathf_Max_m7FA442918DE37E3A00106D1F2E789D65829792B8_inline(L_0, ((int32_t)16), NULL);
		___width1 = L_1;
		// width = Mathf.Max(width, 16); height = Mathf.Max(height, 16);
		int32_t L_2 = ___height2;
		int32_t L_3;
		L_3 = Mathf_Max_m7FA442918DE37E3A00106D1F2E789D65829792B8_inline(L_2, ((int32_t)16), NULL);
		___height2 = L_3;
		// RenderTexture tmp = RenderTexture.GetTemporary(width, height, 0, RenderTextureFormat.Default, RenderTextureReadWrite.Linear);
		int32_t L_4 = ___width1;
		int32_t L_5 = ___height2;
		RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* L_6;
		L_6 = RenderTexture_GetTemporary_mA6619EA324AAE80B6892107C6968092F6F1B4C45(L_4, L_5, 0, 7, 1, NULL);
		V_0 = L_6;
		// RenderTexture previous = RenderTexture.active;
		RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* L_7;
		L_7 = RenderTexture_get_active_mA4434B3E79DEF2C01CAE0A53061598B16443C9E7(NULL);
		V_1 = L_7;
		// Graphics.Blit(source, tmp);
		Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* L_8 = ___source0;
		RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* L_9 = V_0;
		il2cpp_codegen_runtime_class_init_inline(Graphics_t99CD970FFEA58171C70F54DF0C06D315BD452F2C_il2cpp_TypeInfo_var);
		Graphics_Blit_m8D99E16B74C7D3C8F79F4F142C59DB6B38114504(L_8, L_9, NULL);
		// Texture2D outputTex = new Texture2D(width, height, TextureFormat.RGB24, false);
		int32_t L_10 = ___width1;
		int32_t L_11 = ___height2;
		Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* L_12 = (Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4*)il2cpp_codegen_object_new(Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4_il2cpp_TypeInfo_var);
		NullCheck(L_12);
		Texture2D__ctor_mECF60A9EC0638EC353C02C8E99B6B465D23BE917(L_12, L_10, L_11, 3, (bool)0, NULL);
		// outputTex.ReadPixels(new Rect(0, 0, width, height), 0, 0);
		Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* L_13 = L_12;
		int32_t L_14 = ___width1;
		int32_t L_15 = ___height2;
		Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D L_16;
		memset((&L_16), 0, sizeof(L_16));
		Rect__ctor_m18C3033D135097BEE424AAA68D91C706D2647F23((&L_16), (0.0f), (0.0f), ((float)L_14), ((float)L_15), /*hidden argument*/NULL);
		NullCheck(L_13);
		Texture2D_ReadPixels_m6B45DF7C051BF599C72ED09691F21A6C769EEBD9(L_13, L_16, 0, 0, NULL);
		// outputTex.Apply();
		Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* L_17 = L_13;
		NullCheck(L_17);
		Texture2D_Apply_mA014182C9EE0BBF6EEE3B286854F29E50EB972DC(L_17, NULL);
		// RenderTexture.active = previous;
		RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* L_18 = V_1;
		RenderTexture_set_active_m5EE8E2327EF9B306C1425014CC34C41A8384E7AB(L_18, NULL);
		// RenderTexture.ReleaseTemporary(tmp);
		RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* L_19 = V_0;
		RenderTexture_ReleaseTemporary_mEEF2C1990196FF06FDD0DC190928AD3A023EBDD2(L_19, NULL);
		// return outputTex;
		return L_17;
	}
}
// System.Void MicahW.PointGrass.PointGrassCommon::CacheTerrainData(UnityEngine.TerrainData,UnityEngine.TerrainLayer[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointGrassCommon_CacheTerrainData_mE54FEFBA4451B4B69F63A2617DA43149225A3EAE (TerrainData_t615A68EAC648066681875D47FC641496D12F2E24* ___terrain0, TerrainLayerU5BU5D_t259E391D6115F121FCD284E79F62012D70956EB0* ___grassLayers1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&BooleanU5BU5D_tD317D27C31DB892BE79FAE3AEBC0B3FFB73DE9B4_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerable_Contains_TisTerrainLayer_t52E14A94A0CF76B0B5509B7FDFDE64FF8A9FEFF9_m6D7B0AFE1EC7DB5185D1BB1A733E959511E0A6D1_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Texture2DU5BU5D_t05332F1E3F7D4493E304C702201F9BE4F9236191_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	TerrainLayerU5BU5D_t259E391D6115F121FCD284E79F62012D70956EB0* V_0 = NULL;
	int32_t V_1 = 0;
	{
		// heightmapSize = terrain.heightmapResolution;
		TerrainData_t615A68EAC648066681875D47FC641496D12F2E24* L_0 = ___terrain0;
		NullCheck(L_0);
		int32_t L_1;
		L_1 = TerrainData_get_heightmapResolution_m39FE9A5C31A80B28021F8E2484EF5F2664798836(L_0, NULL);
		((PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_StaticFields*)il2cpp_codegen_static_fields_for(PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_il2cpp_TypeInfo_var))->___heightmapSize_8 = L_1;
		// terrainSize = terrain.size;
		TerrainData_t615A68EAC648066681875D47FC641496D12F2E24* L_2 = ___terrain0;
		NullCheck(L_2);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_3;
		L_3 = TerrainData_get_size_mCD3977F344B9DEBFF61DD537D03FEB9473838DA5(L_2, NULL);
		((PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_StaticFields*)il2cpp_codegen_static_fields_for(PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_il2cpp_TypeInfo_var))->___terrainSize_12 = L_3;
		// alphamapWidth = terrain.alphamapWidth;
		TerrainData_t615A68EAC648066681875D47FC641496D12F2E24* L_4 = ___terrain0;
		NullCheck(L_4);
		int32_t L_5;
		L_5 = TerrainData_get_alphamapWidth_m07E5B04B08E87AC9F66D766B363000F94C8612D4(L_4, NULL);
		((PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_StaticFields*)il2cpp_codegen_static_fields_for(PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_il2cpp_TypeInfo_var))->___alphamapWidth_9 = L_5;
		// alphamapHeight = terrain.alphamapHeight;
		TerrainData_t615A68EAC648066681875D47FC641496D12F2E24* L_6 = ___terrain0;
		NullCheck(L_6);
		int32_t L_7;
		L_7 = TerrainData_get_alphamapHeight_m4A8273D6E0E3526A31E2669FBAB240353C086AED(L_6, NULL);
		((PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_StaticFields*)il2cpp_codegen_static_fields_for(PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_il2cpp_TypeInfo_var))->___alphamapHeight_10 = L_7;
		// numLayers = terrain.alphamapLayers;
		TerrainData_t615A68EAC648066681875D47FC641496D12F2E24* L_8 = ___terrain0;
		NullCheck(L_8);
		int32_t L_9;
		L_9 = TerrainData_get_alphamapLayers_mF8A0A4F157F7C56354C5A6E3FABF9F230F410F69(L_8, NULL);
		((PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_StaticFields*)il2cpp_codegen_static_fields_for(PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_il2cpp_TypeInfo_var))->___numLayers_11 = L_9;
		// alphamaps = terrain.GetAlphamaps(0, 0, alphamapWidth, alphamapHeight);
		TerrainData_t615A68EAC648066681875D47FC641496D12F2E24* L_10 = ___terrain0;
		int32_t L_11 = ((PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_StaticFields*)il2cpp_codegen_static_fields_for(PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_il2cpp_TypeInfo_var))->___alphamapWidth_9;
		int32_t L_12 = ((PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_StaticFields*)il2cpp_codegen_static_fields_for(PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_il2cpp_TypeInfo_var))->___alphamapHeight_10;
		NullCheck(L_10);
		SingleU5BU2CU2CU5D_tE902E5192C7283A470AAADB477117789A9682488* L_13;
		L_13 = TerrainData_GetAlphamaps_m2DEF5D2068D54BDAE78661483C1FC4936B06EA01(L_10, 0, 0, L_11, L_12, NULL);
		((PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_StaticFields*)il2cpp_codegen_static_fields_for(PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_il2cpp_TypeInfo_var))->___alphamaps_13 = L_13;
		Il2CppCodeGenWriteBarrier((void**)(&((PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_StaticFields*)il2cpp_codegen_static_fields_for(PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_il2cpp_TypeInfo_var))->___alphamaps_13), (void*)L_13);
		// TerrainLayer[] terrainLayers = terrain.terrainLayers;
		TerrainData_t615A68EAC648066681875D47FC641496D12F2E24* L_14 = ___terrain0;
		NullCheck(L_14);
		TerrainLayerU5BU5D_t259E391D6115F121FCD284E79F62012D70956EB0* L_15;
		L_15 = TerrainData_get_terrainLayers_m3B436DF37DDD9F18A46DD6BF112925AD5B8857C8(L_14, NULL);
		V_0 = L_15;
		// grassLayerMask = new bool[numLayers];
		int32_t L_16 = ((PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_StaticFields*)il2cpp_codegen_static_fields_for(PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_il2cpp_TypeInfo_var))->___numLayers_11;
		BooleanU5BU5D_tD317D27C31DB892BE79FAE3AEBC0B3FFB73DE9B4* L_17 = (BooleanU5BU5D_tD317D27C31DB892BE79FAE3AEBC0B3FFB73DE9B4*)(BooleanU5BU5D_tD317D27C31DB892BE79FAE3AEBC0B3FFB73DE9B4*)SZArrayNew(BooleanU5BU5D_tD317D27C31DB892BE79FAE3AEBC0B3FFB73DE9B4_il2cpp_TypeInfo_var, (uint32_t)L_16);
		((PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_StaticFields*)il2cpp_codegen_static_fields_for(PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_il2cpp_TypeInfo_var))->___grassLayerMask_14 = L_17;
		Il2CppCodeGenWriteBarrier((void**)(&((PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_StaticFields*)il2cpp_codegen_static_fields_for(PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_il2cpp_TypeInfo_var))->___grassLayerMask_14), (void*)L_17);
		// layerTextures = new Texture2D[numLayers];
		int32_t L_18 = ((PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_StaticFields*)il2cpp_codegen_static_fields_for(PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_il2cpp_TypeInfo_var))->___numLayers_11;
		Texture2DU5BU5D_t05332F1E3F7D4493E304C702201F9BE4F9236191* L_19 = (Texture2DU5BU5D_t05332F1E3F7D4493E304C702201F9BE4F9236191*)(Texture2DU5BU5D_t05332F1E3F7D4493E304C702201F9BE4F9236191*)SZArrayNew(Texture2DU5BU5D_t05332F1E3F7D4493E304C702201F9BE4F9236191_il2cpp_TypeInfo_var, (uint32_t)L_18);
		((PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_StaticFields*)il2cpp_codegen_static_fields_for(PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_il2cpp_TypeInfo_var))->___layerTextures_15 = L_19;
		Il2CppCodeGenWriteBarrier((void**)(&((PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_StaticFields*)il2cpp_codegen_static_fields_for(PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_il2cpp_TypeInfo_var))->___layerTextures_15), (void*)L_19);
		// layerOffsets = new Vector2[numLayers];
		int32_t L_20 = ((PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_StaticFields*)il2cpp_codegen_static_fields_for(PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_il2cpp_TypeInfo_var))->___numLayers_11;
		Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* L_21 = (Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA*)(Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA*)SZArrayNew(Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA_il2cpp_TypeInfo_var, (uint32_t)L_20);
		((PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_StaticFields*)il2cpp_codegen_static_fields_for(PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_il2cpp_TypeInfo_var))->___layerOffsets_16 = L_21;
		Il2CppCodeGenWriteBarrier((void**)(&((PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_StaticFields*)il2cpp_codegen_static_fields_for(PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_il2cpp_TypeInfo_var))->___layerOffsets_16), (void*)L_21);
		// layerSizes = new Vector2[numLayers];
		int32_t L_22 = ((PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_StaticFields*)il2cpp_codegen_static_fields_for(PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_il2cpp_TypeInfo_var))->___numLayers_11;
		Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* L_23 = (Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA*)(Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA*)SZArrayNew(Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA_il2cpp_TypeInfo_var, (uint32_t)L_22);
		((PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_StaticFields*)il2cpp_codegen_static_fields_for(PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_il2cpp_TypeInfo_var))->___layerSizes_17 = L_23;
		Il2CppCodeGenWriteBarrier((void**)(&((PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_StaticFields*)il2cpp_codegen_static_fields_for(PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_il2cpp_TypeInfo_var))->___layerSizes_17), (void*)L_23);
		// for (int i = 0; i < numLayers; i++) {
		V_1 = 0;
		goto IL_00f6;
	}

IL_0095:
	{
		// grassLayerMask[i] = grassLayers.Contains(terrainLayers[i]);
		BooleanU5BU5D_tD317D27C31DB892BE79FAE3AEBC0B3FFB73DE9B4* L_24 = ((PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_StaticFields*)il2cpp_codegen_static_fields_for(PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_il2cpp_TypeInfo_var))->___grassLayerMask_14;
		int32_t L_25 = V_1;
		TerrainLayerU5BU5D_t259E391D6115F121FCD284E79F62012D70956EB0* L_26 = ___grassLayers1;
		TerrainLayerU5BU5D_t259E391D6115F121FCD284E79F62012D70956EB0* L_27 = V_0;
		int32_t L_28 = V_1;
		NullCheck(L_27);
		int32_t L_29 = L_28;
		TerrainLayer_t52E14A94A0CF76B0B5509B7FDFDE64FF8A9FEFF9* L_30 = (L_27)->GetAt(static_cast<il2cpp_array_size_t>(L_29));
		bool L_31;
		L_31 = Enumerable_Contains_TisTerrainLayer_t52E14A94A0CF76B0B5509B7FDFDE64FF8A9FEFF9_m6D7B0AFE1EC7DB5185D1BB1A733E959511E0A6D1((RuntimeObject*)L_26, L_30, Enumerable_Contains_TisTerrainLayer_t52E14A94A0CF76B0B5509B7FDFDE64FF8A9FEFF9_m6D7B0AFE1EC7DB5185D1BB1A733E959511E0A6D1_RuntimeMethod_var);
		NullCheck(L_24);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(L_25), (bool)L_31);
		// layerTextures[i] = CreateTextureCopy(terrain.terrainLayers[i].diffuseTexture, 32, 32);
		Texture2DU5BU5D_t05332F1E3F7D4493E304C702201F9BE4F9236191* L_32 = ((PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_StaticFields*)il2cpp_codegen_static_fields_for(PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_il2cpp_TypeInfo_var))->___layerTextures_15;
		int32_t L_33 = V_1;
		TerrainData_t615A68EAC648066681875D47FC641496D12F2E24* L_34 = ___terrain0;
		NullCheck(L_34);
		TerrainLayerU5BU5D_t259E391D6115F121FCD284E79F62012D70956EB0* L_35;
		L_35 = TerrainData_get_terrainLayers_m3B436DF37DDD9F18A46DD6BF112925AD5B8857C8(L_34, NULL);
		int32_t L_36 = V_1;
		NullCheck(L_35);
		int32_t L_37 = L_36;
		TerrainLayer_t52E14A94A0CF76B0B5509B7FDFDE64FF8A9FEFF9* L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		NullCheck(L_38);
		Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* L_39;
		L_39 = TerrainLayer_get_diffuseTexture_mAF75D09F08293C26B26D7D422B4A0ACC9732DD31(L_38, NULL);
		Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* L_40;
		L_40 = PointGrassCommon_CreateTextureCopy_mADE8309152AFE527D5756F91C7491A9B0C2A8C1F(L_39, ((int32_t)32), ((int32_t)32), NULL);
		NullCheck(L_32);
		ArrayElementTypeCheck (L_32, L_40);
		(L_32)->SetAt(static_cast<il2cpp_array_size_t>(L_33), (Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4*)L_40);
		// layerOffsets[i] = terrain.terrainLayers[i].tileOffset;
		Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* L_41 = ((PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_StaticFields*)il2cpp_codegen_static_fields_for(PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_il2cpp_TypeInfo_var))->___layerOffsets_16;
		int32_t L_42 = V_1;
		TerrainData_t615A68EAC648066681875D47FC641496D12F2E24* L_43 = ___terrain0;
		NullCheck(L_43);
		TerrainLayerU5BU5D_t259E391D6115F121FCD284E79F62012D70956EB0* L_44;
		L_44 = TerrainData_get_terrainLayers_m3B436DF37DDD9F18A46DD6BF112925AD5B8857C8(L_43, NULL);
		int32_t L_45 = V_1;
		NullCheck(L_44);
		int32_t L_46 = L_45;
		TerrainLayer_t52E14A94A0CF76B0B5509B7FDFDE64FF8A9FEFF9* L_47 = (L_44)->GetAt(static_cast<il2cpp_array_size_t>(L_46));
		NullCheck(L_47);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_48;
		L_48 = TerrainLayer_get_tileOffset_m1E81B6D002E91BE4EF402337C595227221E8163D(L_47, NULL);
		NullCheck(L_41);
		(L_41)->SetAt(static_cast<il2cpp_array_size_t>(L_42), (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7)L_48);
		// layerSizes[i] = terrain.terrainLayers[i].tileSize;
		Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* L_49 = ((PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_StaticFields*)il2cpp_codegen_static_fields_for(PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_il2cpp_TypeInfo_var))->___layerSizes_17;
		int32_t L_50 = V_1;
		TerrainData_t615A68EAC648066681875D47FC641496D12F2E24* L_51 = ___terrain0;
		NullCheck(L_51);
		TerrainLayerU5BU5D_t259E391D6115F121FCD284E79F62012D70956EB0* L_52;
		L_52 = TerrainData_get_terrainLayers_m3B436DF37DDD9F18A46DD6BF112925AD5B8857C8(L_51, NULL);
		int32_t L_53 = V_1;
		NullCheck(L_52);
		int32_t L_54 = L_53;
		TerrainLayer_t52E14A94A0CF76B0B5509B7FDFDE64FF8A9FEFF9* L_55 = (L_52)->GetAt(static_cast<il2cpp_array_size_t>(L_54));
		NullCheck(L_55);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_56;
		L_56 = TerrainLayer_get_tileSize_mF6313141BE22D6AC7362DA9A40FB41236458E0A3(L_55, NULL);
		NullCheck(L_49);
		(L_49)->SetAt(static_cast<il2cpp_array_size_t>(L_50), (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7)L_56);
		// for (int i = 0; i < numLayers; i++) {
		int32_t L_57 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add(L_57, 1));
	}

IL_00f6:
	{
		// for (int i = 0; i < numLayers; i++) {
		int32_t L_58 = V_1;
		int32_t L_59 = ((PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_StaticFields*)il2cpp_codegen_static_fields_for(PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_il2cpp_TypeInfo_var))->___numLayers_11;
		if ((((int32_t)L_58) < ((int32_t)L_59)))
		{
			goto IL_0095;
		}
	}
	{
		// }
		return;
	}
}
// MicahW.PointGrass.PointGrassCommon/MeshData MicahW.PointGrass.PointGrassCommon::CreateMeshFromTerrainData(UnityEngine.TerrainData,System.Single,System.Int32,System.Int32,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F* PointGrassCommon_CreateMeshFromTerrainData_mA5C0F6BD781BEB33C68AF5BF697E1FAE23002E55 (TerrainData_t615A68EAC648066681875D47FC641496D12F2E24* ___terrain0, float ___densityCutoff1, int32_t ___startX2, int32_t ___startY3, int32_t ___sizeX4, int32_t ___sizeY5, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ColorU5BU5D_t612261CF293F6FFC3D80AB52259FF0DC2B2CC389_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* V_0 = NULL;
	Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* V_1 = NULL;
	Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* V_2 = NULL;
	ColorU5BU5D_t612261CF293F6FFC3D80AB52259FF0DC2B2CC389* V_3 = NULL;
	Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* V_4 = NULL;
	Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* V_5 = NULL;
	float V_6 = 0.0f;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 V_7;
	memset((&V_7), 0, sizeof(V_7));
	int32_t V_8 = 0;
	int32_t V_9 = 0;
	int32_t V_10 = 0;
	int32_t V_11 = 0;
	int32_t V_12 = 0;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_13;
	memset((&V_13), 0, sizeof(V_13));
	float V_14 = 0.0f;
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F V_15;
	memset((&V_15), 0, sizeof(V_15));
	int32_t V_16 = 0;
	int32_t V_17 = 0;
	float V_18 = 0.0f;
	float V_19 = 0.0f;
	int32_t V_20 = 0;
	float V_21 = 0.0f;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 V_22;
	memset((&V_22), 0, sizeof(V_22));
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 V_23;
	memset((&V_23), 0, sizeof(V_23));
	int32_t V_24 = 0;
	int32_t V_25 = 0;
	int32_t V_26 = 0;
	int32_t V_27 = 0;
	{
		// int vertCount = sizeX * sizeY;
		int32_t L_0 = ___sizeX4;
		int32_t L_1 = ___sizeY5;
		// Vector3[] vertices = new Vector3[vertCount];
		int32_t L_2 = ((int32_t)il2cpp_codegen_multiply(L_0, L_1));
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_3 = (Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C*)(Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C*)SZArrayNew(Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C_il2cpp_TypeInfo_var, (uint32_t)L_2);
		V_0 = L_3;
		// Vector3[] normals = new Vector3[vertCount];
		int32_t L_4 = L_2;
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_5 = (Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C*)(Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C*)SZArrayNew(Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C_il2cpp_TypeInfo_var, (uint32_t)L_4);
		V_1 = L_5;
		// Vector2[] uvs = new Vector2[vertCount];
		int32_t L_6 = L_4;
		Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* L_7 = (Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA*)(Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA*)SZArrayNew(Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA_il2cpp_TypeInfo_var, (uint32_t)L_6);
		V_2 = L_7;
		// Color[] colors = new Color[vertCount];
		int32_t L_8 = L_6;
		ColorU5BU5D_t612261CF293F6FFC3D80AB52259FF0DC2B2CC389* L_9 = (ColorU5BU5D_t612261CF293F6FFC3D80AB52259FF0DC2B2CC389*)(ColorU5BU5D_t612261CF293F6FFC3D80AB52259FF0DC2B2CC389*)SZArrayNew(ColorU5BU5D_t612261CF293F6FFC3D80AB52259FF0DC2B2CC389_il2cpp_TypeInfo_var, (uint32_t)L_8);
		V_3 = L_9;
		// Vector2[] attrs = new Vector2[vertCount];
		Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* L_10 = (Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA*)(Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA*)SZArrayNew(Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA_il2cpp_TypeInfo_var, (uint32_t)L_8);
		V_4 = L_10;
		// int triCount = (sizeX - 1) * (sizeY - 1) * 2;
		int32_t L_11 = ___sizeX4;
		int32_t L_12 = ___sizeY5;
		// int[] tris = new int[triCount * 3];
		Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_13 = (Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C*)(Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C*)SZArrayNew(Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C_il2cpp_TypeInfo_var, (uint32_t)((int32_t)il2cpp_codegen_multiply(((int32_t)il2cpp_codegen_multiply(((int32_t)il2cpp_codegen_multiply(((int32_t)il2cpp_codegen_subtract(L_11, 1)), ((int32_t)il2cpp_codegen_subtract(L_12, 1)))), 2)), 3)));
		V_5 = L_13;
		// float maskDivisor = 1f - densityCutoff;
		float L_14 = ___densityCutoff1;
		V_6 = ((float)il2cpp_codegen_subtract((1.0f), L_14));
		// Vector2 uv = Vector2.zero;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_15;
		L_15 = Vector2_get_zero_m32506C40EC2EE7D5D4410BF40D3EE683A3D5F32C_inline(NULL);
		V_7 = L_15;
		// for (int x = 0; x < sizeX; x++) {
		V_8 = 0;
		goto IL_023f;
	}

IL_0054:
	{
		// int terrainSpaceX = x + startX;
		int32_t L_16 = V_8;
		int32_t L_17 = ___startX2;
		V_9 = ((int32_t)il2cpp_codegen_add(L_16, L_17));
		// uv.x = Mathf.Clamp01((float)terrainSpaceX / (heightmapSize - 1));
		int32_t L_18 = V_9;
		int32_t L_19 = ((PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_StaticFields*)il2cpp_codegen_static_fields_for(PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_il2cpp_TypeInfo_var))->___heightmapSize_8;
		float L_20;
		L_20 = Mathf_Clamp01_mA7E048DBDA832D399A581BE4D6DED9FA44CE0F14_inline(((float)(((float)L_18)/((float)((int32_t)il2cpp_codegen_subtract(L_19, 1))))), NULL);
		(&V_7)->___x_0 = L_20;
		// for (int y = 0; y < sizeY; y++) {
		V_10 = 0;
		goto IL_0230;
	}

IL_007a:
	{
		// int terrainSpaceY = y + startY;
		int32_t L_21 = V_10;
		int32_t L_22 = ___startY3;
		V_11 = ((int32_t)il2cpp_codegen_add(L_21, L_22));
		// uv.y = Mathf.Clamp01((float)terrainSpaceY / (heightmapSize - 1));
		int32_t L_23 = V_11;
		int32_t L_24 = ((PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_StaticFields*)il2cpp_codegen_static_fields_for(PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_il2cpp_TypeInfo_var))->___heightmapSize_8;
		float L_25;
		L_25 = Mathf_Clamp01_mA7E048DBDA832D399A581BE4D6DED9FA44CE0F14_inline(((float)(((float)L_23)/((float)((int32_t)il2cpp_codegen_subtract(L_24, 1))))), NULL);
		(&V_7)->___y_1 = L_25;
		// int vertexIndex = x + y * sizeX;
		int32_t L_26 = V_8;
		int32_t L_27 = V_10;
		int32_t L_28 = ___sizeX4;
		V_12 = ((int32_t)il2cpp_codegen_add(L_26, ((int32_t)il2cpp_codegen_multiply(L_27, L_28))));
		// Vector3 vertPos = new Vector3(uv.x, 0f, uv.y);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_29 = V_7;
		float L_30 = L_29.___x_0;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_31 = V_7;
		float L_32 = L_31.___y_1;
		Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline((&V_13), L_30, (0.0f), L_32, NULL);
		// vertPos.Scale(terrainSize);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_33 = ((PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_StaticFields*)il2cpp_codegen_static_fields_for(PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_il2cpp_TypeInfo_var))->___terrainSize_12;
		Vector3_Scale_mE0DC2C1B7902271788591F17DBE7F7F72EC37283_inline((&V_13), L_33, NULL);
		// vertPos.y = terrain.GetHeight(terrainSpaceX, terrainSpaceY);
		TerrainData_t615A68EAC648066681875D47FC641496D12F2E24* L_34 = ___terrain0;
		int32_t L_35 = V_9;
		int32_t L_36 = V_11;
		NullCheck(L_34);
		float L_37;
		L_37 = TerrainData_GetHeight_mC0FAC93E5B8128D5B918EDDDFEE55F6E3E23E8D5(L_34, L_35, L_36, NULL);
		(&V_13)->___y_3 = L_37;
		// float grassMask = 0f;
		V_14 = (0.0f);
		// Color terrainColor = Color.black;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_38;
		L_38 = Color_get_black_mB50217951591A045844C61E7FF31EEE3FEF16737_inline(NULL);
		V_15 = L_38;
		// int coordX = Mathf.FloorToInt(uv.x * (alphamapWidth - 1));
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_39 = V_7;
		float L_40 = L_39.___x_0;
		int32_t L_41 = ((PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_StaticFields*)il2cpp_codegen_static_fields_for(PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_il2cpp_TypeInfo_var))->___alphamapWidth_9;
		int32_t L_42;
		L_42 = Mathf_FloorToInt_m2A39AE881CAEE6B6A4B3BFEF9CA1ED40625F5AB7_inline(((float)il2cpp_codegen_multiply(L_40, ((float)((int32_t)il2cpp_codegen_subtract(L_41, 1))))), NULL);
		V_16 = L_42;
		// int coordY = Mathf.FloorToInt(uv.y * (alphamapHeight - 1));
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_43 = V_7;
		float L_44 = L_43.___y_1;
		int32_t L_45 = ((PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_StaticFields*)il2cpp_codegen_static_fields_for(PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_il2cpp_TypeInfo_var))->___alphamapHeight_10;
		int32_t L_46;
		L_46 = Mathf_FloorToInt_m2A39AE881CAEE6B6A4B3BFEF9CA1ED40625F5AB7_inline(((float)il2cpp_codegen_multiply(L_44, ((float)((int32_t)il2cpp_codegen_subtract(L_45, 1))))), NULL);
		V_17 = L_46;
		// for (int i = 0; i < numLayers; i++) {
		V_20 = 0;
		goto IL_01b5;
	}

IL_011d:
	{
		// float alphamapVal = alphamaps[coordY, coordX, i];
		SingleU5BU2CU2CU5D_tE902E5192C7283A470AAADB477117789A9682488* L_47 = ((PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_StaticFields*)il2cpp_codegen_static_fields_for(PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_il2cpp_TypeInfo_var))->___alphamaps_13;
		int32_t L_48 = V_17;
		int32_t L_49 = V_16;
		int32_t L_50 = V_20;
		NullCheck(L_47);
		float L_51;
		L_51 = (L_47)->GetAt(L_48, L_49, L_50);
		V_21 = L_51;
		// if (grassLayerMask[i]) { grassMask += alphamapVal; }
		BooleanU5BU5D_tD317D27C31DB892BE79FAE3AEBC0B3FFB73DE9B4* L_52 = ((PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_StaticFields*)il2cpp_codegen_static_fields_for(PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_il2cpp_TypeInfo_var))->___grassLayerMask_14;
		int32_t L_53 = V_20;
		NullCheck(L_52);
		int32_t L_54 = L_53;
		uint8_t L_55 = (uint8_t)(L_52)->GetAt(static_cast<il2cpp_array_size_t>(L_54));
		if (!L_55)
		{
			goto IL_0140;
		}
	}
	{
		// if (grassLayerMask[i]) { grassMask += alphamapVal; }
		float L_56 = V_14;
		float L_57 = V_21;
		V_14 = ((float)il2cpp_codegen_add(L_56, L_57));
	}

IL_0140:
	{
		// Vector2 texSize = layerSizes[i];
		Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* L_58 = ((PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_StaticFields*)il2cpp_codegen_static_fields_for(PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_il2cpp_TypeInfo_var))->___layerSizes_17;
		int32_t L_59 = V_20;
		NullCheck(L_58);
		int32_t L_60 = L_59;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_61 = (L_58)->GetAt(static_cast<il2cpp_array_size_t>(L_60));
		V_22 = L_61;
		// Vector2 texUV = new Vector2(vertPos.x / texSize.x, vertPos.z / texSize.y) + layerOffsets[i];
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_62 = V_13;
		float L_63 = L_62.___x_2;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_64 = V_22;
		float L_65 = L_64.___x_0;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_66 = V_13;
		float L_67 = L_66.___z_4;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_68 = V_22;
		float L_69 = L_68.___y_1;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_70;
		memset((&L_70), 0, sizeof(L_70));
		Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline((&L_70), ((float)(L_63/L_65)), ((float)(L_67/L_69)), /*hidden argument*/NULL);
		Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* L_71 = ((PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_StaticFields*)il2cpp_codegen_static_fields_for(PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_il2cpp_TypeInfo_var))->___layerOffsets_16;
		int32_t L_72 = V_20;
		NullCheck(L_71);
		int32_t L_73 = L_72;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_74 = (L_71)->GetAt(static_cast<il2cpp_array_size_t>(L_73));
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_75;
		L_75 = Vector2_op_Addition_m8136742CE6EE33BA4EB81C5F584678455917D2AE_inline(L_70, L_74, NULL);
		V_23 = L_75;
		// terrainColor += layerTextures[i].GetPixelBilinear(texUV.x, texUV.y) * alphamapVal;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_76 = V_15;
		Texture2DU5BU5D_t05332F1E3F7D4493E304C702201F9BE4F9236191* L_77 = ((PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_StaticFields*)il2cpp_codegen_static_fields_for(PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_il2cpp_TypeInfo_var))->___layerTextures_15;
		int32_t L_78 = V_20;
		NullCheck(L_77);
		int32_t L_79 = L_78;
		Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* L_80 = (L_77)->GetAt(static_cast<il2cpp_array_size_t>(L_79));
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_81 = V_23;
		float L_82 = L_81.___x_0;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_83 = V_23;
		float L_84 = L_83.___y_1;
		NullCheck(L_80);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_85;
		L_85 = Texture2D_GetPixelBilinear_m6AE4AF4FD181C478DF0F2C5C329F22A263ABFF5C(L_80, L_82, L_84, NULL);
		float L_86 = V_21;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_87;
		L_87 = Color_op_Multiply_m379B20A820266ACF82A21425B9CAE8DCD773CFBB_inline(L_85, L_86, NULL);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_88;
		L_88 = Color_op_Addition_mA7A51CACA49ED8D23D3D9CA3A0092D32F657E053_inline(L_76, L_87, NULL);
		V_15 = L_88;
		// for (int i = 0; i < numLayers; i++) {
		int32_t L_89 = V_20;
		V_20 = ((int32_t)il2cpp_codegen_add(L_89, 1));
	}

IL_01b5:
	{
		// for (int i = 0; i < numLayers; i++) {
		int32_t L_90 = V_20;
		int32_t L_91 = ((PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_StaticFields*)il2cpp_codegen_static_fields_for(PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_il2cpp_TypeInfo_var))->___numLayers_11;
		if ((((int32_t)L_90) < ((int32_t)L_91)))
		{
			goto IL_011d;
		}
	}
	{
		// if (maskDivisor <= 0f) { grassMask = 0f; }
		float L_92 = V_6;
		if ((!(((float)L_92) <= ((float)(0.0f)))))
		{
			goto IL_01d3;
		}
	}
	{
		// if (maskDivisor <= 0f) { grassMask = 0f; }
		V_14 = (0.0f);
		goto IL_01e1;
	}

IL_01d3:
	{
		// else { grassMask = Mathf.Clamp01((grassMask - densityCutoff) / maskDivisor); }
		float L_93 = V_14;
		float L_94 = ___densityCutoff1;
		float L_95 = V_6;
		float L_96;
		L_96 = Mathf_Clamp01_mA7E048DBDA832D399A581BE4D6DED9FA44CE0F14_inline(((float)(((float)il2cpp_codegen_subtract(L_93, L_94))/L_95)), NULL);
		V_14 = L_96;
	}

IL_01e1:
	{
		// float bladeDensity = grassMask;
		float L_97 = V_14;
		V_18 = L_97;
		// float bladeHeight = grassMask;
		float L_98 = V_14;
		V_19 = L_98;
		// vertices[vertexIndex] = vertPos;
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_99 = V_0;
		int32_t L_100 = V_12;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_101 = V_13;
		NullCheck(L_99);
		(L_99)->SetAt(static_cast<il2cpp_array_size_t>(L_100), (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2)L_101);
		// uvs[vertexIndex] = new Vector2(vertPos.x, vertPos.z);
		Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* L_102 = V_2;
		int32_t L_103 = V_12;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_104 = V_13;
		float L_105 = L_104.___x_2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_106 = V_13;
		float L_107 = L_106.___z_4;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_108;
		memset((&L_108), 0, sizeof(L_108));
		Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline((&L_108), L_105, L_107, /*hidden argument*/NULL);
		NullCheck(L_102);
		(L_102)->SetAt(static_cast<il2cpp_array_size_t>(L_103), (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7)L_108);
		// attrs[vertexIndex] = new Vector2(bladeDensity, bladeHeight);
		Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* L_109 = V_4;
		int32_t L_110 = V_12;
		float L_111 = V_18;
		float L_112 = V_19;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_113;
		memset((&L_113), 0, sizeof(L_113));
		Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline((&L_113), L_111, L_112, /*hidden argument*/NULL);
		NullCheck(L_109);
		(L_109)->SetAt(static_cast<il2cpp_array_size_t>(L_110), (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7)L_113);
		// colors[vertexIndex] = terrainColor;
		ColorU5BU5D_t612261CF293F6FFC3D80AB52259FF0DC2B2CC389* L_114 = V_3;
		int32_t L_115 = V_12;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_116 = V_15;
		NullCheck(L_114);
		(L_114)->SetAt(static_cast<il2cpp_array_size_t>(L_115), (Color_tD001788D726C3A7F1379BEED0260B9591F440C1F)L_116);
		// for (int y = 0; y < sizeY; y++) {
		int32_t L_117 = V_10;
		V_10 = ((int32_t)il2cpp_codegen_add(L_117, 1));
	}

IL_0230:
	{
		// for (int y = 0; y < sizeY; y++) {
		int32_t L_118 = V_10;
		int32_t L_119 = ___sizeY5;
		if ((((int32_t)L_118) < ((int32_t)L_119)))
		{
			goto IL_007a;
		}
	}
	{
		// for (int x = 0; x < sizeX; x++) {
		int32_t L_120 = V_8;
		V_8 = ((int32_t)il2cpp_codegen_add(L_120, 1));
	}

IL_023f:
	{
		// for (int x = 0; x < sizeX; x++) {
		int32_t L_121 = V_8;
		int32_t L_122 = ___sizeX4;
		if ((((int32_t)L_121) < ((int32_t)L_122)))
		{
			goto IL_0054;
		}
	}
	{
		// for (int x = 0; x < sizeX - 1; x++) {
		V_24 = 0;
		goto IL_02c1;
	}

IL_024d:
	{
		// for (int y = 0; y < sizeY - 1; y++) {
		V_25 = 0;
		goto IL_02b3;
	}

IL_0252:
	{
		// int index = x + y * sizeX;
		int32_t L_123 = V_24;
		int32_t L_124 = V_25;
		int32_t L_125 = ___sizeX4;
		V_26 = ((int32_t)il2cpp_codegen_add(L_123, ((int32_t)il2cpp_codegen_multiply(L_124, L_125))));
		// int triIndex = (x + y * (sizeX - 1)) * 6;
		int32_t L_126 = V_24;
		int32_t L_127 = V_25;
		int32_t L_128 = ___sizeX4;
		V_27 = ((int32_t)il2cpp_codegen_multiply(((int32_t)il2cpp_codegen_add(L_126, ((int32_t)il2cpp_codegen_multiply(L_127, ((int32_t)il2cpp_codegen_subtract(L_128, 1)))))), 6));
		// tris[triIndex] = index;
		Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_129 = V_5;
		int32_t L_130 = V_27;
		int32_t L_131 = V_26;
		NullCheck(L_129);
		(L_129)->SetAt(static_cast<il2cpp_array_size_t>(L_130), (int32_t)L_131);
		// tris[triIndex + 1] = index + sizeX;       // +1 Y
		Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_132 = V_5;
		int32_t L_133 = V_27;
		int32_t L_134 = V_26;
		int32_t L_135 = ___sizeX4;
		NullCheck(L_132);
		(L_132)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)il2cpp_codegen_add(L_133, 1))), (int32_t)((int32_t)il2cpp_codegen_add(L_134, L_135)));
		// tris[triIndex + 2] = index + 1;           // +1 X
		Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_136 = V_5;
		int32_t L_137 = V_27;
		int32_t L_138 = V_26;
		NullCheck(L_136);
		(L_136)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)il2cpp_codegen_add(L_137, 2))), (int32_t)((int32_t)il2cpp_codegen_add(L_138, 1)));
		// tris[triIndex + 3] = index + sizeX;       // +1 Y
		Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_139 = V_5;
		int32_t L_140 = V_27;
		int32_t L_141 = V_26;
		int32_t L_142 = ___sizeX4;
		NullCheck(L_139);
		(L_139)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)il2cpp_codegen_add(L_140, 3))), (int32_t)((int32_t)il2cpp_codegen_add(L_141, L_142)));
		// tris[triIndex + 4] = index + sizeX + 1;   // +1 Y +1 X
		Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_143 = V_5;
		int32_t L_144 = V_27;
		int32_t L_145 = V_26;
		int32_t L_146 = ___sizeX4;
		NullCheck(L_143);
		(L_143)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)il2cpp_codegen_add(L_144, 4))), (int32_t)((int32_t)il2cpp_codegen_add(((int32_t)il2cpp_codegen_add(L_145, L_146)), 1)));
		// tris[triIndex + 5] = index + 1;           // +1 X
		Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_147 = V_5;
		int32_t L_148 = V_27;
		int32_t L_149 = V_26;
		NullCheck(L_147);
		(L_147)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)il2cpp_codegen_add(L_148, 5))), (int32_t)((int32_t)il2cpp_codegen_add(L_149, 1)));
		// for (int y = 0; y < sizeY - 1; y++) {
		int32_t L_150 = V_25;
		V_25 = ((int32_t)il2cpp_codegen_add(L_150, 1));
	}

IL_02b3:
	{
		// for (int y = 0; y < sizeY - 1; y++) {
		int32_t L_151 = V_25;
		int32_t L_152 = ___sizeY5;
		if ((((int32_t)L_151) < ((int32_t)((int32_t)il2cpp_codegen_subtract(L_152, 1)))))
		{
			goto IL_0252;
		}
	}
	{
		// for (int x = 0; x < sizeX - 1; x++) {
		int32_t L_153 = V_24;
		V_24 = ((int32_t)il2cpp_codegen_add(L_153, 1));
	}

IL_02c1:
	{
		// for (int x = 0; x < sizeX - 1; x++) {
		int32_t L_154 = V_24;
		int32_t L_155 = ___sizeX4;
		if ((((int32_t)L_154) < ((int32_t)((int32_t)il2cpp_codegen_subtract(L_155, 1)))))
		{
			goto IL_024d;
		}
	}
	{
		// MeshData output = new MeshData(vertices, normals, uvs, colors, tris, attrs);
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_156 = V_0;
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_157 = V_1;
		Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* L_158 = V_2;
		ColorU5BU5D_t612261CF293F6FFC3D80AB52259FF0DC2B2CC389* L_159 = V_3;
		Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_160 = V_5;
		Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* L_161 = V_4;
		MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F* L_162 = (MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F*)il2cpp_codegen_object_new(MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F_il2cpp_TypeInfo_var);
		NullCheck(L_162);
		MeshData__ctor_m70ACE57271D2DB0D80D16027646899AFC5035A50(L_162, L_156, L_157, L_158, L_159, L_160, L_161, NULL);
		// output.RecalculateNormals();
		MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F* L_163 = L_162;
		NullCheck(L_163);
		MeshData_RecalculateNormals_m3DB8055C317DE19049DCC6647BCCB199FAF40917(L_163, NULL);
		// return output;
		return L_163;
	}
}
// MicahW.PointGrass.PointGrassCommon/MeshData MicahW.PointGrass.PointGrassCommon::CreateMeshFromFilters(UnityEngine.Transform,UnityEngine.MeshFilter[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F* PointGrassCommon_CreateMeshFromFilters_mD660AD50FC9DD73F259A0D447B2C689EF1A437B3 (Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___parent0, MeshFilterU5BU5D_tCE3B457E6F7ECE5ECEE9E09150642150448685BA* ___filters1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* V_2 = NULL;
	Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* V_3 = NULL;
	Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* V_4 = NULL;
	Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* V_5 = NULL;
	Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* V_6 = NULL;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	int32_t V_9 = 0;
	int32_t V_10 = 0;
	Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* V_11 = NULL;
	int32_t V_12 = 0;
	int32_t V_13 = 0;
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* V_14 = NULL;
	int32_t V_15 = 0;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_16;
	memset((&V_16), 0, sizeof(V_16));
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_17;
	memset((&V_17), 0, sizeof(V_17));
	int32_t V_18 = 0;
	int32_t V_19 = 0;
	int32_t V_20 = 0;
	{
		// int totalVertCount = 0;
		V_0 = 0;
		// int totalTriCount = 0;
		V_1 = 0;
		// for (int i = 0; i < filters.Length; i++) {
		V_9 = 0;
		goto IL_0050;
	}

IL_0009:
	{
		// if (filters[i] != null && filters[i].sharedMesh != null) {
		MeshFilterU5BU5D_tCE3B457E6F7ECE5ECEE9E09150642150448685BA* L_0 = ___filters1;
		int32_t L_1 = V_9;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		MeshFilter_t6D1CE2473A1E45AC73013400585A1163BF66B2F5* L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_4;
		L_4 = Object_op_Inequality_mD0BE578448EAA61948F25C32F8DD55AB1F778602(L_3, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_4)
		{
			goto IL_004a;
		}
	}
	{
		MeshFilterU5BU5D_tCE3B457E6F7ECE5ECEE9E09150642150448685BA* L_5 = ___filters1;
		int32_t L_6 = V_9;
		NullCheck(L_5);
		int32_t L_7 = L_6;
		MeshFilter_t6D1CE2473A1E45AC73013400585A1163BF66B2F5* L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		NullCheck(L_8);
		Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* L_9;
		L_9 = MeshFilter_get_sharedMesh_mE4ED3E7E31C1DE5097E4980DA996E620F7D7CB8C(L_8, NULL);
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_10;
		L_10 = Object_op_Inequality_mD0BE578448EAA61948F25C32F8DD55AB1F778602(L_9, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_10)
		{
			goto IL_004a;
		}
	}
	{
		// totalVertCount += filters[i].sharedMesh.vertexCount;
		int32_t L_11 = V_0;
		MeshFilterU5BU5D_tCE3B457E6F7ECE5ECEE9E09150642150448685BA* L_12 = ___filters1;
		int32_t L_13 = V_9;
		NullCheck(L_12);
		int32_t L_14 = L_13;
		MeshFilter_t6D1CE2473A1E45AC73013400585A1163BF66B2F5* L_15 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_14));
		NullCheck(L_15);
		Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* L_16;
		L_16 = MeshFilter_get_sharedMesh_mE4ED3E7E31C1DE5097E4980DA996E620F7D7CB8C(L_15, NULL);
		NullCheck(L_16);
		int32_t L_17;
		L_17 = Mesh_get_vertexCount_mB7BE0340AAF272933068D830C8E711FC8978E12C(L_16, NULL);
		V_0 = ((int32_t)il2cpp_codegen_add(L_11, L_17));
		// totalTriCount += filters[i].sharedMesh.triangles.Length;
		int32_t L_18 = V_1;
		MeshFilterU5BU5D_tCE3B457E6F7ECE5ECEE9E09150642150448685BA* L_19 = ___filters1;
		int32_t L_20 = V_9;
		NullCheck(L_19);
		int32_t L_21 = L_20;
		MeshFilter_t6D1CE2473A1E45AC73013400585A1163BF66B2F5* L_22 = (L_19)->GetAt(static_cast<il2cpp_array_size_t>(L_21));
		NullCheck(L_22);
		Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* L_23;
		L_23 = MeshFilter_get_sharedMesh_mE4ED3E7E31C1DE5097E4980DA996E620F7D7CB8C(L_22, NULL);
		NullCheck(L_23);
		Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_24;
		L_24 = Mesh_get_triangles_m33E39B4A383CC613C760FA7E297AC417A433F24B(L_23, NULL);
		NullCheck(L_24);
		V_1 = ((int32_t)il2cpp_codegen_add(L_18, ((int32_t)(((RuntimeArray*)L_24)->max_length))));
	}

IL_004a:
	{
		// for (int i = 0; i < filters.Length; i++) {
		int32_t L_25 = V_9;
		V_9 = ((int32_t)il2cpp_codegen_add(L_25, 1));
	}

IL_0050:
	{
		// for (int i = 0; i < filters.Length; i++) {
		int32_t L_26 = V_9;
		MeshFilterU5BU5D_tCE3B457E6F7ECE5ECEE9E09150642150448685BA* L_27 = ___filters1;
		NullCheck(L_27);
		if ((((int32_t)L_26) < ((int32_t)((int32_t)(((RuntimeArray*)L_27)->max_length)))))
		{
			goto IL_0009;
		}
	}
	{
		// Vector3[] vertsArr = new Vector3[totalVertCount];
		int32_t L_28 = V_0;
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_29 = (Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C*)(Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C*)SZArrayNew(Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C_il2cpp_TypeInfo_var, (uint32_t)L_28);
		V_2 = L_29;
		// Vector3[] normalsArr = new Vector3[totalVertCount];
		int32_t L_30 = V_0;
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_31 = (Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C*)(Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C*)SZArrayNew(Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C_il2cpp_TypeInfo_var, (uint32_t)L_30);
		V_3 = L_31;
		// Vector2[] UVsArr = new Vector2[totalVertCount];
		int32_t L_32 = V_0;
		Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* L_33 = (Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA*)(Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA*)SZArrayNew(Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA_il2cpp_TypeInfo_var, (uint32_t)L_32);
		V_4 = L_33;
		// Vector2[] attrsArr = new Vector2[totalVertCount];
		int32_t L_34 = V_0;
		Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* L_35 = (Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA*)(Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA*)SZArrayNew(Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA_il2cpp_TypeInfo_var, (uint32_t)L_34);
		V_5 = L_35;
		// int[] trisArr = new int[totalTriCount];
		int32_t L_36 = V_1;
		Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_37 = (Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C*)(Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C*)SZArrayNew(Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C_il2cpp_TypeInfo_var, (uint32_t)L_36);
		V_6 = L_37;
		// int processedVertCount = 0;
		V_7 = 0;
		// int processedTriCount = 0;
		V_8 = 0;
		// for (int i = 0; i < filters.Length; i++) {
		V_10 = 0;
		goto IL_0205;
	}

IL_008b:
	{
		// if (filters[i] != null) {
		MeshFilterU5BU5D_tCE3B457E6F7ECE5ECEE9E09150642150448685BA* L_38 = ___filters1;
		int32_t L_39 = V_10;
		NullCheck(L_38);
		int32_t L_40 = L_39;
		MeshFilter_t6D1CE2473A1E45AC73013400585A1163BF66B2F5* L_41 = (L_38)->GetAt(static_cast<il2cpp_array_size_t>(L_40));
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_42;
		L_42 = Object_op_Inequality_mD0BE578448EAA61948F25C32F8DD55AB1F778602(L_41, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_42)
		{
			goto IL_01ff;
		}
	}
	{
		// Mesh filterMesh = filters[i].sharedMesh;
		MeshFilterU5BU5D_tCE3B457E6F7ECE5ECEE9E09150642150448685BA* L_43 = ___filters1;
		int32_t L_44 = V_10;
		NullCheck(L_43);
		int32_t L_45 = L_44;
		MeshFilter_t6D1CE2473A1E45AC73013400585A1163BF66B2F5* L_46 = (L_43)->GetAt(static_cast<il2cpp_array_size_t>(L_45));
		NullCheck(L_46);
		Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* L_47;
		L_47 = MeshFilter_get_sharedMesh_mE4ED3E7E31C1DE5097E4980DA996E620F7D7CB8C(L_46, NULL);
		V_11 = L_47;
		// if (filterMesh != null) {
		Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* L_48 = V_11;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_49;
		L_49 = Object_op_Inequality_mD0BE578448EAA61948F25C32F8DD55AB1F778602(L_48, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_49)
		{
			goto IL_01ff;
		}
	}
	{
		// int meshVertCount = filterMesh.vertexCount;
		Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* L_50 = V_11;
		NullCheck(L_50);
		int32_t L_51;
		L_51 = Mesh_get_vertexCount_mB7BE0340AAF272933068D830C8E711FC8978E12C(L_50, NULL);
		V_12 = L_51;
		// int meshTriCount = filterMesh.triangles.Length;
		Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* L_52 = V_11;
		NullCheck(L_52);
		Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_53;
		L_53 = Mesh_get_triangles_m33E39B4A383CC613C760FA7E297AC417A433F24B(L_52, NULL);
		NullCheck(L_53);
		V_13 = ((int32_t)(((RuntimeArray*)L_53)->max_length));
		// Transform filterTransform = filters[i].transform;
		MeshFilterU5BU5D_tCE3B457E6F7ECE5ECEE9E09150642150448685BA* L_54 = ___filters1;
		int32_t L_55 = V_10;
		NullCheck(L_54);
		int32_t L_56 = L_55;
		MeshFilter_t6D1CE2473A1E45AC73013400585A1163BF66B2F5* L_57 = (L_54)->GetAt(static_cast<il2cpp_array_size_t>(L_56));
		NullCheck(L_57);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_58;
		L_58 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(L_57, NULL);
		V_14 = L_58;
		// for (int j = 0; j < meshVertCount; j++) {
		V_15 = 0;
		goto IL_0130;
	}

IL_00d6:
	{
		// Vector3 newPos = parent.InverseTransformPoint(filterTransform.TransformPoint(filterMesh.vertices[j]));
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_59 = ___parent0;
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_60 = V_14;
		Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* L_61 = V_11;
		NullCheck(L_61);
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_62;
		L_62 = Mesh_get_vertices_mA3577F1B08EDDD54E26AEB3F8FFE4EC247D2ABB9(L_61, NULL);
		int32_t L_63 = V_15;
		NullCheck(L_62);
		int32_t L_64 = L_63;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_65 = (L_62)->GetAt(static_cast<il2cpp_array_size_t>(L_64));
		NullCheck(L_60);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_66;
		L_66 = Transform_TransformPoint_m05BFF013DB830D7BFE44A007703694AE1062EE44(L_60, L_65, NULL);
		NullCheck(L_59);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_67;
		L_67 = Transform_InverseTransformPoint_m18CD395144D9C78F30E15A5B82B6670E792DBA5D(L_59, L_66, NULL);
		V_16 = L_67;
		// Vector3 newNormal = parent.InverseTransformVector(filterTransform.TransformDirection(filterMesh.normals[j]));
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_68 = ___parent0;
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_69 = V_14;
		Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* L_70 = V_11;
		NullCheck(L_70);
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_71;
		L_71 = Mesh_get_normals_m2B6B159B799E6E235EA651FCAB2E18EE5B18ED62(L_70, NULL);
		int32_t L_72 = V_15;
		NullCheck(L_71);
		int32_t L_73 = L_72;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_74 = (L_71)->GetAt(static_cast<il2cpp_array_size_t>(L_73));
		NullCheck(L_69);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_75;
		L_75 = Transform_TransformDirection_m9BE1261DF2D48B7A4A27D31EE24D2D97F89E7757(L_69, L_74, NULL);
		NullCheck(L_68);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_76;
		L_76 = Transform_InverseTransformVector_mBBA687CE32C0394FC9AB4F273D4E4A11F53FB044(L_68, L_75, NULL);
		V_17 = L_76;
		// vertsArr[processedVertCount + j] = newPos;
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_77 = V_2;
		int32_t L_78 = V_7;
		int32_t L_79 = V_15;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_80 = V_16;
		NullCheck(L_77);
		(L_77)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)il2cpp_codegen_add(L_78, L_79))), (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2)L_80);
		// normalsArr[processedVertCount + j] = newNormal;
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_81 = V_3;
		int32_t L_82 = V_7;
		int32_t L_83 = V_15;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_84 = V_17;
		NullCheck(L_81);
		(L_81)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)il2cpp_codegen_add(L_82, L_83))), (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2)L_84);
		// for (int j = 0; j < meshVertCount; j++) {
		int32_t L_85 = V_15;
		V_15 = ((int32_t)il2cpp_codegen_add(L_85, 1));
	}

IL_0130:
	{
		// for (int j = 0; j < meshVertCount; j++) {
		int32_t L_86 = V_15;
		int32_t L_87 = V_12;
		if ((((int32_t)L_86) < ((int32_t)L_87)))
		{
			goto IL_00d6;
		}
	}
	{
		// if (filterMesh.colors != null && filterMesh.colors.Length == meshVertCount) {
		Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* L_88 = V_11;
		NullCheck(L_88);
		ColorU5BU5D_t612261CF293F6FFC3D80AB52259FF0DC2B2CC389* L_89;
		L_89 = Mesh_get_colors_m3A38944EBA064B0E55A24C95C3706193F45B313D(L_88, NULL);
		if (!L_89)
		{
			goto IL_0196;
		}
	}
	{
		Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* L_90 = V_11;
		NullCheck(L_90);
		ColorU5BU5D_t612261CF293F6FFC3D80AB52259FF0DC2B2CC389* L_91;
		L_91 = Mesh_get_colors_m3A38944EBA064B0E55A24C95C3706193F45B313D(L_90, NULL);
		NullCheck(L_91);
		int32_t L_92 = V_12;
		if ((!(((uint32_t)((int32_t)(((RuntimeArray*)L_91)->max_length))) == ((uint32_t)L_92))))
		{
			goto IL_0196;
		}
	}
	{
		// for (int j = 0; j < meshVertCount; j++) {
		V_18 = 0;
		goto IL_018e;
	}

IL_0151:
	{
		// attrsArr[processedVertCount + j] =
		//     new Vector2(filterMesh.colors[i].r, filterMesh.colors[i].g);
		Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* L_93 = V_5;
		int32_t L_94 = V_7;
		int32_t L_95 = V_18;
		Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* L_96 = V_11;
		NullCheck(L_96);
		ColorU5BU5D_t612261CF293F6FFC3D80AB52259FF0DC2B2CC389* L_97;
		L_97 = Mesh_get_colors_m3A38944EBA064B0E55A24C95C3706193F45B313D(L_96, NULL);
		int32_t L_98 = V_10;
		NullCheck(L_97);
		float L_99 = ((L_97)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_98)))->___r_0;
		Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* L_100 = V_11;
		NullCheck(L_100);
		ColorU5BU5D_t612261CF293F6FFC3D80AB52259FF0DC2B2CC389* L_101;
		L_101 = Mesh_get_colors_m3A38944EBA064B0E55A24C95C3706193F45B313D(L_100, NULL);
		int32_t L_102 = V_10;
		NullCheck(L_101);
		float L_103 = ((L_101)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_102)))->___g_1;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_104;
		memset((&L_104), 0, sizeof(L_104));
		Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline((&L_104), L_99, L_103, /*hidden argument*/NULL);
		NullCheck(L_93);
		(L_93)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)il2cpp_codegen_add(L_94, L_95))), (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7)L_104);
		// for (int j = 0; j < meshVertCount; j++) {
		int32_t L_105 = V_18;
		V_18 = ((int32_t)il2cpp_codegen_add(L_105, 1));
	}

IL_018e:
	{
		// for (int j = 0; j < meshVertCount; j++) {
		int32_t L_106 = V_18;
		int32_t L_107 = V_12;
		if ((((int32_t)L_106) < ((int32_t)L_107)))
		{
			goto IL_0151;
		}
	}
	{
		goto IL_01b8;
	}

IL_0196:
	{
		// else { for (int j = 0; j < meshVertCount; j++) { attrsArr[processedVertCount + j] = Vector2.one; } }
		V_19 = 0;
		goto IL_01b2;
	}

IL_019b:
	{
		// else { for (int j = 0; j < meshVertCount; j++) { attrsArr[processedVertCount + j] = Vector2.one; } }
		Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* L_108 = V_5;
		int32_t L_109 = V_7;
		int32_t L_110 = V_19;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_111;
		L_111 = Vector2_get_one_m9097EB8DC23C26118A591AF16702796C3EF51DFB_inline(NULL);
		NullCheck(L_108);
		(L_108)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)il2cpp_codegen_add(L_109, L_110))), (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7)L_111);
		// else { for (int j = 0; j < meshVertCount; j++) { attrsArr[processedVertCount + j] = Vector2.one; } }
		int32_t L_112 = V_19;
		V_19 = ((int32_t)il2cpp_codegen_add(L_112, 1));
	}

IL_01b2:
	{
		// else { for (int j = 0; j < meshVertCount; j++) { attrsArr[processedVertCount + j] = Vector2.one; } }
		int32_t L_113 = V_19;
		int32_t L_114 = V_12;
		if ((((int32_t)L_113) < ((int32_t)L_114)))
		{
			goto IL_019b;
		}
	}

IL_01b8:
	{
		// System.Array.Copy(filterMesh.uv, 0, UVsArr, processedVertCount, meshVertCount);
		Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* L_115 = V_11;
		NullCheck(L_115);
		Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* L_116;
		L_116 = Mesh_get_uv_mA47805C48AB3493FF3727922C43E77880E73519F(L_115, NULL);
		Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* L_117 = V_4;
		int32_t L_118 = V_7;
		int32_t L_119 = V_12;
		Array_Copy_mB4904E17BD92E320613A3251C0205E0786B3BF41((RuntimeArray*)L_116, 0, (RuntimeArray*)L_117, L_118, L_119, NULL);
		// for (int j = 0; j < meshTriCount; j++) {
		V_20 = 0;
		goto IL_01eb;
	}

IL_01d0:
	{
		// trisArr[processedTriCount + j] =
		//     filterMesh.triangles[j] + processedVertCount;
		Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_120 = V_6;
		int32_t L_121 = V_8;
		int32_t L_122 = V_20;
		Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* L_123 = V_11;
		NullCheck(L_123);
		Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_124;
		L_124 = Mesh_get_triangles_m33E39B4A383CC613C760FA7E297AC417A433F24B(L_123, NULL);
		int32_t L_125 = V_20;
		NullCheck(L_124);
		int32_t L_126 = L_125;
		int32_t L_127 = (L_124)->GetAt(static_cast<il2cpp_array_size_t>(L_126));
		int32_t L_128 = V_7;
		NullCheck(L_120);
		(L_120)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)il2cpp_codegen_add(L_121, L_122))), (int32_t)((int32_t)il2cpp_codegen_add(L_127, L_128)));
		// for (int j = 0; j < meshTriCount; j++) {
		int32_t L_129 = V_20;
		V_20 = ((int32_t)il2cpp_codegen_add(L_129, 1));
	}

IL_01eb:
	{
		// for (int j = 0; j < meshTriCount; j++) {
		int32_t L_130 = V_20;
		int32_t L_131 = V_13;
		if ((((int32_t)L_130) < ((int32_t)L_131)))
		{
			goto IL_01d0;
		}
	}
	{
		// processedVertCount += meshVertCount;
		int32_t L_132 = V_7;
		int32_t L_133 = V_12;
		V_7 = ((int32_t)il2cpp_codegen_add(L_132, L_133));
		// processedTriCount += meshTriCount;
		int32_t L_134 = V_8;
		int32_t L_135 = V_13;
		V_8 = ((int32_t)il2cpp_codegen_add(L_134, L_135));
	}

IL_01ff:
	{
		// for (int i = 0; i < filters.Length; i++) {
		int32_t L_136 = V_10;
		V_10 = ((int32_t)il2cpp_codegen_add(L_136, 1));
	}

IL_0205:
	{
		// for (int i = 0; i < filters.Length; i++) {
		int32_t L_137 = V_10;
		MeshFilterU5BU5D_tCE3B457E6F7ECE5ECEE9E09150642150448685BA* L_138 = ___filters1;
		NullCheck(L_138);
		if ((((int32_t)L_137) < ((int32_t)((int32_t)(((RuntimeArray*)L_138)->max_length)))))
		{
			goto IL_008b;
		}
	}
	{
		// MeshData mesh = new MeshData(vertsArr, normalsArr, UVsArr, trisArr, attrsArr);
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_139 = V_2;
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_140 = V_3;
		Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* L_141 = V_4;
		Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_142 = V_6;
		Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* L_143 = V_5;
		MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F* L_144 = (MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F*)il2cpp_codegen_object_new(MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F_il2cpp_TypeInfo_var);
		NullCheck(L_144);
		MeshData__ctor_m4DDE83D4B889F4A8E8CB34522845BB26A776D752(L_144, L_139, L_140, L_141, L_142, L_143, NULL);
		// mesh.RecalculateBounds();
		MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F* L_145 = L_144;
		NullCheck(L_145);
		MeshData_RecalculateBounds_m23CC32EAA976F74CAD691ACF55DEDE635C504655(L_145, NULL);
		// return mesh;
		return L_145;
	}
}
// System.Void MicahW.PointGrass.PointGrassCommon::<GenerateGrassMesh_Cylinder>g__SetVert|17_0(System.Int32,UnityEngine.Vector3,MicahW.PointGrass.PointGrassCommon/<>c__DisplayClass17_0&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointGrassCommon_U3CGenerateGrassMesh_CylinderU3Eg__SetVertU7C17_0_m9E44405726796B377C797C8B52794EF850E59ABD (int32_t ___index0, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___pos1, U3CU3Ec__DisplayClass17_0_t3B43FCEDB710544F3D8D366A3758A6D2CEF70412* p2, const RuntimeMethod* method) 
{
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// Vector2 uv = new Vector2(pos.x * 0.5f + 0.5f, pos.y);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = ___pos1;
		float L_1 = L_0.___x_2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_2 = ___pos1;
		float L_3 = L_2.___y_3;
		Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline((&V_0), ((float)il2cpp_codegen_add(((float)il2cpp_codegen_multiply(L_1, (0.5f))), (0.5f))), L_3, NULL);
		// verts[index] = pos;
		U3CU3Ec__DisplayClass17_0_t3B43FCEDB710544F3D8D366A3758A6D2CEF70412* L_4 = p2;
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_5 = L_4->___verts_0;
		int32_t L_6 = ___index0;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_7 = ___pos1;
		NullCheck(L_5);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(L_6), (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2)L_7);
		// uvs[index] = uv;
		U3CU3Ec__DisplayClass17_0_t3B43FCEDB710544F3D8D366A3758A6D2CEF70412* L_8 = p2;
		Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* L_9 = L_8->___uvs_1;
		int32_t L_10 = ___index0;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_11 = V_0;
		NullCheck(L_9);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(L_10), (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7)L_11);
		// cols[index] = new Color(1f, 1f, 1f, pos.y);
		U3CU3Ec__DisplayClass17_0_t3B43FCEDB710544F3D8D366A3758A6D2CEF70412* L_12 = p2;
		ColorU5BU5D_t612261CF293F6FFC3D80AB52259FF0DC2B2CC389* L_13 = L_12->___cols_2;
		int32_t L_14 = ___index0;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_15 = ___pos1;
		float L_16 = L_15.___y_3;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_17;
		memset((&L_17), 0, sizeof(L_17));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_17), (1.0f), (1.0f), (1.0f), L_16, /*hidden argument*/NULL);
		NullCheck(L_13);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(L_14), (Color_tD001788D726C3A7F1379BEED0260B9591F440C1F)L_17);
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MicahW.PointGrass.PointGrassCommon/MeshPoint::.ctor(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Color,UnityEngine.Vector4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MeshPoint__ctor_m6C2D6745D2F6CAC65B54433FBFAB990C649DC31E (MeshPoint_t033BD3FDC9D80AE452F7B3D583D17CDADF299152* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___position0, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___normal1, Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___color2, Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___extraData3, const RuntimeMethod* method) 
{
	{
		// this.position = position;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = ___position0;
		__this->___position_0 = L_0;
		// this.normal = normal;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_1 = ___normal1;
		__this->___normal_1 = L_1;
		// this.color = color;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_2 = ___color2;
		__this->___color_2 = L_2;
		// this.extraData = extraData;
		Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 L_3 = ___extraData3;
		__this->___extraData_3 = L_3;
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void MeshPoint__ctor_m6C2D6745D2F6CAC65B54433FBFAB990C649DC31E_AdjustorThunk (RuntimeObject* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___position0, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___normal1, Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___color2, Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___extraData3, const RuntimeMethod* method)
{
	MeshPoint_t033BD3FDC9D80AE452F7B3D583D17CDADF299152* _thisAdjusted;
	int32_t _offset = 1;
	_thisAdjusted = reinterpret_cast<MeshPoint_t033BD3FDC9D80AE452F7B3D583D17CDADF299152*>(__this + _offset);
	MeshPoint__ctor_m6C2D6745D2F6CAC65B54433FBFAB990C649DC31E(_thisAdjusted, ___position0, ___normal1, ___color2, ___extraData3, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MicahW.PointGrass.PointGrassCommon/ObjectData::.ctor(UnityEngine.Vector3,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ObjectData__ctor_mB2378318DDD47DCB6AEC9D788C27A66A94AE84CC (ObjectData_t24D94D36E8D49DEB90BBFEE7BA833C910F0AF6C4* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___position0, float ___radius1, float ___strength2, const RuntimeMethod* method) 
{
	{
		// this.position = position;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = ___position0;
		__this->___position_0 = L_0;
		// this.radius = radius;
		float L_1 = ___radius1;
		__this->___radius_1 = L_1;
		// this.strength = strength;
		float L_2 = ___strength2;
		__this->___strength_2 = L_2;
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void ObjectData__ctor_mB2378318DDD47DCB6AEC9D788C27A66A94AE84CC_AdjustorThunk (RuntimeObject* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___position0, float ___radius1, float ___strength2, const RuntimeMethod* method)
{
	ObjectData_t24D94D36E8D49DEB90BBFEE7BA833C910F0AF6C4* _thisAdjusted;
	int32_t _offset = 1;
	_thisAdjusted = reinterpret_cast<ObjectData_t24D94D36E8D49DEB90BBFEE7BA833C910F0AF6C4*>(__this + _offset);
	ObjectData__ctor_mB2378318DDD47DCB6AEC9D788C27A66A94AE84CC(_thisAdjusted, ___position0, ___radius1, ___strength2, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MicahW.PointGrass.PointGrassCommon/MeshData::.ctor(UnityEngine.Vector3[],UnityEngine.Vector3[],UnityEngine.Vector2[],UnityEngine.Color[],System.Int32[],UnityEngine.Vector2[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MeshData__ctor_m70ACE57271D2DB0D80D16027646899AFC5035A50 (MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F* __this, Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* ___verts0, Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* ___normals1, Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* ___UVs2, ColorU5BU5D_t612261CF293F6FFC3D80AB52259FF0DC2B2CC389* ___colours3, Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* ___tris4, Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* ___attributes5, const RuntimeMethod* method) 
{
	{
		// public MeshData(Vector3[] verts, Vector3[] normals, Vector2[] UVs, Color[] colours, int[] tris, Vector2[] attributes) {
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		// this.verts = verts;
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_0 = ___verts0;
		__this->___verts_0 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___verts_0), (void*)L_0);
		// this.normals = normals;
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_1 = ___normals1;
		__this->___normals_1 = L_1;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___normals_1), (void*)L_1);
		// this.UVs = UVs;
		Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* L_2 = ___UVs2;
		__this->___UVs_2 = L_2;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___UVs_2), (void*)L_2);
		// this.colours = colours;
		ColorU5BU5D_t612261CF293F6FFC3D80AB52259FF0DC2B2CC389* L_3 = ___colours3;
		__this->___colours_3 = L_3;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___colours_3), (void*)L_3);
		// this.tris = tris;
		Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_4 = ___tris4;
		__this->___tris_4 = L_4;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___tris_4), (void*)L_4);
		// this.attributes = attributes;
		Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* L_5 = ___attributes5;
		__this->___attributes_5 = L_5;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___attributes_5), (void*)L_5);
		// bounds = new Bounds();
		Bounds_t367E830C64BBF235ED8C3B2F8CF6254FDCAD39C3* L_6 = (&__this->___bounds_6);
		il2cpp_codegen_initobj(L_6, sizeof(Bounds_t367E830C64BBF235ED8C3B2F8CF6254FDCAD39C3));
		// RecalculateBounds();
		MeshData_RecalculateBounds_m23CC32EAA976F74CAD691ACF55DEDE635C504655(__this, NULL);
		// }
		return;
	}
}
// System.Void MicahW.PointGrass.PointGrassCommon/MeshData::.ctor(UnityEngine.Vector3[],UnityEngine.Vector3[],UnityEngine.Vector2[],System.Int32[],UnityEngine.Vector2[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MeshData__ctor_m4DDE83D4B889F4A8E8CB34522845BB26A776D752 (MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F* __this, Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* ___verts0, Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* ___normals1, Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* ___UVs2, Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* ___tris3, Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* ___attributes4, const RuntimeMethod* method) 
{
	{
		// public MeshData(Vector3[] verts, Vector3[] normals, Vector2[] UVs, int[] tris, Vector2[] attributes) {
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		// this.verts = verts;
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_0 = ___verts0;
		__this->___verts_0 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___verts_0), (void*)L_0);
		// this.normals = normals;
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_1 = ___normals1;
		__this->___normals_1 = L_1;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___normals_1), (void*)L_1);
		// this.UVs = UVs;
		Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* L_2 = ___UVs2;
		__this->___UVs_2 = L_2;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___UVs_2), (void*)L_2);
		// this.tris = tris;
		Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_3 = ___tris3;
		__this->___tris_4 = L_3;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___tris_4), (void*)L_3);
		// this.attributes = attributes;
		Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* L_4 = ___attributes4;
		__this->___attributes_5 = L_4;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___attributes_5), (void*)L_4);
		// colours = null;
		__this->___colours_3 = (ColorU5BU5D_t612261CF293F6FFC3D80AB52259FF0DC2B2CC389*)NULL;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___colours_3), (void*)(ColorU5BU5D_t612261CF293F6FFC3D80AB52259FF0DC2B2CC389*)NULL);
		// bounds = new Bounds();
		Bounds_t367E830C64BBF235ED8C3B2F8CF6254FDCAD39C3* L_5 = (&__this->___bounds_6);
		il2cpp_codegen_initobj(L_5, sizeof(Bounds_t367E830C64BBF235ED8C3B2F8CF6254FDCAD39C3));
		// RecalculateBounds();
		MeshData_RecalculateBounds_m23CC32EAA976F74CAD691ACF55DEDE635C504655(__this, NULL);
		// }
		return;
	}
}
// System.Void MicahW.PointGrass.PointGrassCommon/MeshData::.ctor(UnityEngine.Vector3[],UnityEngine.Vector3[],UnityEngine.Vector2[],System.Int32[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MeshData__ctor_mA84B1EAEC99B47A4A6F11607DB3AA143B334926C (MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F* __this, Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* ___verts0, Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* ___normals1, Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* ___UVs2, Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* ___tris3, const RuntimeMethod* method) 
{
	{
		// public MeshData(Vector3[] verts, Vector3[] normals, Vector2[] UVs, int[] tris) {
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		// this.verts = verts;
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_0 = ___verts0;
		__this->___verts_0 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___verts_0), (void*)L_0);
		// this.normals = normals;
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_1 = ___normals1;
		__this->___normals_1 = L_1;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___normals_1), (void*)L_1);
		// this.UVs = UVs;
		Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* L_2 = ___UVs2;
		__this->___UVs_2 = L_2;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___UVs_2), (void*)L_2);
		// this.tris = tris;
		Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_3 = ___tris3;
		__this->___tris_4 = L_3;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___tris_4), (void*)L_3);
		// colours = null;
		__this->___colours_3 = (ColorU5BU5D_t612261CF293F6FFC3D80AB52259FF0DC2B2CC389*)NULL;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___colours_3), (void*)(ColorU5BU5D_t612261CF293F6FFC3D80AB52259FF0DC2B2CC389*)NULL);
		// attributes = null;
		__this->___attributes_5 = (Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA*)NULL;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___attributes_5), (void*)(Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA*)NULL);
		// bounds = new Bounds();
		Bounds_t367E830C64BBF235ED8C3B2F8CF6254FDCAD39C3* L_4 = (&__this->___bounds_6);
		il2cpp_codegen_initobj(L_4, sizeof(Bounds_t367E830C64BBF235ED8C3B2F8CF6254FDCAD39C3));
		// RecalculateBounds();
		MeshData_RecalculateBounds_m23CC32EAA976F74CAD691ACF55DEDE635C504655(__this, NULL);
		// }
		return;
	}
}
// System.Boolean MicahW.PointGrass.PointGrassCommon/MeshData::get_HasColours()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool MeshData_get_HasColours_m67226DD3676EBC3004861960060933A3EFB23FA2 (MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F* __this, const RuntimeMethod* method) 
{
	{
		// public bool HasColours => colours != null && colours.Length > 0;
		ColorU5BU5D_t612261CF293F6FFC3D80AB52259FF0DC2B2CC389* L_0 = __this->___colours_3;
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		ColorU5BU5D_t612261CF293F6FFC3D80AB52259FF0DC2B2CC389* L_1 = __this->___colours_3;
		NullCheck(L_1);
		return (bool)((!(((uint32_t)(((RuntimeArray*)L_1)->max_length)) <= ((uint32_t)0)))? 1 : 0);
	}

IL_0013:
	{
		return (bool)0;
	}
}
// System.Boolean MicahW.PointGrass.PointGrassCommon/MeshData::get_HasAttributes()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool MeshData_get_HasAttributes_m4F13A5EE919CA43A34BBE872AC66756434B30060 (MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F* __this, const RuntimeMethod* method) 
{
	{
		// public bool HasAttributes => attributes != null && attributes.Length > 0;
		Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* L_0 = __this->___attributes_5;
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* L_1 = __this->___attributes_5;
		NullCheck(L_1);
		return (bool)((!(((uint32_t)(((RuntimeArray*)L_1)->max_length)) <= ((uint32_t)0)))? 1 : 0);
	}

IL_0013:
	{
		return (bool)0;
	}
}
// System.Void MicahW.PointGrass.PointGrassCommon/MeshData::RecalculateBounds()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MeshData_RecalculateBounds_m23CC32EAA976F74CAD691ACF55DEDE635C504655 (MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F* __this, const RuntimeMethod* method) 
{
	int32_t V_0 = 0;
	{
		// if (verts == null || verts.Length < 2) { return; }
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_0 = __this->___verts_0;
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_1 = __this->___verts_0;
		NullCheck(L_1);
		if ((((int32_t)((int32_t)(((RuntimeArray*)L_1)->max_length))) >= ((int32_t)2)))
		{
			goto IL_0014;
		}
	}

IL_0013:
	{
		// if (verts == null || verts.Length < 2) { return; }
		return;
	}

IL_0014:
	{
		// bounds = new Bounds(verts[0], Vector3.zero);
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_2 = __this->___verts_0;
		NullCheck(L_2);
		int32_t L_3 = 0;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_5;
		L_5 = Vector3_get_zero_m0C1249C3F25B1C70EAD3CC8B31259975A457AE39_inline(NULL);
		Bounds_t367E830C64BBF235ED8C3B2F8CF6254FDCAD39C3 L_6;
		memset((&L_6), 0, sizeof(L_6));
		Bounds__ctor_mAF7B238B9FBF90C495E5D7951760085A93119C5A((&L_6), L_4, L_5, /*hidden argument*/NULL);
		__this->___bounds_6 = L_6;
		// for (int i = 1; i < verts.Length; i++) { bounds.Encapsulate(verts[i]); }
		V_0 = 1;
		goto IL_004f;
	}

IL_0034:
	{
		// for (int i = 1; i < verts.Length; i++) { bounds.Encapsulate(verts[i]); }
		Bounds_t367E830C64BBF235ED8C3B2F8CF6254FDCAD39C3* L_7 = (&__this->___bounds_6);
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_8 = __this->___verts_0;
		int32_t L_9 = V_0;
		NullCheck(L_8);
		int32_t L_10 = L_9;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_11 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		Bounds_Encapsulate_m1FCA57C58536ADB67B85A703470C6F5BFB837C2F(L_7, L_11, NULL);
		// for (int i = 1; i < verts.Length; i++) { bounds.Encapsulate(verts[i]); }
		int32_t L_12 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add(L_12, 1));
	}

IL_004f:
	{
		// for (int i = 1; i < verts.Length; i++) { bounds.Encapsulate(verts[i]); }
		int32_t L_13 = V_0;
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_14 = __this->___verts_0;
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)((int32_t)(((RuntimeArray*)L_14)->max_length)))))
		{
			goto IL_0034;
		}
	}
	{
		// }
		return;
	}
}
// System.Void MicahW.PointGrass.PointGrassCommon/MeshData::RecalculateNormals()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MeshData_RecalculateNormals_m3DB8055C317DE19049DCC6647BCCB199FAF40917 (MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F* __this, const RuntimeMethod* method) 
{
	int32_t V_0 = 0;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_1;
	memset((&V_1), 0, sizeof(V_1));
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_2;
	memset((&V_2), 0, sizeof(V_2));
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	{
		// System.Array.Clear(normals, 0, normals.Length);
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_0 = __this->___normals_1;
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_1 = __this->___normals_1;
		NullCheck(L_1);
		Array_Clear_m50BAA3751899858B097D3FF2ED31F284703FE5CB((RuntimeArray*)L_0, 0, ((int32_t)(((RuntimeArray*)L_1)->max_length)), NULL);
		// for (int i = 0; i < tris.Length; i += 3) {
		V_0 = 0;
		goto IL_00b8;
	}

IL_001b:
	{
		// Vector3 offsetB = verts[tris[i + 1]] - verts[tris[i]];
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_2 = __this->___verts_0;
		Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_3 = __this->___tris_4;
		int32_t L_4 = V_0;
		NullCheck(L_3);
		int32_t L_5 = ((int32_t)il2cpp_codegen_add(L_4, 1));
		int32_t L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		NullCheck(L_2);
		int32_t L_7 = L_6;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_8 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_9 = __this->___verts_0;
		Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_10 = __this->___tris_4;
		int32_t L_11 = V_0;
		NullCheck(L_10);
		int32_t L_12 = L_11;
		int32_t L_13 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		NullCheck(L_9);
		int32_t L_14 = L_13;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_15 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_14));
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_16;
		L_16 = Vector3_op_Subtraction_mE42023FF80067CB44A1D4A27EB7CF2B24CABB828_inline(L_8, L_15, NULL);
		// Vector3 offsetC = verts[tris[i + 2]] - verts[tris[i]];
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_17 = __this->___verts_0;
		Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_18 = __this->___tris_4;
		int32_t L_19 = V_0;
		NullCheck(L_18);
		int32_t L_20 = ((int32_t)il2cpp_codegen_add(L_19, 2));
		int32_t L_21 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_20));
		NullCheck(L_17);
		int32_t L_22 = L_21;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_23 = (L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_22));
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_24 = __this->___verts_0;
		Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_25 = __this->___tris_4;
		int32_t L_26 = V_0;
		NullCheck(L_25);
		int32_t L_27 = L_26;
		int32_t L_28 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		NullCheck(L_24);
		int32_t L_29 = L_28;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_30 = (L_24)->GetAt(static_cast<il2cpp_array_size_t>(L_29));
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_31;
		L_31 = Vector3_op_Subtraction_mE42023FF80067CB44A1D4A27EB7CF2B24CABB828_inline(L_23, L_30, NULL);
		V_1 = L_31;
		// Vector3 normal = Vector3.Normalize(Vector3.Cross(offsetB, offsetC));
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_32 = V_1;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_33;
		L_33 = Vector3_Cross_mF93A280558BCE756D13B6CC5DCD7DE8A43148987_inline(L_16, L_32, NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_34;
		L_34 = Vector3_Normalize_mEF8349CC39674236CFC694189AFD36E31F89AC8F_inline(L_33, NULL);
		V_2 = L_34;
		// for (int j = 0; j < 3; j++) { normals[tris[i + j]] += normal; }
		V_3 = 0;
		goto IL_00b0;
	}

IL_0086:
	{
		// for (int j = 0; j < 3; j++) { normals[tris[i + j]] += normal; }
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_35 = __this->___normals_1;
		Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_36 = __this->___tris_4;
		int32_t L_37 = V_0;
		int32_t L_38 = V_3;
		NullCheck(L_36);
		int32_t L_39 = ((int32_t)il2cpp_codegen_add(L_37, L_38));
		int32_t L_40 = (L_36)->GetAt(static_cast<il2cpp_array_size_t>(L_39));
		NullCheck(L_35);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* L_41 = ((L_35)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_40)));
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_42 = (*(Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2*)L_41);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_43 = V_2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_44;
		L_44 = Vector3_op_Addition_m78C0EC70CB66E8DCAC225743D82B268DAEE92067_inline(L_42, L_43, NULL);
		*(Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2*)L_41 = L_44;
		// for (int j = 0; j < 3; j++) { normals[tris[i + j]] += normal; }
		int32_t L_45 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add(L_45, 1));
	}

IL_00b0:
	{
		// for (int j = 0; j < 3; j++) { normals[tris[i + j]] += normal; }
		int32_t L_46 = V_3;
		if ((((int32_t)L_46) < ((int32_t)3)))
		{
			goto IL_0086;
		}
	}
	{
		// for (int i = 0; i < tris.Length; i += 3) {
		int32_t L_47 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add(L_47, 3));
	}

IL_00b8:
	{
		// for (int i = 0; i < tris.Length; i += 3) {
		int32_t L_48 = V_0;
		Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_49 = __this->___tris_4;
		NullCheck(L_49);
		if ((((int32_t)L_48) < ((int32_t)((int32_t)(((RuntimeArray*)L_49)->max_length)))))
		{
			goto IL_001b;
		}
	}
	{
		// for (int i = 0; i < normals.Length; i++) { normals[i].Normalize(); }
		V_4 = 0;
		goto IL_00e3;
	}

IL_00cb:
	{
		// for (int i = 0; i < normals.Length; i++) { normals[i].Normalize(); }
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_50 = __this->___normals_1;
		int32_t L_51 = V_4;
		NullCheck(L_50);
		Vector3_Normalize_mC749B887A4C74BA0A2E13E6377F17CCAEB0AADA8_inline(((L_50)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_51))), NULL);
		// for (int i = 0; i < normals.Length; i++) { normals[i].Normalize(); }
		int32_t L_52 = V_4;
		V_4 = ((int32_t)il2cpp_codegen_add(L_52, 1));
	}

IL_00e3:
	{
		// for (int i = 0; i < normals.Length; i++) { normals[i].Normalize(); }
		int32_t L_53 = V_4;
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_54 = __this->___normals_1;
		NullCheck(L_54);
		if ((((int32_t)L_53) < ((int32_t)((int32_t)(((RuntimeArray*)L_54)->max_length)))))
		{
			goto IL_00cb;
		}
	}
	{
		// }
		return;
	}
}
// System.Void MicahW.PointGrass.PointGrassCommon/MeshData::ApplyDensityCutoff(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MeshData_ApplyDensityCutoff_m8F76018CA26B3621D4B38ED51A0EF5101D35B1E4 (MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F* __this, float ___cutoff0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral4BCFADE7ADD10F910A8CCCCFCD42193E98F4132B);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	int32_t V_1 = 0;
	{
		// if (cutoff <= 0f) { return; } // If the cutoff is below 0, we don't need to do anything
		float L_0 = ___cutoff0;
		if ((!(((float)L_0) <= ((float)(0.0f)))))
		{
			goto IL_0009;
		}
	}
	{
		// if (cutoff <= 0f) { return; } // If the cutoff is below 0, we don't need to do anything
		return;
	}

IL_0009:
	{
		// else if (cutoff >= 1f) { // If the cutoff is >= 1f, don't do anything to prevent a divide by zero error. Also log an error
		float L_1 = ___cutoff0;
		if ((!(((float)L_1) >= ((float)(1.0f)))))
		{
			goto IL_001c;
		}
	}
	{
		// Debug.LogError("Point Grass Common - An attempt was made to apply a density cutoff greater than or equal to 1f. This would have caused an error, so no cutoff was applied"); return;
		il2cpp_codegen_runtime_class_init_inline(Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		Debug_LogError_mB00B2B4468EF3CAF041B038D840820FB84C924B2(_stringLiteral4BCFADE7ADD10F910A8CCCCFCD42193E98F4132B, NULL);
		// Debug.LogError("Point Grass Common - An attempt was made to apply a density cutoff greater than or equal to 1f. This would have caused an error, so no cutoff was applied"); return;
		return;
	}

IL_001c:
	{
		// float divisor = (1f - cutoff); // Will cause issues if the cutoff is 1.0
		float L_2 = ___cutoff0;
		V_0 = ((float)il2cpp_codegen_subtract((1.0f), L_2));
		// for (int i = 0; i < attributes.Length; i++) {
		V_1 = 0;
		goto IL_0057;
	}

IL_0028:
	{
		// attributes[i].x = Mathf.Clamp01(attributes[i].x - cutoff) / divisor;
		Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* L_3 = __this->___attributes_5;
		int32_t L_4 = V_1;
		NullCheck(L_3);
		Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* L_5 = __this->___attributes_5;
		int32_t L_6 = V_1;
		NullCheck(L_5);
		float L_7 = ((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6)))->___x_0;
		float L_8 = ___cutoff0;
		float L_9;
		L_9 = Mathf_Clamp01_mA7E048DBDA832D399A581BE4D6DED9FA44CE0F14_inline(((float)il2cpp_codegen_subtract(L_7, L_8)), NULL);
		float L_10 = V_0;
		((L_3)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_4)))->___x_0 = ((float)(L_9/L_10));
		// for (int i = 0; i < attributes.Length; i++) {
		int32_t L_11 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add(L_11, 1));
	}

IL_0057:
	{
		// for (int i = 0; i < attributes.Length; i++) {
		int32_t L_12 = V_1;
		Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* L_13 = __this->___attributes_5;
		NullCheck(L_13);
		if ((((int32_t)L_12) < ((int32_t)((int32_t)(((RuntimeArray*)L_13)->max_length)))))
		{
			goto IL_0028;
		}
	}
	{
		// }
		return;
	}
}
// System.Void MicahW.PointGrass.PointGrassCommon/MeshData::ApplyLengthMapping(UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MeshData_ApplyLengthMapping_mCD709AEF3230ECE96D914FBDF433B3D1DF97C183 (MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F* __this, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___mapping0, const RuntimeMethod* method) 
{
	int32_t V_0 = 0;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* G_B2_0 = NULL;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* G_B1_0 = NULL;
	float G_B3_0 = 0.0f;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* G_B3_1 = NULL;
	{
		// mapping.x = Mathf.Clamp01(mapping.x);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_0 = ___mapping0;
		float L_1 = L_0.___x_0;
		float L_2;
		L_2 = Mathf_Clamp01_mA7E048DBDA832D399A581BE4D6DED9FA44CE0F14_inline(L_1, NULL);
		(&___mapping0)->___x_0 = L_2;
		// mapping.y = mapping.y < mapping.x ? mapping.x : Mathf.Clamp01(mapping.y);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_3 = ___mapping0;
		float L_4 = L_3.___y_1;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_5 = ___mapping0;
		float L_6 = L_5.___x_0;
		G_B1_0 = (&___mapping0);
		if ((((float)L_4) < ((float)L_6)))
		{
			G_B2_0 = (&___mapping0);
			goto IL_002f;
		}
	}
	{
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_7 = ___mapping0;
		float L_8 = L_7.___y_1;
		float L_9;
		L_9 = Mathf_Clamp01_mA7E048DBDA832D399A581BE4D6DED9FA44CE0F14_inline(L_8, NULL);
		G_B3_0 = L_9;
		G_B3_1 = G_B1_0;
		goto IL_0035;
	}

IL_002f:
	{
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_10 = ___mapping0;
		float L_11 = L_10.___x_0;
		G_B3_0 = L_11;
		G_B3_1 = G_B2_0;
	}

IL_0035:
	{
		G_B3_1->___y_1 = G_B3_0;
		// for (int i = 0; i < attributes.Length; i++) {
		V_0 = 0;
		goto IL_0075;
	}

IL_003e:
	{
		// attributes[i].y = Mathf.Lerp(mapping.x, mapping.y, attributes[i].y);
		Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* L_12 = __this->___attributes_5;
		int32_t L_13 = V_0;
		NullCheck(L_12);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_14 = ___mapping0;
		float L_15 = L_14.___x_0;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_16 = ___mapping0;
		float L_17 = L_16.___y_1;
		Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* L_18 = __this->___attributes_5;
		int32_t L_19 = V_0;
		NullCheck(L_18);
		float L_20 = ((L_18)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_19)))->___y_1;
		float L_21;
		L_21 = Mathf_Lerp_m47EF2FFB7647BD0A1FDC26DC03E28B19812139B5_inline(L_15, L_17, L_20, NULL);
		((L_12)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_13)))->___y_1 = L_21;
		// for (int i = 0; i < attributes.Length; i++) {
		int32_t L_22 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add(L_22, 1));
	}

IL_0075:
	{
		// for (int i = 0; i < attributes.Length; i++) {
		int32_t L_23 = V_0;
		Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* L_24 = __this->___attributes_5;
		NullCheck(L_24);
		if ((((int32_t)L_23) < ((int32_t)((int32_t)(((RuntimeArray*)L_24)->max_length)))))
		{
			goto IL_003e;
		}
	}
	{
		// }
		return;
	}
}
// System.Void MicahW.PointGrass.PointGrassCommon/MeshData::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MeshData__cctor_m44C85DCB2151B6174F6AAEA659563486E628DC19 (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static readonly MeshData Empty = new MeshData(null, null, null, null, null);
		MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F* L_0 = (MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F*)il2cpp_codegen_object_new(MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		MeshData__ctor_m4DDE83D4B889F4A8E8CB34522845BB26A776D752(L_0, (Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C*)NULL, (Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C*)NULL, (Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA*)NULL, (Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C*)NULL, (Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA*)NULL, NULL);
		((MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F_StaticFields*)il2cpp_codegen_static_fields_for(MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F_il2cpp_TypeInfo_var))->___Empty_7 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&((MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F_StaticFields*)il2cpp_codegen_static_fields_for(MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F_il2cpp_TypeInfo_var))->___Empty_7), (void*)L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Int32 MicahW.PointGrass.SingleLayer::get_LayerIndex()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t SingleLayer_get_LayerIndex_mB5C8A986682EAAA66CD1371500373D28A93B727E (SingleLayer_tA74696727907F9D2CD336CD4C7AA5C5C8C8FB4ED* __this, const RuntimeMethod* method) 
{
	{
		// public int LayerIndex => m_LayerIndex;
		int32_t L_0 = __this->___m_LayerIndex_0;
		return L_0;
	}
}
IL2CPP_EXTERN_C  int32_t SingleLayer_get_LayerIndex_mB5C8A986682EAAA66CD1371500373D28A93B727E_AdjustorThunk (RuntimeObject* __this, const RuntimeMethod* method)
{
	SingleLayer_tA74696727907F9D2CD336CD4C7AA5C5C8C8FB4ED* _thisAdjusted;
	int32_t _offset = 1;
	_thisAdjusted = reinterpret_cast<SingleLayer_tA74696727907F9D2CD336CD4C7AA5C5C8C8FB4ED*>(__this + _offset);
	int32_t _returnValue;
	_returnValue = SingleLayer_get_LayerIndex_mB5C8A986682EAAA66CD1371500373D28A93B727E_inline(_thisAdjusted, method);
	return _returnValue;
}
// System.Int32 MicahW.PointGrass.SingleLayer::get_Mask()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t SingleLayer_get_Mask_m46E3E593C1213BB974ABBCDD3A0FD09791DC1473 (SingleLayer_tA74696727907F9D2CD336CD4C7AA5C5C8C8FB4ED* __this, const RuntimeMethod* method) 
{
	{
		// public int Mask => 1 << m_LayerIndex;
		int32_t L_0 = __this->___m_LayerIndex_0;
		return ((int32_t)(1<<((int32_t)(L_0&((int32_t)31)))));
	}
}
IL2CPP_EXTERN_C  int32_t SingleLayer_get_Mask_m46E3E593C1213BB974ABBCDD3A0FD09791DC1473_AdjustorThunk (RuntimeObject* __this, const RuntimeMethod* method)
{
	SingleLayer_tA74696727907F9D2CD336CD4C7AA5C5C8C8FB4ED* _thisAdjusted;
	int32_t _offset = 1;
	_thisAdjusted = reinterpret_cast<SingleLayer_tA74696727907F9D2CD336CD4C7AA5C5C8C8FB4ED*>(__this + _offset);
	int32_t _returnValue;
	_returnValue = SingleLayer_get_Mask_m46E3E593C1213BB974ABBCDD3A0FD09791DC1473(_thisAdjusted, method);
	return _returnValue;
}
// System.Void MicahW.PointGrass.SingleLayer::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SingleLayer__ctor_mB56173673F914A84F0A215F602C28EC44017CE31 (SingleLayer_tA74696727907F9D2CD336CD4C7AA5C5C8C8FB4ED* __this, int32_t ___layerIndex0, const RuntimeMethod* method) 
{
	{
		// public SingleLayer(int layerIndex) { m_LayerIndex = layerIndex; }
		int32_t L_0 = ___layerIndex0;
		__this->___m_LayerIndex_0 = L_0;
		// public SingleLayer(int layerIndex) { m_LayerIndex = layerIndex; }
		return;
	}
}
IL2CPP_EXTERN_C  void SingleLayer__ctor_mB56173673F914A84F0A215F602C28EC44017CE31_AdjustorThunk (RuntimeObject* __this, int32_t ___layerIndex0, const RuntimeMethod* method)
{
	SingleLayer_tA74696727907F9D2CD336CD4C7AA5C5C8C8FB4ED* _thisAdjusted;
	int32_t _offset = 1;
	_thisAdjusted = reinterpret_cast<SingleLayer_tA74696727907F9D2CD336CD4C7AA5C5C8C8FB4ED*>(__this + _offset);
	SingleLayer__ctor_mB56173673F914A84F0A215F602C28EC44017CE31_inline(_thisAdjusted, ___layerIndex0, method);
}
// System.Void MicahW.PointGrass.SingleLayer::Set(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SingleLayer_Set_mBBB701BA6BC74136FD35A221A1341CFECEB93BEB (SingleLayer_tA74696727907F9D2CD336CD4C7AA5C5C8C8FB4ED* __this, int32_t ____layerIndex0, const RuntimeMethod* method) 
{
	{
		// if (_layerIndex > 0 && _layerIndex < 32) { m_LayerIndex = _layerIndex; }
		int32_t L_0 = ____layerIndex0;
		if ((((int32_t)L_0) <= ((int32_t)0)))
		{
			goto IL_0010;
		}
	}
	{
		int32_t L_1 = ____layerIndex0;
		if ((((int32_t)L_1) >= ((int32_t)((int32_t)32))))
		{
			goto IL_0010;
		}
	}
	{
		// if (_layerIndex > 0 && _layerIndex < 32) { m_LayerIndex = _layerIndex; }
		int32_t L_2 = ____layerIndex0;
		__this->___m_LayerIndex_0 = L_2;
	}

IL_0010:
	{
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void SingleLayer_Set_mBBB701BA6BC74136FD35A221A1341CFECEB93BEB_AdjustorThunk (RuntimeObject* __this, int32_t ____layerIndex0, const RuntimeMethod* method)
{
	SingleLayer_tA74696727907F9D2CD336CD4C7AA5C5C8C8FB4ED* _thisAdjusted;
	int32_t _offset = 1;
	_thisAdjusted = reinterpret_cast<SingleLayer_tA74696727907F9D2CD336CD4C7AA5C5C8C8FB4ED*>(__this + _offset);
	SingleLayer_Set_mBBB701BA6BC74136FD35A221A1341CFECEB93BEB(_thisAdjusted, ____layerIndex0, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Int32 MicahW.PointGrass.PointGrassDisplacementManager::get_DisplacerCount()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t PointGrassDisplacementManager_get_DisplacerCount_m0526FE46A67C96DC0238F4E9281DB7DAEFF83E66 (PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_mB13C1EB08F28C5DEA82F64C1E42E75932F697955_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private int DisplacerCount => Mathf.Min(displacers.Count, maxDisplacerCount);
		List_1_tDEBBED5B3545B06B3FD16E4ACE9B2C7AB62A1B3E* L_0 = __this->___displacers_6;
		NullCheck(L_0);
		int32_t L_1;
		L_1 = List_1_get_Count_mB13C1EB08F28C5DEA82F64C1E42E75932F697955_inline(L_0, List_1_get_Count_mB13C1EB08F28C5DEA82F64C1E42E75932F697955_RuntimeMethod_var);
		il2cpp_codegen_runtime_class_init_inline(PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900_il2cpp_TypeInfo_var);
		int32_t L_2 = ((PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900_StaticFields*)il2cpp_codegen_static_fields_for(PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900_il2cpp_TypeInfo_var))->___maxDisplacerCount_7;
		int32_t L_3;
		L_3 = Mathf_Min_m888083F74FF5655778F0403BB5E9608BEFDEA8CB_inline(L_1, L_2, NULL);
		return L_3;
	}
}
// System.Void MicahW.PointGrass.PointGrassDisplacementManager::add_OnInitialize(MicahW.PointGrass.PointGrassDisplacementManager/DisplacementDelegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointGrassDisplacementManager_add_OnInitialize_mD1CDC92403A311CF3BBA4C8913EF1BE50C48DD54 (DisplacementDelegate_t4C836C2D37D9FABEC9514A574F9425C4B15B981A* ___value0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DisplacementDelegate_t4C836C2D37D9FABEC9514A574F9425C4B15B981A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	DisplacementDelegate_t4C836C2D37D9FABEC9514A574F9425C4B15B981A* V_0 = NULL;
	DisplacementDelegate_t4C836C2D37D9FABEC9514A574F9425C4B15B981A* V_1 = NULL;
	DisplacementDelegate_t4C836C2D37D9FABEC9514A574F9425C4B15B981A* V_2 = NULL;
	{
		il2cpp_codegen_runtime_class_init_inline(PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900_il2cpp_TypeInfo_var);
		DisplacementDelegate_t4C836C2D37D9FABEC9514A574F9425C4B15B981A* L_0 = ((PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900_StaticFields*)il2cpp_codegen_static_fields_for(PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900_il2cpp_TypeInfo_var))->___OnInitialize_8;
		V_0 = L_0;
	}

IL_0006:
	{
		DisplacementDelegate_t4C836C2D37D9FABEC9514A574F9425C4B15B981A* L_1 = V_0;
		V_1 = L_1;
		DisplacementDelegate_t4C836C2D37D9FABEC9514A574F9425C4B15B981A* L_2 = V_1;
		DisplacementDelegate_t4C836C2D37D9FABEC9514A574F9425C4B15B981A* L_3 = ___value0;
		Delegate_t* L_4;
		L_4 = Delegate_Combine_m1F725AEF318BE6F0426863490691A6F4606E7D00(L_2, L_3, NULL);
		V_2 = ((DisplacementDelegate_t4C836C2D37D9FABEC9514A574F9425C4B15B981A*)CastclassSealed((RuntimeObject*)L_4, DisplacementDelegate_t4C836C2D37D9FABEC9514A574F9425C4B15B981A_il2cpp_TypeInfo_var));
		il2cpp_codegen_runtime_class_init_inline(PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900_il2cpp_TypeInfo_var);
		DisplacementDelegate_t4C836C2D37D9FABEC9514A574F9425C4B15B981A* L_5 = V_2;
		DisplacementDelegate_t4C836C2D37D9FABEC9514A574F9425C4B15B981A* L_6 = V_1;
		DisplacementDelegate_t4C836C2D37D9FABEC9514A574F9425C4B15B981A* L_7;
		L_7 = InterlockedCompareExchangeImpl<DisplacementDelegate_t4C836C2D37D9FABEC9514A574F9425C4B15B981A*>((&((PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900_StaticFields*)il2cpp_codegen_static_fields_for(PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900_il2cpp_TypeInfo_var))->___OnInitialize_8), L_5, L_6);
		V_0 = L_7;
		DisplacementDelegate_t4C836C2D37D9FABEC9514A574F9425C4B15B981A* L_8 = V_0;
		DisplacementDelegate_t4C836C2D37D9FABEC9514A574F9425C4B15B981A* L_9 = V_1;
		if ((!(((RuntimeObject*)(DisplacementDelegate_t4C836C2D37D9FABEC9514A574F9425C4B15B981A*)L_8) == ((RuntimeObject*)(DisplacementDelegate_t4C836C2D37D9FABEC9514A574F9425C4B15B981A*)L_9))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void MicahW.PointGrass.PointGrassDisplacementManager::remove_OnInitialize(MicahW.PointGrass.PointGrassDisplacementManager/DisplacementDelegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointGrassDisplacementManager_remove_OnInitialize_m5F94865C071988D7AF7EF522E3ABFAFFABF7E6B0 (DisplacementDelegate_t4C836C2D37D9FABEC9514A574F9425C4B15B981A* ___value0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DisplacementDelegate_t4C836C2D37D9FABEC9514A574F9425C4B15B981A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	DisplacementDelegate_t4C836C2D37D9FABEC9514A574F9425C4B15B981A* V_0 = NULL;
	DisplacementDelegate_t4C836C2D37D9FABEC9514A574F9425C4B15B981A* V_1 = NULL;
	DisplacementDelegate_t4C836C2D37D9FABEC9514A574F9425C4B15B981A* V_2 = NULL;
	{
		il2cpp_codegen_runtime_class_init_inline(PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900_il2cpp_TypeInfo_var);
		DisplacementDelegate_t4C836C2D37D9FABEC9514A574F9425C4B15B981A* L_0 = ((PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900_StaticFields*)il2cpp_codegen_static_fields_for(PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900_il2cpp_TypeInfo_var))->___OnInitialize_8;
		V_0 = L_0;
	}

IL_0006:
	{
		DisplacementDelegate_t4C836C2D37D9FABEC9514A574F9425C4B15B981A* L_1 = V_0;
		V_1 = L_1;
		DisplacementDelegate_t4C836C2D37D9FABEC9514A574F9425C4B15B981A* L_2 = V_1;
		DisplacementDelegate_t4C836C2D37D9FABEC9514A574F9425C4B15B981A* L_3 = ___value0;
		Delegate_t* L_4;
		L_4 = Delegate_Remove_m8B7DD5661308FA972E23CA1CC3FC9CEB355504E3(L_2, L_3, NULL);
		V_2 = ((DisplacementDelegate_t4C836C2D37D9FABEC9514A574F9425C4B15B981A*)CastclassSealed((RuntimeObject*)L_4, DisplacementDelegate_t4C836C2D37D9FABEC9514A574F9425C4B15B981A_il2cpp_TypeInfo_var));
		il2cpp_codegen_runtime_class_init_inline(PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900_il2cpp_TypeInfo_var);
		DisplacementDelegate_t4C836C2D37D9FABEC9514A574F9425C4B15B981A* L_5 = V_2;
		DisplacementDelegate_t4C836C2D37D9FABEC9514A574F9425C4B15B981A* L_6 = V_1;
		DisplacementDelegate_t4C836C2D37D9FABEC9514A574F9425C4B15B981A* L_7;
		L_7 = InterlockedCompareExchangeImpl<DisplacementDelegate_t4C836C2D37D9FABEC9514A574F9425C4B15B981A*>((&((PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900_StaticFields*)il2cpp_codegen_static_fields_for(PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900_il2cpp_TypeInfo_var))->___OnInitialize_8), L_5, L_6);
		V_0 = L_7;
		DisplacementDelegate_t4C836C2D37D9FABEC9514A574F9425C4B15B981A* L_8 = V_0;
		DisplacementDelegate_t4C836C2D37D9FABEC9514A574F9425C4B15B981A* L_9 = V_1;
		if ((!(((RuntimeObject*)(DisplacementDelegate_t4C836C2D37D9FABEC9514A574F9425C4B15B981A*)L_8) == ((RuntimeObject*)(DisplacementDelegate_t4C836C2D37D9FABEC9514A574F9425C4B15B981A*)L_9))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void MicahW.PointGrass.PointGrassDisplacementManager::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointGrassDisplacementManager_Awake_mAF13AF01B1DED72D347730F1F9B43FD0DC1A7E01 (PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ComputeBuffer_t51EADA9015EBCC1B982C5584E9AB2734415A8233_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_mF6D991AAA0C3CF45DA3EC80A854889E3DD9268CB_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_tDEBBED5B3545B06B3FD16E4ACE9B2C7AB62A1B3E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	DisplacementDelegate_t4C836C2D37D9FABEC9514A574F9425C4B15B981A* G_B4_0 = NULL;
	DisplacementDelegate_t4C836C2D37D9FABEC9514A574F9425C4B15B981A* G_B3_0 = NULL;
	{
		// if (instance != null) { Destroy(this); }
		il2cpp_codegen_runtime_class_init_inline(PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900_il2cpp_TypeInfo_var);
		PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900* L_0 = ((PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900_StaticFields*)il2cpp_codegen_static_fields_for(PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900_il2cpp_TypeInfo_var))->___instance_4;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Inequality_mD0BE578448EAA61948F25C32F8DD55AB1F778602(L_0, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_1)
		{
			goto IL_0014;
		}
	}
	{
		// if (instance != null) { Destroy(this); }
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		Object_Destroy_mE97D0A766419A81296E8D4E5C23D01D3FE91ACBB(__this, NULL);
		return;
	}

IL_0014:
	{
		// instance = this;
		il2cpp_codegen_runtime_class_init_inline(PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900_il2cpp_TypeInfo_var);
		((PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900_StaticFields*)il2cpp_codegen_static_fields_for(PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900_il2cpp_TypeInfo_var))->___instance_4 = __this;
		Il2CppCodeGenWriteBarrier((void**)(&((PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900_StaticFields*)il2cpp_codegen_static_fields_for(PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900_il2cpp_TypeInfo_var))->___instance_4), (void*)__this);
		// objectsBuffer = new ComputeBuffer(maxDisplacerCount, ObjectData.stride);
		int32_t L_2 = ((PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900_StaticFields*)il2cpp_codegen_static_fields_for(PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900_il2cpp_TypeInfo_var))->___maxDisplacerCount_7;
		ComputeBuffer_t51EADA9015EBCC1B982C5584E9AB2734415A8233* L_3 = (ComputeBuffer_t51EADA9015EBCC1B982C5584E9AB2734415A8233*)il2cpp_codegen_object_new(ComputeBuffer_t51EADA9015EBCC1B982C5584E9AB2734415A8233_il2cpp_TypeInfo_var);
		NullCheck(L_3);
		ComputeBuffer__ctor_mE40DE5EF5ADAC29B6B4DECBD1EE33E8526202617(L_3, L_2, ((int32_t)20), NULL);
		__this->___objectsBuffer_5 = L_3;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___objectsBuffer_5), (void*)L_3);
		// displacers = new List<PointGrassDisplacer>();
		List_1_tDEBBED5B3545B06B3FD16E4ACE9B2C7AB62A1B3E* L_4 = (List_1_tDEBBED5B3545B06B3FD16E4ACE9B2C7AB62A1B3E*)il2cpp_codegen_object_new(List_1_tDEBBED5B3545B06B3FD16E4ACE9B2C7AB62A1B3E_il2cpp_TypeInfo_var);
		NullCheck(L_4);
		List_1__ctor_mF6D991AAA0C3CF45DA3EC80A854889E3DD9268CB(L_4, List_1__ctor_mF6D991AAA0C3CF45DA3EC80A854889E3DD9268CB_RuntimeMethod_var);
		__this->___displacers_6 = L_4;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___displacers_6), (void*)L_4);
		// OnInitialize?.Invoke(this);
		DisplacementDelegate_t4C836C2D37D9FABEC9514A574F9425C4B15B981A* L_5 = ((PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900_StaticFields*)il2cpp_codegen_static_fields_for(PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900_il2cpp_TypeInfo_var))->___OnInitialize_8;
		DisplacementDelegate_t4C836C2D37D9FABEC9514A574F9425C4B15B981A* L_6 = L_5;
		G_B3_0 = L_6;
		if (L_6)
		{
			G_B4_0 = L_6;
			goto IL_0041;
		}
	}
	{
		return;
	}

IL_0041:
	{
		NullCheck(G_B4_0);
		DisplacementDelegate_Invoke_m5D544DE149EA8535E783D20BD994198770497B8D_inline(G_B4_0, __this, NULL);
		// }
		return;
	}
}
// System.Void MicahW.PointGrass.PointGrassDisplacementManager::OnDisable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointGrassDisplacementManager_OnDisable_m898149390DFAF08C40B552FAB668A5803BC82107 (PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Clear_mA21FF0362F71CC4E90FB3BC2BD3AA7B955DE93FC_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (instance == this) {
		il2cpp_codegen_runtime_class_init_inline(PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900_il2cpp_TypeInfo_var);
		PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900* L_0 = ((PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900_StaticFields*)il2cpp_codegen_static_fields_for(PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900_il2cpp_TypeInfo_var))->___instance_4;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Equality_mB6120F782D83091EF56A198FCEBCF066DB4A9605(L_0, __this, NULL);
		if (!L_1)
		{
			goto IL_0029;
		}
	}
	{
		// instance = null;
		il2cpp_codegen_runtime_class_init_inline(PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900_il2cpp_TypeInfo_var);
		((PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900_StaticFields*)il2cpp_codegen_static_fields_for(PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900_il2cpp_TypeInfo_var))->___instance_4 = (PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900*)NULL;
		Il2CppCodeGenWriteBarrier((void**)(&((PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900_StaticFields*)il2cpp_codegen_static_fields_for(PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900_il2cpp_TypeInfo_var))->___instance_4), (void*)(PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900*)NULL);
		// objectsBuffer.Release();
		ComputeBuffer_t51EADA9015EBCC1B982C5584E9AB2734415A8233* L_2 = __this->___objectsBuffer_5;
		NullCheck(L_2);
		ComputeBuffer_Release_mF1F157C929A0A5B2FDCD703A286EE09723450B72(L_2, NULL);
		// displacers.Clear();
		List_1_tDEBBED5B3545B06B3FD16E4ACE9B2C7AB62A1B3E* L_3 = __this->___displacers_6;
		NullCheck(L_3);
		List_1_Clear_mA21FF0362F71CC4E90FB3BC2BD3AA7B955DE93FC_inline(L_3, List_1_Clear_mA21FF0362F71CC4E90FB3BC2BD3AA7B955DE93FC_RuntimeMethod_var);
	}

IL_0029:
	{
		// }
		return;
	}
}
// System.Void MicahW.PointGrass.PointGrassDisplacementManager::LateUpdate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointGrassDisplacementManager_LateUpdate_m158729EC12F6BCD84E77ED5264A436B0E8341C7E (PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900* __this, const RuntimeMethod* method) 
{
	{
		// private void LateUpdate() { UpdateBuffer(); }
		PointGrassDisplacementManager_UpdateBuffer_m852C3B19D6D618139E8B3913E875A1DA1A48996F(__this, NULL);
		// private void LateUpdate() { UpdateBuffer(); }
		return;
	}
}
// System.Void MicahW.PointGrass.PointGrassDisplacementManager::UpdateBuffer()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointGrassDisplacementManager_UpdateBuffer_m852C3B19D6D618139E8B3913E875A1DA1A48996F (PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerable_Select_TisPointGrassDisplacer_tB9B22DA401FDA294990903A742F3ACA3351A82FF_TisObjectData_t24D94D36E8D49DEB90BBFEE7BA833C910F0AF6C4_m8A5429699E824BA5B507C2A87B19D7F34D84CC1D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerable_ToArray_TisObjectData_t24D94D36E8D49DEB90BBFEE7BA833C910F0AF6C4_mBA811FFD832E9653BA91207907FC396E52CB0DDF_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_t868371C41B8A357BB81D47C6157D60859AD0411F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_U3CUpdateBufferU3Eb__13_0_m28DE49B834780F799169B47BA3327FE3FCFF9CFA_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_tC71A09B989D2FDC7808DBE99B3025974C73DE7C5_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	ObjectDataU5BU5D_tFCDCD60E1D45526EC9B864EF342ACB7834D38930* V_0 = NULL;
	Func_2_t868371C41B8A357BB81D47C6157D60859AD0411F* G_B2_0 = NULL;
	List_1_tDEBBED5B3545B06B3FD16E4ACE9B2C7AB62A1B3E* G_B2_1 = NULL;
	Func_2_t868371C41B8A357BB81D47C6157D60859AD0411F* G_B1_0 = NULL;
	List_1_tDEBBED5B3545B06B3FD16E4ACE9B2C7AB62A1B3E* G_B1_1 = NULL;
	{
		// var objectData = displacers.Select(a => a.GetObjectData()).ToArray();
		List_1_tDEBBED5B3545B06B3FD16E4ACE9B2C7AB62A1B3E* L_0 = __this->___displacers_6;
		il2cpp_codegen_runtime_class_init_inline(U3CU3Ec_tC71A09B989D2FDC7808DBE99B3025974C73DE7C5_il2cpp_TypeInfo_var);
		Func_2_t868371C41B8A357BB81D47C6157D60859AD0411F* L_1 = ((U3CU3Ec_tC71A09B989D2FDC7808DBE99B3025974C73DE7C5_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tC71A09B989D2FDC7808DBE99B3025974C73DE7C5_il2cpp_TypeInfo_var))->___U3CU3E9__13_0_1;
		Func_2_t868371C41B8A357BB81D47C6157D60859AD0411F* L_2 = L_1;
		G_B1_0 = L_2;
		G_B1_1 = L_0;
		if (L_2)
		{
			G_B2_0 = L_2;
			G_B2_1 = L_0;
			goto IL_0025;
		}
	}
	{
		il2cpp_codegen_runtime_class_init_inline(U3CU3Ec_tC71A09B989D2FDC7808DBE99B3025974C73DE7C5_il2cpp_TypeInfo_var);
		U3CU3Ec_tC71A09B989D2FDC7808DBE99B3025974C73DE7C5* L_3 = ((U3CU3Ec_tC71A09B989D2FDC7808DBE99B3025974C73DE7C5_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tC71A09B989D2FDC7808DBE99B3025974C73DE7C5_il2cpp_TypeInfo_var))->___U3CU3E9_0;
		Func_2_t868371C41B8A357BB81D47C6157D60859AD0411F* L_4 = (Func_2_t868371C41B8A357BB81D47C6157D60859AD0411F*)il2cpp_codegen_object_new(Func_2_t868371C41B8A357BB81D47C6157D60859AD0411F_il2cpp_TypeInfo_var);
		NullCheck(L_4);
		Func_2__ctor_mF8D09AC6B6F3894CB2E7089BC68605790564651B(L_4, L_3, (intptr_t)((void*)U3CU3Ec_U3CUpdateBufferU3Eb__13_0_m28DE49B834780F799169B47BA3327FE3FCFF9CFA_RuntimeMethod_var), NULL);
		Func_2_t868371C41B8A357BB81D47C6157D60859AD0411F* L_5 = L_4;
		((U3CU3Ec_tC71A09B989D2FDC7808DBE99B3025974C73DE7C5_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tC71A09B989D2FDC7808DBE99B3025974C73DE7C5_il2cpp_TypeInfo_var))->___U3CU3E9__13_0_1 = L_5;
		Il2CppCodeGenWriteBarrier((void**)(&((U3CU3Ec_tC71A09B989D2FDC7808DBE99B3025974C73DE7C5_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tC71A09B989D2FDC7808DBE99B3025974C73DE7C5_il2cpp_TypeInfo_var))->___U3CU3E9__13_0_1), (void*)L_5);
		G_B2_0 = L_5;
		G_B2_1 = G_B1_1;
	}

IL_0025:
	{
		RuntimeObject* L_6;
		L_6 = Enumerable_Select_TisPointGrassDisplacer_tB9B22DA401FDA294990903A742F3ACA3351A82FF_TisObjectData_t24D94D36E8D49DEB90BBFEE7BA833C910F0AF6C4_m8A5429699E824BA5B507C2A87B19D7F34D84CC1D(G_B2_1, G_B2_0, Enumerable_Select_TisPointGrassDisplacer_tB9B22DA401FDA294990903A742F3ACA3351A82FF_TisObjectData_t24D94D36E8D49DEB90BBFEE7BA833C910F0AF6C4_m8A5429699E824BA5B507C2A87B19D7F34D84CC1D_RuntimeMethod_var);
		ObjectDataU5BU5D_tFCDCD60E1D45526EC9B864EF342ACB7834D38930* L_7;
		L_7 = Enumerable_ToArray_TisObjectData_t24D94D36E8D49DEB90BBFEE7BA833C910F0AF6C4_mBA811FFD832E9653BA91207907FC396E52CB0DDF(L_6, Enumerable_ToArray_TisObjectData_t24D94D36E8D49DEB90BBFEE7BA833C910F0AF6C4_mBA811FFD832E9653BA91207907FC396E52CB0DDF_RuntimeMethod_var);
		V_0 = L_7;
		// objectsBuffer.SetData(objectData, 0, 0, DisplacerCount);
		ComputeBuffer_t51EADA9015EBCC1B982C5584E9AB2734415A8233* L_8 = __this->___objectsBuffer_5;
		ObjectDataU5BU5D_tFCDCD60E1D45526EC9B864EF342ACB7834D38930* L_9 = V_0;
		int32_t L_10;
		L_10 = PointGrassDisplacementManager_get_DisplacerCount_m0526FE46A67C96DC0238F4E9281DB7DAEFF83E66(__this, NULL);
		NullCheck(L_8);
		ComputeBuffer_SetData_m5D75D81304937FD04C820B1015AFA066DF36DC0E(L_8, (RuntimeArray*)L_9, 0, 0, L_10, NULL);
		// }
		return;
	}
}
// System.Void MicahW.PointGrass.PointGrassDisplacementManager::AddDisplacer(MicahW.PointGrass.PointGrassDisplacer)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointGrassDisplacementManager_AddDisplacer_m34B41A7480D16398EC5BF58ECFB2A73F92EFEE6F (PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900* __this, PointGrassDisplacer_tB9B22DA401FDA294990903A742F3ACA3351A82FF* ___displacer0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mB3A89627A69CB0296DA00911471E9FD7DDDD58F5_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public void AddDisplacer(PointGrassDisplacer displacer) => displacers.Add(displacer);
		List_1_tDEBBED5B3545B06B3FD16E4ACE9B2C7AB62A1B3E* L_0 = __this->___displacers_6;
		PointGrassDisplacer_tB9B22DA401FDA294990903A742F3ACA3351A82FF* L_1 = ___displacer0;
		NullCheck(L_0);
		List_1_Add_mB3A89627A69CB0296DA00911471E9FD7DDDD58F5_inline(L_0, L_1, List_1_Add_mB3A89627A69CB0296DA00911471E9FD7DDDD58F5_RuntimeMethod_var);
		return;
	}
}
// System.Void MicahW.PointGrass.PointGrassDisplacementManager::RemoveDisplacer(MicahW.PointGrass.PointGrassDisplacer)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointGrassDisplacementManager_RemoveDisplacer_m610EACCC6E76F1C21D580178793521035C34B32A (PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900* __this, PointGrassDisplacer_tB9B22DA401FDA294990903A742F3ACA3351A82FF* ___displacer0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Remove_m5C0188F84A17A37FED29306A626DF5015F87088D_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public void RemoveDisplacer(PointGrassDisplacer displacer) => displacers.Remove(displacer);
		List_1_tDEBBED5B3545B06B3FD16E4ACE9B2C7AB62A1B3E* L_0 = __this->___displacers_6;
		PointGrassDisplacer_tB9B22DA401FDA294990903A742F3ACA3351A82FF* L_1 = ___displacer0;
		NullCheck(L_0);
		bool L_2;
		L_2 = List_1_Remove_m5C0188F84A17A37FED29306A626DF5015F87088D(L_0, L_1, List_1_Remove_m5C0188F84A17A37FED29306A626DF5015F87088D_RuntimeMethod_var);
		return;
	}
}
// System.Void MicahW.PointGrass.PointGrassDisplacementManager::UpdatePropertyBlock(UnityEngine.MaterialPropertyBlock&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointGrassDisplacementManager_UpdatePropertyBlock_mE705BEB14C825F65DBF87F0DA864B492DFBAC365 (PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900* __this, MaterialPropertyBlock_t2308669579033A857EFE6E4831909F638B27411D** ___block0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// block.SetBuffer(ID_ObjBuff, objectsBuffer);
		MaterialPropertyBlock_t2308669579033A857EFE6E4831909F638B27411D** L_0 = ___block0;
		MaterialPropertyBlock_t2308669579033A857EFE6E4831909F638B27411D* L_1 = *((MaterialPropertyBlock_t2308669579033A857EFE6E4831909F638B27411D**)L_0);
		int32_t L_2 = ((PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_StaticFields*)il2cpp_codegen_static_fields_for(PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_il2cpp_TypeInfo_var))->___ID_ObjBuff_3;
		ComputeBuffer_t51EADA9015EBCC1B982C5584E9AB2734415A8233* L_3 = __this->___objectsBuffer_5;
		NullCheck(L_1);
		MaterialPropertyBlock_SetBuffer_m9FF172AF2C41F7CBCC50F4AC4C6109B15289BF73(L_1, L_2, L_3, NULL);
		// block.SetInt(ID_ObjCount, DisplacerCount);
		MaterialPropertyBlock_t2308669579033A857EFE6E4831909F638B27411D** L_4 = ___block0;
		MaterialPropertyBlock_t2308669579033A857EFE6E4831909F638B27411D* L_5 = *((MaterialPropertyBlock_t2308669579033A857EFE6E4831909F638B27411D**)L_4);
		int32_t L_6 = ((PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_StaticFields*)il2cpp_codegen_static_fields_for(PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_il2cpp_TypeInfo_var))->___ID_ObjCount_4;
		int32_t L_7;
		L_7 = PointGrassDisplacementManager_get_DisplacerCount_m0526FE46A67C96DC0238F4E9281DB7DAEFF83E66(__this, NULL);
		NullCheck(L_5);
		MaterialPropertyBlock_SetInt_mB07015BD8FF1A7D2FF70D7FB89C0FDFAE06B86F8(L_5, L_6, L_7, NULL);
		// }
		return;
	}
}
// System.Void MicahW.PointGrass.PointGrassDisplacementManager::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointGrassDisplacementManager__ctor_m613DF958029CBB7AE2CCBA0A61A7A9A07012EC7F (PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900* __this, const RuntimeMethod* method) 
{
	{
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
// System.Void MicahW.PointGrass.PointGrassDisplacementManager::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointGrassDisplacementManager__cctor_m36A658B29ED41A6CF72524B532CF9A7CE0B03509 (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private static readonly int maxDisplacerCount = 32;
		((PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900_StaticFields*)il2cpp_codegen_static_fields_for(PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900_il2cpp_TypeInfo_var))->___maxDisplacerCount_7 = ((int32_t)32);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
void DisplacementDelegate_Invoke_m5D544DE149EA8535E783D20BD994198770497B8D_Multicast(DisplacementDelegate_t4C836C2D37D9FABEC9514A574F9425C4B15B981A* __this, PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900* ___manager0, const RuntimeMethod* method)
{
	il2cpp_array_size_t length = __this->___delegates_13->max_length;
	Delegate_t** delegatesToInvoke = reinterpret_cast<Delegate_t**>(__this->___delegates_13->GetAddressAtUnchecked(0));
	for (il2cpp_array_size_t i = 0; i < length; i++)
	{
		DisplacementDelegate_t4C836C2D37D9FABEC9514A574F9425C4B15B981A* currentDelegate = reinterpret_cast<DisplacementDelegate_t4C836C2D37D9FABEC9514A574F9425C4B15B981A*>(delegatesToInvoke[i]);
		typedef void (*FunctionPointerType) (RuntimeObject*, PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900*, const RuntimeMethod*);
		((FunctionPointerType)currentDelegate->___invoke_impl_1)((Il2CppObject*)currentDelegate->___method_code_6, ___manager0, reinterpret_cast<RuntimeMethod*>(currentDelegate->___method_3));
	}
}
void DisplacementDelegate_Invoke_m5D544DE149EA8535E783D20BD994198770497B8D_OpenInst(DisplacementDelegate_t4C836C2D37D9FABEC9514A574F9425C4B15B981A* __this, PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900* ___manager0, const RuntimeMethod* method)
{
	NullCheck(___manager0);
	typedef void (*FunctionPointerType) (PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900*, const RuntimeMethod*);
	((FunctionPointerType)__this->___method_ptr_0)(___manager0, method);
}
void DisplacementDelegate_Invoke_m5D544DE149EA8535E783D20BD994198770497B8D_OpenStatic(DisplacementDelegate_t4C836C2D37D9FABEC9514A574F9425C4B15B981A* __this, PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900* ___manager0, const RuntimeMethod* method)
{
	typedef void (*FunctionPointerType) (PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900*, const RuntimeMethod*);
	((FunctionPointerType)__this->___method_ptr_0)(___manager0, method);
}
void DisplacementDelegate_Invoke_m5D544DE149EA8535E783D20BD994198770497B8D_OpenStaticInvoker(DisplacementDelegate_t4C836C2D37D9FABEC9514A574F9425C4B15B981A* __this, PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900* ___manager0, const RuntimeMethod* method)
{
	InvokerActionInvoker1< PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900* >::Invoke(__this->___method_ptr_0, method, NULL, ___manager0);
}
void DisplacementDelegate_Invoke_m5D544DE149EA8535E783D20BD994198770497B8D_ClosedStaticInvoker(DisplacementDelegate_t4C836C2D37D9FABEC9514A574F9425C4B15B981A* __this, PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900* ___manager0, const RuntimeMethod* method)
{
	InvokerActionInvoker2< RuntimeObject*, PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900* >::Invoke(__this->___method_ptr_0, method, NULL, __this->___m_target_2, ___manager0);
}
void DisplacementDelegate_Invoke_m5D544DE149EA8535E783D20BD994198770497B8D_OpenVirtual(DisplacementDelegate_t4C836C2D37D9FABEC9514A574F9425C4B15B981A* __this, PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900* ___manager0, const RuntimeMethod* method)
{
	NullCheck(___manager0);
	VirtualActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(method), ___manager0);
}
void DisplacementDelegate_Invoke_m5D544DE149EA8535E783D20BD994198770497B8D_OpenInterface(DisplacementDelegate_t4C836C2D37D9FABEC9514A574F9425C4B15B981A* __this, PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900* ___manager0, const RuntimeMethod* method)
{
	NullCheck(___manager0);
	InterfaceActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(method), il2cpp_codegen_method_get_declaring_type(method), ___manager0);
}
void DisplacementDelegate_Invoke_m5D544DE149EA8535E783D20BD994198770497B8D_OpenGenericVirtual(DisplacementDelegate_t4C836C2D37D9FABEC9514A574F9425C4B15B981A* __this, PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900* ___manager0, const RuntimeMethod* method)
{
	NullCheck(___manager0);
	GenericVirtualActionInvoker0::Invoke(method, ___manager0);
}
void DisplacementDelegate_Invoke_m5D544DE149EA8535E783D20BD994198770497B8D_OpenGenericInterface(DisplacementDelegate_t4C836C2D37D9FABEC9514A574F9425C4B15B981A* __this, PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900* ___manager0, const RuntimeMethod* method)
{
	NullCheck(___manager0);
	GenericInterfaceActionInvoker0::Invoke(method, ___manager0);
}
// System.Void MicahW.PointGrass.PointGrassDisplacementManager/DisplacementDelegate::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DisplacementDelegate__ctor_m5FC18C77D43CFAAC46030EE3782AB012C1852A2B (DisplacementDelegate_t4C836C2D37D9FABEC9514A574F9425C4B15B981A* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) 
{
	__this->___method_ptr_0 = il2cpp_codegen_get_virtual_call_method_pointer((RuntimeMethod*)___method1);
	__this->___method_3 = ___method1;
	__this->___m_target_2 = ___object0;
	Il2CppCodeGenWriteBarrier((void**)(&__this->___m_target_2), (void*)___object0);
	int parameterCount = il2cpp_codegen_method_parameter_count((RuntimeMethod*)___method1);
	__this->___method_code_6 = (intptr_t)__this;
	if (MethodIsStatic((RuntimeMethod*)___method1))
	{
		bool isOpen = parameterCount == 1;
		if (il2cpp_codegen_call_method_via_invoker((RuntimeMethod*)___method1))
			if (isOpen)
				__this->___invoke_impl_1 = (intptr_t)&DisplacementDelegate_Invoke_m5D544DE149EA8535E783D20BD994198770497B8D_OpenStaticInvoker;
			else
				__this->___invoke_impl_1 = (intptr_t)&DisplacementDelegate_Invoke_m5D544DE149EA8535E783D20BD994198770497B8D_ClosedStaticInvoker;
		else
			if (isOpen)
				__this->___invoke_impl_1 = (intptr_t)&DisplacementDelegate_Invoke_m5D544DE149EA8535E783D20BD994198770497B8D_OpenStatic;
			else
				{
					__this->___invoke_impl_1 = (intptr_t)__this->___method_ptr_0;
					__this->___method_code_6 = (intptr_t)__this->___m_target_2;
				}
	}
	else
	{
		bool isOpen = parameterCount == 0;
		if (isOpen)
		{
			if (__this->___method_is_virtual_12)
			{
				if (il2cpp_codegen_method_is_generic_instance_method((RuntimeMethod*)___method1))
					if (il2cpp_codegen_method_is_interface_method((RuntimeMethod*)___method1))
						__this->___invoke_impl_1 = (intptr_t)&DisplacementDelegate_Invoke_m5D544DE149EA8535E783D20BD994198770497B8D_OpenGenericInterface;
					else
						__this->___invoke_impl_1 = (intptr_t)&DisplacementDelegate_Invoke_m5D544DE149EA8535E783D20BD994198770497B8D_OpenGenericVirtual;
				else
					if (il2cpp_codegen_method_is_interface_method((RuntimeMethod*)___method1))
						__this->___invoke_impl_1 = (intptr_t)&DisplacementDelegate_Invoke_m5D544DE149EA8535E783D20BD994198770497B8D_OpenInterface;
					else
						__this->___invoke_impl_1 = (intptr_t)&DisplacementDelegate_Invoke_m5D544DE149EA8535E783D20BD994198770497B8D_OpenVirtual;
			}
			else
			{
				__this->___invoke_impl_1 = (intptr_t)&DisplacementDelegate_Invoke_m5D544DE149EA8535E783D20BD994198770497B8D_OpenInst;
			}
		}
		else
		{
			if (___object0 == NULL)
				il2cpp_codegen_raise_exception(il2cpp_codegen_get_argument_exception(NULL, "Delegate to an instance method cannot have null 'this'."), NULL);
			__this->___invoke_impl_1 = (intptr_t)__this->___method_ptr_0;
			__this->___method_code_6 = (intptr_t)__this->___m_target_2;
		}
	}
	__this->___extra_arg_5 = (intptr_t)&DisplacementDelegate_Invoke_m5D544DE149EA8535E783D20BD994198770497B8D_Multicast;
}
// System.Void MicahW.PointGrass.PointGrassDisplacementManager/DisplacementDelegate::Invoke(MicahW.PointGrass.PointGrassDisplacementManager)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DisplacementDelegate_Invoke_m5D544DE149EA8535E783D20BD994198770497B8D (DisplacementDelegate_t4C836C2D37D9FABEC9514A574F9425C4B15B981A* __this, PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900* ___manager0, const RuntimeMethod* method) 
{
	typedef void (*FunctionPointerType) (RuntimeObject*, PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900*, const RuntimeMethod*);
	((FunctionPointerType)__this->___invoke_impl_1)((Il2CppObject*)__this->___method_code_6, ___manager0, reinterpret_cast<RuntimeMethod*>(__this->___method_3));
}
// System.IAsyncResult MicahW.PointGrass.PointGrassDisplacementManager/DisplacementDelegate::BeginInvoke(MicahW.PointGrass.PointGrassDisplacementManager,System.AsyncCallback,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* DisplacementDelegate_BeginInvoke_m0E606E6CE383DFF7108CFBF1E37B6066114ABEBA (DisplacementDelegate_t4C836C2D37D9FABEC9514A574F9425C4B15B981A* __this, PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900* ___manager0, AsyncCallback_t7FEF460CBDCFB9C5FA2EF776984778B9A4145F4C* ___callback1, RuntimeObject* ___object2, const RuntimeMethod* method) 
{
	void *__d_args[2] = {0};
	__d_args[0] = ___manager0;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void MicahW.PointGrass.PointGrassDisplacementManager/DisplacementDelegate::EndInvoke(System.IAsyncResult)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DisplacementDelegate_EndInvoke_m4269C29D065D5E2622B2F23184EF82132D62D8C5 (DisplacementDelegate_t4C836C2D37D9FABEC9514A574F9425C4B15B981A* __this, RuntimeObject* ___result0, const RuntimeMethod* method) 
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MicahW.PointGrass.PointGrassDisplacementManager/<>c::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__cctor_m1164B4575D6E45ED9E21975B33041E40006D5D18 (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_tC71A09B989D2FDC7808DBE99B3025974C73DE7C5_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CU3Ec_tC71A09B989D2FDC7808DBE99B3025974C73DE7C5* L_0 = (U3CU3Ec_tC71A09B989D2FDC7808DBE99B3025974C73DE7C5*)il2cpp_codegen_object_new(U3CU3Ec_tC71A09B989D2FDC7808DBE99B3025974C73DE7C5_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		U3CU3Ec__ctor_m81B88999DD9498F0E72DF82C81657C2AEB967FE3(L_0, NULL);
		((U3CU3Ec_tC71A09B989D2FDC7808DBE99B3025974C73DE7C5_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tC71A09B989D2FDC7808DBE99B3025974C73DE7C5_il2cpp_TypeInfo_var))->___U3CU3E9_0 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&((U3CU3Ec_tC71A09B989D2FDC7808DBE99B3025974C73DE7C5_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tC71A09B989D2FDC7808DBE99B3025974C73DE7C5_il2cpp_TypeInfo_var))->___U3CU3E9_0), (void*)L_0);
		return;
	}
}
// System.Void MicahW.PointGrass.PointGrassDisplacementManager/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m81B88999DD9498F0E72DF82C81657C2AEB967FE3 (U3CU3Ec_tC71A09B989D2FDC7808DBE99B3025974C73DE7C5* __this, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		return;
	}
}
// MicahW.PointGrass.PointGrassCommon/ObjectData MicahW.PointGrass.PointGrassDisplacementManager/<>c::<UpdateBuffer>b__13_0(MicahW.PointGrass.PointGrassDisplacer)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ObjectData_t24D94D36E8D49DEB90BBFEE7BA833C910F0AF6C4 U3CU3Ec_U3CUpdateBufferU3Eb__13_0_m28DE49B834780F799169B47BA3327FE3FCFF9CFA (U3CU3Ec_tC71A09B989D2FDC7808DBE99B3025974C73DE7C5* __this, PointGrassDisplacer_tB9B22DA401FDA294990903A742F3ACA3351A82FF* ___a0, const RuntimeMethod* method) 
{
	{
		// var objectData = displacers.Select(a => a.GetObjectData()).ToArray();
		PointGrassDisplacer_tB9B22DA401FDA294990903A742F3ACA3351A82FF* L_0 = ___a0;
		NullCheck(L_0);
		ObjectData_t24D94D36E8D49DEB90BBFEE7BA833C910F0AF6C4 L_1;
		L_1 = PointGrassDisplacer_GetObjectData_m910E224560C96B24B45954527158E492A9CF4859(L_0, NULL);
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MicahW.PointGrass.PointGrassDisplacer::Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointGrassDisplacer_Reset_m372EADAF5A72A9C79F8B31C93AEDC70A66716764 (PointGrassDisplacer_tB9B22DA401FDA294990903A742F3ACA3351A82FF* __this, const RuntimeMethod* method) 
{
	{
		// localPosition = Vector3.zero;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0;
		L_0 = Vector3_get_zero_m0C1249C3F25B1C70EAD3CC8B31259975A457AE39_inline(NULL);
		__this->___localPosition_4 = L_0;
		// radius = 0.5f;
		__this->___radius_5 = (0.5f);
		// strength = 1f;
		__this->___strength_6 = (1.0f);
		// }
		return;
	}
}
// System.Void MicahW.PointGrass.PointGrassDisplacer::OnEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointGrassDisplacer_OnEnable_m24FE571048E2F0EB323702C02DD0C33EF143E448 (PointGrassDisplacer_tB9B22DA401FDA294990903A742F3ACA3351A82FF* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DisplacementDelegate_t4C836C2D37D9FABEC9514A574F9425C4B15B981A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PointGrassDisplacer_Initialize_m45044718CDB24241F017732A234C381653A0650A_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (PGDM.instance != null) { PGDM.instance.AddDisplacer(this); } // If the component was added at runtime
		il2cpp_codegen_runtime_class_init_inline(PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900_il2cpp_TypeInfo_var);
		PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900* L_0 = ((PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900_StaticFields*)il2cpp_codegen_static_fields_for(PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900_il2cpp_TypeInfo_var))->___instance_4;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Inequality_mD0BE578448EAA61948F25C32F8DD55AB1F778602(L_0, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		// if (PGDM.instance != null) { PGDM.instance.AddDisplacer(this); } // If the component was added at runtime
		il2cpp_codegen_runtime_class_init_inline(PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900_il2cpp_TypeInfo_var);
		PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900* L_2 = ((PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900_StaticFields*)il2cpp_codegen_static_fields_for(PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900_il2cpp_TypeInfo_var))->___instance_4;
		NullCheck(L_2);
		PointGrassDisplacementManager_AddDisplacer_m34B41A7480D16398EC5BF58ECFB2A73F92EFEE6F(L_2, __this, NULL);
		return;
	}

IL_0019:
	{
		// else { PGDM.OnInitialize += Initialize; }                        // If the component initialized before the manager
		DisplacementDelegate_t4C836C2D37D9FABEC9514A574F9425C4B15B981A* L_3 = (DisplacementDelegate_t4C836C2D37D9FABEC9514A574F9425C4B15B981A*)il2cpp_codegen_object_new(DisplacementDelegate_t4C836C2D37D9FABEC9514A574F9425C4B15B981A_il2cpp_TypeInfo_var);
		NullCheck(L_3);
		DisplacementDelegate__ctor_m5FC18C77D43CFAAC46030EE3782AB012C1852A2B(L_3, __this, (intptr_t)((void*)PointGrassDisplacer_Initialize_m45044718CDB24241F017732A234C381653A0650A_RuntimeMethod_var), NULL);
		il2cpp_codegen_runtime_class_init_inline(PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900_il2cpp_TypeInfo_var);
		PointGrassDisplacementManager_add_OnInitialize_mD1CDC92403A311CF3BBA4C8913EF1BE50C48DD54(L_3, NULL);
		// }
		return;
	}
}
// System.Void MicahW.PointGrass.PointGrassDisplacer::OnDisable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointGrassDisplacer_OnDisable_m0AA403F7A98845FEC1603359586985C16CEAF61E (PointGrassDisplacer_tB9B22DA401FDA294990903A742F3ACA3351A82FF* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (PGDM.instance != null) { PGDM.instance.RemoveDisplacer(this); }
		il2cpp_codegen_runtime_class_init_inline(PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900_il2cpp_TypeInfo_var);
		PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900* L_0 = ((PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900_StaticFields*)il2cpp_codegen_static_fields_for(PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900_il2cpp_TypeInfo_var))->___instance_4;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Inequality_mD0BE578448EAA61948F25C32F8DD55AB1F778602(L_0, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		// if (PGDM.instance != null) { PGDM.instance.RemoveDisplacer(this); }
		il2cpp_codegen_runtime_class_init_inline(PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900_il2cpp_TypeInfo_var);
		PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900* L_2 = ((PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900_StaticFields*)il2cpp_codegen_static_fields_for(PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900_il2cpp_TypeInfo_var))->___instance_4;
		NullCheck(L_2);
		PointGrassDisplacementManager_RemoveDisplacer_m610EACCC6E76F1C21D580178793521035C34B32A(L_2, __this, NULL);
	}

IL_0018:
	{
		// }
		return;
	}
}
// System.Void MicahW.PointGrass.PointGrassDisplacer::Initialize(MicahW.PointGrass.PointGrassDisplacementManager)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointGrassDisplacer_Initialize_m45044718CDB24241F017732A234C381653A0650A (PointGrassDisplacer_tB9B22DA401FDA294990903A742F3ACA3351A82FF* __this, PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900* ___manager0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DisplacementDelegate_t4C836C2D37D9FABEC9514A574F9425C4B15B981A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PointGrassDisplacer_Initialize_m45044718CDB24241F017732A234C381653A0650A_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// manager.AddDisplacer(this);
		PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900* L_0 = ___manager0;
		NullCheck(L_0);
		PointGrassDisplacementManager_AddDisplacer_m34B41A7480D16398EC5BF58ECFB2A73F92EFEE6F(L_0, __this, NULL);
		// PGDM.OnInitialize -= Initialize; // Unsubscribe from the event
		DisplacementDelegate_t4C836C2D37D9FABEC9514A574F9425C4B15B981A* L_1 = (DisplacementDelegate_t4C836C2D37D9FABEC9514A574F9425C4B15B981A*)il2cpp_codegen_object_new(DisplacementDelegate_t4C836C2D37D9FABEC9514A574F9425C4B15B981A_il2cpp_TypeInfo_var);
		NullCheck(L_1);
		DisplacementDelegate__ctor_m5FC18C77D43CFAAC46030EE3782AB012C1852A2B(L_1, __this, (intptr_t)((void*)PointGrassDisplacer_Initialize_m45044718CDB24241F017732A234C381653A0650A_RuntimeMethod_var), NULL);
		il2cpp_codegen_runtime_class_init_inline(PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900_il2cpp_TypeInfo_var);
		PointGrassDisplacementManager_remove_OnInitialize_m5F94865C071988D7AF7EF522E3ABFAFFABF7E6B0(L_1, NULL);
		// }
		return;
	}
}
// MicahW.PointGrass.PointGrassCommon/ObjectData MicahW.PointGrass.PointGrassDisplacer::GetObjectData()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ObjectData_t24D94D36E8D49DEB90BBFEE7BA833C910F0AF6C4 PointGrassDisplacer_GetObjectData_m910E224560C96B24B45954527158E492A9CF4859 (PointGrassDisplacer_tB9B22DA401FDA294990903A742F3ACA3351A82FF* __this, const RuntimeMethod* method) 
{
	{
		// public ObjectData GetObjectData() => new ObjectData(transform.TransformPoint(localPosition), radius, strength);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_0;
		L_0 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(__this, NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_1 = __this->___localPosition_4;
		NullCheck(L_0);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_2;
		L_2 = Transform_TransformPoint_m05BFF013DB830D7BFE44A007703694AE1062EE44(L_0, L_1, NULL);
		float L_3 = __this->___radius_5;
		float L_4 = __this->___strength_6;
		ObjectData_t24D94D36E8D49DEB90BBFEE7BA833C910F0AF6C4 L_5;
		memset((&L_5), 0, sizeof(L_5));
		ObjectData__ctor_mB2378318DDD47DCB6AEC9D788C27A66A94AE84CC((&L_5), L_2, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void MicahW.PointGrass.PointGrassDisplacer::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointGrassDisplacer__ctor_m1D4A021DA0DC02AD9821550EB8B99231E3F612C7 (PointGrassDisplacer_tB9B22DA401FDA294990903A742F3ACA3351A82FF* __this, const RuntimeMethod* method) 
{
	{
		// public Vector3 localPosition = Vector3.zero;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0;
		L_0 = Vector3_get_zero_m0C1249C3F25B1C70EAD3CC8B31259975A457AE39_inline(NULL);
		__this->___localPosition_4 = L_0;
		// public float radius = 0.5f;
		__this->___radius_5 = (0.5f);
		// public float strength = 1f;
		__this->___strength_6 = (1.0f);
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MicahW.PointGrass.PointGrassWind::OnEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointGrassWind_OnEnable_m9D03B4E3F96CD5584E00FBA036873EA66302869C (PointGrassWind_t2002E1D6C3245367514FF9A0270205781DEB2C67* __this, const RuntimeMethod* method) 
{
	{
		// GetShaderIDs();
		PointGrassWind_GetShaderIDs_mF3766A5521D8AFCA5B72DD11BA5EA10B02FCC8C9(NULL);
		// currentNoisePosition = Vector3.zero;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0;
		L_0 = Vector3_get_zero_m0C1249C3F25B1C70EAD3CC8B31259975A457AE39_inline(NULL);
		__this->___currentNoisePosition_8 = L_0;
		// RefreshValues();
		PointGrassWind_RefreshValues_m9AF23283B96B07EF4B748964644941DD789CB8EA(__this, NULL);
		// }
		return;
	}
}
// System.Void MicahW.PointGrass.PointGrassWind::LateUpdate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointGrassWind_LateUpdate_m72593FADB6033D1FC96B9565E6AE8A669CC58895 (PointGrassWind_t2002E1D6C3245367514FF9A0270205781DEB2C67* __this, const RuntimeMethod* method) 
{
	{
		// currentNoisePosition += windScroll * Time.deltaTime;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = __this->___currentNoisePosition_8;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_1 = __this->___windScroll_7;
		float L_2;
		L_2 = Time_get_deltaTime_mC3195000401F0FD167DD2F948FD2BC58330D0865(NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_3;
		L_3 = Vector3_op_Multiply_m87BA7C578F96C8E49BB07088DAAC4649F83B0353_inline(L_1, L_2, NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_4;
		L_4 = Vector3_op_Addition_m78C0EC70CB66E8DCAC225743D82B268DAEE92067_inline(L_0, L_3, NULL);
		__this->___currentNoisePosition_8 = L_4;
		// RefreshValues();
		PointGrassWind_RefreshValues_m9AF23283B96B07EF4B748964644941DD789CB8EA(__this, NULL);
		// }
		return;
	}
}
// System.Void MicahW.PointGrass.PointGrassWind::GetShaderIDs()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointGrassWind_GetShaderIDs_mF3766A5521D8AFCA5B72DD11BA5EA10B02FCC8C9 (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PointGrassWind_t2002E1D6C3245367514FF9A0270205781DEB2C67_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral0C0964978D1C92B07D383BD488C886667A5B2474);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral88169464D968B27B7856C0A38FD386DE5FDAFF49);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF11EAC0A40707F9A5D0649BA624E242ECCE752E5);
		s_Il2CppMethodInitialized = true;
	}
	{
		// ID_vecA = Shader.PropertyToID("_PG_VectorA");
		int32_t L_0;
		L_0 = Shader_PropertyToID_mE98523D50F5656CAE89B30695C458253EB8956CA(_stringLiteral88169464D968B27B7856C0A38FD386DE5FDAFF49, NULL);
		((PointGrassWind_t2002E1D6C3245367514FF9A0270205781DEB2C67_StaticFields*)il2cpp_codegen_static_fields_for(PointGrassWind_t2002E1D6C3245367514FF9A0270205781DEB2C67_il2cpp_TypeInfo_var))->___ID_vecA_9 = L_0;
		// ID_vecB = Shader.PropertyToID("_PG_VectorB");
		int32_t L_1;
		L_1 = Shader_PropertyToID_mE98523D50F5656CAE89B30695C458253EB8956CA(_stringLiteral0C0964978D1C92B07D383BD488C886667A5B2474, NULL);
		((PointGrassWind_t2002E1D6C3245367514FF9A0270205781DEB2C67_StaticFields*)il2cpp_codegen_static_fields_for(PointGrassWind_t2002E1D6C3245367514FF9A0270205781DEB2C67_il2cpp_TypeInfo_var))->___ID_vecB_10 = L_1;
		// ID_valA = Shader.PropertyToID("_PG_ValueA");
		int32_t L_2;
		L_2 = Shader_PropertyToID_mE98523D50F5656CAE89B30695C458253EB8956CA(_stringLiteralF11EAC0A40707F9A5D0649BA624E242ECCE752E5, NULL);
		((PointGrassWind_t2002E1D6C3245367514FF9A0270205781DEB2C67_StaticFields*)il2cpp_codegen_static_fields_for(PointGrassWind_t2002E1D6C3245367514FF9A0270205781DEB2C67_il2cpp_TypeInfo_var))->___ID_valA_11 = L_2;
		// }
		return;
	}
}
// System.Void MicahW.PointGrass.PointGrassWind::RefreshValues()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointGrassWind_RefreshValues_m9AF23283B96B07EF4B748964644941DD789CB8EA (PointGrassWind_t2002E1D6C3245367514FF9A0270205781DEB2C67* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PointGrassWind_t2002E1D6C3245367514FF9A0270205781DEB2C67_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	PackedProperties_tAE6E15A41456BA8070C1C0B4F6D3DA9E28624A32 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// PackedProperties properties = PackProperties();
		PackedProperties_tAE6E15A41456BA8070C1C0B4F6D3DA9E28624A32 L_0;
		L_0 = PointGrassWind_PackProperties_mFEB1D1032F4CBADF707C35634FA8BE9169FF526B(__this, NULL);
		V_0 = L_0;
		// Shader.SetGlobalVector(ID_vecA, properties.vecA);
		int32_t L_1 = ((PointGrassWind_t2002E1D6C3245367514FF9A0270205781DEB2C67_StaticFields*)il2cpp_codegen_static_fields_for(PointGrassWind_t2002E1D6C3245367514FF9A0270205781DEB2C67_il2cpp_TypeInfo_var))->___ID_vecA_9;
		PackedProperties_tAE6E15A41456BA8070C1C0B4F6D3DA9E28624A32 L_2 = V_0;
		Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 L_3 = L_2.___vecA_0;
		Shader_SetGlobalVector_mDC5F45B008D44A2C8BF6D450CFE8B58B847C8190(L_1, L_3, NULL);
		// Shader.SetGlobalVector(ID_vecB, properties.vecB);
		int32_t L_4 = ((PointGrassWind_t2002E1D6C3245367514FF9A0270205781DEB2C67_StaticFields*)il2cpp_codegen_static_fields_for(PointGrassWind_t2002E1D6C3245367514FF9A0270205781DEB2C67_il2cpp_TypeInfo_var))->___ID_vecB_10;
		PackedProperties_tAE6E15A41456BA8070C1C0B4F6D3DA9E28624A32 L_5 = V_0;
		Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 L_6 = L_5.___vecB_1;
		Shader_SetGlobalVector_mDC5F45B008D44A2C8BF6D450CFE8B58B847C8190(L_4, L_6, NULL);
		// Shader.SetGlobalFloat(ID_valA, properties.valA);
		int32_t L_7 = ((PointGrassWind_t2002E1D6C3245367514FF9A0270205781DEB2C67_StaticFields*)il2cpp_codegen_static_fields_for(PointGrassWind_t2002E1D6C3245367514FF9A0270205781DEB2C67_il2cpp_TypeInfo_var))->___ID_valA_11;
		PackedProperties_tAE6E15A41456BA8070C1C0B4F6D3DA9E28624A32 L_8 = V_0;
		float L_9 = L_8.___valA_2;
		Shader_SetGlobalFloat_mE7D0DA2B0A62925E093B318785AF82A173794AFC(L_7, L_9, NULL);
		// }
		return;
	}
}
// MicahW.PointGrass.PointGrassWind/PackedProperties MicahW.PointGrass.PointGrassWind::PackProperties()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR PackedProperties_tAE6E15A41456BA8070C1C0B4F6D3DA9E28624A32 PointGrassWind_PackProperties_mFEB1D1032F4CBADF707C35634FA8BE9169FF526B (PointGrassWind_t2002E1D6C3245367514FF9A0270205781DEB2C67* __this, const RuntimeMethod* method) 
{
	{
		// private PackedProperties PackProperties() => new PackedProperties(windDirection, currentNoisePosition, noiseRange, windScale);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = __this->___windDirection_6;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_1 = __this->___currentNoisePosition_8;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_2 = __this->___noiseRange_5;
		float L_3 = __this->___windScale_4;
		PackedProperties_tAE6E15A41456BA8070C1C0B4F6D3DA9E28624A32 L_4;
		memset((&L_4), 0, sizeof(L_4));
		PackedProperties__ctor_m1ADC969557DD708678FAFC6C730181CDAFEAC8A2((&L_4), L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void MicahW.PointGrass.PointGrassWind::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointGrassWind__ctor_m30AEB1C86052693AFE52034CDDFE4C4ED39A0F9A (PointGrassWind_t2002E1D6C3245367514FF9A0270205781DEB2C67* __this, const RuntimeMethod* method) 
{
	{
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MicahW.PointGrass.PointGrassWind/PackedProperties::.ctor(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector2,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PackedProperties__ctor_m1ADC969557DD708678FAFC6C730181CDAFEAC8A2 (PackedProperties_tAE6E15A41456BA8070C1C0B4F6D3DA9E28624A32* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___windDirection0, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___windScroll1, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___noiseRange2, float ___windScale3, const RuntimeMethod* method) 
{
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 V_0;
	memset((&V_0), 0, sizeof(V_0));
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		// Vector4 vecA = windDirection;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = ___windDirection0;
		Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 L_1;
		L_1 = Vector4_op_Implicit_m2ECA73F345A7AD84144133E9E51657204002B12D_inline(L_0, NULL);
		V_0 = L_1;
		// vecA.w = noiseRange.x;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_2 = ___noiseRange2;
		float L_3 = L_2.___x_0;
		(&V_0)->___w_4 = L_3;
		// Vector4 vecB = windScroll;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_4 = ___windScroll1;
		Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 L_5;
		L_5 = Vector4_op_Implicit_m2ECA73F345A7AD84144133E9E51657204002B12D_inline(L_4, NULL);
		V_1 = L_5;
		// vecB.w = noiseRange.y;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_6 = ___noiseRange2;
		float L_7 = L_6.___y_1;
		(&V_1)->___w_4 = L_7;
		// this.vecA = vecA;
		Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 L_8 = V_0;
		__this->___vecA_0 = L_8;
		// this.vecB = vecB;
		Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 L_9 = V_1;
		__this->___vecB_1 = L_9;
		// valA = windScale;
		float L_10 = ___windScale3;
		__this->___valA_2 = L_10;
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void PackedProperties__ctor_m1ADC969557DD708678FAFC6C730181CDAFEAC8A2_AdjustorThunk (RuntimeObject* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___windDirection0, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___windScroll1, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___noiseRange2, float ___windScale3, const RuntimeMethod* method)
{
	PackedProperties_tAE6E15A41456BA8070C1C0B4F6D3DA9E28624A32* _thisAdjusted;
	int32_t _offset = 1;
	_thisAdjusted = reinterpret_cast<PackedProperties_tAE6E15A41456BA8070C1C0B4F6D3DA9E28624A32*>(__this + _offset);
	PackedProperties__ctor_m1ADC969557DD708678FAFC6C730181CDAFEAC8A2(_thisAdjusted, ___windDirection0, ___windScroll1, ___noiseRange2, ___windScale3, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean MicahW.PointGrass.PointGrassRenderer::get_UsingMultipleMeshes()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PointGrassRenderer_get_UsingMultipleMeshes_m61A92AF3BC219534CD301983C34CECB07E6A3E59 (PointGrassRenderer_tCAB9C932B5AEF5960C04E43451965E31C6A471C2* __this, const RuntimeMethod* method) 
{
	{
		// private bool UsingMultipleMeshes => bladeType == BladeType.Mesh && multipleMeshes;
		int32_t L_0 = __this->___bladeType_11;
		if ((!(((uint32_t)L_0) == ((uint32_t)2))))
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = __this->___multipleMeshes_12;
		return L_1;
	}

IL_0010:
	{
		return (bool)0;
	}
}
// System.Void MicahW.PointGrass.PointGrassRenderer::Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointGrassRenderer_Reset_m56465633422BCAF773FAD656147169CBD7553DCA (PointGrassRenderer_tCAB9C932B5AEF5960C04E43451965E31C6A471C2* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MaterialU5BU5D_t2B1D11C42DB07A4400C0535F92DBB87A2E346D3D_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MeshU5BU5D_t178CA36422FC397211E68FB7E39C5B2F95619689_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// ClearBuffers();
		PointGrassRenderer_ClearBuffers_mE07D764621EE41CB4ED709B578227440F728B60A(__this, NULL);
		// bladeType = BladeType.Flat;
		__this->___bladeType_11 = 0;
		// multipleMeshes = false;
		__this->___multipleMeshes_12 = (bool)0;
		// grassBladeMesh = null;
		__this->___grassBladeMesh_13 = (Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4*)NULL;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___grassBladeMesh_13), (void*)(Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4*)NULL);
		// grassBladeMeshes = new Mesh[1];
		MeshU5BU5D_t178CA36422FC397211E68FB7E39C5B2F95619689* L_0 = (MeshU5BU5D_t178CA36422FC397211E68FB7E39C5B2F95619689*)(MeshU5BU5D_t178CA36422FC397211E68FB7E39C5B2F95619689*)SZArrayNew(MeshU5BU5D_t178CA36422FC397211E68FB7E39C5B2F95619689_il2cpp_TypeInfo_var, (uint32_t)1);
		__this->___grassBladeMeshes_14 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___grassBladeMeshes_14), (void*)L_0);
		// meshDensityValues = new float[] { 1f };
		SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* L_1 = (SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C*)(SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C*)SZArrayNew(SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C_il2cpp_TypeInfo_var, (uint32_t)1);
		SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* L_2 = L_1;
		NullCheck(L_2);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (float)(1.0f));
		__this->___meshDensityValues_15 = L_2;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___meshDensityValues_15), (void*)L_2);
		// multipleMaterials = false;
		__this->___multipleMaterials_16 = (bool)0;
		// material = null;
		__this->___material_17 = (Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3*)NULL;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___material_17), (void*)(Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3*)NULL);
		// materials = new Material[1];
		MaterialU5BU5D_t2B1D11C42DB07A4400C0535F92DBB87A2E346D3D* L_3 = (MaterialU5BU5D_t2B1D11C42DB07A4400C0535F92DBB87A2E346D3D*)(MaterialU5BU5D_t2B1D11C42DB07A4400C0535F92DBB87A2E346D3D*)SZArrayNew(MaterialU5BU5D_t2B1D11C42DB07A4400C0535F92DBB87A2E346D3D_il2cpp_TypeInfo_var, (uint32_t)1);
		__this->___materials_18 = L_3;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___materials_18), (void*)L_3);
		// shadowMode = UnityEngine.Rendering.ShadowCastingMode.On;
		__this->___shadowMode_19 = 1;
		// renderLayer.Set(gameObject.layer);
		SingleLayer_tA74696727907F9D2CD336CD4C7AA5C5C8C8FB4ED* L_4 = (&__this->___renderLayer_20);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_5;
		L_5 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(__this, NULL);
		NullCheck(L_5);
		int32_t L_6;
		L_6 = GameObject_get_layer_m108902B9C89E9F837CE06B9942AA42307450FEAF(L_5, NULL);
		SingleLayer_Set_mBBB701BA6BC74136FD35A221A1341CFECEB93BEB(L_4, L_6, NULL);
		// pointCount = 1000f;
		__this->___pointCount_21 = (1000.0f);
		// multiplyByArea = false;
		__this->___multiplyByArea_22 = (bool)0;
		// pointLODFactor = 1f;
		__this->___pointLODFactor_23 = (1.0f);
		// randomiseSeed = true;
		__this->___randomiseSeed_24 = (bool)1;
		// seed = 0;
		__this->___seed_25 = 0;
		// overwriteNormalDirection = false;
		__this->___overwriteNormalDirection_26 = (bool)0;
		// forcedNormal = Vector3.up;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_7;
		L_7 = Vector3_get_up_m128AF3FDC820BF59D5DE86D973E7DE3F20C3AEBA_inline(NULL);
		__this->___forcedNormal_27 = L_7;
		// useDensity = true;
		__this->___useDensity_28 = (bool)1;
		// densityCutoff = 0.5f;
		__this->___densityCutoff_29 = (0.5f);
		// useLength = true;
		__this->___useLength_30 = (bool)1;
		// projectType = ProjectionType.None;
		__this->___projectType_38 = 0;
		// projectMask = ~0;
		LayerMask_t97CB6BDADEDC3D6423C7BCFEA7F86DA2EC6241DB L_8;
		L_8 = LayerMask_op_Implicit_m01C8996A2CB2085328B9C33539C43139660D8222((-1), NULL);
		__this->___projectMask_39 = L_8;
		// }
		return;
	}
}
// System.Void MicahW.PointGrass.PointGrassRenderer::OnEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointGrassRenderer_OnEnable_mAAD03A50636AA34D588C4F740A42C7D0E0DD1083 (PointGrassRenderer_tCAB9C932B5AEF5960C04E43451965E31C6A471C2* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (!CompatibilityCheck()) return;
		bool L_0;
		L_0 = PointGrassRenderer_CompatibilityCheck_mF24B3CB5752F3B46635B710E9399DC0169EDC298(__this, NULL);
		if (L_0)
		{
			goto IL_0009;
		}
	}
	{
		// if (!CompatibilityCheck()) return;
		return;
	}

IL_0009:
	{
		// if (!PropertyIDsInitialized) { FindPropertyIDs(); }
		bool L_1;
		L_1 = PointGrassCommon_get_PropertyIDsInitialized_m9958FC8398CDF68DA0FFF72DAA3C35469B07D1A1_inline(NULL);
		if (L_1)
		{
			goto IL_0015;
		}
	}
	{
		// if (!PropertyIDsInitialized) { FindPropertyIDs(); }
		PointGrassCommon_FindPropertyIDs_m4B83779C3F568CC8BFEEFEC4FB0A50B525997711(NULL);
	}

IL_0015:
	{
		// if (grassMeshFlat == null || grassMeshCyl == null) { GenerateGrassMeshes(); }
		Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* L_2 = ((PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_StaticFields*)il2cpp_codegen_static_fields_for(PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_il2cpp_TypeInfo_var))->___grassMeshFlat_0;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_3;
		L_3 = Object_op_Equality_mB6120F782D83091EF56A198FCEBCF066DB4A9605(L_2, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (L_3)
		{
			goto IL_002f;
		}
	}
	{
		Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* L_4 = ((PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_StaticFields*)il2cpp_codegen_static_fields_for(PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_il2cpp_TypeInfo_var))->___grassMeshCyl_1;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_5;
		L_5 = Object_op_Equality_mB6120F782D83091EF56A198FCEBCF066DB4A9605(L_4, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_5)
		{
			goto IL_0034;
		}
	}

IL_002f:
	{
		// if (grassMeshFlat == null || grassMeshCyl == null) { GenerateGrassMeshes(); }
		PointGrassCommon_GenerateGrassMeshes_m1187A0027530C7B935B788DF2975503346224C28(NULL);
	}

IL_0034:
	{
		// BuildPoints();
		PointGrassRenderer_BuildPoints_m22ADAC358BA12DDC83B6F4E9AAB9317E4A53FA13(__this, NULL);
		// }
		return;
	}
}
// System.Void MicahW.PointGrass.PointGrassRenderer::OnDisable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointGrassRenderer_OnDisable_mD6B91B87B90C3A9D61CEA537026CCCFCDAC9DC28 (PointGrassRenderer_tCAB9C932B5AEF5960C04E43451965E31C6A471C2* __this, const RuntimeMethod* method) 
{
	{
		// private void OnDisable() { ClearBuffers(); }
		PointGrassRenderer_ClearBuffers_mE07D764621EE41CB4ED709B578227440F728B60A(__this, NULL);
		// private void OnDisable() { ClearBuffers(); }
		return;
	}
}
// System.Void MicahW.PointGrass.PointGrassRenderer::LateUpdate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointGrassRenderer_LateUpdate_mDB02656F534CADAF622888481915E2866BF206E2 (PointGrassRenderer_tCAB9C932B5AEF5960C04E43451965E31C6A471C2* __this, const RuntimeMethod* method) 
{
	{
		// private void LateUpdate() { DrawGrass(); }
		PointGrassRenderer_DrawGrass_m3930D3BDAD2C3E7C6EB7DB2B1206DE93B1095288(__this, NULL);
		// private void LateUpdate() { DrawGrass(); }
		return;
	}
}
// System.Boolean MicahW.PointGrass.PointGrassRenderer::CompatibilityCheck()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PointGrassRenderer_CompatibilityCheck_mF24B3CB5752F3B46635B710E9399DC0169EDC298 (PointGrassRenderer_tCAB9C932B5AEF5960C04E43451965E31C6A471C2* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral8118A337F70EBA77C08A715F57383297414C016F);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC3C49CF9E0D06A1BABDDB923B010206544D63157);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE4CF68ABCF7518481A531AEFC2E0812182AD4226);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (!SystemInfo.supportsInstancing) {
		bool L_0;
		L_0 = SystemInfo_get_supportsInstancing_m8EF067060BFA3D50A266342D26A392747DA4FF3E(NULL);
		if (L_0)
		{
			goto IL_002e;
		}
	}
	{
		// Debug.LogError($"This system doesn't support instanced draw calls. \"{gameObject.name}\" is unable to render its point grass");
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(__this, NULL);
		NullCheck(L_1);
		String_t* L_2;
		L_2 = Object_get_name_mAC2F6B897CF1303BA4249B4CB55271AFACBB6392(L_1, NULL);
		String_t* L_3;
		L_3 = String_Concat_m8855A6DE10F84DA7F4EC113CADDB59873A25573B(_stringLiteralC3C49CF9E0D06A1BABDDB923B010206544D63157, L_2, _stringLiteral8118A337F70EBA77C08A715F57383297414C016F, NULL);
		il2cpp_codegen_runtime_class_init_inline(Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		Debug_LogError_mB00B2B4468EF3CAF041B038D840820FB84C924B2(L_3, NULL);
		// Disable();
		PointGrassRenderer_U3CCompatibilityCheckU3Eg__DisableU7C43_0_mAD8E4C2F34F3754FDA2660945EF8B86F40B9AF3C(__this, NULL);
		// return false;
		return (bool)0;
	}

IL_002e:
	{
		// else if (SystemInfo.graphicsShaderLevel < 45) {
		int32_t L_4;
		L_4 = SystemInfo_get_graphicsShaderLevel_m9E6B001FA80EFBFC92EF4E7440AE64828B15070F(NULL);
		if ((((int32_t)L_4) >= ((int32_t)((int32_t)45))))
		{
			goto IL_0049;
		}
	}
	{
		// Debug.LogError($"This system doesn't support shader model 4.5. Compute buffers are unsupported in the point grass shaders");
		il2cpp_codegen_runtime_class_init_inline(Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		Debug_LogError_mB00B2B4468EF3CAF041B038D840820FB84C924B2(_stringLiteralE4CF68ABCF7518481A531AEFC2E0812182AD4226, NULL);
		// Disable();
		PointGrassRenderer_U3CCompatibilityCheckU3Eg__DisableU7C43_0_mAD8E4C2F34F3754FDA2660945EF8B86F40B9AF3C(__this, NULL);
		// return false;
		return (bool)0;
	}

IL_0049:
	{
		// return true;
		return (bool)1;
	}
}
// System.Void MicahW.PointGrass.PointGrassRenderer::BuildPoints()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointGrassRenderer_BuildPoints_m22ADAC358BA12DDC83B6F4E9AAB9317E4A53FA13 (PointGrassRenderer_tCAB9C932B5AEF5960C04E43451965E31C6A471C2* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Nullable_1__ctor_m75F3ABB694E26670F021136BD3B9E71A65948BC2_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Nullable_1_t9C51B084784B716FFF4ED4575C63CFD8A71A86FE V_1;
	memset((&V_1), 0, sizeof(V_1));
	int32_t G_B3_0 = 0;
	{
		// ClearBuffers();
		PointGrassRenderer_ClearBuffers_mE07D764621EE41CB4ED709B578227440F728B60A(__this, NULL);
		// int seed = randomiseSeed ? Random.Range(int.MinValue, int.MaxValue) : this.seed;
		bool L_0 = __this->___randomiseSeed_24;
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_1 = __this->___seed_25;
		G_B3_0 = L_1;
		goto IL_0025;
	}

IL_0016:
	{
		int32_t L_2;
		L_2 = Random_Range_m6763D9767F033357F88B6637F048F4ACA4123B68(((int32_t)-2147483648LL), ((int32_t)2147483647LL), NULL);
		G_B3_0 = L_2;
	}

IL_0025:
	{
		V_0 = G_B3_0;
		// Vector3? overwriteNormal = null;
		il2cpp_codegen_initobj((&V_1), sizeof(Nullable_1_t9C51B084784B716FFF4ED4575C63CFD8A71A86FE));
		// if (overwriteNormalDirection) {
		bool L_3 = __this->___overwriteNormalDirection_26;
		if (!L_3)
		{
			goto IL_005f;
		}
	}
	{
		// forcedNormal = forcedNormal.normalized;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* L_4 = (&__this->___forcedNormal_27);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_5;
		L_5 = Vector3_get_normalized_m736BBF65D5CDA7A18414370D15B4DFCC1E466F07_inline(L_4, NULL);
		__this->___forcedNormal_27 = L_5;
		// overwriteNormal = transform.InverseTransformDirection(forcedNormal);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_6;
		L_6 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(__this, NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_7 = __this->___forcedNormal_27;
		NullCheck(L_6);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_8;
		L_8 = Transform_InverseTransformDirection_m69C077B881A98B08C7F231EFC49429C906FBC575(L_6, L_7, NULL);
		Nullable_1__ctor_m75F3ABB694E26670F021136BD3B9E71A65948BC2((&V_1), L_8, Nullable_1__ctor_m75F3ABB694E26670F021136BD3B9E71A65948BC2_RuntimeMethod_var);
	}

IL_005f:
	{
		// if (distSource == DistributionSource.TerrainData && chunkCount.x > 1 && chunkCount.y > 1) { BuildPoints_Terrain(seed, overwriteNormal); }
		int32_t L_9 = __this->___distSource_4;
		if ((!(((uint32_t)L_9) == ((uint32_t)2))))
		{
			goto IL_008d;
		}
	}
	{
		Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A* L_10 = (&__this->___chunkCount_8);
		int32_t L_11;
		L_11 = Vector2Int_get_x_mA2CACB1B6E6B5AD0CCC32B2CD2EDCE3ECEB50576_inline(L_10, NULL);
		if ((((int32_t)L_11) <= ((int32_t)1)))
		{
			goto IL_008d;
		}
	}
	{
		Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A* L_12 = (&__this->___chunkCount_8);
		int32_t L_13;
		L_13 = Vector2Int_get_y_m48454163ECF0B463FB5A16A0C4FC4B14DB0768B3_inline(L_12, NULL);
		if ((((int32_t)L_13) <= ((int32_t)1)))
		{
			goto IL_008d;
		}
	}
	{
		// if (distSource == DistributionSource.TerrainData && chunkCount.x > 1 && chunkCount.y > 1) { BuildPoints_Terrain(seed, overwriteNormal); }
		int32_t L_14 = V_0;
		Nullable_1_t9C51B084784B716FFF4ED4575C63CFD8A71A86FE L_15 = V_1;
		PointGrassRenderer_BuildPoints_Terrain_mE2B10018E53754D361B63EDF70C17C8A1C706CE2(__this, L_14, L_15, NULL);
		return;
	}

IL_008d:
	{
		// else { BuildPoints_Mesh(seed, overwriteNormal); }
		int32_t L_16 = V_0;
		Nullable_1_t9C51B084784B716FFF4ED4575C63CFD8A71A86FE L_17 = V_1;
		PointGrassRenderer_BuildPoints_Mesh_m68283F3A64997B2E73127B91105D2501F74225BB(__this, L_16, L_17, NULL);
		// }
		return;
	}
}
// System.Void MicahW.PointGrass.PointGrassRenderer::BuildPoints_Mesh(System.Int32,System.Nullable`1<UnityEngine.Vector3>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointGrassRenderer_BuildPoints_Mesh_m68283F3A64997B2E73127B91105D2501F74225BB (PointGrassRenderer_tCAB9C932B5AEF5960C04E43451965E31C6A471C2* __this, int32_t ___seed0, Nullable_1_t9C51B084784B716FFF4ED4575C63CFD8A71A86FE ___overwriteNormal1, const RuntimeMethod* method) 
{
	MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F* V_0 = NULL;
	MeshPointU5BU5D_tBCEA150F23C3861E840A87B994115F06D7490DD8* V_1 = NULL;
	{
		// if (!GetMeshData(out MeshData mesh)) { return; }
		bool L_0;
		L_0 = PointGrassRenderer_GetMeshData_mCF6800F541A60360ACDF687BB51CA1DD1B7B173D(__this, (&V_0), NULL);
		if (L_0)
		{
			goto IL_000b;
		}
	}
	{
		// if (!GetMeshData(out MeshData mesh)) { return; }
		return;
	}

IL_000b:
	{
		// if (projectType == ProjectionType.ProjectMesh) { ProjectBaseMesh(ref mesh, projectMask, transform); }
		int32_t L_1 = __this->___projectType_38;
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_0027;
		}
	}
	{
		// if (projectType == ProjectionType.ProjectMesh) { ProjectBaseMesh(ref mesh, projectMask, transform); }
		LayerMask_t97CB6BDADEDC3D6423C7BCFEA7F86DA2EC6241DB L_2 = __this->___projectMask_39;
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_3;
		L_3 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(__this, NULL);
		PointGrassCommon_ProjectBaseMesh_m5562159B69B1FD110DEC077043E7DE5D4020F74B((&V_0), L_2, L_3, NULL);
	}

IL_0027:
	{
		// boundingBox = mesh.bounds;
		MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F* L_4 = V_0;
		NullCheck(L_4);
		Bounds_t367E830C64BBF235ED8C3B2F8CF6254FDCAD39C3 L_5 = L_4->___bounds_6;
		__this->___boundingBox_34 = L_5;
		// MeshPoint[] points = DistributePoints(mesh, transform.localScale, pointCount, seed, multiplyByArea, overwriteNormal, true, useDensity, useLength, densityCutoff, lengthMapping);
		MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F* L_6 = V_0;
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_7;
		L_7 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(__this, NULL);
		NullCheck(L_7);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_8;
		L_8 = Transform_get_localScale_m804A002A53A645CDFCD15BB0F37209162720363F(L_7, NULL);
		float L_9 = __this->___pointCount_21;
		int32_t L_10 = ___seed0;
		bool L_11 = __this->___multiplyByArea_22;
		Nullable_1_t9C51B084784B716FFF4ED4575C63CFD8A71A86FE L_12 = ___overwriteNormal1;
		bool L_13 = __this->___useDensity_28;
		bool L_14 = __this->___useLength_30;
		float L_15 = __this->___densityCutoff_29;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_16 = __this->___lengthMapping_31;
		MeshPointU5BU5D_tBCEA150F23C3861E840A87B994115F06D7490DD8* L_17;
		L_17 = DistributePointsAlongMesh_DistributePoints_m697A346959B6378FF0BF569C2DD2C8189E81AAF4(L_6, L_8, L_9, L_10, L_11, L_12, (bool)1, L_13, L_14, L_15, L_16, NULL);
		V_1 = L_17;
		// if (points != null && points.Length > 0) { CreateBuffers(points); }
		MeshPointU5BU5D_tBCEA150F23C3861E840A87B994115F06D7490DD8* L_18 = V_1;
		if (!L_18)
		{
			goto IL_007a;
		}
	}
	{
		MeshPointU5BU5D_tBCEA150F23C3861E840A87B994115F06D7490DD8* L_19 = V_1;
		NullCheck(L_19);
		if (!(((RuntimeArray*)L_19)->max_length))
		{
			goto IL_007a;
		}
	}
	{
		// if (points != null && points.Length > 0) { CreateBuffers(points); }
		MeshPointU5BU5D_tBCEA150F23C3861E840A87B994115F06D7490DD8* L_20 = V_1;
		PointGrassRenderer_CreateBuffers_mFC53982D8596D1E77FE4711A0D9FEC742A925FAB(__this, L_20, NULL);
	}

IL_007a:
	{
		// }
		return;
	}
}
// System.Void MicahW.PointGrass.PointGrassRenderer::BuildPoints_Terrain(System.Int32,System.Nullable`1<UnityEngine.Vector3>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointGrassRenderer_BuildPoints_Terrain_mE2B10018E53754D361B63EDF70C17C8A1C706CE2 (PointGrassRenderer_tCAB9C932B5AEF5960C04E43451965E31C6A471C2* __this, int32_t ___seed0, Nullable_1_t9C51B084784B716FFF4ED4575C63CFD8A71A86FE ___overwriteNormal1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_AddRange_m17F664FC05537795FFF2C58E6DF293488A469EA4_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_AddRange_m77FEC24D7D0DA5225D0ECC76F11E793A228F30CD_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_m03B29C69F9974D985EFCE1B1D91AC7AC4F578723_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_m28265F4F8593F134812477A243BBEF0513421160_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_m930CA3F1A016B820967E8D53B5D8B2EEB9F10DAC_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_ToArray_m18ACE74090BCEB952354342ADF397C950A01321A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_ToArray_mACDC8BDC8BB93E18220068239BC4D9F8065DAF41_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_ToArray_mAD5B129F08D52D3E82AE1915C038DBD06D0AADF3_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_m6495AB7C6BDDC4D06EE93E691A9DC2C6FC791911_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_m776C5CB754FECF0324C22F3651FA444D287BFBDC_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_m9FDBD09FEFDB8C3F98E74C51E846806B1523EE85_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_m7CF0642F7F16E3F98C217C5CDEAB8C0554FEF85E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t7D73463EA17F4D29A37E8110AB60DAD5C80EBF65_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t7DA451437393B82A39CEE1076115D7FD8B970B3B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_tAFCDB9CCBAF11013AA7B0EC7BB51B10127AB467C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t7DA451437393B82A39CEE1076115D7FD8B970B3B* V_0 = NULL;
	List_1_tAFCDB9CCBAF11013AA7B0EC7BB51B10127AB467C* V_1 = NULL;
	List_1_t7D73463EA17F4D29A37E8110AB60DAD5C80EBF65* V_2 = NULL;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_3;
	memset((&V_3), 0, sizeof(V_3));
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F* V_6 = NULL;
	int32_t V_7 = 0;
	MeshPointU5BU5D_tBCEA150F23C3861E840A87B994115F06D7490DD8* V_8 = NULL;
	Bounds_t367E830C64BBF235ED8C3B2F8CF6254FDCAD39C3 V_9;
	memset((&V_9), 0, sizeof(V_9));
	ComputeBufferU5BU5D_t7832804740B13E96807A836AD90ADF1477D7FE27* V_10 = NULL;
	MaterialPropertyBlockU5BU5D_t6911DDC471FC23F2AF58FF0140ED595529EC5F5B* V_11 = NULL;
	ComputeBuffer_t51EADA9015EBCC1B982C5584E9AB2734415A8233* V_12 = NULL;
	MaterialPropertyBlock_t2308669579033A857EFE6E4831909F638B27411D* V_13 = NULL;
	int32_t V_14 = 0;
	{
		// List<ComputeBuffer> bufferList = new List<ComputeBuffer>();
		List_1_t7DA451437393B82A39CEE1076115D7FD8B970B3B* L_0 = (List_1_t7DA451437393B82A39CEE1076115D7FD8B970B3B*)il2cpp_codegen_object_new(List_1_t7DA451437393B82A39CEE1076115D7FD8B970B3B_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		List_1__ctor_m776C5CB754FECF0324C22F3651FA444D287BFBDC(L_0, List_1__ctor_m776C5CB754FECF0324C22F3651FA444D287BFBDC_RuntimeMethod_var);
		V_0 = L_0;
		// List<MaterialPropertyBlock> blockList = new List<MaterialPropertyBlock>();
		List_1_tAFCDB9CCBAF11013AA7B0EC7BB51B10127AB467C* L_1 = (List_1_tAFCDB9CCBAF11013AA7B0EC7BB51B10127AB467C*)il2cpp_codegen_object_new(List_1_tAFCDB9CCBAF11013AA7B0EC7BB51B10127AB467C_il2cpp_TypeInfo_var);
		NullCheck(L_1);
		List_1__ctor_m9FDBD09FEFDB8C3F98E74C51E846806B1523EE85(L_1, List_1__ctor_m9FDBD09FEFDB8C3F98E74C51E846806B1523EE85_RuntimeMethod_var);
		V_1 = L_1;
		// List<Bounds> boundsList = new List<Bounds>();
		List_1_t7D73463EA17F4D29A37E8110AB60DAD5C80EBF65* L_2 = (List_1_t7D73463EA17F4D29A37E8110AB60DAD5C80EBF65*)il2cpp_codegen_object_new(List_1_t7D73463EA17F4D29A37E8110AB60DAD5C80EBF65_il2cpp_TypeInfo_var);
		NullCheck(L_2);
		List_1__ctor_m6495AB7C6BDDC4D06EE93E691A9DC2C6FC791911(L_2, List_1__ctor_m6495AB7C6BDDC4D06EE93E691A9DC2C6FC791911_RuntimeMethod_var);
		V_2 = L_2;
		// Vector3 chunkSize = terrain.size;
		TerrainData_t615A68EAC648066681875D47FC641496D12F2E24* L_3 = __this->___terrain_6;
		NullCheck(L_3);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_4;
		L_4 = TerrainData_get_size_mCD3977F344B9DEBFF61DD537D03FEB9473838DA5(L_3, NULL);
		V_3 = L_4;
		// chunkSize.x /= chunkCount.x;
		float* L_5 = (&(&V_3)->___x_2);
		float* L_6 = L_5;
		float L_7 = *((float*)L_6);
		Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A* L_8 = (&__this->___chunkCount_8);
		int32_t L_9;
		L_9 = Vector2Int_get_x_mA2CACB1B6E6B5AD0CCC32B2CD2EDCE3ECEB50576_inline(L_8, NULL);
		*((float*)L_6) = (float)((float)(L_7/((float)L_9)));
		// chunkSize.z /= chunkCount.y;
		float* L_10 = (&(&V_3)->___z_4);
		float* L_11 = L_10;
		float L_12 = *((float*)L_11);
		Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A* L_13 = (&__this->___chunkCount_8);
		int32_t L_14;
		L_14 = Vector2Int_get_y_m48454163ECF0B463FB5A16A0C4FC4B14DB0768B3_inline(L_13, NULL);
		*((float*)L_11) = (float)((float)(L_12/((float)L_14)));
		// CacheTerrainData(terrain, terrainLayers);
		TerrainData_t615A68EAC648066681875D47FC641496D12F2E24* L_15 = __this->___terrain_6;
		TerrainLayerU5BU5D_t259E391D6115F121FCD284E79F62012D70956EB0* L_16 = __this->___terrainLayers_7;
		PointGrassCommon_CacheTerrainData_mE54FEFBA4451B4B69F63A2617DA43149225A3EAE(L_15, L_16, NULL);
		// for (int x = 0; x < chunkCount.x; x++) {
		V_4 = 0;
		goto IL_018f;
	}

IL_0065:
	{
		// for (int y = 0; y < chunkCount.y; y++) {
		V_5 = 0;
		goto IL_0177;
	}

IL_006d:
	{
		// bool validMesh = GetTerrainMeshData(out MeshData data, new Vector2Int(x, y));
		int32_t L_17 = V_4;
		int32_t L_18 = V_5;
		Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A L_19;
		memset((&L_19), 0, sizeof(L_19));
		Vector2Int__ctor_mC20D1312133EB8CB63EC11067088B043660F11CE_inline((&L_19), L_17, L_18, /*hidden argument*/NULL);
		bool L_20;
		L_20 = PointGrassRenderer_GetTerrainMeshData_mE1FA1BE05C1FCB35601AD79D3C1B0219ECA2BEF8(__this, (&V_6), L_19, NULL);
		// if (!validMesh) { continue; }
		if (!L_20)
		{
			goto IL_0171;
		}
	}
	{
		// int newSeed = seed + x + y * chunkCount.x;
		int32_t L_21 = ___seed0;
		int32_t L_22 = V_4;
		int32_t L_23 = V_5;
		Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A* L_24 = (&__this->___chunkCount_8);
		int32_t L_25;
		L_25 = Vector2Int_get_x_mA2CACB1B6E6B5AD0CCC32B2CD2EDCE3ECEB50576_inline(L_24, NULL);
		V_7 = ((int32_t)il2cpp_codegen_add(((int32_t)il2cpp_codegen_add(L_21, L_22)), ((int32_t)il2cpp_codegen_multiply(L_23, L_25))));
		// MeshPoint[] points = DistributePoints(data, transform.localScale, pointCount, newSeed, multiplyByArea, overwriteNormal, true, useDensity, useLength, -1f, lengthMapping);
		MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F* L_26 = V_6;
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_27;
		L_27 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(__this, NULL);
		NullCheck(L_27);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_28;
		L_28 = Transform_get_localScale_m804A002A53A645CDFCD15BB0F37209162720363F(L_27, NULL);
		float L_29 = __this->___pointCount_21;
		int32_t L_30 = V_7;
		bool L_31 = __this->___multiplyByArea_22;
		Nullable_1_t9C51B084784B716FFF4ED4575C63CFD8A71A86FE L_32 = ___overwriteNormal1;
		bool L_33 = __this->___useDensity_28;
		bool L_34 = __this->___useLength_30;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_35 = __this->___lengthMapping_31;
		MeshPointU5BU5D_tBCEA150F23C3861E840A87B994115F06D7490DD8* L_36;
		L_36 = DistributePointsAlongMesh_DistributePoints_m697A346959B6378FF0BF569C2DD2C8189E81AAF4(L_26, L_28, L_29, L_30, L_31, L_32, (bool)1, L_33, L_34, (-1.0f), L_35, NULL);
		V_8 = L_36;
		// if (points == null || points.Length == 0) { continue; }
		MeshPointU5BU5D_tBCEA150F23C3861E840A87B994115F06D7490DD8* L_37 = V_8;
		if (!L_37)
		{
			goto IL_0171;
		}
	}
	{
		MeshPointU5BU5D_tBCEA150F23C3861E840A87B994115F06D7490DD8* L_38 = V_8;
		NullCheck(L_38);
		if (!(((RuntimeArray*)L_38)->max_length))
		{
			goto IL_0171;
		}
	}
	{
		// if (UsingMultipleMeshes) {
		bool L_39;
		L_39 = PointGrassRenderer_get_UsingMultipleMeshes_m61A92AF3BC219534CD301983C34CECB07E6A3E59(__this, NULL);
		if (!L_39)
		{
			goto IL_010c;
		}
	}
	{
		// CreateBuffersFromPoints_Multi(points, out ComputeBuffer[] buffers, out MaterialPropertyBlock[] blocks);
		MeshPointU5BU5D_tBCEA150F23C3861E840A87B994115F06D7490DD8* L_40 = V_8;
		PointGrassRenderer_CreateBuffersFromPoints_Multi_m0FB6D3F6742650ACA37DF48E03DE221ABC7EF599(__this, L_40, (&V_10), (&V_11), NULL);
		// if (buffers == null) { continue; }
		ComputeBufferU5BU5D_t7832804740B13E96807A836AD90ADF1477D7FE27* L_41 = V_10;
		if (!L_41)
		{
			goto IL_0171;
		}
	}
	{
		// bufferList.AddRange(buffers);
		List_1_t7DA451437393B82A39CEE1076115D7FD8B970B3B* L_42 = V_0;
		ComputeBufferU5BU5D_t7832804740B13E96807A836AD90ADF1477D7FE27* L_43 = V_10;
		NullCheck(L_42);
		List_1_AddRange_m17F664FC05537795FFF2C58E6DF293488A469EA4(L_42, (RuntimeObject*)L_43, List_1_AddRange_m17F664FC05537795FFF2C58E6DF293488A469EA4_RuntimeMethod_var);
		// blockList.AddRange(blocks);
		List_1_tAFCDB9CCBAF11013AA7B0EC7BB51B10127AB467C* L_44 = V_1;
		MaterialPropertyBlockU5BU5D_t6911DDC471FC23F2AF58FF0140ED595529EC5F5B* L_45 = V_11;
		NullCheck(L_44);
		List_1_AddRange_m77FEC24D7D0DA5225D0ECC76F11E793A228F30CD(L_44, (RuntimeObject*)L_45, List_1_AddRange_m77FEC24D7D0DA5225D0ECC76F11E793A228F30CD_RuntimeMethod_var);
		goto IL_0128;
	}

IL_010c:
	{
		// CreateBufferFromPoints(points, out ComputeBuffer buffer, out MaterialPropertyBlock block);
		MeshPointU5BU5D_tBCEA150F23C3861E840A87B994115F06D7490DD8* L_46 = V_8;
		PointGrassRenderer_CreateBufferFromPoints_mFC5340A5C557D32EAEDECCC71E3D48114E5DCF07(__this, L_46, (&V_12), (&V_13), NULL);
		// bufferList.Add(buffer);
		List_1_t7DA451437393B82A39CEE1076115D7FD8B970B3B* L_47 = V_0;
		ComputeBuffer_t51EADA9015EBCC1B982C5584E9AB2734415A8233* L_48 = V_12;
		NullCheck(L_47);
		List_1_Add_m28265F4F8593F134812477A243BBEF0513421160_inline(L_47, L_48, List_1_Add_m28265F4F8593F134812477A243BBEF0513421160_RuntimeMethod_var);
		// blockList.Add(block);
		List_1_tAFCDB9CCBAF11013AA7B0EC7BB51B10127AB467C* L_49 = V_1;
		MaterialPropertyBlock_t2308669579033A857EFE6E4831909F638B27411D* L_50 = V_13;
		NullCheck(L_49);
		List_1_Add_m03B29C69F9974D985EFCE1B1D91AC7AC4F578723_inline(L_49, L_50, List_1_Add_m03B29C69F9974D985EFCE1B1D91AC7AC4F578723_RuntimeMethod_var);
	}

IL_0128:
	{
		// Bounds bounds = new Bounds(points[0].position, Vector3.zero);
		MeshPointU5BU5D_tBCEA150F23C3861E840A87B994115F06D7490DD8* L_51 = V_8;
		NullCheck(L_51);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_52 = ((L_51)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))->___position_0;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_53;
		L_53 = Vector3_get_zero_m0C1249C3F25B1C70EAD3CC8B31259975A457AE39_inline(NULL);
		Bounds__ctor_mAF7B238B9FBF90C495E5D7951760085A93119C5A((&V_9), L_52, L_53, NULL);
		// for (int i = 1; i < points.Length; i++) { bounds.Encapsulate(points[i].position); }
		V_14 = 1;
		goto IL_0161;
	}

IL_0146:
	{
		// for (int i = 1; i < points.Length; i++) { bounds.Encapsulate(points[i].position); }
		MeshPointU5BU5D_tBCEA150F23C3861E840A87B994115F06D7490DD8* L_54 = V_8;
		int32_t L_55 = V_14;
		NullCheck(L_54);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_56 = ((L_54)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_55)))->___position_0;
		Bounds_Encapsulate_m1FCA57C58536ADB67B85A703470C6F5BFB837C2F((&V_9), L_56, NULL);
		// for (int i = 1; i < points.Length; i++) { bounds.Encapsulate(points[i].position); }
		int32_t L_57 = V_14;
		V_14 = ((int32_t)il2cpp_codegen_add(L_57, 1));
	}

IL_0161:
	{
		// for (int i = 1; i < points.Length; i++) { bounds.Encapsulate(points[i].position); }
		int32_t L_58 = V_14;
		MeshPointU5BU5D_tBCEA150F23C3861E840A87B994115F06D7490DD8* L_59 = V_8;
		NullCheck(L_59);
		if ((((int32_t)L_58) < ((int32_t)((int32_t)(((RuntimeArray*)L_59)->max_length)))))
		{
			goto IL_0146;
		}
	}
	{
		// boundsList.Add(bounds);
		List_1_t7D73463EA17F4D29A37E8110AB60DAD5C80EBF65* L_60 = V_2;
		Bounds_t367E830C64BBF235ED8C3B2F8CF6254FDCAD39C3 L_61 = V_9;
		NullCheck(L_60);
		List_1_Add_m930CA3F1A016B820967E8D53B5D8B2EEB9F10DAC_inline(L_60, L_61, List_1_Add_m930CA3F1A016B820967E8D53B5D8B2EEB9F10DAC_RuntimeMethod_var);
	}

IL_0171:
	{
		// for (int y = 0; y < chunkCount.y; y++) {
		int32_t L_62 = V_5;
		V_5 = ((int32_t)il2cpp_codegen_add(L_62, 1));
	}

IL_0177:
	{
		// for (int y = 0; y < chunkCount.y; y++) {
		int32_t L_63 = V_5;
		Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A* L_64 = (&__this->___chunkCount_8);
		int32_t L_65;
		L_65 = Vector2Int_get_y_m48454163ECF0B463FB5A16A0C4FC4B14DB0768B3_inline(L_64, NULL);
		if ((((int32_t)L_63) < ((int32_t)L_65)))
		{
			goto IL_006d;
		}
	}
	{
		// for (int x = 0; x < chunkCount.x; x++) {
		int32_t L_66 = V_4;
		V_4 = ((int32_t)il2cpp_codegen_add(L_66, 1));
	}

IL_018f:
	{
		// for (int x = 0; x < chunkCount.x; x++) {
		int32_t L_67 = V_4;
		Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A* L_68 = (&__this->___chunkCount_8);
		int32_t L_69;
		L_69 = Vector2Int_get_x_mA2CACB1B6E6B5AD0CCC32B2CD2EDCE3ECEB50576_inline(L_68, NULL);
		if ((((int32_t)L_67) < ((int32_t)L_69)))
		{
			goto IL_0065;
		}
	}
	{
		// if (bufferList.Count > 0) {
		List_1_t7DA451437393B82A39CEE1076115D7FD8B970B3B* L_70 = V_0;
		NullCheck(L_70);
		int32_t L_71;
		L_71 = List_1_get_Count_m7CF0642F7F16E3F98C217C5CDEAB8C0554FEF85E_inline(L_70, List_1_get_Count_m7CF0642F7F16E3F98C217C5CDEAB8C0554FEF85E_RuntimeMethod_var);
		if ((((int32_t)L_71) <= ((int32_t)0)))
		{
			goto IL_01ce;
		}
	}
	{
		// pointBuffers = bufferList.ToArray();
		List_1_t7DA451437393B82A39CEE1076115D7FD8B970B3B* L_72 = V_0;
		NullCheck(L_72);
		ComputeBufferU5BU5D_t7832804740B13E96807A836AD90ADF1477D7FE27* L_73;
		L_73 = List_1_ToArray_mACDC8BDC8BB93E18220068239BC4D9F8065DAF41(L_72, List_1_ToArray_mACDC8BDC8BB93E18220068239BC4D9F8065DAF41_RuntimeMethod_var);
		__this->___pointBuffers_35 = L_73;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___pointBuffers_35), (void*)L_73);
		// materialBlocks = blockList.ToArray();
		List_1_tAFCDB9CCBAF11013AA7B0EC7BB51B10127AB467C* L_74 = V_1;
		NullCheck(L_74);
		MaterialPropertyBlockU5BU5D_t6911DDC471FC23F2AF58FF0140ED595529EC5F5B* L_75;
		L_75 = List_1_ToArray_mAD5B129F08D52D3E82AE1915C038DBD06D0AADF3(L_74, List_1_ToArray_mAD5B129F08D52D3E82AE1915C038DBD06D0AADF3_RuntimeMethod_var);
		__this->___materialBlocks_36 = L_75;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___materialBlocks_36), (void*)L_75);
		// boundingBoxes = boundsList.ToArray();
		List_1_t7D73463EA17F4D29A37E8110AB60DAD5C80EBF65* L_76 = V_2;
		NullCheck(L_76);
		BoundsU5BU5D_t45563ED55B62FA0536E8117454C56C0CFA5B25F5* L_77;
		L_77 = List_1_ToArray_m18ACE74090BCEB952354342ADF397C950A01321A(L_76, List_1_ToArray_m18ACE74090BCEB952354342ADF397C950A01321A_RuntimeMethod_var);
		__this->___boundingBoxes_37 = L_77;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___boundingBoxes_37), (void*)L_77);
	}

IL_01ce:
	{
		// }
		return;
	}
}
// System.Boolean MicahW.PointGrass.PointGrassRenderer::GetMeshData(MicahW.PointGrass.PointGrassCommon/MeshData&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PointGrassRenderer_GetMeshData_mCF6800F541A60360ACDF687BB51CA1DD1B7B173D (PointGrassRenderer_tCAB9C932B5AEF5960C04E43451965E31C6A471C2* __this, MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F** ___meshData0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisMeshFilter_t6D1CE2473A1E45AC73013400585A1163BF66B2F5_mB82F66059DFB5715DD85BDED1D90BC03A6C9E623_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* V_4 = NULL;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	{
		// meshData = MeshData.Empty;
		MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F** L_0 = ___meshData0;
		il2cpp_codegen_runtime_class_init_inline(MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F_il2cpp_TypeInfo_var);
		MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F* L_1 = ((MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F_StaticFields*)il2cpp_codegen_static_fields_for(MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F_il2cpp_TypeInfo_var))->___Empty_7;
		*((RuntimeObject**)L_0) = (RuntimeObject*)L_1;
		Il2CppCodeGenWriteBarrier((void**)(RuntimeObject**)L_0, (void*)(RuntimeObject*)L_1);
		// switch (distSource) {
		int32_t L_2 = __this->___distSource_4;
		V_1 = L_2;
		int32_t L_3 = V_1;
		switch (L_3)
		{
			case 0:
			{
				goto IL_0029;
			}
			case 1:
			{
				goto IL_011a;
			}
			case 2:
			{
				goto IL_024b;
			}
			case 3:
			{
				goto IL_0269;
			}
		}
	}
	{
		goto IL_0299;
	}

IL_0029:
	{
		// if (baseMesh == null) { goto case DistributionSource.MeshFilter; }
		Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* L_4 = __this->___baseMesh_5;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_5;
		L_5 = Object_op_Equality_mB6120F782D83091EF56A198FCEBCF066DB4A9605(L_4, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (L_5)
		{
			goto IL_011a;
		}
	}
	{
		// Vector2[] meshAttributes = new Vector2[baseMesh.vertexCount];
		Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* L_6 = __this->___baseMesh_5;
		NullCheck(L_6);
		int32_t L_7;
		L_7 = Mesh_get_vertexCount_mB7BE0340AAF272933068D830C8E711FC8978E12C(L_6, NULL);
		Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* L_8 = (Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA*)(Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA*)SZArrayNew(Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA_il2cpp_TypeInfo_var, (uint32_t)L_7);
		V_0 = L_8;
		// if (baseMesh.colors != null && baseMesh.colors.Length == baseMesh.vertexCount && (useDensity || useLength)) {
		Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* L_9 = __this->___baseMesh_5;
		NullCheck(L_9);
		ColorU5BU5D_t612261CF293F6FFC3D80AB52259FF0DC2B2CC389* L_10;
		L_10 = Mesh_get_colors_m3A38944EBA064B0E55A24C95C3706193F45B313D(L_9, NULL);
		if (!L_10)
		{
			goto IL_00ca;
		}
	}
	{
		Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* L_11 = __this->___baseMesh_5;
		NullCheck(L_11);
		ColorU5BU5D_t612261CF293F6FFC3D80AB52259FF0DC2B2CC389* L_12;
		L_12 = Mesh_get_colors_m3A38944EBA064B0E55A24C95C3706193F45B313D(L_11, NULL);
		NullCheck(L_12);
		Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* L_13 = __this->___baseMesh_5;
		NullCheck(L_13);
		int32_t L_14;
		L_14 = Mesh_get_vertexCount_mB7BE0340AAF272933068D830C8E711FC8978E12C(L_13, NULL);
		if ((!(((uint32_t)((int32_t)(((RuntimeArray*)L_12)->max_length))) == ((uint32_t)L_14))))
		{
			goto IL_00ca;
		}
	}
	{
		bool L_15 = __this->___useDensity_28;
		if (L_15)
		{
			goto IL_0082;
		}
	}
	{
		bool L_16 = __this->___useLength_30;
		if (!L_16)
		{
			goto IL_00ca;
		}
	}

IL_0082:
	{
		// for (int i = 0; i < meshAttributes.Length; i++) { meshAttributes[i] = new Vector2(baseMesh.colors[i].r, baseMesh.colors[i].g); }
		V_2 = 0;
		goto IL_00c2;
	}

IL_0086:
	{
		// for (int i = 0; i < meshAttributes.Length; i++) { meshAttributes[i] = new Vector2(baseMesh.colors[i].r, baseMesh.colors[i].g); }
		Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* L_17 = V_0;
		int32_t L_18 = V_2;
		Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* L_19 = __this->___baseMesh_5;
		NullCheck(L_19);
		ColorU5BU5D_t612261CF293F6FFC3D80AB52259FF0DC2B2CC389* L_20;
		L_20 = Mesh_get_colors_m3A38944EBA064B0E55A24C95C3706193F45B313D(L_19, NULL);
		int32_t L_21 = V_2;
		NullCheck(L_20);
		float L_22 = ((L_20)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_21)))->___r_0;
		Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* L_23 = __this->___baseMesh_5;
		NullCheck(L_23);
		ColorU5BU5D_t612261CF293F6FFC3D80AB52259FF0DC2B2CC389* L_24;
		L_24 = Mesh_get_colors_m3A38944EBA064B0E55A24C95C3706193F45B313D(L_23, NULL);
		int32_t L_25 = V_2;
		NullCheck(L_24);
		float L_26 = ((L_24)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_25)))->___g_1;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_27;
		memset((&L_27), 0, sizeof(L_27));
		Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline((&L_27), L_22, L_26, /*hidden argument*/NULL);
		NullCheck(L_17);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(L_18), (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7)L_27);
		// for (int i = 0; i < meshAttributes.Length; i++) { meshAttributes[i] = new Vector2(baseMesh.colors[i].r, baseMesh.colors[i].g); }
		int32_t L_28 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add(L_28, 1));
	}

IL_00c2:
	{
		// for (int i = 0; i < meshAttributes.Length; i++) { meshAttributes[i] = new Vector2(baseMesh.colors[i].r, baseMesh.colors[i].g); }
		int32_t L_29 = V_2;
		Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* L_30 = V_0;
		NullCheck(L_30);
		if ((((int32_t)L_29) < ((int32_t)((int32_t)(((RuntimeArray*)L_30)->max_length)))))
		{
			goto IL_0086;
		}
	}
	{
		goto IL_00e4;
	}

IL_00ca:
	{
		// else { for (int i = 0; i < meshAttributes.Length; i++) { meshAttributes[i] = Vector2.one; } }
		V_3 = 0;
		goto IL_00de;
	}

IL_00ce:
	{
		// else { for (int i = 0; i < meshAttributes.Length; i++) { meshAttributes[i] = Vector2.one; } }
		Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* L_31 = V_0;
		int32_t L_32 = V_3;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_33;
		L_33 = Vector2_get_one_m9097EB8DC23C26118A591AF16702796C3EF51DFB_inline(NULL);
		NullCheck(L_31);
		(L_31)->SetAt(static_cast<il2cpp_array_size_t>(L_32), (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7)L_33);
		// else { for (int i = 0; i < meshAttributes.Length; i++) { meshAttributes[i] = Vector2.one; } }
		int32_t L_34 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add(L_34, 1));
	}

IL_00de:
	{
		// else { for (int i = 0; i < meshAttributes.Length; i++) { meshAttributes[i] = Vector2.one; } }
		int32_t L_35 = V_3;
		Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* L_36 = V_0;
		NullCheck(L_36);
		if ((((int32_t)L_35) < ((int32_t)((int32_t)(((RuntimeArray*)L_36)->max_length)))))
		{
			goto IL_00ce;
		}
	}

IL_00e4:
	{
		// meshData = new MeshData(baseMesh.vertices, baseMesh.normals, baseMesh.uv, baseMesh.triangles, meshAttributes);
		MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F** L_37 = ___meshData0;
		Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* L_38 = __this->___baseMesh_5;
		NullCheck(L_38);
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_39;
		L_39 = Mesh_get_vertices_mA3577F1B08EDDD54E26AEB3F8FFE4EC247D2ABB9(L_38, NULL);
		Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* L_40 = __this->___baseMesh_5;
		NullCheck(L_40);
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_41;
		L_41 = Mesh_get_normals_m2B6B159B799E6E235EA651FCAB2E18EE5B18ED62(L_40, NULL);
		Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* L_42 = __this->___baseMesh_5;
		NullCheck(L_42);
		Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* L_43;
		L_43 = Mesh_get_uv_mA47805C48AB3493FF3727922C43E77880E73519F(L_42, NULL);
		Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* L_44 = __this->___baseMesh_5;
		NullCheck(L_44);
		Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_45;
		L_45 = Mesh_get_triangles_m33E39B4A383CC613C760FA7E297AC417A433F24B(L_44, NULL);
		Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* L_46 = V_0;
		MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F* L_47 = (MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F*)il2cpp_codegen_object_new(MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F_il2cpp_TypeInfo_var);
		NullCheck(L_47);
		MeshData__ctor_m4DDE83D4B889F4A8E8CB34522845BB26A776D752(L_47, L_39, L_41, L_43, L_45, L_46, NULL);
		*((RuntimeObject**)L_37) = (RuntimeObject*)L_47;
		Il2CppCodeGenWriteBarrier((void**)(RuntimeObject**)L_37, (void*)(RuntimeObject*)L_47);
		// return true;
		return (bool)1;
	}

IL_011a:
	{
		// filter = GetComponent<MeshFilter>();
		MeshFilter_t6D1CE2473A1E45AC73013400585A1163BF66B2F5* L_48;
		L_48 = Component_GetComponent_TisMeshFilter_t6D1CE2473A1E45AC73013400585A1163BF66B2F5_mB82F66059DFB5715DD85BDED1D90BC03A6C9E623(__this, Component_GetComponent_TisMeshFilter_t6D1CE2473A1E45AC73013400585A1163BF66B2F5_mB82F66059DFB5715DD85BDED1D90BC03A6C9E623_RuntimeMethod_var);
		__this->___filter_10 = L_48;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___filter_10), (void*)L_48);
		// if (filter != null) {
		MeshFilter_t6D1CE2473A1E45AC73013400585A1163BF66B2F5* L_49 = __this->___filter_10;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_50;
		L_50 = Object_op_Inequality_mD0BE578448EAA61948F25C32F8DD55AB1F778602(L_49, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_50)
		{
			goto IL_0299;
		}
	}
	{
		// baseMesh = filter.sharedMesh;
		MeshFilter_t6D1CE2473A1E45AC73013400585A1163BF66B2F5* L_51 = __this->___filter_10;
		NullCheck(L_51);
		Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* L_52;
		L_52 = MeshFilter_get_sharedMesh_mE4ED3E7E31C1DE5097E4980DA996E620F7D7CB8C(L_51, NULL);
		__this->___baseMesh_5 = L_52;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___baseMesh_5), (void*)L_52);
		// if (baseMesh != null) {
		Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* L_53 = __this->___baseMesh_5;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_54;
		L_54 = Object_op_Inequality_mD0BE578448EAA61948F25C32F8DD55AB1F778602(L_53, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_54)
		{
			goto IL_0299;
		}
	}
	{
		// Vector2[] filterAttributes = new Vector2[baseMesh.vertexCount];
		Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* L_55 = __this->___baseMesh_5;
		NullCheck(L_55);
		int32_t L_56;
		L_56 = Mesh_get_vertexCount_mB7BE0340AAF272933068D830C8E711FC8978E12C(L_55, NULL);
		Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* L_57 = (Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA*)(Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA*)SZArrayNew(Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA_il2cpp_TypeInfo_var, (uint32_t)L_56);
		V_4 = L_57;
		// if (baseMesh.colors != null && baseMesh.colors.Length == baseMesh.vertexCount && (useDensity || useLength)) {
		Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* L_58 = __this->___baseMesh_5;
		NullCheck(L_58);
		ColorU5BU5D_t612261CF293F6FFC3D80AB52259FF0DC2B2CC389* L_59;
		L_59 = Mesh_get_colors_m3A38944EBA064B0E55A24C95C3706193F45B313D(L_58, NULL);
		if (!L_59)
		{
			goto IL_01f3;
		}
	}
	{
		Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* L_60 = __this->___baseMesh_5;
		NullCheck(L_60);
		ColorU5BU5D_t612261CF293F6FFC3D80AB52259FF0DC2B2CC389* L_61;
		L_61 = Mesh_get_colors_m3A38944EBA064B0E55A24C95C3706193F45B313D(L_60, NULL);
		NullCheck(L_61);
		Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* L_62 = __this->___baseMesh_5;
		NullCheck(L_62);
		int32_t L_63;
		L_63 = Mesh_get_vertexCount_mB7BE0340AAF272933068D830C8E711FC8978E12C(L_62, NULL);
		if ((!(((uint32_t)((int32_t)(((RuntimeArray*)L_61)->max_length))) == ((uint32_t)L_63))))
		{
			goto IL_01f3;
		}
	}
	{
		bool L_64 = __this->___useDensity_28;
		if (L_64)
		{
			goto IL_01a2;
		}
	}
	{
		bool L_65 = __this->___useLength_30;
		if (!L_65)
		{
			goto IL_01f3;
		}
	}

IL_01a2:
	{
		// for (int i = 0; i < filterAttributes.Length; i++) { filterAttributes[i] = new Vector2(baseMesh.colors[i].r, baseMesh.colors[i].g); }
		V_5 = 0;
		goto IL_01e9;
	}

IL_01a7:
	{
		// for (int i = 0; i < filterAttributes.Length; i++) { filterAttributes[i] = new Vector2(baseMesh.colors[i].r, baseMesh.colors[i].g); }
		Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* L_66 = V_4;
		int32_t L_67 = V_5;
		Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* L_68 = __this->___baseMesh_5;
		NullCheck(L_68);
		ColorU5BU5D_t612261CF293F6FFC3D80AB52259FF0DC2B2CC389* L_69;
		L_69 = Mesh_get_colors_m3A38944EBA064B0E55A24C95C3706193F45B313D(L_68, NULL);
		int32_t L_70 = V_5;
		NullCheck(L_69);
		float L_71 = ((L_69)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_70)))->___r_0;
		Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* L_72 = __this->___baseMesh_5;
		NullCheck(L_72);
		ColorU5BU5D_t612261CF293F6FFC3D80AB52259FF0DC2B2CC389* L_73;
		L_73 = Mesh_get_colors_m3A38944EBA064B0E55A24C95C3706193F45B313D(L_72, NULL);
		int32_t L_74 = V_5;
		NullCheck(L_73);
		float L_75 = ((L_73)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_74)))->___g_1;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_76;
		memset((&L_76), 0, sizeof(L_76));
		Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline((&L_76), L_71, L_75, /*hidden argument*/NULL);
		NullCheck(L_66);
		(L_66)->SetAt(static_cast<il2cpp_array_size_t>(L_67), (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7)L_76);
		// for (int i = 0; i < filterAttributes.Length; i++) { filterAttributes[i] = new Vector2(baseMesh.colors[i].r, baseMesh.colors[i].g); }
		int32_t L_77 = V_5;
		V_5 = ((int32_t)il2cpp_codegen_add(L_77, 1));
	}

IL_01e9:
	{
		// for (int i = 0; i < filterAttributes.Length; i++) { filterAttributes[i] = new Vector2(baseMesh.colors[i].r, baseMesh.colors[i].g); }
		int32_t L_78 = V_5;
		Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* L_79 = V_4;
		NullCheck(L_79);
		if ((((int32_t)L_78) < ((int32_t)((int32_t)(((RuntimeArray*)L_79)->max_length)))))
		{
			goto IL_01a7;
		}
	}
	{
		goto IL_0214;
	}

IL_01f3:
	{
		// else { for (int i = 0; i < filterAttributes.Length; i++) { filterAttributes[i] = Vector2.one; } }
		V_6 = 0;
		goto IL_020c;
	}

IL_01f8:
	{
		// else { for (int i = 0; i < filterAttributes.Length; i++) { filterAttributes[i] = Vector2.one; } }
		Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* L_80 = V_4;
		int32_t L_81 = V_6;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_82;
		L_82 = Vector2_get_one_m9097EB8DC23C26118A591AF16702796C3EF51DFB_inline(NULL);
		NullCheck(L_80);
		(L_80)->SetAt(static_cast<il2cpp_array_size_t>(L_81), (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7)L_82);
		// else { for (int i = 0; i < filterAttributes.Length; i++) { filterAttributes[i] = Vector2.one; } }
		int32_t L_83 = V_6;
		V_6 = ((int32_t)il2cpp_codegen_add(L_83, 1));
	}

IL_020c:
	{
		// else { for (int i = 0; i < filterAttributes.Length; i++) { filterAttributes[i] = Vector2.one; } }
		int32_t L_84 = V_6;
		Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* L_85 = V_4;
		NullCheck(L_85);
		if ((((int32_t)L_84) < ((int32_t)((int32_t)(((RuntimeArray*)L_85)->max_length)))))
		{
			goto IL_01f8;
		}
	}

IL_0214:
	{
		// meshData = new MeshData(baseMesh.vertices, baseMesh.normals, baseMesh.uv, baseMesh.triangles, filterAttributes);
		MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F** L_86 = ___meshData0;
		Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* L_87 = __this->___baseMesh_5;
		NullCheck(L_87);
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_88;
		L_88 = Mesh_get_vertices_mA3577F1B08EDDD54E26AEB3F8FFE4EC247D2ABB9(L_87, NULL);
		Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* L_89 = __this->___baseMesh_5;
		NullCheck(L_89);
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_90;
		L_90 = Mesh_get_normals_m2B6B159B799E6E235EA651FCAB2E18EE5B18ED62(L_89, NULL);
		Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* L_91 = __this->___baseMesh_5;
		NullCheck(L_91);
		Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* L_92;
		L_92 = Mesh_get_uv_mA47805C48AB3493FF3727922C43E77880E73519F(L_91, NULL);
		Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* L_93 = __this->___baseMesh_5;
		NullCheck(L_93);
		Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_94;
		L_94 = Mesh_get_triangles_m33E39B4A383CC613C760FA7E297AC417A433F24B(L_93, NULL);
		Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* L_95 = V_4;
		MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F* L_96 = (MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F*)il2cpp_codegen_object_new(MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F_il2cpp_TypeInfo_var);
		NullCheck(L_96);
		MeshData__ctor_m4DDE83D4B889F4A8E8CB34522845BB26A776D752(L_96, L_88, L_90, L_92, L_94, L_95, NULL);
		*((RuntimeObject**)L_86) = (RuntimeObject*)L_96;
		Il2CppCodeGenWriteBarrier((void**)(RuntimeObject**)L_86, (void*)(RuntimeObject*)L_96);
		// return true;
		return (bool)1;
	}

IL_024b:
	{
		// CacheTerrainData(terrain, terrainLayers);
		TerrainData_t615A68EAC648066681875D47FC641496D12F2E24* L_97 = __this->___terrain_6;
		TerrainLayerU5BU5D_t259E391D6115F121FCD284E79F62012D70956EB0* L_98 = __this->___terrainLayers_7;
		PointGrassCommon_CacheTerrainData_mE54FEFBA4451B4B69F63A2617DA43149225A3EAE(L_97, L_98, NULL);
		// return GetTerrainMeshData(out meshData, Vector2Int.zero);
		MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F** L_99 = ___meshData0;
		Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A L_100;
		L_100 = Vector2Int_get_zero_mF92C338E9CB9434105090E675E04D20A29649553_inline(NULL);
		bool L_101;
		L_101 = PointGrassRenderer_GetTerrainMeshData_mE1FA1BE05C1FCB35601AD79D3C1B0219ECA2BEF8(__this, L_99, L_100, NULL);
		return L_101;
	}

IL_0269:
	{
		// if (sceneFilters != null && sceneFilters.Length > 0) {
		MeshFilterU5BU5D_tCE3B457E6F7ECE5ECEE9E09150642150448685BA* L_102 = __this->___sceneFilters_9;
		if (!L_102)
		{
			goto IL_0299;
		}
	}
	{
		MeshFilterU5BU5D_tCE3B457E6F7ECE5ECEE9E09150642150448685BA* L_103 = __this->___sceneFilters_9;
		NullCheck(L_103);
		if (!(((RuntimeArray*)L_103)->max_length))
		{
			goto IL_0299;
		}
	}
	{
		// meshData = CreateMeshFromFilters(transform, sceneFilters);
		MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F** L_104 = ___meshData0;
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_105;
		L_105 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(__this, NULL);
		MeshFilterU5BU5D_tCE3B457E6F7ECE5ECEE9E09150642150448685BA* L_106 = __this->___sceneFilters_9;
		MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F* L_107;
		L_107 = PointGrassCommon_CreateMeshFromFilters_mD660AD50FC9DD73F259A0D447B2C689EF1A437B3(L_105, L_106, NULL);
		*((RuntimeObject**)L_104) = (RuntimeObject*)L_107;
		Il2CppCodeGenWriteBarrier((void**)(RuntimeObject**)L_104, (void*)(RuntimeObject*)L_107);
		// if (meshData.verts.Length > 0) { return true; }
		MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F** L_108 = ___meshData0;
		MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F* L_109 = *((MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F**)L_108);
		NullCheck(L_109);
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_110 = L_109->___verts_0;
		NullCheck(L_110);
		if (!(((RuntimeArray*)L_110)->max_length))
		{
			goto IL_0299;
		}
	}
	{
		// if (meshData.verts.Length > 0) { return true; }
		return (bool)1;
	}

IL_0299:
	{
		// return false;
		return (bool)0;
	}
}
// System.Boolean MicahW.PointGrass.PointGrassRenderer::GetTerrainMeshData(MicahW.PointGrass.PointGrassCommon/MeshData&,UnityEngine.Vector2Int)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PointGrassRenderer_GetTerrainMeshData_mE1FA1BE05C1FCB35601AD79D3C1B0219ECA2BEF8 (PointGrassRenderer_tCAB9C932B5AEF5960C04E43451965E31C6A471C2* __this, MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F** ___meshData0, Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A ___chunkCoord1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	float V_4 = 0.0f;
	float G_B5_0 = 0.0f;
	{
		// meshData = MeshData.Empty;
		MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F** L_0 = ___meshData0;
		il2cpp_codegen_runtime_class_init_inline(MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F_il2cpp_TypeInfo_var);
		MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F* L_1 = ((MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F_StaticFields*)il2cpp_codegen_static_fields_for(MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F_il2cpp_TypeInfo_var))->___Empty_7;
		*((RuntimeObject**)L_0) = (RuntimeObject*)L_1;
		Il2CppCodeGenWriteBarrier((void**)(RuntimeObject**)L_0, (void*)(RuntimeObject*)L_1);
		// if (terrain != null && terrainLayers != null) {
		TerrainData_t615A68EAC648066681875D47FC641496D12F2E24* L_2 = __this->___terrain_6;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_3;
		L_3 = Object_op_Inequality_mD0BE578448EAA61948F25C32F8DD55AB1F778602(L_2, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_3)
		{
			goto IL_00d9;
		}
	}
	{
		TerrainLayerU5BU5D_t259E391D6115F121FCD284E79F62012D70956EB0* L_4 = __this->___terrainLayers_7;
		if (!L_4)
		{
			goto IL_00d9;
		}
	}
	{
		// int terrainSize = terrain.heightmapResolution;
		TerrainData_t615A68EAC648066681875D47FC641496D12F2E24* L_5 = __this->___terrain_6;
		NullCheck(L_5);
		int32_t L_6;
		L_6 = TerrainData_get_heightmapResolution_m39FE9A5C31A80B28021F8E2484EF5F2664798836(L_5, NULL);
		// int startX = Mathf.FloorToInt((float)terrainSize * chunkCoord.x / chunkCount.x);
		int32_t L_7 = L_6;
		int32_t L_8;
		L_8 = Vector2Int_get_x_mA2CACB1B6E6B5AD0CCC32B2CD2EDCE3ECEB50576_inline((&___chunkCoord1), NULL);
		Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A* L_9 = (&__this->___chunkCount_8);
		int32_t L_10;
		L_10 = Vector2Int_get_x_mA2CACB1B6E6B5AD0CCC32B2CD2EDCE3ECEB50576_inline(L_9, NULL);
		int32_t L_11;
		L_11 = Mathf_FloorToInt_m2A39AE881CAEE6B6A4B3BFEF9CA1ED40625F5AB7_inline(((float)(((float)il2cpp_codegen_multiply(((float)L_7), ((float)L_8)))/((float)L_10))), NULL);
		V_0 = L_11;
		// int startY = Mathf.FloorToInt((float)terrainSize * chunkCoord.y / chunkCount.y);
		int32_t L_12 = L_7;
		int32_t L_13;
		L_13 = Vector2Int_get_y_m48454163ECF0B463FB5A16A0C4FC4B14DB0768B3_inline((&___chunkCoord1), NULL);
		Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A* L_14 = (&__this->___chunkCount_8);
		int32_t L_15;
		L_15 = Vector2Int_get_y_m48454163ECF0B463FB5A16A0C4FC4B14DB0768B3_inline(L_14, NULL);
		int32_t L_16;
		L_16 = Mathf_FloorToInt_m2A39AE881CAEE6B6A4B3BFEF9CA1ED40625F5AB7_inline(((float)(((float)il2cpp_codegen_multiply(((float)L_12), ((float)L_13)))/((float)L_15))), NULL);
		V_1 = L_16;
		// int endX = Mathf.CeilToInt((float)terrainSize * (chunkCoord.x + 1) / chunkCount.x);
		int32_t L_17 = L_12;
		int32_t L_18;
		L_18 = Vector2Int_get_x_mA2CACB1B6E6B5AD0CCC32B2CD2EDCE3ECEB50576_inline((&___chunkCoord1), NULL);
		Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A* L_19 = (&__this->___chunkCount_8);
		int32_t L_20;
		L_20 = Vector2Int_get_x_mA2CACB1B6E6B5AD0CCC32B2CD2EDCE3ECEB50576_inline(L_19, NULL);
		int32_t L_21;
		L_21 = Mathf_CeilToInt_mF2BF9F4261B3431DC20E10A46CFEEED103C48963_inline(((float)(((float)il2cpp_codegen_multiply(((float)L_17), ((float)((int32_t)il2cpp_codegen_add(L_18, 1)))))/((float)L_20))), NULL);
		V_2 = L_21;
		// int endY = Mathf.CeilToInt((float)terrainSize * (chunkCoord.y + 1) / chunkCount.y);
		int32_t L_22;
		L_22 = Vector2Int_get_y_m48454163ECF0B463FB5A16A0C4FC4B14DB0768B3_inline((&___chunkCoord1), NULL);
		Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A* L_23 = (&__this->___chunkCount_8);
		int32_t L_24;
		L_24 = Vector2Int_get_y_m48454163ECF0B463FB5A16A0C4FC4B14DB0768B3_inline(L_23, NULL);
		int32_t L_25;
		L_25 = Mathf_CeilToInt_mF2BF9F4261B3431DC20E10A46CFEEED103C48963_inline(((float)(((float)il2cpp_codegen_multiply(((float)L_17), ((float)((int32_t)il2cpp_codegen_add(L_22, 1)))))/((float)L_24))), NULL);
		V_3 = L_25;
		// float denCutoff = useDensity ? densityCutoff : 0f;
		bool L_26 = __this->___useDensity_28;
		if (L_26)
		{
			goto IL_00b8;
		}
	}
	{
		G_B5_0 = (0.0f);
		goto IL_00be;
	}

IL_00b8:
	{
		float L_27 = __this->___densityCutoff_29;
		G_B5_0 = L_27;
	}

IL_00be:
	{
		V_4 = G_B5_0;
		// meshData = CreateMeshFromTerrainData(terrain, denCutoff, startX, startY, endX - startX, endY - startY);
		MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F** L_28 = ___meshData0;
		TerrainData_t615A68EAC648066681875D47FC641496D12F2E24* L_29 = __this->___terrain_6;
		float L_30 = V_4;
		int32_t L_31 = V_0;
		int32_t L_32 = V_1;
		int32_t L_33 = V_2;
		int32_t L_34 = V_0;
		int32_t L_35 = V_3;
		int32_t L_36 = V_1;
		MeshData_t82D2A39DC3E6D73E63EB6E528904A7EE24F40E4F* L_37;
		L_37 = PointGrassCommon_CreateMeshFromTerrainData_mA5C0F6BD781BEB33C68AF5BF697E1FAE23002E55(L_29, L_30, L_31, L_32, ((int32_t)il2cpp_codegen_subtract(L_33, L_34)), ((int32_t)il2cpp_codegen_subtract(L_35, L_36)), NULL);
		*((RuntimeObject**)L_28) = (RuntimeObject*)L_37;
		Il2CppCodeGenWriteBarrier((void**)(RuntimeObject**)L_28, (void*)(RuntimeObject*)L_37);
		// return true;
		return (bool)1;
	}

IL_00d9:
	{
		// return false;
		return (bool)0;
	}
}
// System.Void MicahW.PointGrass.PointGrassRenderer::CreateBuffers(MicahW.PointGrass.PointGrassCommon/MeshPoint[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointGrassRenderer_CreateBuffers_mFC53982D8596D1E77FE4711A0D9FEC742A925FAB (PointGrassRenderer_tCAB9C932B5AEF5960C04E43451965E31C6A471C2* __this, MeshPointU5BU5D_tBCEA150F23C3861E840A87B994115F06D7490DD8* ___points0, const RuntimeMethod* method) 
{
	{
		// if (UsingMultipleMeshes) { CreateBuffersFromPoints_Multi(points, out pointBuffers, out materialBlocks); }
		bool L_0;
		L_0 = PointGrassRenderer_get_UsingMultipleMeshes_m61A92AF3BC219534CD301983C34CECB07E6A3E59(__this, NULL);
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		// if (UsingMultipleMeshes) { CreateBuffersFromPoints_Multi(points, out pointBuffers, out materialBlocks); }
		MeshPointU5BU5D_tBCEA150F23C3861E840A87B994115F06D7490DD8* L_1 = ___points0;
		ComputeBufferU5BU5D_t7832804740B13E96807A836AD90ADF1477D7FE27** L_2 = (&__this->___pointBuffers_35);
		MaterialPropertyBlockU5BU5D_t6911DDC471FC23F2AF58FF0140ED595529EC5F5B** L_3 = (&__this->___materialBlocks_36);
		PointGrassRenderer_CreateBuffersFromPoints_Multi_m0FB6D3F6742650ACA37DF48E03DE221ABC7EF599(__this, L_1, L_2, L_3, NULL);
		return;
	}

IL_001c:
	{
		// else { CreateBufferFromPoints(points, out pointBuffer, out materialBlock); }
		MeshPointU5BU5D_tBCEA150F23C3861E840A87B994115F06D7490DD8* L_4 = ___points0;
		ComputeBuffer_t51EADA9015EBCC1B982C5584E9AB2734415A8233** L_5 = (&__this->___pointBuffer_32);
		MaterialPropertyBlock_t2308669579033A857EFE6E4831909F638B27411D** L_6 = (&__this->___materialBlock_33);
		PointGrassRenderer_CreateBufferFromPoints_mFC5340A5C557D32EAEDECCC71E3D48114E5DCF07(__this, L_4, L_5, L_6, NULL);
		// }
		return;
	}
}
// System.Void MicahW.PointGrass.PointGrassRenderer::ClearBuffers()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointGrassRenderer_ClearBuffers_mE07D764621EE41CB4ED709B578227440F728B60A (PointGrassRenderer_tCAB9C932B5AEF5960C04E43451965E31C6A471C2* __this, const RuntimeMethod* method) 
{
	int32_t V_0 = 0;
	{
		// if (pointBuffer != null) { pointBuffer.Release(); }
		ComputeBuffer_t51EADA9015EBCC1B982C5584E9AB2734415A8233* L_0 = __this->___pointBuffer_32;
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		// if (pointBuffer != null) { pointBuffer.Release(); }
		ComputeBuffer_t51EADA9015EBCC1B982C5584E9AB2734415A8233* L_1 = __this->___pointBuffer_32;
		NullCheck(L_1);
		ComputeBuffer_Release_mF1F157C929A0A5B2FDCD703A286EE09723450B72(L_1, NULL);
	}

IL_0013:
	{
		// if (pointBuffers != null) {
		ComputeBufferU5BU5D_t7832804740B13E96807A836AD90ADF1477D7FE27* L_2 = __this->___pointBuffers_35;
		if (!L_2)
		{
			goto IL_0042;
		}
	}
	{
		// for (int i = 0; i < pointBuffers.Length; i++) { pointBuffers[i].Release(); }
		V_0 = 0;
		goto IL_0030;
	}

IL_001f:
	{
		// for (int i = 0; i < pointBuffers.Length; i++) { pointBuffers[i].Release(); }
		ComputeBufferU5BU5D_t7832804740B13E96807A836AD90ADF1477D7FE27* L_3 = __this->___pointBuffers_35;
		int32_t L_4 = V_0;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		ComputeBuffer_t51EADA9015EBCC1B982C5584E9AB2734415A8233* L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		NullCheck(L_6);
		ComputeBuffer_Release_mF1F157C929A0A5B2FDCD703A286EE09723450B72(L_6, NULL);
		// for (int i = 0; i < pointBuffers.Length; i++) { pointBuffers[i].Release(); }
		int32_t L_7 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add(L_7, 1));
	}

IL_0030:
	{
		// for (int i = 0; i < pointBuffers.Length; i++) { pointBuffers[i].Release(); }
		int32_t L_8 = V_0;
		ComputeBufferU5BU5D_t7832804740B13E96807A836AD90ADF1477D7FE27* L_9 = __this->___pointBuffers_35;
		NullCheck(L_9);
		if ((((int32_t)L_8) < ((int32_t)((int32_t)(((RuntimeArray*)L_9)->max_length)))))
		{
			goto IL_001f;
		}
	}
	{
		// pointBuffers = null;
		__this->___pointBuffers_35 = (ComputeBufferU5BU5D_t7832804740B13E96807A836AD90ADF1477D7FE27*)NULL;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___pointBuffers_35), (void*)(ComputeBufferU5BU5D_t7832804740B13E96807A836AD90ADF1477D7FE27*)NULL);
	}

IL_0042:
	{
		// if (boundingBoxes != null) { boundingBoxes = null; }
		BoundsU5BU5D_t45563ED55B62FA0536E8117454C56C0CFA5B25F5* L_10 = __this->___boundingBoxes_37;
		if (!L_10)
		{
			goto IL_0051;
		}
	}
	{
		// if (boundingBoxes != null) { boundingBoxes = null; }
		__this->___boundingBoxes_37 = (BoundsU5BU5D_t45563ED55B62FA0536E8117454C56C0CFA5B25F5*)NULL;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___boundingBoxes_37), (void*)(BoundsU5BU5D_t45563ED55B62FA0536E8117454C56C0CFA5B25F5*)NULL);
	}

IL_0051:
	{
		// }
		return;
	}
}
// System.Void MicahW.PointGrass.PointGrassRenderer::CreateBufferFromPoints(MicahW.PointGrass.PointGrassCommon/MeshPoint[],UnityEngine.ComputeBuffer&,UnityEngine.MaterialPropertyBlock&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointGrassRenderer_CreateBufferFromPoints_mFC5340A5C557D32EAEDECCC71E3D48114E5DCF07 (PointGrassRenderer_tCAB9C932B5AEF5960C04E43451965E31C6A471C2* __this, MeshPointU5BU5D_tBCEA150F23C3861E840A87B994115F06D7490DD8* ___points0, ComputeBuffer_t51EADA9015EBCC1B982C5584E9AB2734415A8233** ___buffer1, MaterialPropertyBlock_t2308669579033A857EFE6E4831909F638B27411D** ___block2, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ComputeBuffer_t51EADA9015EBCC1B982C5584E9AB2734415A8233_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (points == null || points.Length == 0) { buffer = null; block = null; return; }
		MeshPointU5BU5D_tBCEA150F23C3861E840A87B994115F06D7490DD8* L_0 = ___points0;
		if (!L_0)
		{
			goto IL_0007;
		}
	}
	{
		MeshPointU5BU5D_tBCEA150F23C3861E840A87B994115F06D7490DD8* L_1 = ___points0;
		NullCheck(L_1);
		if ((((RuntimeArray*)L_1)->max_length))
		{
			goto IL_000e;
		}
	}

IL_0007:
	{
		// if (points == null || points.Length == 0) { buffer = null; block = null; return; }
		ComputeBuffer_t51EADA9015EBCC1B982C5584E9AB2734415A8233** L_2 = ___buffer1;
		*((RuntimeObject**)L_2) = (RuntimeObject*)NULL;
		Il2CppCodeGenWriteBarrier((void**)(RuntimeObject**)L_2, (void*)(RuntimeObject*)NULL);
		// if (points == null || points.Length == 0) { buffer = null; block = null; return; }
		MaterialPropertyBlock_t2308669579033A857EFE6E4831909F638B27411D** L_3 = ___block2;
		*((RuntimeObject**)L_3) = (RuntimeObject*)NULL;
		Il2CppCodeGenWriteBarrier((void**)(RuntimeObject**)L_3, (void*)(RuntimeObject*)NULL);
		// if (points == null || points.Length == 0) { buffer = null; block = null; return; }
		return;
	}

IL_000e:
	{
		// buffer = new ComputeBuffer(points.Length, MeshPoint.stride);
		ComputeBuffer_t51EADA9015EBCC1B982C5584E9AB2734415A8233** L_4 = ___buffer1;
		MeshPointU5BU5D_tBCEA150F23C3861E840A87B994115F06D7490DD8* L_5 = ___points0;
		NullCheck(L_5);
		ComputeBuffer_t51EADA9015EBCC1B982C5584E9AB2734415A8233* L_6 = (ComputeBuffer_t51EADA9015EBCC1B982C5584E9AB2734415A8233*)il2cpp_codegen_object_new(ComputeBuffer_t51EADA9015EBCC1B982C5584E9AB2734415A8233_il2cpp_TypeInfo_var);
		NullCheck(L_6);
		ComputeBuffer__ctor_mE40DE5EF5ADAC29B6B4DECBD1EE33E8526202617(L_6, ((int32_t)(((RuntimeArray*)L_5)->max_length)), ((int32_t)56), NULL);
		*((RuntimeObject**)L_4) = (RuntimeObject*)L_6;
		Il2CppCodeGenWriteBarrier((void**)(RuntimeObject**)L_4, (void*)(RuntimeObject*)L_6);
		// buffer.SetData(points);
		ComputeBuffer_t51EADA9015EBCC1B982C5584E9AB2734415A8233** L_7 = ___buffer1;
		ComputeBuffer_t51EADA9015EBCC1B982C5584E9AB2734415A8233* L_8 = *((ComputeBuffer_t51EADA9015EBCC1B982C5584E9AB2734415A8233**)L_7);
		MeshPointU5BU5D_tBCEA150F23C3861E840A87B994115F06D7490DD8* L_9 = ___points0;
		NullCheck(L_8);
		ComputeBuffer_SetData_m9F845E6B347CE028FA9A987D740FC642D828013A(L_8, (RuntimeArray*)L_9, NULL);
		// block = CreateMaterialPropertyBlock(buffer);
		MaterialPropertyBlock_t2308669579033A857EFE6E4831909F638B27411D** L_10 = ___block2;
		ComputeBuffer_t51EADA9015EBCC1B982C5584E9AB2734415A8233** L_11 = ___buffer1;
		ComputeBuffer_t51EADA9015EBCC1B982C5584E9AB2734415A8233* L_12 = *((ComputeBuffer_t51EADA9015EBCC1B982C5584E9AB2734415A8233**)L_11);
		MaterialPropertyBlock_t2308669579033A857EFE6E4831909F638B27411D* L_13;
		L_13 = PointGrassRenderer_CreateMaterialPropertyBlock_m0FC42DFE135544DE358968620AEAD8A957FC46B0(__this, L_12, NULL);
		*((RuntimeObject**)L_10) = (RuntimeObject*)L_13;
		Il2CppCodeGenWriteBarrier((void**)(RuntimeObject**)L_10, (void*)(RuntimeObject*)L_13);
		// }
		return;
	}
}
// System.Void MicahW.PointGrass.PointGrassRenderer::CreateBuffersFromPoints_Multi(MicahW.PointGrass.PointGrassCommon/MeshPoint[],UnityEngine.ComputeBuffer[]&,UnityEngine.MaterialPropertyBlock[]&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointGrassRenderer_CreateBuffersFromPoints_Multi_m0FB6D3F6742650ACA37DF48E03DE221ABC7EF599 (PointGrassRenderer_tCAB9C932B5AEF5960C04E43451965E31C6A471C2* __this, MeshPointU5BU5D_tBCEA150F23C3861E840A87B994115F06D7490DD8* ___points0, ComputeBufferU5BU5D_t7832804740B13E96807A836AD90ADF1477D7FE27** ___buffers1, MaterialPropertyBlockU5BU5D_t6911DDC471FC23F2AF58FF0140ED595529EC5F5B** ___blocks2, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ComputeBufferU5BU5D_t7832804740B13E96807A836AD90ADF1477D7FE27_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ComputeBuffer_t51EADA9015EBCC1B982C5584E9AB2734415A8233_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MaterialPropertyBlockU5BU5D_t6911DDC471FC23F2AF58FF0140ED595529EC5F5B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	float V_2 = 0.0f;
	SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* V_3 = NULL;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	float V_6 = 0.0f;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	int32_t V_9 = 0;
	int32_t V_10 = 0;
	int32_t V_11 = 0;
	int32_t V_12 = 0;
	int32_t V_13 = 0;
	{
		// if (points == null || points.Length == 0) { buffers = null; blocks = null; return; }
		MeshPointU5BU5D_tBCEA150F23C3861E840A87B994115F06D7490DD8* L_0 = ___points0;
		if (!L_0)
		{
			goto IL_0007;
		}
	}
	{
		MeshPointU5BU5D_tBCEA150F23C3861E840A87B994115F06D7490DD8* L_1 = ___points0;
		NullCheck(L_1);
		if ((((RuntimeArray*)L_1)->max_length))
		{
			goto IL_000e;
		}
	}

IL_0007:
	{
		// if (points == null || points.Length == 0) { buffers = null; blocks = null; return; }
		ComputeBufferU5BU5D_t7832804740B13E96807A836AD90ADF1477D7FE27** L_2 = ___buffers1;
		*((RuntimeObject**)L_2) = (RuntimeObject*)NULL;
		Il2CppCodeGenWriteBarrier((void**)(RuntimeObject**)L_2, (void*)(RuntimeObject*)NULL);
		// if (points == null || points.Length == 0) { buffers = null; blocks = null; return; }
		MaterialPropertyBlockU5BU5D_t6911DDC471FC23F2AF58FF0140ED595529EC5F5B** L_3 = ___blocks2;
		*((RuntimeObject**)L_3) = (RuntimeObject*)NULL;
		Il2CppCodeGenWriteBarrier((void**)(RuntimeObject**)L_3, (void*)(RuntimeObject*)NULL);
		// if (points == null || points.Length == 0) { buffers = null; blocks = null; return; }
		return;
	}

IL_000e:
	{
		// int pointCount = points.Length;
		MeshPointU5BU5D_tBCEA150F23C3861E840A87B994115F06D7490DD8* L_4 = ___points0;
		NullCheck(L_4);
		V_0 = ((int32_t)(((RuntimeArray*)L_4)->max_length));
		// int meshCount = grassBladeMeshes.Length;
		MeshU5BU5D_t178CA36422FC397211E68FB7E39C5B2F95619689* L_5 = __this->___grassBladeMeshes_14;
		NullCheck(L_5);
		V_1 = ((int32_t)(((RuntimeArray*)L_5)->max_length));
		// if (pointCount < meshCount) { buffers = null; blocks = null; } // Since some buffers would have a size of 0, return nothing to prevent errors
		int32_t L_6 = V_0;
		int32_t L_7 = V_1;
		if ((((int32_t)L_6) >= ((int32_t)L_7)))
		{
			goto IL_0026;
		}
	}
	{
		// if (pointCount < meshCount) { buffers = null; blocks = null; } // Since some buffers would have a size of 0, return nothing to prevent errors
		ComputeBufferU5BU5D_t7832804740B13E96807A836AD90ADF1477D7FE27** L_8 = ___buffers1;
		*((RuntimeObject**)L_8) = (RuntimeObject*)NULL;
		Il2CppCodeGenWriteBarrier((void**)(RuntimeObject**)L_8, (void*)(RuntimeObject*)NULL);
		// if (pointCount < meshCount) { buffers = null; blocks = null; } // Since some buffers would have a size of 0, return nothing to prevent errors
		MaterialPropertyBlockU5BU5D_t6911DDC471FC23F2AF58FF0140ED595529EC5F5B** L_9 = ___blocks2;
		*((RuntimeObject**)L_9) = (RuntimeObject*)NULL;
		Il2CppCodeGenWriteBarrier((void**)(RuntimeObject**)L_9, (void*)(RuntimeObject*)NULL);
		return;
	}

IL_0026:
	{
		// float sum = 0f;
		V_2 = (0.0f);
		// float[] densities = new float[meshCount];
		int32_t L_10 = V_1;
		SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* L_11 = (SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C*)(SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C*)SZArrayNew(SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C_il2cpp_TypeInfo_var, (uint32_t)L_10);
		V_3 = L_11;
		// for (int i = 0; i < meshCount; i++) { sum += meshDensityValues[i]; }
		V_5 = 0;
		goto IL_004a;
	}

IL_0038:
	{
		// for (int i = 0; i < meshCount; i++) { sum += meshDensityValues[i]; }
		float L_12 = V_2;
		SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* L_13 = __this->___meshDensityValues_15;
		int32_t L_14 = V_5;
		NullCheck(L_13);
		int32_t L_15 = L_14;
		float L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		V_2 = ((float)il2cpp_codegen_add(L_12, L_16));
		// for (int i = 0; i < meshCount; i++) { sum += meshDensityValues[i]; }
		int32_t L_17 = V_5;
		V_5 = ((int32_t)il2cpp_codegen_add(L_17, 1));
	}

IL_004a:
	{
		// for (int i = 0; i < meshCount; i++) { sum += meshDensityValues[i]; }
		int32_t L_18 = V_5;
		int32_t L_19 = V_1;
		if ((((int32_t)L_18) < ((int32_t)L_19)))
		{
			goto IL_0038;
		}
	}
	{
		// if (sum <= 0) { // Sum is zero. Default to (1f / meshCount) to prevent divide by zero errors
		float L_20 = V_2;
		if ((!(((float)L_20) <= ((float)(0.0f)))))
		{
			goto IL_0079;
		}
	}
	{
		// float val = 1f / meshCount;
		int32_t L_21 = V_1;
		V_6 = ((float)((1.0f)/((float)L_21)));
		// for (int i = 0; i < meshCount; i++) { densities[i] = val; }
		V_7 = 0;
		goto IL_0072;
	}

IL_0066:
	{
		// for (int i = 0; i < meshCount; i++) { densities[i] = val; }
		SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* L_22 = V_3;
		int32_t L_23 = V_7;
		float L_24 = V_6;
		NullCheck(L_22);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(L_23), (float)L_24);
		// for (int i = 0; i < meshCount; i++) { densities[i] = val; }
		int32_t L_25 = V_7;
		V_7 = ((int32_t)il2cpp_codegen_add(L_25, 1));
	}

IL_0072:
	{
		// for (int i = 0; i < meshCount; i++) { densities[i] = val; }
		int32_t L_26 = V_7;
		int32_t L_27 = V_1;
		if ((((int32_t)L_26) < ((int32_t)L_27)))
		{
			goto IL_0066;
		}
	}
	{
		goto IL_0098;
	}

IL_0079:
	{
		// else for (int i = 0; i < meshCount; i++) { densities[i] = meshDensityValues[i] / sum; } // Sum is greater than 0. Use a divide
		V_8 = 0;
		goto IL_0093;
	}

IL_007e:
	{
		// else for (int i = 0; i < meshCount; i++) { densities[i] = meshDensityValues[i] / sum; } // Sum is greater than 0. Use a divide
		SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* L_28 = V_3;
		int32_t L_29 = V_8;
		SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* L_30 = __this->___meshDensityValues_15;
		int32_t L_31 = V_8;
		NullCheck(L_30);
		int32_t L_32 = L_31;
		float L_33 = (L_30)->GetAt(static_cast<il2cpp_array_size_t>(L_32));
		float L_34 = V_2;
		NullCheck(L_28);
		(L_28)->SetAt(static_cast<il2cpp_array_size_t>(L_29), (float)((float)(L_33/L_34)));
		// else for (int i = 0; i < meshCount; i++) { densities[i] = meshDensityValues[i] / sum; } // Sum is greater than 0. Use a divide
		int32_t L_35 = V_8;
		V_8 = ((int32_t)il2cpp_codegen_add(L_35, 1));
	}

IL_0093:
	{
		// else for (int i = 0; i < meshCount; i++) { densities[i] = meshDensityValues[i] / sum; } // Sum is greater than 0. Use a divide
		int32_t L_36 = V_8;
		int32_t L_37 = V_1;
		if ((((int32_t)L_36) < ((int32_t)L_37)))
		{
			goto IL_007e;
		}
	}

IL_0098:
	{
		// buffers = new ComputeBuffer[meshCount];
		ComputeBufferU5BU5D_t7832804740B13E96807A836AD90ADF1477D7FE27** L_38 = ___buffers1;
		int32_t L_39 = V_1;
		ComputeBufferU5BU5D_t7832804740B13E96807A836AD90ADF1477D7FE27* L_40 = (ComputeBufferU5BU5D_t7832804740B13E96807A836AD90ADF1477D7FE27*)(ComputeBufferU5BU5D_t7832804740B13E96807A836AD90ADF1477D7FE27*)SZArrayNew(ComputeBufferU5BU5D_t7832804740B13E96807A836AD90ADF1477D7FE27_il2cpp_TypeInfo_var, (uint32_t)L_39);
		*((RuntimeObject**)L_38) = (RuntimeObject*)L_40;
		Il2CppCodeGenWriteBarrier((void**)(RuntimeObject**)L_38, (void*)(RuntimeObject*)L_40);
		// blocks = new MaterialPropertyBlock[meshCount];
		MaterialPropertyBlockU5BU5D_t6911DDC471FC23F2AF58FF0140ED595529EC5F5B** L_41 = ___blocks2;
		int32_t L_42 = V_1;
		MaterialPropertyBlockU5BU5D_t6911DDC471FC23F2AF58FF0140ED595529EC5F5B* L_43 = (MaterialPropertyBlockU5BU5D_t6911DDC471FC23F2AF58FF0140ED595529EC5F5B*)(MaterialPropertyBlockU5BU5D_t6911DDC471FC23F2AF58FF0140ED595529EC5F5B*)SZArrayNew(MaterialPropertyBlockU5BU5D_t6911DDC471FC23F2AF58FF0140ED595529EC5F5B_il2cpp_TypeInfo_var, (uint32_t)L_42);
		*((RuntimeObject**)L_41) = (RuntimeObject*)L_43;
		Il2CppCodeGenWriteBarrier((void**)(RuntimeObject**)L_41, (void*)(RuntimeObject*)L_43);
		// int dataPointer = 0;
		V_4 = 0;
		// for (int i = 0; i < meshCount; i++) {
		V_9 = 0;
		goto IL_011b;
	}

IL_00b0:
	{
		// int targetCount = Mathf.RoundToInt(pointCount * densities[i]); // The target size of the buffer
		int32_t L_44 = V_0;
		SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* L_45 = V_3;
		int32_t L_46 = V_9;
		NullCheck(L_45);
		int32_t L_47 = L_46;
		float L_48 = (L_45)->GetAt(static_cast<il2cpp_array_size_t>(L_47));
		int32_t L_49;
		L_49 = Mathf_RoundToInt_m60F8B66CF27F1FA75AA219342BD184B75771EB4B_inline(((float)il2cpp_codegen_multiply(((float)L_44), L_48)), NULL);
		V_10 = L_49;
		// int remainingPoints = pointCount - dataPointer; // The remaining number of points
		int32_t L_50 = V_0;
		int32_t L_51 = V_4;
		V_11 = ((int32_t)il2cpp_codegen_subtract(L_50, L_51));
		// int remainingBuffers = (meshCount - i) - 1; // The number of remaining buffers (excluding the buffer we're creating)
		int32_t L_52 = V_1;
		int32_t L_53 = V_9;
		V_12 = ((int32_t)il2cpp_codegen_subtract(((int32_t)il2cpp_codegen_subtract(L_52, L_53)), 1));
		// int bufferSize = Mathf.Max(1, Mathf.Min(remainingPoints - (remainingBuffers), targetCount)); // The final size of the buffer
		int32_t L_54 = V_11;
		int32_t L_55 = V_12;
		int32_t L_56 = V_10;
		int32_t L_57;
		L_57 = Mathf_Min_m888083F74FF5655778F0403BB5E9608BEFDEA8CB_inline(((int32_t)il2cpp_codegen_subtract(L_54, L_55)), L_56, NULL);
		int32_t L_58;
		L_58 = Mathf_Max_m7FA442918DE37E3A00106D1F2E789D65829792B8_inline(1, L_57, NULL);
		V_13 = L_58;
		// buffers[i] = new ComputeBuffer(bufferSize, MeshPoint.stride); // Create the buffer
		ComputeBufferU5BU5D_t7832804740B13E96807A836AD90ADF1477D7FE27** L_59 = ___buffers1;
		ComputeBufferU5BU5D_t7832804740B13E96807A836AD90ADF1477D7FE27* L_60 = *((ComputeBufferU5BU5D_t7832804740B13E96807A836AD90ADF1477D7FE27**)L_59);
		int32_t L_61 = V_9;
		int32_t L_62 = V_13;
		ComputeBuffer_t51EADA9015EBCC1B982C5584E9AB2734415A8233* L_63 = (ComputeBuffer_t51EADA9015EBCC1B982C5584E9AB2734415A8233*)il2cpp_codegen_object_new(ComputeBuffer_t51EADA9015EBCC1B982C5584E9AB2734415A8233_il2cpp_TypeInfo_var);
		NullCheck(L_63);
		ComputeBuffer__ctor_mE40DE5EF5ADAC29B6B4DECBD1EE33E8526202617(L_63, L_62, ((int32_t)56), NULL);
		NullCheck(L_60);
		ArrayElementTypeCheck (L_60, L_63);
		(L_60)->SetAt(static_cast<il2cpp_array_size_t>(L_61), (ComputeBuffer_t51EADA9015EBCC1B982C5584E9AB2734415A8233*)L_63);
		// buffers[i].SetData(points, dataPointer, 0, bufferSize); // Fill the buffer with the next set of points
		ComputeBufferU5BU5D_t7832804740B13E96807A836AD90ADF1477D7FE27** L_64 = ___buffers1;
		ComputeBufferU5BU5D_t7832804740B13E96807A836AD90ADF1477D7FE27* L_65 = *((ComputeBufferU5BU5D_t7832804740B13E96807A836AD90ADF1477D7FE27**)L_64);
		int32_t L_66 = V_9;
		NullCheck(L_65);
		int32_t L_67 = L_66;
		ComputeBuffer_t51EADA9015EBCC1B982C5584E9AB2734415A8233* L_68 = (L_65)->GetAt(static_cast<il2cpp_array_size_t>(L_67));
		MeshPointU5BU5D_tBCEA150F23C3861E840A87B994115F06D7490DD8* L_69 = ___points0;
		int32_t L_70 = V_4;
		int32_t L_71 = V_13;
		NullCheck(L_68);
		ComputeBuffer_SetData_m5D75D81304937FD04C820B1015AFA066DF36DC0E(L_68, (RuntimeArray*)L_69, L_70, 0, L_71, NULL);
		// blocks[i] = CreateMaterialPropertyBlock(buffers[i]); // Create a material property block
		MaterialPropertyBlockU5BU5D_t6911DDC471FC23F2AF58FF0140ED595529EC5F5B** L_72 = ___blocks2;
		MaterialPropertyBlockU5BU5D_t6911DDC471FC23F2AF58FF0140ED595529EC5F5B* L_73 = *((MaterialPropertyBlockU5BU5D_t6911DDC471FC23F2AF58FF0140ED595529EC5F5B**)L_72);
		int32_t L_74 = V_9;
		ComputeBufferU5BU5D_t7832804740B13E96807A836AD90ADF1477D7FE27** L_75 = ___buffers1;
		ComputeBufferU5BU5D_t7832804740B13E96807A836AD90ADF1477D7FE27* L_76 = *((ComputeBufferU5BU5D_t7832804740B13E96807A836AD90ADF1477D7FE27**)L_75);
		int32_t L_77 = V_9;
		NullCheck(L_76);
		int32_t L_78 = L_77;
		ComputeBuffer_t51EADA9015EBCC1B982C5584E9AB2734415A8233* L_79 = (L_76)->GetAt(static_cast<il2cpp_array_size_t>(L_78));
		MaterialPropertyBlock_t2308669579033A857EFE6E4831909F638B27411D* L_80;
		L_80 = PointGrassRenderer_CreateMaterialPropertyBlock_m0FC42DFE135544DE358968620AEAD8A957FC46B0(__this, L_79, NULL);
		NullCheck(L_73);
		ArrayElementTypeCheck (L_73, L_80);
		(L_73)->SetAt(static_cast<il2cpp_array_size_t>(L_74), (MaterialPropertyBlock_t2308669579033A857EFE6E4831909F638B27411D*)L_80);
		// dataPointer += bufferSize; // Increase the data pointer
		int32_t L_81 = V_4;
		int32_t L_82 = V_13;
		V_4 = ((int32_t)il2cpp_codegen_add(L_81, L_82));
		// for (int i = 0; i < meshCount; i++) {
		int32_t L_83 = V_9;
		V_9 = ((int32_t)il2cpp_codegen_add(L_83, 1));
	}

IL_011b:
	{
		// for (int i = 0; i < meshCount; i++) {
		int32_t L_84 = V_9;
		int32_t L_85 = V_1;
		if ((((int32_t)L_84) < ((int32_t)L_85)))
		{
			goto IL_00b0;
		}
	}
	{
		// }
		return;
	}
}
// System.Void MicahW.PointGrass.PointGrassRenderer::DrawGrass()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointGrassRenderer_DrawGrass_m3930D3BDAD2C3E7C6EB7DB2B1206DE93B1095288 (PointGrassRenderer_tCAB9C932B5AEF5960C04E43451965E31C6A471C2* __this, const RuntimeMethod* method) 
{
	Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* V_0 = NULL;
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* V_1 = NULL;
	Bounds_t367E830C64BBF235ED8C3B2F8CF6254FDCAD39C3 V_2;
	memset((&V_2), 0, sizeof(V_2));
	bool V_3 = false;
	bool V_4 = false;
	bool V_5 = false;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	int32_t V_9 = 0;
	int32_t V_10 = 0;
	int32_t G_B6_0 = 0;
	int32_t G_B9_0 = 0;
	int32_t G_B12_0 = 0;
	{
		// if (pointBuffer == null && pointBuffers == null) { return; }
		ComputeBuffer_t51EADA9015EBCC1B982C5584E9AB2734415A8233* L_0 = __this->___pointBuffer_32;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ComputeBufferU5BU5D_t7832804740B13E96807A836AD90ADF1477D7FE27* L_1 = __this->___pointBuffers_35;
		if (L_1)
		{
			goto IL_0011;
		}
	}
	{
		// if (pointBuffer == null && pointBuffers == null) { return; }
		return;
	}

IL_0011:
	{
		// Mesh mesh = GetGrassMesh();
		Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* L_2;
		L_2 = PointGrassRenderer_GetGrassMesh_mE60BB12C500D3FF1E47167072BC6943666771124(__this, NULL);
		V_0 = L_2;
		// Material mat = material;
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_3 = __this->___material_17;
		V_1 = L_3;
		// Bounds finalBounds = TransformBounds(GetLocalBounds());
		Bounds_t367E830C64BBF235ED8C3B2F8CF6254FDCAD39C3 L_4;
		L_4 = PointGrassRenderer_GetLocalBounds_mD32C528489677A59FBD7360789D895D7361C5E4D(__this, NULL);
		Bounds_t367E830C64BBF235ED8C3B2F8CF6254FDCAD39C3 L_5;
		L_5 = PointGrassRenderer_TransformBounds_m4DDAF13C40FA22C3FD9D439ECE335DF21345A5A1(__this, L_4, NULL);
		V_2 = L_5;
		// bool hasPointBuffers = pointBuffers != null && pointBuffers.Length > 0;
		ComputeBufferU5BU5D_t7832804740B13E96807A836AD90ADF1477D7FE27* L_6 = __this->___pointBuffers_35;
		if (!L_6)
		{
			goto IL_0040;
		}
	}
	{
		ComputeBufferU5BU5D_t7832804740B13E96807A836AD90ADF1477D7FE27* L_7 = __this->___pointBuffers_35;
		NullCheck(L_7);
		G_B6_0 = ((!(((uint32_t)(((RuntimeArray*)L_7)->max_length)) <= ((uint32_t)0)))? 1 : 0);
		goto IL_0041;
	}

IL_0040:
	{
		G_B6_0 = 0;
	}

IL_0041:
	{
		V_3 = (bool)G_B6_0;
		// bool useMultipleMeshes = UsingMultipleMeshes && grassBladeMeshes != null && hasPointBuffers;
		bool L_8;
		L_8 = PointGrassRenderer_get_UsingMultipleMeshes_m61A92AF3BC219534CD301983C34CECB07E6A3E59(__this, NULL);
		if (!L_8)
		{
			goto IL_0055;
		}
	}
	{
		MeshU5BU5D_t178CA36422FC397211E68FB7E39C5B2F95619689* L_9 = __this->___grassBladeMeshes_14;
		G_B9_0 = ((!(((RuntimeObject*)(MeshU5BU5D_t178CA36422FC397211E68FB7E39C5B2F95619689*)L_9) <= ((RuntimeObject*)(RuntimeObject*)NULL)))? 1 : 0);
		goto IL_0056;
	}

IL_0055:
	{
		G_B9_0 = 0;
	}

IL_0056:
	{
		bool L_10 = V_3;
		V_4 = (bool)((int32_t)(G_B9_0&(int32_t)L_10));
		// bool useMultipleMats = multipleMaterials && materials != null && hasPointBuffers;
		bool L_11 = __this->___multipleMaterials_16;
		if (!L_11)
		{
			goto IL_006d;
		}
	}
	{
		MaterialU5BU5D_t2B1D11C42DB07A4400C0535F92DBB87A2E346D3D* L_12 = __this->___materials_18;
		G_B12_0 = ((!(((RuntimeObject*)(MaterialU5BU5D_t2B1D11C42DB07A4400C0535F92DBB87A2E346D3D*)L_12) <= ((RuntimeObject*)(RuntimeObject*)NULL)))? 1 : 0);
		goto IL_006e;
	}

IL_006d:
	{
		G_B12_0 = 0;
	}

IL_006e:
	{
		bool L_13 = V_3;
		V_5 = (bool)((int32_t)(G_B12_0&(int32_t)L_13));
		// if (boundingBoxes != null && boundingBoxes.Length > 0) {
		BoundsU5BU5D_t45563ED55B62FA0536E8117454C56C0CFA5B25F5* L_14 = __this->___boundingBoxes_37;
		if (!L_14)
		{
			goto IL_0173;
		}
	}
	{
		BoundsU5BU5D_t45563ED55B62FA0536E8117454C56C0CFA5B25F5* L_15 = __this->___boundingBoxes_37;
		NullCheck(L_15);
		if (!(((RuntimeArray*)L_15)->max_length))
		{
			goto IL_0173;
		}
	}
	{
		// if (hasPointBuffers) {
		bool L_16 = V_3;
		if (!L_16)
		{
			goto IL_014c;
		}
	}
	{
		// int buffersPerBounds = pointBuffers.Length / boundingBoxes.Length;
		ComputeBufferU5BU5D_t7832804740B13E96807A836AD90ADF1477D7FE27* L_17 = __this->___pointBuffers_35;
		NullCheck(L_17);
		BoundsU5BU5D_t45563ED55B62FA0536E8117454C56C0CFA5B25F5* L_18 = __this->___boundingBoxes_37;
		NullCheck(L_18);
		V_6 = ((int32_t)(((int32_t)(((RuntimeArray*)L_17)->max_length))/((int32_t)(((RuntimeArray*)L_18)->max_length))));
		// useMultipleMeshes &= grassBladeMeshes.Length >= buffersPerBounds;
		bool L_19 = V_4;
		MeshU5BU5D_t178CA36422FC397211E68FB7E39C5B2F95619689* L_20 = __this->___grassBladeMeshes_14;
		NullCheck(L_20);
		int32_t L_21 = V_6;
		V_4 = (bool)((int32_t)((int32_t)L_19&((((int32_t)((((int32_t)((int32_t)(((RuntimeArray*)L_20)->max_length))) < ((int32_t)L_21))? 1 : 0)) == ((int32_t)0))? 1 : 0)));
		// useMultipleMats &= materials.Length >= buffersPerBounds;
		bool L_22 = V_5;
		MaterialU5BU5D_t2B1D11C42DB07A4400C0535F92DBB87A2E346D3D* L_23 = __this->___materials_18;
		NullCheck(L_23);
		int32_t L_24 = V_6;
		V_5 = (bool)((int32_t)((int32_t)L_22&((((int32_t)((((int32_t)((int32_t)(((RuntimeArray*)L_23)->max_length))) < ((int32_t)L_24))? 1 : 0)) == ((int32_t)0))? 1 : 0)));
		// for (int i = 0; i < boundingBoxes.Length; i++) {
		V_7 = 0;
		goto IL_013f;
	}

IL_00cf:
	{
		// finalBounds = TransformBounds(boundingBoxes[i]);
		BoundsU5BU5D_t45563ED55B62FA0536E8117454C56C0CFA5B25F5* L_25 = __this->___boundingBoxes_37;
		int32_t L_26 = V_7;
		NullCheck(L_25);
		int32_t L_27 = L_26;
		Bounds_t367E830C64BBF235ED8C3B2F8CF6254FDCAD39C3 L_28 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		Bounds_t367E830C64BBF235ED8C3B2F8CF6254FDCAD39C3 L_29;
		L_29 = PointGrassRenderer_TransformBounds_m4DDAF13C40FA22C3FD9D439ECE335DF21345A5A1(__this, L_28, NULL);
		V_2 = L_29;
		// for (int j = 0; j < buffersPerBounds; j++) {
		V_8 = 0;
		goto IL_0133;
	}

IL_00e8:
	{
		// int index = i * buffersPerBounds + j;
		int32_t L_30 = V_7;
		int32_t L_31 = V_6;
		int32_t L_32 = V_8;
		V_9 = ((int32_t)il2cpp_codegen_add(((int32_t)il2cpp_codegen_multiply(L_30, L_31)), L_32));
		// if (useMultipleMeshes) {
		bool L_33 = V_4;
		if (!L_33)
		{
			goto IL_010e;
		}
	}
	{
		// mesh = grassBladeMeshes[j];
		MeshU5BU5D_t178CA36422FC397211E68FB7E39C5B2F95619689* L_34 = __this->___grassBladeMeshes_14;
		int32_t L_35 = V_8;
		NullCheck(L_34);
		int32_t L_36 = L_35;
		Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* L_37 = (L_34)->GetAt(static_cast<il2cpp_array_size_t>(L_36));
		V_0 = L_37;
		// if (useMultipleMats) { mat = materials[j]; }
		bool L_38 = V_5;
		if (!L_38)
		{
			goto IL_010e;
		}
	}
	{
		// if (useMultipleMats) { mat = materials[j]; }
		MaterialU5BU5D_t2B1D11C42DB07A4400C0535F92DBB87A2E346D3D* L_39 = __this->___materials_18;
		int32_t L_40 = V_8;
		NullCheck(L_39);
		int32_t L_41 = L_40;
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_42 = (L_39)->GetAt(static_cast<il2cpp_array_size_t>(L_41));
		V_1 = L_42;
	}

IL_010e:
	{
		// DrawGrassBuffer(pointBuffers[index], ref materialBlocks[index], mesh, mat, finalBounds);
		ComputeBufferU5BU5D_t7832804740B13E96807A836AD90ADF1477D7FE27* L_43 = __this->___pointBuffers_35;
		int32_t L_44 = V_9;
		NullCheck(L_43);
		int32_t L_45 = L_44;
		ComputeBuffer_t51EADA9015EBCC1B982C5584E9AB2734415A8233* L_46 = (L_43)->GetAt(static_cast<il2cpp_array_size_t>(L_45));
		MaterialPropertyBlockU5BU5D_t6911DDC471FC23F2AF58FF0140ED595529EC5F5B* L_47 = __this->___materialBlocks_36;
		int32_t L_48 = V_9;
		NullCheck(L_47);
		Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* L_49 = V_0;
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_50 = V_1;
		Bounds_t367E830C64BBF235ED8C3B2F8CF6254FDCAD39C3 L_51 = V_2;
		PointGrassRenderer_DrawGrassBuffer_m97B71865FE29413A6BA689202968F9B24C097B88(__this, L_46, ((L_47)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_48))), L_49, L_50, L_51, NULL);
		// for (int j = 0; j < buffersPerBounds; j++) {
		int32_t L_52 = V_8;
		V_8 = ((int32_t)il2cpp_codegen_add(L_52, 1));
	}

IL_0133:
	{
		// for (int j = 0; j < buffersPerBounds; j++) {
		int32_t L_53 = V_8;
		int32_t L_54 = V_6;
		if ((((int32_t)L_53) < ((int32_t)L_54)))
		{
			goto IL_00e8;
		}
	}
	{
		// for (int i = 0; i < boundingBoxes.Length; i++) {
		int32_t L_55 = V_7;
		V_7 = ((int32_t)il2cpp_codegen_add(L_55, 1));
	}

IL_013f:
	{
		// for (int i = 0; i < boundingBoxes.Length; i++) {
		int32_t L_56 = V_7;
		BoundsU5BU5D_t45563ED55B62FA0536E8117454C56C0CFA5B25F5* L_57 = __this->___boundingBoxes_37;
		NullCheck(L_57);
		if ((((int32_t)L_56) < ((int32_t)((int32_t)(((RuntimeArray*)L_57)->max_length)))))
		{
			goto IL_00cf;
		}
	}
	{
		return;
	}

IL_014c:
	{
		// else { DrawGrassBuffer(pointBuffer, ref materialBlock, mesh, mat, TransformBounds(boundingBoxes[0])); }
		ComputeBuffer_t51EADA9015EBCC1B982C5584E9AB2734415A8233* L_58 = __this->___pointBuffer_32;
		MaterialPropertyBlock_t2308669579033A857EFE6E4831909F638B27411D** L_59 = (&__this->___materialBlock_33);
		Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* L_60 = V_0;
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_61 = V_1;
		BoundsU5BU5D_t45563ED55B62FA0536E8117454C56C0CFA5B25F5* L_62 = __this->___boundingBoxes_37;
		NullCheck(L_62);
		int32_t L_63 = 0;
		Bounds_t367E830C64BBF235ED8C3B2F8CF6254FDCAD39C3 L_64 = (L_62)->GetAt(static_cast<il2cpp_array_size_t>(L_63));
		Bounds_t367E830C64BBF235ED8C3B2F8CF6254FDCAD39C3 L_65;
		L_65 = PointGrassRenderer_TransformBounds_m4DDAF13C40FA22C3FD9D439ECE335DF21345A5A1(__this, L_64, NULL);
		PointGrassRenderer_DrawGrassBuffer_m97B71865FE29413A6BA689202968F9B24C097B88(__this, L_58, L_59, L_60, L_61, L_65, NULL);
		return;
	}

IL_0173:
	{
		// else if (hasPointBuffers) {
		bool L_66 = V_3;
		if (!L_66)
		{
			goto IL_0200;
		}
	}
	{
		// useMultipleMeshes &= grassBladeMeshes.Length >= pointBuffers.Length;
		bool L_67 = V_4;
		MeshU5BU5D_t178CA36422FC397211E68FB7E39C5B2F95619689* L_68 = __this->___grassBladeMeshes_14;
		NullCheck(L_68);
		ComputeBufferU5BU5D_t7832804740B13E96807A836AD90ADF1477D7FE27* L_69 = __this->___pointBuffers_35;
		NullCheck(L_69);
		V_4 = (bool)((int32_t)((int32_t)L_67&((((int32_t)((((int32_t)((int32_t)(((RuntimeArray*)L_68)->max_length))) < ((int32_t)((int32_t)(((RuntimeArray*)L_69)->max_length))))? 1 : 0)) == ((int32_t)0))? 1 : 0)));
		// useMultipleMats &= materials.Length >= pointBuffers.Length;
		bool L_70 = V_5;
		MaterialU5BU5D_t2B1D11C42DB07A4400C0535F92DBB87A2E346D3D* L_71 = __this->___materials_18;
		NullCheck(L_71);
		ComputeBufferU5BU5D_t7832804740B13E96807A836AD90ADF1477D7FE27* L_72 = __this->___pointBuffers_35;
		NullCheck(L_72);
		V_5 = (bool)((int32_t)((int32_t)L_70&((((int32_t)((((int32_t)((int32_t)(((RuntimeArray*)L_71)->max_length))) < ((int32_t)((int32_t)(((RuntimeArray*)L_72)->max_length))))? 1 : 0)) == ((int32_t)0))? 1 : 0)));
		// for (int i = 0; i < pointBuffers.Length; i++) {
		V_10 = 0;
		goto IL_01f3;
	}

IL_01b2:
	{
		// if (useMultipleMeshes) {
		bool L_73 = V_4;
		if (!L_73)
		{
			goto IL_01ce;
		}
	}
	{
		// mesh = grassBladeMeshes[i];
		MeshU5BU5D_t178CA36422FC397211E68FB7E39C5B2F95619689* L_74 = __this->___grassBladeMeshes_14;
		int32_t L_75 = V_10;
		NullCheck(L_74);
		int32_t L_76 = L_75;
		Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* L_77 = (L_74)->GetAt(static_cast<il2cpp_array_size_t>(L_76));
		V_0 = L_77;
		// if (useMultipleMats) { mat = materials[i]; }
		bool L_78 = V_5;
		if (!L_78)
		{
			goto IL_01ce;
		}
	}
	{
		// if (useMultipleMats) { mat = materials[i]; }
		MaterialU5BU5D_t2B1D11C42DB07A4400C0535F92DBB87A2E346D3D* L_79 = __this->___materials_18;
		int32_t L_80 = V_10;
		NullCheck(L_79);
		int32_t L_81 = L_80;
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_82 = (L_79)->GetAt(static_cast<il2cpp_array_size_t>(L_81));
		V_1 = L_82;
	}

IL_01ce:
	{
		// DrawGrassBuffer(pointBuffers[i], ref materialBlocks[i], mesh, mat, finalBounds);
		ComputeBufferU5BU5D_t7832804740B13E96807A836AD90ADF1477D7FE27* L_83 = __this->___pointBuffers_35;
		int32_t L_84 = V_10;
		NullCheck(L_83);
		int32_t L_85 = L_84;
		ComputeBuffer_t51EADA9015EBCC1B982C5584E9AB2734415A8233* L_86 = (L_83)->GetAt(static_cast<il2cpp_array_size_t>(L_85));
		MaterialPropertyBlockU5BU5D_t6911DDC471FC23F2AF58FF0140ED595529EC5F5B* L_87 = __this->___materialBlocks_36;
		int32_t L_88 = V_10;
		NullCheck(L_87);
		Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* L_89 = V_0;
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_90 = V_1;
		Bounds_t367E830C64BBF235ED8C3B2F8CF6254FDCAD39C3 L_91 = V_2;
		PointGrassRenderer_DrawGrassBuffer_m97B71865FE29413A6BA689202968F9B24C097B88(__this, L_86, ((L_87)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_88))), L_89, L_90, L_91, NULL);
		// for (int i = 0; i < pointBuffers.Length; i++) {
		int32_t L_92 = V_10;
		V_10 = ((int32_t)il2cpp_codegen_add(L_92, 1));
	}

IL_01f3:
	{
		// for (int i = 0; i < pointBuffers.Length; i++) {
		int32_t L_93 = V_10;
		ComputeBufferU5BU5D_t7832804740B13E96807A836AD90ADF1477D7FE27* L_94 = __this->___pointBuffers_35;
		NullCheck(L_94);
		if ((((int32_t)L_93) < ((int32_t)((int32_t)(((RuntimeArray*)L_94)->max_length)))))
		{
			goto IL_01b2;
		}
	}
	{
		return;
	}

IL_0200:
	{
		// else { DrawGrassBuffer(pointBuffer, ref materialBlock, mesh, mat, finalBounds); }
		ComputeBuffer_t51EADA9015EBCC1B982C5584E9AB2734415A8233* L_95 = __this->___pointBuffer_32;
		MaterialPropertyBlock_t2308669579033A857EFE6E4831909F638B27411D** L_96 = (&__this->___materialBlock_33);
		Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* L_97 = V_0;
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_98 = V_1;
		Bounds_t367E830C64BBF235ED8C3B2F8CF6254FDCAD39C3 L_99 = V_2;
		PointGrassRenderer_DrawGrassBuffer_m97B71865FE29413A6BA689202968F9B24C097B88(__this, L_95, L_96, L_97, L_98, L_99, NULL);
		// }
		return;
	}
}
// System.Void MicahW.PointGrass.PointGrassRenderer::DrawGrassBuffer(UnityEngine.ComputeBuffer,UnityEngine.MaterialPropertyBlock&,UnityEngine.Mesh,UnityEngine.Material,UnityEngine.Bounds)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointGrassRenderer_DrawGrassBuffer_m97B71865FE29413A6BA689202968F9B24C097B88 (PointGrassRenderer_tCAB9C932B5AEF5960C04E43451965E31C6A471C2* __this, ComputeBuffer_t51EADA9015EBCC1B982C5584E9AB2734415A8233* ___buffer0, MaterialPropertyBlock_t2308669579033A857EFE6E4831909F638B27411D** ___block1, Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* ___mesh2, Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___mat3, Bounds_t367E830C64BBF235ED8C3B2F8CF6254FDCAD39C3 ___bounds4, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Graphics_t99CD970FFEA58171C70F54DF0C06D315BD452F2C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// if (buffer == null || !buffer.IsValid()) { return; }
		ComputeBuffer_t51EADA9015EBCC1B982C5584E9AB2734415A8233* L_0 = ___buffer0;
		if (!L_0)
		{
			goto IL_000b;
		}
	}
	{
		ComputeBuffer_t51EADA9015EBCC1B982C5584E9AB2734415A8233* L_1 = ___buffer0;
		NullCheck(L_1);
		bool L_2;
		L_2 = ComputeBuffer_IsValid_mC64E2E1D72BA70F04D177F82C3E258494B929B5D(L_1, NULL);
		if (L_2)
		{
			goto IL_000c;
		}
	}

IL_000b:
	{
		// if (buffer == null || !buffer.IsValid()) { return; }
		return;
	}

IL_000c:
	{
		// int count = Mathf.CeilToInt(buffer.count * pointLODFactor);
		ComputeBuffer_t51EADA9015EBCC1B982C5584E9AB2734415A8233* L_3 = ___buffer0;
		NullCheck(L_3);
		int32_t L_4;
		L_4 = ComputeBuffer_get_count_m4DAA2D2714BA7A46F007697F601E4446F1049506(L_3, NULL);
		float L_5 = __this->___pointLODFactor_23;
		int32_t L_6;
		L_6 = Mathf_CeilToInt_mF2BF9F4261B3431DC20E10A46CFEEED103C48963_inline(((float)il2cpp_codegen_multiply(((float)L_4), L_5)), NULL);
		V_0 = L_6;
		// if (mesh != null && mat != null && block != null && count > 0) {
		Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* L_7 = ___mesh2;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_8;
		L_8 = Object_op_Inequality_mD0BE578448EAA61948F25C32F8DD55AB1F778602(L_7, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_8)
		{
			goto IL_0074;
		}
	}
	{
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_9 = ___mat3;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_10;
		L_10 = Object_op_Inequality_mD0BE578448EAA61948F25C32F8DD55AB1F778602(L_9, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_10)
		{
			goto IL_0074;
		}
	}
	{
		MaterialPropertyBlock_t2308669579033A857EFE6E4831909F638B27411D** L_11 = ___block1;
		MaterialPropertyBlock_t2308669579033A857EFE6E4831909F638B27411D* L_12 = *((MaterialPropertyBlock_t2308669579033A857EFE6E4831909F638B27411D**)L_11);
		if (!L_12)
		{
			goto IL_0074;
		}
	}
	{
		int32_t L_13 = V_0;
		if ((((int32_t)L_13) <= ((int32_t)0)))
		{
			goto IL_0074;
		}
	}
	{
		// bounds = AddBoundsExtrusion(bounds);
		Bounds_t367E830C64BBF235ED8C3B2F8CF6254FDCAD39C3 L_14 = ___bounds4;
		Bounds_t367E830C64BBF235ED8C3B2F8CF6254FDCAD39C3 L_15;
		L_15 = PointGrassRenderer_AddBoundsExtrusion_m2D5303CFC7A6FFCC3A0F3DDD2D8AE89AD7D55305(__this, L_14, NULL);
		___bounds4 = L_15;
		// UpdateMaterialPropertyBlock(ref block, transform);
		MaterialPropertyBlock_t2308669579033A857EFE6E4831909F638B27411D** L_16 = ___block1;
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_17;
		L_17 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(__this, NULL);
		PointGrassCommon_UpdateMaterialPropertyBlock_mC29A761CEB43323C5150BA2BBFDB228ABA7427C6(L_16, L_17, NULL);
		// Graphics.DrawMeshInstancedProcedural(mesh, 0, mat, bounds, count, block, shadowMode, true, renderLayer.LayerIndex, null);
		Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* L_18 = ___mesh2;
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_19 = ___mat3;
		Bounds_t367E830C64BBF235ED8C3B2F8CF6254FDCAD39C3 L_20 = ___bounds4;
		int32_t L_21 = V_0;
		MaterialPropertyBlock_t2308669579033A857EFE6E4831909F638B27411D** L_22 = ___block1;
		MaterialPropertyBlock_t2308669579033A857EFE6E4831909F638B27411D* L_23 = *((MaterialPropertyBlock_t2308669579033A857EFE6E4831909F638B27411D**)L_22);
		int32_t L_24 = __this->___shadowMode_19;
		SingleLayer_tA74696727907F9D2CD336CD4C7AA5C5C8C8FB4ED* L_25 = (&__this->___renderLayer_20);
		int32_t L_26;
		L_26 = SingleLayer_get_LayerIndex_mB5C8A986682EAAA66CD1371500373D28A93B727E_inline(L_25, NULL);
		il2cpp_codegen_runtime_class_init_inline(Graphics_t99CD970FFEA58171C70F54DF0C06D315BD452F2C_il2cpp_TypeInfo_var);
		Graphics_DrawMeshInstancedProcedural_m177E33D3685441D2943AD08303050E28D85CB3F2(L_18, 0, L_19, L_20, L_21, L_23, L_24, (bool)1, L_26, (Camera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184*)NULL, 1, (LightProbeProxyVolume_t431001CA94D2BB5DB419E2A89E7D8116E4E1B658*)NULL, NULL);
	}

IL_0074:
	{
		// }
		return;
	}
}
// UnityEngine.MaterialPropertyBlock MicahW.PointGrass.PointGrassRenderer::CreateMaterialPropertyBlock(UnityEngine.ComputeBuffer)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR MaterialPropertyBlock_t2308669579033A857EFE6E4831909F638B27411D* PointGrassRenderer_CreateMaterialPropertyBlock_m0FC42DFE135544DE358968620AEAD8A957FC46B0 (PointGrassRenderer_tCAB9C932B5AEF5960C04E43451965E31C6A471C2* __this, ComputeBuffer_t51EADA9015EBCC1B982C5584E9AB2734415A8233* ___pointBuffer0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MaterialPropertyBlock_t2308669579033A857EFE6E4831909F638B27411D_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	MaterialPropertyBlock_t2308669579033A857EFE6E4831909F638B27411D* V_0 = NULL;
	{
		// MaterialPropertyBlock block = new MaterialPropertyBlock();
		MaterialPropertyBlock_t2308669579033A857EFE6E4831909F638B27411D* L_0 = (MaterialPropertyBlock_t2308669579033A857EFE6E4831909F638B27411D*)il2cpp_codegen_object_new(MaterialPropertyBlock_t2308669579033A857EFE6E4831909F638B27411D_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		MaterialPropertyBlock__ctor_m14C3432585F7BB65028BCD64A0FD6607A1B490FB(L_0, NULL);
		V_0 = L_0;
		// block.SetBuffer(ID_PointBuff, pointBuffer);
		MaterialPropertyBlock_t2308669579033A857EFE6E4831909F638B27411D* L_1 = V_0;
		int32_t L_2 = ((PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_StaticFields*)il2cpp_codegen_static_fields_for(PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_il2cpp_TypeInfo_var))->___ID_PointBuff_2;
		ComputeBuffer_t51EADA9015EBCC1B982C5584E9AB2734415A8233* L_3 = ___pointBuffer0;
		NullCheck(L_1);
		MaterialPropertyBlock_SetBuffer_m9FF172AF2C41F7CBCC50F4AC4C6109B15289BF73(L_1, L_2, L_3, NULL);
		// UpdateMaterialPropertyBlock(ref block, transform);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_4;
		L_4 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(__this, NULL);
		PointGrassCommon_UpdateMaterialPropertyBlock_mC29A761CEB43323C5150BA2BBFDB228ABA7427C6((&V_0), L_4, NULL);
		// return block;
		MaterialPropertyBlock_t2308669579033A857EFE6E4831909F638B27411D* L_5 = V_0;
		return L_5;
	}
}
// UnityEngine.Mesh MicahW.PointGrass.PointGrassRenderer::GetGrassMesh()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* PointGrassRenderer_GetGrassMesh_mE60BB12C500D3FF1E47167072BC6943666771124 (PointGrassRenderer_tCAB9C932B5AEF5960C04E43451965E31C6A471C2* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// switch (bladeType) {
		int32_t L_0 = __this->___bladeType_11;
		V_0 = L_0;
		int32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_001b;
			}
			case 1:
			{
				goto IL_0021;
			}
			case 2:
			{
				goto IL_0027;
			}
		}
	}
	{
		goto IL_002e;
	}

IL_001b:
	{
		// case BladeType.Flat:        return grassMeshFlat;
		Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* L_2 = ((PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_StaticFields*)il2cpp_codegen_static_fields_for(PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_il2cpp_TypeInfo_var))->___grassMeshFlat_0;
		return L_2;
	}

IL_0021:
	{
		// case BladeType.Cylindrical: return grassMeshCyl;
		Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* L_3 = ((PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_StaticFields*)il2cpp_codegen_static_fields_for(PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_il2cpp_TypeInfo_var))->___grassMeshCyl_1;
		return L_3;
	}

IL_0027:
	{
		// case BladeType.Mesh:        return grassBladeMesh;
		Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* L_4 = __this->___grassBladeMesh_13;
		return L_4;
	}

IL_002e:
	{
		// default:                    throw new System.ArgumentException(message: "Invalid enum value");
		ArgumentException_tAD90411542A20A9C72D5CDA3A84181D8B947A263* L_5 = (ArgumentException_tAD90411542A20A9C72D5CDA3A84181D8B947A263*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&ArgumentException_tAD90411542A20A9C72D5CDA3A84181D8B947A263_il2cpp_TypeInfo_var)));
		NullCheck(L_5);
		ArgumentException__ctor_m026938A67AF9D36BB7ED27F80425D7194B514465(L_5, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral6A5A4AFA6CE99EA3CE9AA1DC42E26C493F5B85D9)), NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&PointGrassRenderer_GetGrassMesh_mE60BB12C500D3FF1E47167072BC6943666771124_RuntimeMethod_var)));
	}
}
// UnityEngine.Bounds MicahW.PointGrass.PointGrassRenderer::GetLocalBounds()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Bounds_t367E830C64BBF235ED8C3B2F8CF6254FDCAD39C3 PointGrassRenderer_GetLocalBounds_mD32C528489677A59FBD7360789D895D7361C5E4D (PointGrassRenderer_tCAB9C932B5AEF5960C04E43451965E31C6A471C2* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// switch (distSource) {
		int32_t L_0 = __this->___distSource_4;
		V_0 = L_0;
		int32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_001f;
			}
			case 1:
			{
				goto IL_001f;
			}
			case 2:
			{
				goto IL_0026;
			}
			case 3:
			{
				goto IL_005a;
			}
		}
	}
	{
		goto IL_0061;
	}

IL_001f:
	{
		// return boundingBox;
		Bounds_t367E830C64BBF235ED8C3B2F8CF6254FDCAD39C3 L_2 = __this->___boundingBox_34;
		return L_2;
	}

IL_0026:
	{
		// if (terrain == null) { goto default; }
		TerrainData_t615A68EAC648066681875D47FC641496D12F2E24* L_3 = __this->___terrain_6;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_4;
		L_4 = Object_op_Equality_mB6120F782D83091EF56A198FCEBCF066DB4A9605(L_3, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (L_4)
		{
			goto IL_0061;
		}
	}
	{
		// else { return new Bounds(terrain.size * 0.5f, terrain.size); }
		TerrainData_t615A68EAC648066681875D47FC641496D12F2E24* L_5 = __this->___terrain_6;
		NullCheck(L_5);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_6;
		L_6 = TerrainData_get_size_mCD3977F344B9DEBFF61DD537D03FEB9473838DA5(L_5, NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_7;
		L_7 = Vector3_op_Multiply_m87BA7C578F96C8E49BB07088DAAC4649F83B0353_inline(L_6, (0.5f), NULL);
		TerrainData_t615A68EAC648066681875D47FC641496D12F2E24* L_8 = __this->___terrain_6;
		NullCheck(L_8);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_9;
		L_9 = TerrainData_get_size_mCD3977F344B9DEBFF61DD537D03FEB9473838DA5(L_8, NULL);
		Bounds_t367E830C64BBF235ED8C3B2F8CF6254FDCAD39C3 L_10;
		memset((&L_10), 0, sizeof(L_10));
		Bounds__ctor_mAF7B238B9FBF90C495E5D7951760085A93119C5A((&L_10), L_7, L_9, /*hidden argument*/NULL);
		return L_10;
	}

IL_005a:
	{
		// return boundingBox;
		Bounds_t367E830C64BBF235ED8C3B2F8CF6254FDCAD39C3 L_11 = __this->___boundingBox_34;
		return L_11;
	}

IL_0061:
	{
		// return new Bounds(Vector3.zero, Vector3.one);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_12;
		L_12 = Vector3_get_zero_m0C1249C3F25B1C70EAD3CC8B31259975A457AE39_inline(NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_13;
		L_13 = Vector3_get_one_mC9B289F1E15C42C597180C9FE6FB492495B51D02_inline(NULL);
		Bounds_t367E830C64BBF235ED8C3B2F8CF6254FDCAD39C3 L_14;
		memset((&L_14), 0, sizeof(L_14));
		Bounds__ctor_mAF7B238B9FBF90C495E5D7951760085A93119C5A((&L_14), L_12, L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
// UnityEngine.Bounds MicahW.PointGrass.PointGrassRenderer::TransformBounds(UnityEngine.Bounds)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Bounds_t367E830C64BBF235ED8C3B2F8CF6254FDCAD39C3 PointGrassRenderer_TransformBounds_m4DDAF13C40FA22C3FD9D439ECE335DF21345A5A1 (PointGrassRenderer_tCAB9C932B5AEF5960C04E43451965E31C6A471C2* __this, Bounds_t367E830C64BBF235ED8C3B2F8CF6254FDCAD39C3 ___localBounds0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_0;
	memset((&V_0), 0, sizeof(V_0));
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_1;
	memset((&V_1), 0, sizeof(V_1));
	Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* V_2 = NULL;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	{
		// Vector3 min = localBounds.min;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0;
		L_0 = Bounds_get_min_m465AC9BBE1DE5D8E8AD95AC19B9899068FEEBB13((&___localBounds0), NULL);
		V_0 = L_0;
		// Vector3 max = localBounds.max;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_1;
		L_1 = Bounds_get_max_m6446F2AB97C1E57CA89467B9DE52D4EB61F1CB09((&___localBounds0), NULL);
		V_1 = L_1;
		// Vector3[] points = new Vector3[] {
		//     min, max,
		// 
		//     new Vector3(max.x, min.y, min.z),
		//     new Vector3(min.x, max.y, min.z),
		//     new Vector3(min.x, min.y, max.z),
		// 
		//     new Vector3(min.x, max.y, max.z),
		//     new Vector3(max.x, min.y, max.z),
		//     new Vector3(max.x, max.y, min.z),
		// };
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_2 = (Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C*)(Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C*)SZArrayNew(Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C_il2cpp_TypeInfo_var, (uint32_t)8);
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_3 = L_2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_4 = V_0;
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2)L_4);
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_5 = L_3;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_6 = V_1;
		NullCheck(L_5);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(1), (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2)L_6);
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_7 = L_5;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_8 = V_1;
		float L_9 = L_8.___x_2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_10 = V_0;
		float L_11 = L_10.___y_3;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_12 = V_0;
		float L_13 = L_12.___z_4;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_14;
		memset((&L_14), 0, sizeof(L_14));
		Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline((&L_14), L_9, L_11, L_13, /*hidden argument*/NULL);
		NullCheck(L_7);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(2), (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2)L_14);
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_15 = L_7;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_16 = V_0;
		float L_17 = L_16.___x_2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_18 = V_1;
		float L_19 = L_18.___y_3;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_20 = V_0;
		float L_21 = L_20.___z_4;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_22;
		memset((&L_22), 0, sizeof(L_22));
		Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline((&L_22), L_17, L_19, L_21, /*hidden argument*/NULL);
		NullCheck(L_15);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(3), (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2)L_22);
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_23 = L_15;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_24 = V_0;
		float L_25 = L_24.___x_2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_26 = V_0;
		float L_27 = L_26.___y_3;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_28 = V_1;
		float L_29 = L_28.___z_4;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_30;
		memset((&L_30), 0, sizeof(L_30));
		Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline((&L_30), L_25, L_27, L_29, /*hidden argument*/NULL);
		NullCheck(L_23);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(4), (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2)L_30);
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_31 = L_23;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_32 = V_0;
		float L_33 = L_32.___x_2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_34 = V_1;
		float L_35 = L_34.___y_3;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_36 = V_1;
		float L_37 = L_36.___z_4;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_38;
		memset((&L_38), 0, sizeof(L_38));
		Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline((&L_38), L_33, L_35, L_37, /*hidden argument*/NULL);
		NullCheck(L_31);
		(L_31)->SetAt(static_cast<il2cpp_array_size_t>(5), (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2)L_38);
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_39 = L_31;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_40 = V_1;
		float L_41 = L_40.___x_2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_42 = V_0;
		float L_43 = L_42.___y_3;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_44 = V_1;
		float L_45 = L_44.___z_4;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_46;
		memset((&L_46), 0, sizeof(L_46));
		Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline((&L_46), L_41, L_43, L_45, /*hidden argument*/NULL);
		NullCheck(L_39);
		(L_39)->SetAt(static_cast<il2cpp_array_size_t>(6), (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2)L_46);
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_47 = L_39;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_48 = V_1;
		float L_49 = L_48.___x_2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_50 = V_1;
		float L_51 = L_50.___y_3;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_52 = V_0;
		float L_53 = L_52.___z_4;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_54;
		memset((&L_54), 0, sizeof(L_54));
		Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline((&L_54), L_49, L_51, L_53, /*hidden argument*/NULL);
		NullCheck(L_47);
		(L_47)->SetAt(static_cast<il2cpp_array_size_t>(7), (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2)L_54);
		V_2 = L_47;
		// for (int i = 0; i < points.Length; i++) { points[i] = transform.TransformPoint(points[i]); }
		V_3 = 0;
		goto IL_00fc;
	}

IL_00df:
	{
		// for (int i = 0; i < points.Length; i++) { points[i] = transform.TransformPoint(points[i]); }
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_55 = V_2;
		int32_t L_56 = V_3;
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_57;
		L_57 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(__this, NULL);
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_58 = V_2;
		int32_t L_59 = V_3;
		NullCheck(L_58);
		int32_t L_60 = L_59;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_61 = (L_58)->GetAt(static_cast<il2cpp_array_size_t>(L_60));
		NullCheck(L_57);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_62;
		L_62 = Transform_TransformPoint_m05BFF013DB830D7BFE44A007703694AE1062EE44(L_57, L_61, NULL);
		NullCheck(L_55);
		(L_55)->SetAt(static_cast<il2cpp_array_size_t>(L_56), (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2)L_62);
		// for (int i = 0; i < points.Length; i++) { points[i] = transform.TransformPoint(points[i]); }
		int32_t L_63 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add(L_63, 1));
	}

IL_00fc:
	{
		// for (int i = 0; i < points.Length; i++) { points[i] = transform.TransformPoint(points[i]); }
		int32_t L_64 = V_3;
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_65 = V_2;
		NullCheck(L_65);
		if ((((int32_t)L_64) < ((int32_t)((int32_t)(((RuntimeArray*)L_65)->max_length)))))
		{
			goto IL_00df;
		}
	}
	{
		// localBounds = new Bounds(points[0], Vector3.zero);
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_66 = V_2;
		NullCheck(L_66);
		int32_t L_67 = 0;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_68 = (L_66)->GetAt(static_cast<il2cpp_array_size_t>(L_67));
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_69;
		L_69 = Vector3_get_zero_m0C1249C3F25B1C70EAD3CC8B31259975A457AE39_inline(NULL);
		Bounds__ctor_mAF7B238B9FBF90C495E5D7951760085A93119C5A((&___localBounds0), L_68, L_69, NULL);
		// for (int i = 1; i < points.Length; i++) { localBounds.Encapsulate(points[i]); }
		V_4 = 1;
		goto IL_012f;
	}

IL_011a:
	{
		// for (int i = 1; i < points.Length; i++) { localBounds.Encapsulate(points[i]); }
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_70 = V_2;
		int32_t L_71 = V_4;
		NullCheck(L_70);
		int32_t L_72 = L_71;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_73 = (L_70)->GetAt(static_cast<il2cpp_array_size_t>(L_72));
		Bounds_Encapsulate_m1FCA57C58536ADB67B85A703470C6F5BFB837C2F((&___localBounds0), L_73, NULL);
		// for (int i = 1; i < points.Length; i++) { localBounds.Encapsulate(points[i]); }
		int32_t L_74 = V_4;
		V_4 = ((int32_t)il2cpp_codegen_add(L_74, 1));
	}

IL_012f:
	{
		// for (int i = 1; i < points.Length; i++) { localBounds.Encapsulate(points[i]); }
		int32_t L_75 = V_4;
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_76 = V_2;
		NullCheck(L_76);
		if ((((int32_t)L_75) < ((int32_t)((int32_t)(((RuntimeArray*)L_76)->max_length)))))
		{
			goto IL_011a;
		}
	}
	{
		// return localBounds;
		Bounds_t367E830C64BBF235ED8C3B2F8CF6254FDCAD39C3 L_77 = ___localBounds0;
		return L_77;
	}
}
// UnityEngine.Bounds MicahW.PointGrass.PointGrassRenderer::AddBoundsExtrusion(UnityEngine.Bounds)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Bounds_t367E830C64BBF235ED8C3B2F8CF6254FDCAD39C3 PointGrassRenderer_AddBoundsExtrusion_m2D5303CFC7A6FFCC3A0F3DDD2D8AE89AD7D55305 (PointGrassRenderer_tCAB9C932B5AEF5960C04E43451965E31C6A471C2* __this, Bounds_t367E830C64BBF235ED8C3B2F8CF6254FDCAD39C3 ___worldSpaceBounds0, const RuntimeMethod* method) 
{
	{
		// worldSpaceBounds.center += boundingBoxOffset.center;
		Bounds_t367E830C64BBF235ED8C3B2F8CF6254FDCAD39C3* L_0 = (&___worldSpaceBounds0);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_1;
		L_1 = Bounds_get_center_m5B05F81CB835EB6DD8628FDA24B638F477984DC3(L_0, NULL);
		Bounds_t367E830C64BBF235ED8C3B2F8CF6254FDCAD39C3* L_2 = (&__this->___boundingBoxOffset_40);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_3;
		L_3 = Bounds_get_center_m5B05F81CB835EB6DD8628FDA24B638F477984DC3(L_2, NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_4;
		L_4 = Vector3_op_Addition_m78C0EC70CB66E8DCAC225743D82B268DAEE92067_inline(L_1, L_3, NULL);
		Bounds_set_center_m891869DD5B1BEEE2D17907BBFB7EB79AAE44884B(L_0, L_4, NULL);
		// worldSpaceBounds.size += boundingBoxOffset.size;
		Bounds_t367E830C64BBF235ED8C3B2F8CF6254FDCAD39C3* L_5 = (&___worldSpaceBounds0);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_6;
		L_6 = Bounds_get_size_m0699A53A55A78B3201D7270D6F338DFA91B6FAD4(L_5, NULL);
		Bounds_t367E830C64BBF235ED8C3B2F8CF6254FDCAD39C3* L_7 = (&__this->___boundingBoxOffset_40);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_8;
		L_8 = Bounds_get_size_m0699A53A55A78B3201D7270D6F338DFA91B6FAD4(L_7, NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_9;
		L_9 = Vector3_op_Addition_m78C0EC70CB66E8DCAC225743D82B268DAEE92067_inline(L_6, L_8, NULL);
		Bounds_set_size_m950CFB68CDD1BF409E770509A38B958E1AE68128(L_5, L_9, NULL);
		// return worldSpaceBounds;
		Bounds_t367E830C64BBF235ED8C3B2F8CF6254FDCAD39C3 L_10 = ___worldSpaceBounds0;
		return L_10;
	}
}
// System.Void MicahW.PointGrass.PointGrassRenderer::SetDistributionSource(UnityEngine.Mesh)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointGrassRenderer_SetDistributionSource_mD92AFB9FF364F5C8B6AA62964D2F6851DAAC898E (PointGrassRenderer_tCAB9C932B5AEF5960C04E43451965E31C6A471C2* __this, Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* ___mesh0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB3E5B2D8F6FE296B992C16F74EE1A5F2DE2F9CDD);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralFF915760C44B3E7D7328B47C84AD434F656A679E);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (mesh == null) { Debug.LogError($"An attempt was made to set the distribution source on \"{gameObject.name}\" to null. Make sure the input distribution source is not null"); return; }
		Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* L_0 = ___mesh0;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Equality_mB6120F782D83091EF56A198FCEBCF066DB4A9605(L_0, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_1)
		{
			goto IL_0029;
		}
	}
	{
		// if (mesh == null) { Debug.LogError($"An attempt was made to set the distribution source on \"{gameObject.name}\" to null. Make sure the input distribution source is not null"); return; }
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2;
		L_2 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(__this, NULL);
		NullCheck(L_2);
		String_t* L_3;
		L_3 = Object_get_name_mAC2F6B897CF1303BA4249B4CB55271AFACBB6392(L_2, NULL);
		String_t* L_4;
		L_4 = String_Concat_m8855A6DE10F84DA7F4EC113CADDB59873A25573B(_stringLiteralB3E5B2D8F6FE296B992C16F74EE1A5F2DE2F9CDD, L_3, _stringLiteralFF915760C44B3E7D7328B47C84AD434F656A679E, NULL);
		il2cpp_codegen_runtime_class_init_inline(Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		Debug_LogError_mB00B2B4468EF3CAF041B038D840820FB84C924B2(L_4, NULL);
		// if (mesh == null) { Debug.LogError($"An attempt was made to set the distribution source on \"{gameObject.name}\" to null. Make sure the input distribution source is not null"); return; }
		return;
	}

IL_0029:
	{
		// distSource = DistributionSource.Mesh; baseMesh = mesh; // Set the distribution source and base mesh
		__this->___distSource_4 = 0;
		// distSource = DistributionSource.Mesh; baseMesh = mesh; // Set the distribution source and base mesh
		Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* L_5 = ___mesh0;
		__this->___baseMesh_5 = L_5;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___baseMesh_5), (void*)L_5);
		// }
		return;
	}
}
// System.Void MicahW.PointGrass.PointGrassRenderer::SetDistributionSource(UnityEngine.MeshFilter)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointGrassRenderer_SetDistributionSource_mF4A9B204F14B12FCC37F992B6793992FAC5FECB5 (PointGrassRenderer_tCAB9C932B5AEF5960C04E43451965E31C6A471C2* __this, MeshFilter_t6D1CE2473A1E45AC73013400585A1163BF66B2F5* ___filter0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MeshFilterU5BU5D_tCE3B457E6F7ECE5ECEE9E09150642150448685BA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB3E5B2D8F6FE296B992C16F74EE1A5F2DE2F9CDD);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralFF915760C44B3E7D7328B47C84AD434F656A679E);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (filter == null) { Debug.LogError($"An attempt was made to set the distribution source on \"{gameObject.name}\" to null. Make sure the input distribution source is not null"); return; }
		MeshFilter_t6D1CE2473A1E45AC73013400585A1163BF66B2F5* L_0 = ___filter0;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Equality_mB6120F782D83091EF56A198FCEBCF066DB4A9605(L_0, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_1)
		{
			goto IL_0029;
		}
	}
	{
		// if (filter == null) { Debug.LogError($"An attempt was made to set the distribution source on \"{gameObject.name}\" to null. Make sure the input distribution source is not null"); return; }
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2;
		L_2 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(__this, NULL);
		NullCheck(L_2);
		String_t* L_3;
		L_3 = Object_get_name_mAC2F6B897CF1303BA4249B4CB55271AFACBB6392(L_2, NULL);
		String_t* L_4;
		L_4 = String_Concat_m8855A6DE10F84DA7F4EC113CADDB59873A25573B(_stringLiteralB3E5B2D8F6FE296B992C16F74EE1A5F2DE2F9CDD, L_3, _stringLiteralFF915760C44B3E7D7328B47C84AD434F656A679E, NULL);
		il2cpp_codegen_runtime_class_init_inline(Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		Debug_LogError_mB00B2B4468EF3CAF041B038D840820FB84C924B2(L_4, NULL);
		// if (filter == null) { Debug.LogError($"An attempt was made to set the distribution source on \"{gameObject.name}\" to null. Make sure the input distribution source is not null"); return; }
		return;
	}

IL_0029:
	{
		// if (filter.gameObject == gameObject) { distSource = DistributionSource.MeshFilter; } // If the filter is on this object, then we just need to set the distribution source (since the MeshFilter distribution source doesn't keep a reference to the filter)
		MeshFilter_t6D1CE2473A1E45AC73013400585A1163BF66B2F5* L_5 = ___filter0;
		NullCheck(L_5);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_6;
		L_6 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_5, NULL);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_7;
		L_7 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(__this, NULL);
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_8;
		L_8 = Object_op_Equality_mB6120F782D83091EF56A198FCEBCF066DB4A9605(L_6, L_7, NULL);
		if (!L_8)
		{
			goto IL_0044;
		}
	}
	{
		// if (filter.gameObject == gameObject) { distSource = DistributionSource.MeshFilter; } // If the filter is on this object, then we just need to set the distribution source (since the MeshFilter distribution source doesn't keep a reference to the filter)
		__this->___distSource_4 = 1;
		return;
	}

IL_0044:
	{
		// else { SetDistributionSource(new MeshFilter[] { filter }); } // Since the filter isn't on this object, we need to use the scene filters distribution source
		MeshFilterU5BU5D_tCE3B457E6F7ECE5ECEE9E09150642150448685BA* L_9 = (MeshFilterU5BU5D_tCE3B457E6F7ECE5ECEE9E09150642150448685BA*)(MeshFilterU5BU5D_tCE3B457E6F7ECE5ECEE9E09150642150448685BA*)SZArrayNew(MeshFilterU5BU5D_tCE3B457E6F7ECE5ECEE9E09150642150448685BA_il2cpp_TypeInfo_var, (uint32_t)1);
		MeshFilterU5BU5D_tCE3B457E6F7ECE5ECEE9E09150642150448685BA* L_10 = L_9;
		MeshFilter_t6D1CE2473A1E45AC73013400585A1163BF66B2F5* L_11 = ___filter0;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(0), (MeshFilter_t6D1CE2473A1E45AC73013400585A1163BF66B2F5*)L_11);
		PointGrassRenderer_SetDistributionSource_m88344D60FD8578ECD8AE1B7A3A596E06C514B978(__this, L_10, NULL);
		// }
		return;
	}
}
// System.Void MicahW.PointGrass.PointGrassRenderer::SetDistributionSource(UnityEngine.TerrainData)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointGrassRenderer_SetDistributionSource_m75F9D26F46EE2525591694087F2C7D38DE1470F3 (PointGrassRenderer_tCAB9C932B5AEF5960C04E43451965E31C6A471C2* __this, TerrainData_t615A68EAC648066681875D47FC641496D12F2E24* ___terrain0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB3E5B2D8F6FE296B992C16F74EE1A5F2DE2F9CDD);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralFF915760C44B3E7D7328B47C84AD434F656A679E);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (terrain == null) { Debug.LogError($"An attempt was made to set the distribution source on \"{gameObject.name}\" to null. Make sure the input distribution source is not null"); return; }
		TerrainData_t615A68EAC648066681875D47FC641496D12F2E24* L_0 = ___terrain0;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Equality_mB6120F782D83091EF56A198FCEBCF066DB4A9605(L_0, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_1)
		{
			goto IL_0029;
		}
	}
	{
		// if (terrain == null) { Debug.LogError($"An attempt was made to set the distribution source on \"{gameObject.name}\" to null. Make sure the input distribution source is not null"); return; }
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2;
		L_2 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(__this, NULL);
		NullCheck(L_2);
		String_t* L_3;
		L_3 = Object_get_name_mAC2F6B897CF1303BA4249B4CB55271AFACBB6392(L_2, NULL);
		String_t* L_4;
		L_4 = String_Concat_m8855A6DE10F84DA7F4EC113CADDB59873A25573B(_stringLiteralB3E5B2D8F6FE296B992C16F74EE1A5F2DE2F9CDD, L_3, _stringLiteralFF915760C44B3E7D7328B47C84AD434F656A679E, NULL);
		il2cpp_codegen_runtime_class_init_inline(Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		Debug_LogError_mB00B2B4468EF3CAF041B038D840820FB84C924B2(L_4, NULL);
		// if (terrain == null) { Debug.LogError($"An attempt was made to set the distribution source on \"{gameObject.name}\" to null. Make sure the input distribution source is not null"); return; }
		return;
	}

IL_0029:
	{
		// distSource = DistributionSource.TerrainData; this.terrain = terrain; // Set the distribution source and terrain data
		__this->___distSource_4 = 2;
		// distSource = DistributionSource.TerrainData; this.terrain = terrain; // Set the distribution source and terrain data
		TerrainData_t615A68EAC648066681875D47FC641496D12F2E24* L_5 = ___terrain0;
		__this->___terrain_6 = L_5;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___terrain_6), (void*)L_5);
		// }
		return;
	}
}
// System.Void MicahW.PointGrass.PointGrassRenderer::SetDistributionSource(UnityEngine.MeshFilter[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointGrassRenderer_SetDistributionSource_m88344D60FD8578ECD8AE1B7A3A596E06C514B978 (PointGrassRenderer_tCAB9C932B5AEF5960C04E43451965E31C6A471C2* __this, MeshFilterU5BU5D_tCE3B457E6F7ECE5ECEE9E09150642150448685BA* ___sceneFilters0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB3E5B2D8F6FE296B992C16F74EE1A5F2DE2F9CDD);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralFF915760C44B3E7D7328B47C84AD434F656A679E);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (sceneFilters == null) { Debug.LogError($"An attempt was made to set the distribution source on \"{gameObject.name}\" to null. Make sure the input distribution source is not null"); return; }
		MeshFilterU5BU5D_tCE3B457E6F7ECE5ECEE9E09150642150448685BA* L_0 = ___sceneFilters0;
		if (L_0)
		{
			goto IL_0023;
		}
	}
	{
		// if (sceneFilters == null) { Debug.LogError($"An attempt was made to set the distribution source on \"{gameObject.name}\" to null. Make sure the input distribution source is not null"); return; }
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(__this, NULL);
		NullCheck(L_1);
		String_t* L_2;
		L_2 = Object_get_name_mAC2F6B897CF1303BA4249B4CB55271AFACBB6392(L_1, NULL);
		String_t* L_3;
		L_3 = String_Concat_m8855A6DE10F84DA7F4EC113CADDB59873A25573B(_stringLiteralB3E5B2D8F6FE296B992C16F74EE1A5F2DE2F9CDD, L_2, _stringLiteralFF915760C44B3E7D7328B47C84AD434F656A679E, NULL);
		il2cpp_codegen_runtime_class_init_inline(Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		Debug_LogError_mB00B2B4468EF3CAF041B038D840820FB84C924B2(L_3, NULL);
		// if (sceneFilters == null) { Debug.LogError($"An attempt was made to set the distribution source on \"{gameObject.name}\" to null. Make sure the input distribution source is not null"); return; }
		return;
	}

IL_0023:
	{
		// distSource = DistributionSource.SceneFilters; this.sceneFilters = sceneFilters; // Set the distribution source and scene filter references
		__this->___distSource_4 = 3;
		// distSource = DistributionSource.SceneFilters; this.sceneFilters = sceneFilters; // Set the distribution source and scene filter references
		MeshFilterU5BU5D_tCE3B457E6F7ECE5ECEE9E09150642150448685BA* L_4 = ___sceneFilters0;
		__this->___sceneFilters_9 = L_4;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___sceneFilters_9), (void*)L_4);
		// }
		return;
	}
}
// System.Void MicahW.PointGrass.PointGrassRenderer::SetBladeType(MicahW.PointGrass.PointGrassCommon/BladeType)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointGrassRenderer_SetBladeType_mF4BC8DC77A9FC34419A19830C292A4C415A6C805 (PointGrassRenderer_tCAB9C932B5AEF5960C04E43451965E31C6A471C2* __this, int32_t ___type0, const RuntimeMethod* method) 
{
	{
		// public void SetBladeType(BladeType type) { bladeType = type; }
		int32_t L_0 = ___type0;
		__this->___bladeType_11 = L_0;
		// public void SetBladeType(BladeType type) { bladeType = type; }
		return;
	}
}
// System.Void MicahW.PointGrass.PointGrassRenderer::SetBladeMesh(UnityEngine.Mesh)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointGrassRenderer_SetBladeMesh_mE55D9726DCDFCAC335303BB80BDC47A5A172CC98 (PointGrassRenderer_tCAB9C932B5AEF5960C04E43451965E31C6A471C2* __this, Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* ___mesh0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralD65D851CDF4779B341333708570AA533D90FABFC);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE066450B3C698B7EEB9191269100E6ADAB08BEA6);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (mesh == null) { Debug.LogError($"An attempt was made to set the blade mesh on \"{gameObject.name}\" to null or an empty array. Make sure the input blade mesh is not null"); return; }
		Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* L_0 = ___mesh0;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Equality_mB6120F782D83091EF56A198FCEBCF066DB4A9605(L_0, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_1)
		{
			goto IL_0029;
		}
	}
	{
		// if (mesh == null) { Debug.LogError($"An attempt was made to set the blade mesh on \"{gameObject.name}\" to null or an empty array. Make sure the input blade mesh is not null"); return; }
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2;
		L_2 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(__this, NULL);
		NullCheck(L_2);
		String_t* L_3;
		L_3 = Object_get_name_mAC2F6B897CF1303BA4249B4CB55271AFACBB6392(L_2, NULL);
		String_t* L_4;
		L_4 = String_Concat_m8855A6DE10F84DA7F4EC113CADDB59873A25573B(_stringLiteralD65D851CDF4779B341333708570AA533D90FABFC, L_3, _stringLiteralE066450B3C698B7EEB9191269100E6ADAB08BEA6, NULL);
		il2cpp_codegen_runtime_class_init_inline(Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		Debug_LogError_mB00B2B4468EF3CAF041B038D840820FB84C924B2(L_4, NULL);
		// if (mesh == null) { Debug.LogError($"An attempt was made to set the blade mesh on \"{gameObject.name}\" to null or an empty array. Make sure the input blade mesh is not null"); return; }
		return;
	}

IL_0029:
	{
		// SetBladeType(BladeType.Mesh);
		PointGrassRenderer_SetBladeType_mF4BC8DC77A9FC34419A19830C292A4C415A6C805_inline(__this, 2, NULL);
		// grassBladeMesh = mesh;
		Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* L_5 = ___mesh0;
		__this->___grassBladeMesh_13 = L_5;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___grassBladeMesh_13), (void*)L_5);
		// multipleMeshes = false;
		__this->___multipleMeshes_12 = (bool)0;
		// }
		return;
	}
}
// System.Void MicahW.PointGrass.PointGrassRenderer::SetBladeMesh(UnityEngine.Mesh[],System.Single[],UnityEngine.Material[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointGrassRenderer_SetBladeMesh_m7EFCC1B0EA053D81B4E6A3C0AF493B50C7240E7C (PointGrassRenderer_tCAB9C932B5AEF5960C04E43451965E31C6A471C2* __this, MeshU5BU5D_t178CA36422FC397211E68FB7E39C5B2F95619689* ___meshes0, SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* ___meshDensityValues1, MaterialU5BU5D_t2B1D11C42DB07A4400C0535F92DBB87A2E346D3D* ___materials2, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Array_Resize_TisSingle_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_m879C2A54DAFE78F46D1185B50ED527EE182BFB04_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral9D7439C5F97ECD9F4F649172D7F9D7165E71ECC3);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE7DC65FC74777E1C0F193ECF8DD2F17B43E4AEED);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (meshes == null || meshes.Length == 0) { Debug.LogError($"An attempt was made to set the blade meshes on \"{gameObject.name}\" to null or an empty array. Make sure the input blade meshes are not null"); return; }
		MeshU5BU5D_t178CA36422FC397211E68FB7E39C5B2F95619689* L_0 = ___meshes0;
		if (!L_0)
		{
			goto IL_0007;
		}
	}
	{
		MeshU5BU5D_t178CA36422FC397211E68FB7E39C5B2F95619689* L_1 = ___meshes0;
		NullCheck(L_1);
		if ((((RuntimeArray*)L_1)->max_length))
		{
			goto IL_0027;
		}
	}

IL_0007:
	{
		// if (meshes == null || meshes.Length == 0) { Debug.LogError($"An attempt was made to set the blade meshes on \"{gameObject.name}\" to null or an empty array. Make sure the input blade meshes are not null"); return; }
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2;
		L_2 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(__this, NULL);
		NullCheck(L_2);
		String_t* L_3;
		L_3 = Object_get_name_mAC2F6B897CF1303BA4249B4CB55271AFACBB6392(L_2, NULL);
		String_t* L_4;
		L_4 = String_Concat_m8855A6DE10F84DA7F4EC113CADDB59873A25573B(_stringLiteralE7DC65FC74777E1C0F193ECF8DD2F17B43E4AEED, L_3, _stringLiteral9D7439C5F97ECD9F4F649172D7F9D7165E71ECC3, NULL);
		il2cpp_codegen_runtime_class_init_inline(Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		Debug_LogError_mB00B2B4468EF3CAF041B038D840820FB84C924B2(L_4, NULL);
		// if (meshes == null || meshes.Length == 0) { Debug.LogError($"An attempt was made to set the blade meshes on \"{gameObject.name}\" to null or an empty array. Make sure the input blade meshes are not null"); return; }
		return;
	}

IL_0027:
	{
		// if (meshes.Length == 1) { SetBladeMesh(meshes[0]); } // If there's only one mesh in the array, use the normal mesh blade mode instead
		MeshU5BU5D_t178CA36422FC397211E68FB7E39C5B2F95619689* L_5 = ___meshes0;
		NullCheck(L_5);
		if ((!(((uint32_t)((int32_t)(((RuntimeArray*)L_5)->max_length))) == ((uint32_t)1))))
		{
			goto IL_0037;
		}
	}
	{
		// if (meshes.Length == 1) { SetBladeMesh(meshes[0]); } // If there's only one mesh in the array, use the normal mesh blade mode instead
		MeshU5BU5D_t178CA36422FC397211E68FB7E39C5B2F95619689* L_6 = ___meshes0;
		NullCheck(L_6);
		int32_t L_7 = 0;
		Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* L_8 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		PointGrassRenderer_SetBladeMesh_mE55D9726DCDFCAC335303BB80BDC47A5A172CC98(__this, L_8, NULL);
		return;
	}

IL_0037:
	{
		// SetBladeType(BladeType.Mesh);
		PointGrassRenderer_SetBladeType_mF4BC8DC77A9FC34419A19830C292A4C415A6C805_inline(__this, 2, NULL);
		// grassBladeMeshes = meshes;
		MeshU5BU5D_t178CA36422FC397211E68FB7E39C5B2F95619689* L_9 = ___meshes0;
		__this->___grassBladeMeshes_14 = L_9;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___grassBladeMeshes_14), (void*)L_9);
		// multipleMeshes = true;
		__this->___multipleMeshes_12 = (bool)1;
		// if (meshDensityValues != null && meshDensityValues.Length > 0) { this.meshDensityValues = meshDensityValues; }
		SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* L_10 = ___meshDensityValues1;
		if (!L_10)
		{
			goto IL_005a;
		}
	}
	{
		SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* L_11 = ___meshDensityValues1;
		NullCheck(L_11);
		if (!(((RuntimeArray*)L_11)->max_length))
		{
			goto IL_005a;
		}
	}
	{
		// if (meshDensityValues != null && meshDensityValues.Length > 0) { this.meshDensityValues = meshDensityValues; }
		SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* L_12 = ___meshDensityValues1;
		__this->___meshDensityValues_15 = L_12;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___meshDensityValues_15), (void*)L_12);
	}

IL_005a:
	{
		// if (this.meshDensityValues.Length != meshes.Length) { System.Array.Resize(ref this.meshDensityValues, meshes.Length); }
		SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* L_13 = __this->___meshDensityValues_15;
		NullCheck(L_13);
		MeshU5BU5D_t178CA36422FC397211E68FB7E39C5B2F95619689* L_14 = ___meshes0;
		NullCheck(L_14);
		if ((((int32_t)((int32_t)(((RuntimeArray*)L_13)->max_length))) == ((int32_t)((int32_t)(((RuntimeArray*)L_14)->max_length)))))
		{
			goto IL_0075;
		}
	}
	{
		// if (this.meshDensityValues.Length != meshes.Length) { System.Array.Resize(ref this.meshDensityValues, meshes.Length); }
		SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C** L_15 = (&__this->___meshDensityValues_15);
		MeshU5BU5D_t178CA36422FC397211E68FB7E39C5B2F95619689* L_16 = ___meshes0;
		NullCheck(L_16);
		Array_Resize_TisSingle_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_m879C2A54DAFE78F46D1185B50ED527EE182BFB04(L_15, ((int32_t)(((RuntimeArray*)L_16)->max_length)), Array_Resize_TisSingle_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_m879C2A54DAFE78F46D1185B50ED527EE182BFB04_RuntimeMethod_var);
	}

IL_0075:
	{
		// if (materials != null && materials.Length > 0) { SetMaterials(materials); }
		MaterialU5BU5D_t2B1D11C42DB07A4400C0535F92DBB87A2E346D3D* L_17 = ___materials2;
		if (!L_17)
		{
			goto IL_0083;
		}
	}
	{
		MaterialU5BU5D_t2B1D11C42DB07A4400C0535F92DBB87A2E346D3D* L_18 = ___materials2;
		NullCheck(L_18);
		if (!(((RuntimeArray*)L_18)->max_length))
		{
			goto IL_0083;
		}
	}
	{
		// if (materials != null && materials.Length > 0) { SetMaterials(materials); }
		MaterialU5BU5D_t2B1D11C42DB07A4400C0535F92DBB87A2E346D3D* L_19 = ___materials2;
		PointGrassRenderer_SetMaterials_mF165DA9F1096738EBA94283B8AF3C7BDA4F02DAC(__this, L_19, NULL);
	}

IL_0083:
	{
		// }
		return;
	}
}
// System.Void MicahW.PointGrass.PointGrassRenderer::SetBladeDensities(System.Single[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointGrassRenderer_SetBladeDensities_mDDCC9D21547179AC46182BC9C554F127571BED69 (PointGrassRenderer_tCAB9C932B5AEF5960C04E43451965E31C6A471C2* __this, SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* ___densities0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Array_Resize_TisSingle_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_m879C2A54DAFE78F46D1185B50ED527EE182BFB04_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral0EFE6FA17D252C5C2E805ED3D4A3CD84F1AA816C);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralCF0C3727D9DD1CD08C91EB1B8399286422952900);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (densities == null || densities.Length == 0) { Debug.LogError($"An attempt was made to set the blade densities on \"{gameObject.name}\" to null or an empty array. Make sure the input blade densities are not null"); return; }
		SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* L_0 = ___densities0;
		if (!L_0)
		{
			goto IL_0007;
		}
	}
	{
		SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* L_1 = ___densities0;
		NullCheck(L_1);
		if ((((RuntimeArray*)L_1)->max_length))
		{
			goto IL_0027;
		}
	}

IL_0007:
	{
		// if (densities == null || densities.Length == 0) { Debug.LogError($"An attempt was made to set the blade densities on \"{gameObject.name}\" to null or an empty array. Make sure the input blade densities are not null"); return; }
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2;
		L_2 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(__this, NULL);
		NullCheck(L_2);
		String_t* L_3;
		L_3 = Object_get_name_mAC2F6B897CF1303BA4249B4CB55271AFACBB6392(L_2, NULL);
		String_t* L_4;
		L_4 = String_Concat_m8855A6DE10F84DA7F4EC113CADDB59873A25573B(_stringLiteralCF0C3727D9DD1CD08C91EB1B8399286422952900, L_3, _stringLiteral0EFE6FA17D252C5C2E805ED3D4A3CD84F1AA816C, NULL);
		il2cpp_codegen_runtime_class_init_inline(Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		Debug_LogError_mB00B2B4468EF3CAF041B038D840820FB84C924B2(L_4, NULL);
		// if (densities == null || densities.Length == 0) { Debug.LogError($"An attempt was made to set the blade densities on \"{gameObject.name}\" to null or an empty array. Make sure the input blade densities are not null"); return; }
		return;
	}

IL_0027:
	{
		// if (densities.Length != grassBladeMeshes.Length) { System.Array.Resize(ref densities, grassBladeMeshes.Length); }
		SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* L_5 = ___densities0;
		NullCheck(L_5);
		MeshU5BU5D_t178CA36422FC397211E68FB7E39C5B2F95619689* L_6 = __this->___grassBladeMeshes_14;
		NullCheck(L_6);
		if ((((int32_t)((int32_t)(((RuntimeArray*)L_5)->max_length))) == ((int32_t)((int32_t)(((RuntimeArray*)L_6)->max_length)))))
		{
			goto IL_0043;
		}
	}
	{
		// if (densities.Length != grassBladeMeshes.Length) { System.Array.Resize(ref densities, grassBladeMeshes.Length); }
		MeshU5BU5D_t178CA36422FC397211E68FB7E39C5B2F95619689* L_7 = __this->___grassBladeMeshes_14;
		NullCheck(L_7);
		Array_Resize_TisSingle_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_m879C2A54DAFE78F46D1185B50ED527EE182BFB04((&___densities0), ((int32_t)(((RuntimeArray*)L_7)->max_length)), Array_Resize_TisSingle_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_m879C2A54DAFE78F46D1185B50ED527EE182BFB04_RuntimeMethod_var);
	}

IL_0043:
	{
		// meshDensityValues = densities;
		SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* L_8 = ___densities0;
		__this->___meshDensityValues_15 = L_8;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___meshDensityValues_15), (void*)L_8);
		// }
		return;
	}
}
// System.Void MicahW.PointGrass.PointGrassRenderer::SetMaterial(UnityEngine.Material)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointGrassRenderer_SetMaterial_mF9A26D70C3BC4E8BDE7F1E2D8BDF0E796EDB6BCF (PointGrassRenderer_tCAB9C932B5AEF5960C04E43451965E31C6A471C2* __this, Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___mat0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDC2F51E5A87BA3DD01A3464780589AFB8E1761AA);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralEC5CDEAF8345F709CD3D3E03D811C6FE724AD794);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (mat == null) { Debug.LogError($"An attempt was made to set the blade material on \"{gameObject.name}\" to null or an empty array. Make sure the input blade material is not null"); return; }
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_0 = ___mat0;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Equality_mB6120F782D83091EF56A198FCEBCF066DB4A9605(L_0, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_1)
		{
			goto IL_0029;
		}
	}
	{
		// if (mat == null) { Debug.LogError($"An attempt was made to set the blade material on \"{gameObject.name}\" to null or an empty array. Make sure the input blade material is not null"); return; }
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2;
		L_2 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(__this, NULL);
		NullCheck(L_2);
		String_t* L_3;
		L_3 = Object_get_name_mAC2F6B897CF1303BA4249B4CB55271AFACBB6392(L_2, NULL);
		String_t* L_4;
		L_4 = String_Concat_m8855A6DE10F84DA7F4EC113CADDB59873A25573B(_stringLiteralEC5CDEAF8345F709CD3D3E03D811C6FE724AD794, L_3, _stringLiteralDC2F51E5A87BA3DD01A3464780589AFB8E1761AA, NULL);
		il2cpp_codegen_runtime_class_init_inline(Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		Debug_LogError_mB00B2B4468EF3CAF041B038D840820FB84C924B2(L_4, NULL);
		// if (mat == null) { Debug.LogError($"An attempt was made to set the blade material on \"{gameObject.name}\" to null or an empty array. Make sure the input blade material is not null"); return; }
		return;
	}

IL_0029:
	{
		// multipleMaterials = false;
		__this->___multipleMaterials_16 = (bool)0;
		// material = mat;
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_5 = ___mat0;
		__this->___material_17 = L_5;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___material_17), (void*)L_5);
		// }
		return;
	}
}
// System.Void MicahW.PointGrass.PointGrassRenderer::SetMaterials(UnityEngine.Material[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointGrassRenderer_SetMaterials_mF165DA9F1096738EBA94283B8AF3C7BDA4F02DAC (PointGrassRenderer_tCAB9C932B5AEF5960C04E43451965E31C6A471C2* __this, MaterialU5BU5D_t2B1D11C42DB07A4400C0535F92DBB87A2E346D3D* ___materials0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Array_Resize_TisMaterial_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3_m79C251DDDBE015E6DB85FE487F8748D4613BCC7C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral28CD797346D19735D91A839AA66F72C5C932CF79);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral4AFB7467DBFB075C643B3F66B3583BE23188AF8B);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (materials == null || materials.Length == 0) { Debug.LogError($"An attempt was made to set the blade materials on \"{gameObject.name}\" to null or an empty array. Make sure the input blade materials are not null"); return; }
		MaterialU5BU5D_t2B1D11C42DB07A4400C0535F92DBB87A2E346D3D* L_0 = ___materials0;
		if (!L_0)
		{
			goto IL_0007;
		}
	}
	{
		MaterialU5BU5D_t2B1D11C42DB07A4400C0535F92DBB87A2E346D3D* L_1 = ___materials0;
		NullCheck(L_1);
		if ((((RuntimeArray*)L_1)->max_length))
		{
			goto IL_0027;
		}
	}

IL_0007:
	{
		// if (materials == null || materials.Length == 0) { Debug.LogError($"An attempt was made to set the blade materials on \"{gameObject.name}\" to null or an empty array. Make sure the input blade materials are not null"); return; }
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2;
		L_2 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(__this, NULL);
		NullCheck(L_2);
		String_t* L_3;
		L_3 = Object_get_name_mAC2F6B897CF1303BA4249B4CB55271AFACBB6392(L_2, NULL);
		String_t* L_4;
		L_4 = String_Concat_m8855A6DE10F84DA7F4EC113CADDB59873A25573B(_stringLiteral4AFB7467DBFB075C643B3F66B3583BE23188AF8B, L_3, _stringLiteral28CD797346D19735D91A839AA66F72C5C932CF79, NULL);
		il2cpp_codegen_runtime_class_init_inline(Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		Debug_LogError_mB00B2B4468EF3CAF041B038D840820FB84C924B2(L_4, NULL);
		// if (materials == null || materials.Length == 0) { Debug.LogError($"An attempt was made to set the blade materials on \"{gameObject.name}\" to null or an empty array. Make sure the input blade materials are not null"); return; }
		return;
	}

IL_0027:
	{
		// if (materials.Length == 1) { SetMaterial(materials[0]); }
		MaterialU5BU5D_t2B1D11C42DB07A4400C0535F92DBB87A2E346D3D* L_5 = ___materials0;
		NullCheck(L_5);
		if ((!(((uint32_t)((int32_t)(((RuntimeArray*)L_5)->max_length))) == ((uint32_t)1))))
		{
			goto IL_0037;
		}
	}
	{
		// if (materials.Length == 1) { SetMaterial(materials[0]); }
		MaterialU5BU5D_t2B1D11C42DB07A4400C0535F92DBB87A2E346D3D* L_6 = ___materials0;
		NullCheck(L_6);
		int32_t L_7 = 0;
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_8 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		PointGrassRenderer_SetMaterial_mF9A26D70C3BC4E8BDE7F1E2D8BDF0E796EDB6BCF(__this, L_8, NULL);
		return;
	}

IL_0037:
	{
		// multipleMaterials = true;
		__this->___multipleMaterials_16 = (bool)1;
		// if (materials.Length != grassBladeMeshes.Length) { System.Array.Resize(ref materials, grassBladeMeshes.Length); }
		MaterialU5BU5D_t2B1D11C42DB07A4400C0535F92DBB87A2E346D3D* L_9 = ___materials0;
		NullCheck(L_9);
		MeshU5BU5D_t178CA36422FC397211E68FB7E39C5B2F95619689* L_10 = __this->___grassBladeMeshes_14;
		NullCheck(L_10);
		if ((((int32_t)((int32_t)(((RuntimeArray*)L_9)->max_length))) == ((int32_t)((int32_t)(((RuntimeArray*)L_10)->max_length)))))
		{
			goto IL_005a;
		}
	}
	{
		// if (materials.Length != grassBladeMeshes.Length) { System.Array.Resize(ref materials, grassBladeMeshes.Length); }
		MeshU5BU5D_t178CA36422FC397211E68FB7E39C5B2F95619689* L_11 = __this->___grassBladeMeshes_14;
		NullCheck(L_11);
		Array_Resize_TisMaterial_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3_m79C251DDDBE015E6DB85FE487F8748D4613BCC7C((&___materials0), ((int32_t)(((RuntimeArray*)L_11)->max_length)), Array_Resize_TisMaterial_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3_m79C251DDDBE015E6DB85FE487F8748D4613BCC7C_RuntimeMethod_var);
	}

IL_005a:
	{
		// this.materials = materials;
		MaterialU5BU5D_t2B1D11C42DB07A4400C0535F92DBB87A2E346D3D* L_12 = ___materials0;
		__this->___materials_18 = L_12;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___materials_18), (void*)L_12);
		// }
		return;
	}
}
// System.Void MicahW.PointGrass.PointGrassRenderer::SetShadowMode(UnityEngine.Rendering.ShadowCastingMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointGrassRenderer_SetShadowMode_mCA8965F6C3EF23B8E9298EBFBAB7E37642840709 (PointGrassRenderer_tCAB9C932B5AEF5960C04E43451965E31C6A471C2* __this, int32_t ___mode0, const RuntimeMethod* method) 
{
	{
		// public void SetShadowMode(UnityEngine.Rendering.ShadowCastingMode mode) { shadowMode = mode; }
		int32_t L_0 = ___mode0;
		__this->___shadowMode_19 = L_0;
		// public void SetShadowMode(UnityEngine.Rendering.ShadowCastingMode mode) { shadowMode = mode; }
		return;
	}
}
// System.Void MicahW.PointGrass.PointGrassRenderer::SetRenderLayer(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointGrassRenderer_SetRenderLayer_m8D795E6EC60BD400A501392384C25FBB52D15AA6 (PointGrassRenderer_tCAB9C932B5AEF5960C04E43451965E31C6A471C2* __this, int32_t ___layer0, const RuntimeMethod* method) 
{
	{
		// public void SetRenderLayer(int layer) { renderLayer.Set(layer); }
		SingleLayer_tA74696727907F9D2CD336CD4C7AA5C5C8C8FB4ED* L_0 = (&__this->___renderLayer_20);
		int32_t L_1 = ___layer0;
		SingleLayer_Set_mBBB701BA6BC74136FD35A221A1341CFECEB93BEB(L_0, L_1, NULL);
		// public void SetRenderLayer(int layer) { renderLayer.Set(layer); }
		return;
	}
}
// System.Void MicahW.PointGrass.PointGrassRenderer::SetRenderLayer(MicahW.PointGrass.SingleLayer)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointGrassRenderer_SetRenderLayer_m5E5C5358264C9EE01F09B2FDC581ED0396B14D03 (PointGrassRenderer_tCAB9C932B5AEF5960C04E43451965E31C6A471C2* __this, SingleLayer_tA74696727907F9D2CD336CD4C7AA5C5C8C8FB4ED ___layer0, const RuntimeMethod* method) 
{
	{
		// public void SetRenderLayer(SingleLayer layer) { renderLayer = layer; }
		SingleLayer_tA74696727907F9D2CD336CD4C7AA5C5C8C8FB4ED L_0 = ___layer0;
		__this->___renderLayer_20 = L_0;
		// public void SetRenderLayer(SingleLayer layer) { renderLayer = layer; }
		return;
	}
}
// System.Void MicahW.PointGrass.PointGrassRenderer::SetPointCount(System.Single,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointGrassRenderer_SetPointCount_mB91F62C99EF3847582EA4A991660AF9EF85909A6 (PointGrassRenderer_tCAB9C932B5AEF5960C04E43451965E31C6A471C2* __this, float ___count0, bool ___multiplyByArea1, const RuntimeMethod* method) 
{
	{
		// pointCount = count;
		float L_0 = ___count0;
		__this->___pointCount_21 = L_0;
		// this.multiplyByArea = multiplyByArea;
		bool L_1 = ___multiplyByArea1;
		__this->___multiplyByArea_22 = L_1;
		// }
		return;
	}
}
// System.Void MicahW.PointGrass.PointGrassRenderer::SetPointLODFactor(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointGrassRenderer_SetPointLODFactor_m6B56E6698E218C3A8EF8EBF54A6E5BA39F01974B (PointGrassRenderer_tCAB9C932B5AEF5960C04E43451965E31C6A471C2* __this, float ___value0, const RuntimeMethod* method) 
{
	{
		// public void SetPointLODFactor(float value) { pointLODFactor = Mathf.Clamp01(value); }
		float L_0 = ___value0;
		float L_1;
		L_1 = Mathf_Clamp01_mA7E048DBDA832D399A581BE4D6DED9FA44CE0F14_inline(L_0, NULL);
		__this->___pointLODFactor_23 = L_1;
		// public void SetPointLODFactor(float value) { pointLODFactor = Mathf.Clamp01(value); }
		return;
	}
}
// System.Void MicahW.PointGrass.PointGrassRenderer::SetSeed(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointGrassRenderer_SetSeed_m1E7BFC08547D043358AC9B37853C86B71F16A5AB (PointGrassRenderer_tCAB9C932B5AEF5960C04E43451965E31C6A471C2* __this, int32_t ___seed0, const RuntimeMethod* method) 
{
	{
		// public void SetSeed(int seed) { randomiseSeed = false; this.seed = seed; }
		__this->___randomiseSeed_24 = (bool)0;
		// public void SetSeed(int seed) { randomiseSeed = false; this.seed = seed; }
		int32_t L_0 = ___seed0;
		__this->___seed_25 = L_0;
		// public void SetSeed(int seed) { randomiseSeed = false; this.seed = seed; }
		return;
	}
}
// System.Void MicahW.PointGrass.PointGrassRenderer::SetSeed(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointGrassRenderer_SetSeed_m7006831D12C690BB89E3CAE12641BA53806EEB99 (PointGrassRenderer_tCAB9C932B5AEF5960C04E43451965E31C6A471C2* __this, bool ___randomise0, const RuntimeMethod* method) 
{
	{
		// public void SetSeed(bool randomise) { randomiseSeed = randomise; }
		bool L_0 = ___randomise0;
		__this->___randomiseSeed_24 = L_0;
		// public void SetSeed(bool randomise) { randomiseSeed = randomise; }
		return;
	}
}
// System.Void MicahW.PointGrass.PointGrassRenderer::SetOverwriteNormal(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointGrassRenderer_SetOverwriteNormal_m886584547687DBF3796D81D48CAFE6DD101B1DED (PointGrassRenderer_tCAB9C932B5AEF5960C04E43451965E31C6A471C2* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___normal0, const RuntimeMethod* method) 
{
	{
		// overwriteNormalDirection = true;
		__this->___overwriteNormalDirection_26 = (bool)1;
		// forcedNormal = normal.normalized;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0;
		L_0 = Vector3_get_normalized_m736BBF65D5CDA7A18414370D15B4DFCC1E466F07_inline((&___normal0), NULL);
		__this->___forcedNormal_27 = L_0;
		// }
		return;
	}
}
// System.Void MicahW.PointGrass.PointGrassRenderer::SetOverwriteNormal(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointGrassRenderer_SetOverwriteNormal_m90C4E02215600133BB9BE781320E40F6BF0C8432 (PointGrassRenderer_tCAB9C932B5AEF5960C04E43451965E31C6A471C2* __this, bool ___enabled0, const RuntimeMethod* method) 
{
	{
		// public void SetOverwriteNormal(bool enabled) { overwriteNormalDirection = enabled; }
		bool L_0 = ___enabled0;
		__this->___overwriteNormalDirection_26 = L_0;
		// public void SetOverwriteNormal(bool enabled) { overwriteNormalDirection = enabled; }
		return;
	}
}
// System.Void MicahW.PointGrass.PointGrassRenderer::SetDensity(System.Boolean,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointGrassRenderer_SetDensity_mEE9B214C95DD40BA047238BFF26605923DF0A922 (PointGrassRenderer_tCAB9C932B5AEF5960C04E43451965E31C6A471C2* __this, bool ___enabled0, float ___cutoff1, const RuntimeMethod* method) 
{
	{
		// useDensity = enabled;
		bool L_0 = ___enabled0;
		__this->___useDensity_28 = L_0;
		// densityCutoff = cutoff;
		float L_1 = ___cutoff1;
		__this->___densityCutoff_29 = L_1;
		// }
		return;
	}
}
// System.Void MicahW.PointGrass.PointGrassRenderer::SetLength(System.Boolean,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointGrassRenderer_SetLength_m60833ECF4D40582FCC58115819F40FFEB1B048E4 (PointGrassRenderer_tCAB9C932B5AEF5960C04E43451965E31C6A471C2* __this, bool ___enabled0, float ___rangeMin1, float ___rangeMax2, const RuntimeMethod* method) 
{
	{
		// useLength = enabled;
		bool L_0 = ___enabled0;
		__this->___useLength_30 = L_0;
		// lengthMapping = new Vector2(rangeMin, rangeMax);
		float L_1 = ___rangeMin1;
		float L_2 = ___rangeMax2;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_3;
		memset((&L_3), 0, sizeof(L_3));
		Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline((&L_3), L_1, L_2, /*hidden argument*/NULL);
		__this->___lengthMapping_31 = L_3;
		// }
		return;
	}
}
// System.Void MicahW.PointGrass.PointGrassRenderer::SetProjection(MicahW.PointGrass.PointGrassCommon/ProjectionType,UnityEngine.LayerMask)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointGrassRenderer_SetProjection_m3658FABFD119D3E8F6883AA55378A2270FC03F68 (PointGrassRenderer_tCAB9C932B5AEF5960C04E43451965E31C6A471C2* __this, int32_t ___type0, LayerMask_t97CB6BDADEDC3D6423C7BCFEA7F86DA2EC6241DB ___mask1, const RuntimeMethod* method) 
{
	{
		// projectType = type;
		int32_t L_0 = ___type0;
		__this->___projectType_38 = L_0;
		// projectMask = mask;
		LayerMask_t97CB6BDADEDC3D6423C7BCFEA7F86DA2EC6241DB L_1 = ___mask1;
		__this->___projectMask_39 = L_1;
		// }
		return;
	}
}
// System.Void MicahW.PointGrass.PointGrassRenderer::SetBoundingBoxOffset(UnityEngine.Bounds)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointGrassRenderer_SetBoundingBoxOffset_m343441B544ABFF460275045A9DC2FDFD753CF66A (PointGrassRenderer_tCAB9C932B5AEF5960C04E43451965E31C6A471C2* __this, Bounds_t367E830C64BBF235ED8C3B2F8CF6254FDCAD39C3 ___bounds0, const RuntimeMethod* method) 
{
	{
		// public void SetBoundingBoxOffset(Bounds bounds) { boundingBoxOffset = bounds; }
		Bounds_t367E830C64BBF235ED8C3B2F8CF6254FDCAD39C3 L_0 = ___bounds0;
		__this->___boundingBoxOffset_40 = L_0;
		// public void SetBoundingBoxOffset(Bounds bounds) { boundingBoxOffset = bounds; }
		return;
	}
}
// MicahW.PointGrass.PointGrassRenderer/DebugInformation MicahW.PointGrass.PointGrassRenderer::GetDebugInfo()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR DebugInformation_tEF4E30A924E4C1CAD82F63774AD5738CD5B726C5 PointGrassRenderer_GetDebugInfo_m03C7213070DB1796513F105275D6250450C9500E (PointGrassRenderer_tCAB9C932B5AEF5960C04E43451965E31C6A471C2* __this, const RuntimeMethod* method) 
{
	DebugInformation_tEF4E30A924E4C1CAD82F63774AD5738CD5B726C5 V_0;
	memset((&V_0), 0, sizeof(V_0));
	DebugInformation_tEF4E30A924E4C1CAD82F63774AD5738CD5B726C5 V_1;
	memset((&V_1), 0, sizeof(V_1));
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	{
		// if (!enabled) {
		bool L_0;
		L_0 = Behaviour_get_enabled_mAAC9F15E9EBF552217A5AE2681589CC0BFA300C1(__this, NULL);
		if (L_0)
		{
			goto IL_003a;
		}
	}
	{
		// return new DebugInformation() {
		//     totalPointCount = 0,
		//     usingMultipleBuffers = false,
		//     bufferCount = 0,
		//     smallestBuffer = 0,
		//     largestBuffer = 0
		// };
		il2cpp_codegen_initobj((&V_1), sizeof(DebugInformation_tEF4E30A924E4C1CAD82F63774AD5738CD5B726C5));
		(&V_1)->___totalPointCount_0 = 0;
		(&V_1)->___usingMultipleBuffers_1 = (bool)0;
		(&V_1)->___bufferCount_2 = 0;
		(&V_1)->___smallestBuffer_3 = 0;
		(&V_1)->___largestBuffer_4 = 0;
		DebugInformation_tEF4E30A924E4C1CAD82F63774AD5738CD5B726C5 L_1 = V_1;
		return L_1;
	}

IL_003a:
	{
		// DebugInformation info = new DebugInformation();
		il2cpp_codegen_initobj((&V_0), sizeof(DebugInformation_tEF4E30A924E4C1CAD82F63774AD5738CD5B726C5));
		// info.usingMultipleBuffers = pointBuffers != null;
		ComputeBufferU5BU5D_t7832804740B13E96807A836AD90ADF1477D7FE27* L_2 = __this->___pointBuffers_35;
		(&V_0)->___usingMultipleBuffers_1 = (bool)((!(((RuntimeObject*)(ComputeBufferU5BU5D_t7832804740B13E96807A836AD90ADF1477D7FE27*)L_2) <= ((RuntimeObject*)(RuntimeObject*)NULL)))? 1 : 0);
		// if (info.usingMultipleBuffers) {
		DebugInformation_tEF4E30A924E4C1CAD82F63774AD5738CD5B726C5 L_3 = V_0;
		bool L_4 = L_3.___usingMultipleBuffers_1;
		if (!L_4)
		{
			goto IL_00f5;
		}
	}
	{
		// int smallest = int.MaxValue;
		V_2 = ((int32_t)2147483647LL);
		// int largest = int.MinValue;
		V_3 = ((int32_t)-2147483648LL);
		// for (int i = 0; i < pointBuffers.Length; i++) {
		V_4 = 0;
		goto IL_00c8;
	}

IL_006e:
	{
		// if (pointBuffers[i] != null && pointBuffers[i].IsValid()) {
		ComputeBufferU5BU5D_t7832804740B13E96807A836AD90ADF1477D7FE27* L_5 = __this->___pointBuffers_35;
		int32_t L_6 = V_4;
		NullCheck(L_5);
		int32_t L_7 = L_6;
		ComputeBuffer_t51EADA9015EBCC1B982C5584E9AB2734415A8233* L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		if (!L_8)
		{
			goto IL_00c2;
		}
	}
	{
		ComputeBufferU5BU5D_t7832804740B13E96807A836AD90ADF1477D7FE27* L_9 = __this->___pointBuffers_35;
		int32_t L_10 = V_4;
		NullCheck(L_9);
		int32_t L_11 = L_10;
		ComputeBuffer_t51EADA9015EBCC1B982C5584E9AB2734415A8233* L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		NullCheck(L_12);
		bool L_13;
		L_13 = ComputeBuffer_IsValid_mC64E2E1D72BA70F04D177F82C3E258494B929B5D(L_12, NULL);
		if (!L_13)
		{
			goto IL_00c2;
		}
	}
	{
		// int count = pointBuffers[i].count;
		ComputeBufferU5BU5D_t7832804740B13E96807A836AD90ADF1477D7FE27* L_14 = __this->___pointBuffers_35;
		int32_t L_15 = V_4;
		NullCheck(L_14);
		int32_t L_16 = L_15;
		ComputeBuffer_t51EADA9015EBCC1B982C5584E9AB2734415A8233* L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		NullCheck(L_17);
		int32_t L_18;
		L_18 = ComputeBuffer_get_count_m4DAA2D2714BA7A46F007697F601E4446F1049506(L_17, NULL);
		V_5 = L_18;
		// info.totalPointCount += pointBuffers[i].count;
		int32_t* L_19 = (&(&V_0)->___totalPointCount_0);
		int32_t* L_20 = L_19;
		int32_t L_21 = *((int32_t*)L_20);
		ComputeBufferU5BU5D_t7832804740B13E96807A836AD90ADF1477D7FE27* L_22 = __this->___pointBuffers_35;
		int32_t L_23 = V_4;
		NullCheck(L_22);
		int32_t L_24 = L_23;
		ComputeBuffer_t51EADA9015EBCC1B982C5584E9AB2734415A8233* L_25 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		NullCheck(L_25);
		int32_t L_26;
		L_26 = ComputeBuffer_get_count_m4DAA2D2714BA7A46F007697F601E4446F1049506(L_25, NULL);
		*((int32_t*)L_20) = (int32_t)((int32_t)il2cpp_codegen_add(L_21, L_26));
		// if (count < smallest) { smallest = count; }
		int32_t L_27 = V_5;
		int32_t L_28 = V_2;
		if ((((int32_t)L_27) >= ((int32_t)L_28)))
		{
			goto IL_00ba;
		}
	}
	{
		// if (count < smallest) { smallest = count; }
		int32_t L_29 = V_5;
		V_2 = L_29;
	}

IL_00ba:
	{
		// if (count > largest) { largest = count; }
		int32_t L_30 = V_5;
		int32_t L_31 = V_3;
		if ((((int32_t)L_30) <= ((int32_t)L_31)))
		{
			goto IL_00c2;
		}
	}
	{
		// if (count > largest) { largest = count; }
		int32_t L_32 = V_5;
		V_3 = L_32;
	}

IL_00c2:
	{
		// for (int i = 0; i < pointBuffers.Length; i++) {
		int32_t L_33 = V_4;
		V_4 = ((int32_t)il2cpp_codegen_add(L_33, 1));
	}

IL_00c8:
	{
		// for (int i = 0; i < pointBuffers.Length; i++) {
		int32_t L_34 = V_4;
		ComputeBufferU5BU5D_t7832804740B13E96807A836AD90ADF1477D7FE27* L_35 = __this->___pointBuffers_35;
		NullCheck(L_35);
		if ((((int32_t)L_34) < ((int32_t)((int32_t)(((RuntimeArray*)L_35)->max_length)))))
		{
			goto IL_006e;
		}
	}
	{
		// info.bufferCount = pointBuffers.Length;
		ComputeBufferU5BU5D_t7832804740B13E96807A836AD90ADF1477D7FE27* L_36 = __this->___pointBuffers_35;
		NullCheck(L_36);
		(&V_0)->___bufferCount_2 = ((int32_t)(((RuntimeArray*)L_36)->max_length));
		// info.smallestBuffer = smallest;
		int32_t L_37 = V_2;
		(&V_0)->___smallestBuffer_3 = L_37;
		// info.largestBuffer = largest;
		int32_t L_38 = V_3;
		(&V_0)->___largestBuffer_4 = L_38;
		goto IL_013e;
	}

IL_00f5:
	{
		// else if (pointBuffer != null && pointBuffer.IsValid()) {
		ComputeBuffer_t51EADA9015EBCC1B982C5584E9AB2734415A8233* L_39 = __this->___pointBuffer_32;
		if (!L_39)
		{
			goto IL_013e;
		}
	}
	{
		ComputeBuffer_t51EADA9015EBCC1B982C5584E9AB2734415A8233* L_40 = __this->___pointBuffer_32;
		NullCheck(L_40);
		bool L_41;
		L_41 = ComputeBuffer_IsValid_mC64E2E1D72BA70F04D177F82C3E258494B929B5D(L_40, NULL);
		if (!L_41)
		{
			goto IL_013e;
		}
	}
	{
		// info.totalPointCount = pointBuffer.count;
		ComputeBuffer_t51EADA9015EBCC1B982C5584E9AB2734415A8233* L_42 = __this->___pointBuffer_32;
		NullCheck(L_42);
		int32_t L_43;
		L_43 = ComputeBuffer_get_count_m4DAA2D2714BA7A46F007697F601E4446F1049506(L_42, NULL);
		(&V_0)->___totalPointCount_0 = L_43;
		// info.bufferCount = 1;
		(&V_0)->___bufferCount_2 = 1;
		// info.smallestBuffer = info.totalPointCount;
		DebugInformation_tEF4E30A924E4C1CAD82F63774AD5738CD5B726C5 L_44 = V_0;
		int32_t L_45 = L_44.___totalPointCount_0;
		(&V_0)->___smallestBuffer_3 = L_45;
		// info.largestBuffer = info.totalPointCount;
		DebugInformation_tEF4E30A924E4C1CAD82F63774AD5738CD5B726C5 L_46 = V_0;
		int32_t L_47 = L_46.___totalPointCount_0;
		(&V_0)->___largestBuffer_4 = L_47;
	}

IL_013e:
	{
		// return info;
		DebugInformation_tEF4E30A924E4C1CAD82F63774AD5738CD5B726C5 L_48 = V_0;
		return L_48;
	}
}
// System.Void MicahW.PointGrass.PointGrassRenderer::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointGrassRenderer__ctor_mEE8F13109E74534D3943C869A8C52BB4AECF23FC (PointGrassRenderer_tCAB9C932B5AEF5960C04E43451965E31C6A471C2* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MaterialU5BU5D_t2B1D11C42DB07A4400C0535F92DBB87A2E346D3D_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MeshU5BU5D_t178CA36422FC397211E68FB7E39C5B2F95619689_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public Vector2Int chunkCount = new Vector2Int(8, 8);
		Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A L_0;
		memset((&L_0), 0, sizeof(L_0));
		Vector2Int__ctor_mC20D1312133EB8CB63EC11067088B043660F11CE_inline((&L_0), 8, 8, /*hidden argument*/NULL);
		__this->___chunkCount_8 = L_0;
		// public Mesh[] grassBladeMeshes = new Mesh[1];
		MeshU5BU5D_t178CA36422FC397211E68FB7E39C5B2F95619689* L_1 = (MeshU5BU5D_t178CA36422FC397211E68FB7E39C5B2F95619689*)(MeshU5BU5D_t178CA36422FC397211E68FB7E39C5B2F95619689*)SZArrayNew(MeshU5BU5D_t178CA36422FC397211E68FB7E39C5B2F95619689_il2cpp_TypeInfo_var, (uint32_t)1);
		__this->___grassBladeMeshes_14 = L_1;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___grassBladeMeshes_14), (void*)L_1);
		// public float[] meshDensityValues = new float[] { 1f };
		SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* L_2 = (SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C*)(SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C*)SZArrayNew(SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C_il2cpp_TypeInfo_var, (uint32_t)1);
		SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* L_3 = L_2;
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (float)(1.0f));
		__this->___meshDensityValues_15 = L_3;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___meshDensityValues_15), (void*)L_3);
		// public Material[] materials = new Material[1];
		MaterialU5BU5D_t2B1D11C42DB07A4400C0535F92DBB87A2E346D3D* L_4 = (MaterialU5BU5D_t2B1D11C42DB07A4400C0535F92DBB87A2E346D3D*)(MaterialU5BU5D_t2B1D11C42DB07A4400C0535F92DBB87A2E346D3D*)SZArrayNew(MaterialU5BU5D_t2B1D11C42DB07A4400C0535F92DBB87A2E346D3D_il2cpp_TypeInfo_var, (uint32_t)1);
		__this->___materials_18 = L_4;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___materials_18), (void*)L_4);
		// public UnityEngine.Rendering.ShadowCastingMode shadowMode = UnityEngine.Rendering.ShadowCastingMode.On;
		__this->___shadowMode_19 = 1;
		// public float pointCount = 1000f;
		__this->___pointCount_21 = (1000.0f);
		// [Range(0f, 1f)] public float pointLODFactor = 1f;
		__this->___pointLODFactor_23 = (1.0f);
		// public bool randomiseSeed = true;
		__this->___randomiseSeed_24 = (bool)1;
		// public Vector3 forcedNormal = Vector3.up;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_5;
		L_5 = Vector3_get_up_m128AF3FDC820BF59D5DE86D973E7DE3F20C3AEBA_inline(NULL);
		__this->___forcedNormal_27 = L_5;
		// public bool useDensity = true;
		__this->___useDensity_28 = (bool)1;
		// [Range(0f, 1f)] public float densityCutoff = 0.5f;
		__this->___densityCutoff_29 = (0.5f);
		// public bool useLength = true;
		__this->___useLength_30 = (bool)1;
		// public Vector2 lengthMapping = new Vector2(0f, 1f);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_6;
		memset((&L_6), 0, sizeof(L_6));
		Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline((&L_6), (0.0f), (1.0f), /*hidden argument*/NULL);
		__this->___lengthMapping_31 = L_6;
		// public LayerMask projectMask = ~0;
		LayerMask_t97CB6BDADEDC3D6423C7BCFEA7F86DA2EC6241DB L_7;
		L_7 = LayerMask_op_Implicit_m01C8996A2CB2085328B9C33539C43139660D8222((-1), NULL);
		__this->___projectMask_39 = L_7;
		// public Bounds boundingBoxOffset = new Bounds(Vector3.zero, Vector3.one);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_8;
		L_8 = Vector3_get_zero_m0C1249C3F25B1C70EAD3CC8B31259975A457AE39_inline(NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_9;
		L_9 = Vector3_get_one_mC9B289F1E15C42C597180C9FE6FB492495B51D02_inline(NULL);
		Bounds_t367E830C64BBF235ED8C3B2F8CF6254FDCAD39C3 L_10;
		memset((&L_10), 0, sizeof(L_10));
		Bounds__ctor_mAF7B238B9FBF90C495E5D7951760085A93119C5A((&L_10), L_8, L_9, /*hidden argument*/NULL);
		__this->___boundingBoxOffset_40 = L_10;
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
// System.Void MicahW.PointGrass.PointGrassRenderer::<CompatibilityCheck>g__Disable|43_0()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointGrassRenderer_U3CCompatibilityCheckU3Eg__DisableU7C43_0_mAD8E4C2F34F3754FDA2660945EF8B86F40B9AF3C (PointGrassRenderer_tCAB9C932B5AEF5960C04E43451965E31C6A471C2* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Destroy(this);
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		Object_Destroy_mE97D0A766419A81296E8D4E5C23D01D3FE91ACBB(__this, NULL);
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: MicahW.PointGrass.PointGrassRenderer/DebugInformation
IL2CPP_EXTERN_C void DebugInformation_tEF4E30A924E4C1CAD82F63774AD5738CD5B726C5_marshal_pinvoke(const DebugInformation_tEF4E30A924E4C1CAD82F63774AD5738CD5B726C5& unmarshaled, DebugInformation_tEF4E30A924E4C1CAD82F63774AD5738CD5B726C5_marshaled_pinvoke& marshaled)
{
	marshaled.___totalPointCount_0 = unmarshaled.___totalPointCount_0;
	marshaled.___usingMultipleBuffers_1 = static_cast<int32_t>(unmarshaled.___usingMultipleBuffers_1);
	marshaled.___bufferCount_2 = unmarshaled.___bufferCount_2;
	marshaled.___smallestBuffer_3 = unmarshaled.___smallestBuffer_3;
	marshaled.___largestBuffer_4 = unmarshaled.___largestBuffer_4;
}
IL2CPP_EXTERN_C void DebugInformation_tEF4E30A924E4C1CAD82F63774AD5738CD5B726C5_marshal_pinvoke_back(const DebugInformation_tEF4E30A924E4C1CAD82F63774AD5738CD5B726C5_marshaled_pinvoke& marshaled, DebugInformation_tEF4E30A924E4C1CAD82F63774AD5738CD5B726C5& unmarshaled)
{
	int32_t unmarshaledtotalPointCount_temp_0 = 0;
	unmarshaledtotalPointCount_temp_0 = marshaled.___totalPointCount_0;
	unmarshaled.___totalPointCount_0 = unmarshaledtotalPointCount_temp_0;
	bool unmarshaledusingMultipleBuffers_temp_1 = false;
	unmarshaledusingMultipleBuffers_temp_1 = static_cast<bool>(marshaled.___usingMultipleBuffers_1);
	unmarshaled.___usingMultipleBuffers_1 = unmarshaledusingMultipleBuffers_temp_1;
	int32_t unmarshaledbufferCount_temp_2 = 0;
	unmarshaledbufferCount_temp_2 = marshaled.___bufferCount_2;
	unmarshaled.___bufferCount_2 = unmarshaledbufferCount_temp_2;
	int32_t unmarshaledsmallestBuffer_temp_3 = 0;
	unmarshaledsmallestBuffer_temp_3 = marshaled.___smallestBuffer_3;
	unmarshaled.___smallestBuffer_3 = unmarshaledsmallestBuffer_temp_3;
	int32_t unmarshaledlargestBuffer_temp_4 = 0;
	unmarshaledlargestBuffer_temp_4 = marshaled.___largestBuffer_4;
	unmarshaled.___largestBuffer_4 = unmarshaledlargestBuffer_temp_4;
}
// Conversion method for clean up from marshalling of: MicahW.PointGrass.PointGrassRenderer/DebugInformation
IL2CPP_EXTERN_C void DebugInformation_tEF4E30A924E4C1CAD82F63774AD5738CD5B726C5_marshal_pinvoke_cleanup(DebugInformation_tEF4E30A924E4C1CAD82F63774AD5738CD5B726C5_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: MicahW.PointGrass.PointGrassRenderer/DebugInformation
IL2CPP_EXTERN_C void DebugInformation_tEF4E30A924E4C1CAD82F63774AD5738CD5B726C5_marshal_com(const DebugInformation_tEF4E30A924E4C1CAD82F63774AD5738CD5B726C5& unmarshaled, DebugInformation_tEF4E30A924E4C1CAD82F63774AD5738CD5B726C5_marshaled_com& marshaled)
{
	marshaled.___totalPointCount_0 = unmarshaled.___totalPointCount_0;
	marshaled.___usingMultipleBuffers_1 = static_cast<int32_t>(unmarshaled.___usingMultipleBuffers_1);
	marshaled.___bufferCount_2 = unmarshaled.___bufferCount_2;
	marshaled.___smallestBuffer_3 = unmarshaled.___smallestBuffer_3;
	marshaled.___largestBuffer_4 = unmarshaled.___largestBuffer_4;
}
IL2CPP_EXTERN_C void DebugInformation_tEF4E30A924E4C1CAD82F63774AD5738CD5B726C5_marshal_com_back(const DebugInformation_tEF4E30A924E4C1CAD82F63774AD5738CD5B726C5_marshaled_com& marshaled, DebugInformation_tEF4E30A924E4C1CAD82F63774AD5738CD5B726C5& unmarshaled)
{
	int32_t unmarshaledtotalPointCount_temp_0 = 0;
	unmarshaledtotalPointCount_temp_0 = marshaled.___totalPointCount_0;
	unmarshaled.___totalPointCount_0 = unmarshaledtotalPointCount_temp_0;
	bool unmarshaledusingMultipleBuffers_temp_1 = false;
	unmarshaledusingMultipleBuffers_temp_1 = static_cast<bool>(marshaled.___usingMultipleBuffers_1);
	unmarshaled.___usingMultipleBuffers_1 = unmarshaledusingMultipleBuffers_temp_1;
	int32_t unmarshaledbufferCount_temp_2 = 0;
	unmarshaledbufferCount_temp_2 = marshaled.___bufferCount_2;
	unmarshaled.___bufferCount_2 = unmarshaledbufferCount_temp_2;
	int32_t unmarshaledsmallestBuffer_temp_3 = 0;
	unmarshaledsmallestBuffer_temp_3 = marshaled.___smallestBuffer_3;
	unmarshaled.___smallestBuffer_3 = unmarshaledsmallestBuffer_temp_3;
	int32_t unmarshaledlargestBuffer_temp_4 = 0;
	unmarshaledlargestBuffer_temp_4 = marshaled.___largestBuffer_4;
	unmarshaled.___largestBuffer_4 = unmarshaledlargestBuffer_temp_4;
}
// Conversion method for clean up from marshalling of: MicahW.PointGrass.PointGrassRenderer/DebugInformation
IL2CPP_EXTERN_C void DebugInformation_tEF4E30A924E4C1CAD82F63774AD5738CD5B726C5_marshal_com_cleanup(DebugInformation_tEF4E30A924E4C1CAD82F63774AD5738CD5B726C5_marshaled_com& marshaled)
{
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Quaternion_get_eulerAngles_m2DB5158B5C3A71FD60FC8A6EE43D3AAA1CFED122_inline (Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974* __this, const RuntimeMethod* method) 
{
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_0 = (*(Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974*)__this);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_1;
		L_1 = Quaternion_Internal_ToEulerRad_m5BD0EEC543120C320DC77FCCDFD2CE2E6BD3F1A8(L_0, NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_2;
		L_2 = Vector3_op_Multiply_m87BA7C578F96C8E49BB07088DAAC4649F83B0353_inline(L_1, (57.2957802f), NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_3;
		L_3 = Quaternion_Internal_MakePositive_m73E2D01920CB0DFE661A55022C129E8617F0C9A8(L_2, NULL);
		V_0 = L_3;
		goto IL_001e;
	}

IL_001e:
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_4 = V_0;
		return L_4;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_get_zero_m0C1249C3F25B1C70EAD3CC8B31259975A457AE39_inline (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = ((Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_StaticFields*)il2cpp_codegen_static_fields_for(Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_il2cpp_TypeInfo_var))->___zeroVector_5;
		V_0 = L_0;
		goto IL_0009;
	}

IL_0009:
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_1 = V_0;
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* __this, float ___x0, float ___y1, const RuntimeMethod* method) 
{
	{
		float L_0 = ___x0;
		__this->___x_0 = L_0;
		float L_1 = ___y1;
		__this->___y_1 = L_1;
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method) 
{
	{
		float L_0 = ___x0;
		__this->___x_2 = L_0;
		float L_1 = ___y1;
		__this->___y_3 = L_1;
		float L_2 = ___z2;
		__this->___z_4 = L_2;
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_get_up_m128AF3FDC820BF59D5DE86D973E7DE3F20C3AEBA_inline (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = ((Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_StaticFields*)il2cpp_codegen_static_fields_for(Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_il2cpp_TypeInfo_var))->___upVector_7;
		V_0 = L_0;
		goto IL_0009;
	}

IL_0009:
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_1 = V_0;
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_get_right_mFF573AFBBB2186E7AFA1BA7CA271A78DF67E4EA0_inline (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = ((Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_StaticFields*)il2cpp_codegen_static_fields_for(Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_il2cpp_TypeInfo_var))->___rightVector_10;
		V_0 = L_0;
		goto IL_0009;
	}

IL_0009:
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_1 = V_0;
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 Quaternion_op_Multiply_mCB375FCCC12A2EC8F9EB824A1BFB4453B58C2012_inline (Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___lhs0, Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___rhs1, const RuntimeMethod* method) 
{
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_0 = ___lhs0;
		float L_1 = L_0.___w_3;
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_2 = ___rhs1;
		float L_3 = L_2.___x_0;
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_4 = ___lhs0;
		float L_5 = L_4.___x_0;
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_6 = ___rhs1;
		float L_7 = L_6.___w_3;
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_8 = ___lhs0;
		float L_9 = L_8.___y_1;
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_10 = ___rhs1;
		float L_11 = L_10.___z_2;
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_12 = ___lhs0;
		float L_13 = L_12.___z_2;
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_14 = ___rhs1;
		float L_15 = L_14.___y_1;
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_16 = ___lhs0;
		float L_17 = L_16.___w_3;
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_18 = ___rhs1;
		float L_19 = L_18.___y_1;
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_20 = ___lhs0;
		float L_21 = L_20.___y_1;
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_22 = ___rhs1;
		float L_23 = L_22.___w_3;
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_24 = ___lhs0;
		float L_25 = L_24.___z_2;
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_26 = ___rhs1;
		float L_27 = L_26.___x_0;
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_28 = ___lhs0;
		float L_29 = L_28.___x_0;
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_30 = ___rhs1;
		float L_31 = L_30.___z_2;
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_32 = ___lhs0;
		float L_33 = L_32.___w_3;
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_34 = ___rhs1;
		float L_35 = L_34.___z_2;
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_36 = ___lhs0;
		float L_37 = L_36.___z_2;
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_38 = ___rhs1;
		float L_39 = L_38.___w_3;
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_40 = ___lhs0;
		float L_41 = L_40.___x_0;
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_42 = ___rhs1;
		float L_43 = L_42.___y_1;
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_44 = ___lhs0;
		float L_45 = L_44.___y_1;
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_46 = ___rhs1;
		float L_47 = L_46.___x_0;
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_48 = ___lhs0;
		float L_49 = L_48.___w_3;
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_50 = ___rhs1;
		float L_51 = L_50.___w_3;
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_52 = ___lhs0;
		float L_53 = L_52.___x_0;
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_54 = ___rhs1;
		float L_55 = L_54.___x_0;
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_56 = ___lhs0;
		float L_57 = L_56.___y_1;
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_58 = ___rhs1;
		float L_59 = L_58.___y_1;
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_60 = ___lhs0;
		float L_61 = L_60.___z_2;
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_62 = ___rhs1;
		float L_63 = L_62.___z_2;
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_64;
		memset((&L_64), 0, sizeof(L_64));
		Quaternion__ctor_m868FD60AA65DD5A8AC0C5DEB0608381A8D85FCD8_inline((&L_64), ((float)il2cpp_codegen_subtract(((float)il2cpp_codegen_add(((float)il2cpp_codegen_add(((float)il2cpp_codegen_multiply(L_1, L_3)), ((float)il2cpp_codegen_multiply(L_5, L_7)))), ((float)il2cpp_codegen_multiply(L_9, L_11)))), ((float)il2cpp_codegen_multiply(L_13, L_15)))), ((float)il2cpp_codegen_subtract(((float)il2cpp_codegen_add(((float)il2cpp_codegen_add(((float)il2cpp_codegen_multiply(L_17, L_19)), ((float)il2cpp_codegen_multiply(L_21, L_23)))), ((float)il2cpp_codegen_multiply(L_25, L_27)))), ((float)il2cpp_codegen_multiply(L_29, L_31)))), ((float)il2cpp_codegen_subtract(((float)il2cpp_codegen_add(((float)il2cpp_codegen_add(((float)il2cpp_codegen_multiply(L_33, L_35)), ((float)il2cpp_codegen_multiply(L_37, L_39)))), ((float)il2cpp_codegen_multiply(L_41, L_43)))), ((float)il2cpp_codegen_multiply(L_45, L_47)))), ((float)il2cpp_codegen_subtract(((float)il2cpp_codegen_subtract(((float)il2cpp_codegen_subtract(((float)il2cpp_codegen_multiply(L_49, L_51)), ((float)il2cpp_codegen_multiply(L_53, L_55)))), ((float)il2cpp_codegen_multiply(L_57, L_59)))), ((float)il2cpp_codegen_multiply(L_61, L_63)))), /*hidden argument*/NULL);
		V_0 = L_64;
		goto IL_00e5;
	}

IL_00e5:
	{
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_65 = V_0;
		return L_65;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_op_Multiply_m87BA7C578F96C8E49BB07088DAAC4649F83B0353_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___a0, float ___d1, const RuntimeMethod* method) 
{
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = ___a0;
		float L_1 = L_0.___x_2;
		float L_2 = ___d1;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_3 = ___a0;
		float L_4 = L_3.___y_3;
		float L_5 = ___d1;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_6 = ___a0;
		float L_7 = L_6.___z_4;
		float L_8 = ___d1;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_9;
		memset((&L_9), 0, sizeof(L_9));
		Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline((&L_9), ((float)il2cpp_codegen_multiply(L_1, L_2)), ((float)il2cpp_codegen_multiply(L_4, L_5)), ((float)il2cpp_codegen_multiply(L_7, L_8)), /*hidden argument*/NULL);
		V_0 = L_9;
		goto IL_0021;
	}

IL_0021:
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_10 = V_0;
		return L_10;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_MoveTowards_m0363264647799F3173AC37F8E819F98298249B08_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___current0, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___target1, float ___maxDistanceDelta2, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Math_tEB65DE7CA8B083C412C969C92981C030865486CE_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	bool V_5 = false;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_6;
	memset((&V_6), 0, sizeof(V_6));
	int32_t G_B4_0 = 0;
	int32_t G_B6_0 = 0;
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = ___target1;
		float L_1 = L_0.___x_2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_2 = ___current0;
		float L_3 = L_2.___x_2;
		V_0 = ((float)il2cpp_codegen_subtract(L_1, L_3));
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_4 = ___target1;
		float L_5 = L_4.___y_3;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_6 = ___current0;
		float L_7 = L_6.___y_3;
		V_1 = ((float)il2cpp_codegen_subtract(L_5, L_7));
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_8 = ___target1;
		float L_9 = L_8.___z_4;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_10 = ___current0;
		float L_11 = L_10.___z_4;
		V_2 = ((float)il2cpp_codegen_subtract(L_9, L_11));
		float L_12 = V_0;
		float L_13 = V_0;
		float L_14 = V_1;
		float L_15 = V_1;
		float L_16 = V_2;
		float L_17 = V_2;
		V_3 = ((float)il2cpp_codegen_add(((float)il2cpp_codegen_add(((float)il2cpp_codegen_multiply(L_12, L_13)), ((float)il2cpp_codegen_multiply(L_14, L_15)))), ((float)il2cpp_codegen_multiply(L_16, L_17))));
		float L_18 = V_3;
		if ((((float)L_18) == ((float)(0.0f))))
		{
			goto IL_0055;
		}
	}
	{
		float L_19 = ___maxDistanceDelta2;
		if ((!(((float)L_19) >= ((float)(0.0f)))))
		{
			goto IL_0052;
		}
	}
	{
		float L_20 = V_3;
		float L_21 = ___maxDistanceDelta2;
		float L_22 = ___maxDistanceDelta2;
		G_B4_0 = ((((int32_t)((!(((float)L_20) <= ((float)((float)il2cpp_codegen_multiply(L_21, L_22)))))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0053;
	}

IL_0052:
	{
		G_B4_0 = 0;
	}

IL_0053:
	{
		G_B6_0 = G_B4_0;
		goto IL_0056;
	}

IL_0055:
	{
		G_B6_0 = 1;
	}

IL_0056:
	{
		V_5 = (bool)G_B6_0;
		bool L_23 = V_5;
		if (!L_23)
		{
			goto IL_0061;
		}
	}
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_24 = ___target1;
		V_6 = L_24;
		goto IL_009b;
	}

IL_0061:
	{
		float L_25 = V_3;
		il2cpp_codegen_runtime_class_init_inline(Math_tEB65DE7CA8B083C412C969C92981C030865486CE_il2cpp_TypeInfo_var);
		double L_26;
		L_26 = sqrt(((double)L_25));
		V_4 = ((float)L_26);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_27 = ___current0;
		float L_28 = L_27.___x_2;
		float L_29 = V_0;
		float L_30 = V_4;
		float L_31 = ___maxDistanceDelta2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_32 = ___current0;
		float L_33 = L_32.___y_3;
		float L_34 = V_1;
		float L_35 = V_4;
		float L_36 = ___maxDistanceDelta2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_37 = ___current0;
		float L_38 = L_37.___z_4;
		float L_39 = V_2;
		float L_40 = V_4;
		float L_41 = ___maxDistanceDelta2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_42;
		memset((&L_42), 0, sizeof(L_42));
		Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline((&L_42), ((float)il2cpp_codegen_add(L_28, ((float)il2cpp_codegen_multiply(((float)(L_29/L_30)), L_31)))), ((float)il2cpp_codegen_add(L_33, ((float)il2cpp_codegen_multiply(((float)(L_34/L_35)), L_36)))), ((float)il2cpp_codegen_add(L_38, ((float)il2cpp_codegen_multiply(((float)(L_39/L_40)), L_41)))), /*hidden argument*/NULL);
		V_6 = L_42;
		goto IL_009b;
	}

IL_009b:
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_43 = V_6;
		return L_43;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_op_Addition_m78C0EC70CB66E8DCAC225743D82B268DAEE92067_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___a0, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___b1, const RuntimeMethod* method) 
{
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = ___a0;
		float L_1 = L_0.___x_2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_2 = ___b1;
		float L_3 = L_2.___x_2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_4 = ___a0;
		float L_5 = L_4.___y_3;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_6 = ___b1;
		float L_7 = L_6.___y_3;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_8 = ___a0;
		float L_9 = L_8.___z_4;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_10 = ___b1;
		float L_11 = L_10.___z_4;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_12;
		memset((&L_12), 0, sizeof(L_12));
		Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline((&L_12), ((float)il2cpp_codegen_add(L_1, L_3)), ((float)il2cpp_codegen_add(L_5, L_7)), ((float)il2cpp_codegen_add(L_9, L_11)), /*hidden argument*/NULL);
		V_0 = L_12;
		goto IL_0030;
	}

IL_0030:
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_13 = V_0;
		return L_13;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3_Scale_mE0DC2C1B7902271788591F17DBE7F7F72EC37283_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___scale0, const RuntimeMethod* method) 
{
	{
		float L_0 = __this->___x_2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_1 = ___scale0;
		float L_2 = L_1.___x_2;
		__this->___x_2 = ((float)il2cpp_codegen_multiply(L_0, L_2));
		float L_3 = __this->___y_3;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_4 = ___scale0;
		float L_5 = L_4.___y_3;
		__this->___y_3 = ((float)il2cpp_codegen_multiply(L_3, L_5));
		float L_6 = __this->___z_4;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_7 = ___scale0;
		float L_8 = L_7.___z_4;
		__this->___z_4 = ((float)il2cpp_codegen_multiply(L_6, L_8));
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t Mathf_FloorToInt_m2A39AE881CAEE6B6A4B3BFEF9CA1ED40625F5AB7_inline (float ___f0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Math_tEB65DE7CA8B083C412C969C92981C030865486CE_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		float L_0 = ___f0;
		il2cpp_codegen_runtime_class_init_inline(Math_tEB65DE7CA8B083C412C969C92981C030865486CE_il2cpp_TypeInfo_var);
		double L_1;
		L_1 = floor(((double)L_0));
		V_0 = il2cpp_codegen_cast_double_to_int<int32_t>(L_1);
		goto IL_000c;
	}

IL_000c:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 Vector2_op_Multiply_m2D984B613020089BF5165BA4CA10988E2DC771FE_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___a0, float ___d1, const RuntimeMethod* method) 
{
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_0 = ___a0;
		float L_1 = L_0.___x_0;
		float L_2 = ___d1;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_3 = ___a0;
		float L_4 = L_3.___y_1;
		float L_5 = ___d1;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_6;
		memset((&L_6), 0, sizeof(L_6));
		Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline((&L_6), ((float)il2cpp_codegen_multiply(L_1, L_2)), ((float)il2cpp_codegen_multiply(L_4, L_5)), /*hidden argument*/NULL);
		V_0 = L_6;
		goto IL_0019;
	}

IL_0019:
	{
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_7 = V_0;
		return L_7;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 Vector2_op_Addition_m8136742CE6EE33BA4EB81C5F584678455917D2AE_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___a0, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___b1, const RuntimeMethod* method) 
{
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_0 = ___a0;
		float L_1 = L_0.___x_0;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_2 = ___b1;
		float L_3 = L_2.___x_0;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_4 = ___a0;
		float L_5 = L_4.___y_1;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_6 = ___b1;
		float L_7 = L_6.___y_1;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_8;
		memset((&L_8), 0, sizeof(L_8));
		Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline((&L_8), ((float)il2cpp_codegen_add(L_1, L_3)), ((float)il2cpp_codegen_add(L_5, L_7)), /*hidden argument*/NULL);
		V_0 = L_8;
		goto IL_0023;
	}

IL_0023:
	{
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_9 = V_0;
		return L_9;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 Vector4_op_Implicit_mB193CD8DA20DEB9E9F95CFEB5A2B1B9B3B7ECFEB_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___v0, const RuntimeMethod* method) 
{
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_0 = ___v0;
		float L_1 = L_0.___x_0;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_2 = ___v0;
		float L_3 = L_2.___y_1;
		Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 L_4;
		memset((&L_4), 0, sizeof(L_4));
		Vector4__ctor_m96B2CD8B862B271F513AF0BDC2EABD58E4DBC813_inline((&L_4), L_1, L_3, (0.0f), (0.0f), /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_001f;
	}

IL_001f:
	{
		Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 L_5 = V_0;
		return L_5;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Color_tD001788D726C3A7F1379BEED0260B9591F440C1F Color_op_Multiply_m379B20A820266ACF82A21425B9CAE8DCD773CFBB_inline (Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___a0, float ___b1, const RuntimeMethod* method) 
{
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_0 = ___a0;
		float L_1 = L_0.___r_0;
		float L_2 = ___b1;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_3 = ___a0;
		float L_4 = L_3.___g_1;
		float L_5 = ___b1;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_6 = ___a0;
		float L_7 = L_6.___b_2;
		float L_8 = ___b1;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_9 = ___a0;
		float L_10 = L_9.___a_3;
		float L_11 = ___b1;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_12;
		memset((&L_12), 0, sizeof(L_12));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_12), ((float)il2cpp_codegen_multiply(L_1, L_2)), ((float)il2cpp_codegen_multiply(L_4, L_5)), ((float)il2cpp_codegen_multiply(L_7, L_8)), ((float)il2cpp_codegen_multiply(L_10, L_11)), /*hidden argument*/NULL);
		V_0 = L_12;
		goto IL_0029;
	}

IL_0029:
	{
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_13 = V_0;
		return L_13;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Color_tD001788D726C3A7F1379BEED0260B9591F440C1F Color_op_Addition_mA7A51CACA49ED8D23D3D9CA3A0092D32F657E053_inline (Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___a0, Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___b1, const RuntimeMethod* method) 
{
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_0 = ___a0;
		float L_1 = L_0.___r_0;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_2 = ___b1;
		float L_3 = L_2.___r_0;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_4 = ___a0;
		float L_5 = L_4.___g_1;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_6 = ___b1;
		float L_7 = L_6.___g_1;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_8 = ___a0;
		float L_9 = L_8.___b_2;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_10 = ___b1;
		float L_11 = L_10.___b_2;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_12 = ___a0;
		float L_13 = L_12.___a_3;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_14 = ___b1;
		float L_15 = L_14.___a_3;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_16;
		memset((&L_16), 0, sizeof(L_16));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_16), ((float)il2cpp_codegen_add(L_1, L_3)), ((float)il2cpp_codegen_add(L_5, L_7)), ((float)il2cpp_codegen_add(L_9, L_11)), ((float)il2cpp_codegen_add(L_13, L_15)), /*hidden argument*/NULL);
		V_0 = L_16;
		goto IL_003d;
	}

IL_003d:
	{
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_17 = V_0;
		return L_17;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Color_tD001788D726C3A7F1379BEED0260B9591F440C1F Color_get_white_m068F5AF879B0FCA584E3693F762EA41BB65532C6_inline (const RuntimeMethod* method) 
{
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_0;
		memset((&L_0), 0, sizeof(L_0));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_0), (1.0f), (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_001d;
	}

IL_001d:
	{
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_1 = V_0;
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_op_Subtraction_mE42023FF80067CB44A1D4A27EB7CF2B24CABB828_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___a0, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___b1, const RuntimeMethod* method) 
{
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = ___a0;
		float L_1 = L_0.___x_2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_2 = ___b1;
		float L_3 = L_2.___x_2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_4 = ___a0;
		float L_5 = L_4.___y_3;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_6 = ___b1;
		float L_7 = L_6.___y_3;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_8 = ___a0;
		float L_9 = L_8.___z_4;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_10 = ___b1;
		float L_11 = L_10.___z_4;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_12;
		memset((&L_12), 0, sizeof(L_12));
		Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline((&L_12), ((float)il2cpp_codegen_subtract(L_1, L_3)), ((float)il2cpp_codegen_subtract(L_5, L_7)), ((float)il2cpp_codegen_subtract(L_9, L_11)), /*hidden argument*/NULL);
		V_0 = L_12;
		goto IL_0030;
	}

IL_0030:
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_13 = V_0;
		return L_13;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_Cross_mF93A280558BCE756D13B6CC5DCD7DE8A43148987_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___lhs0, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___rhs1, const RuntimeMethod* method) 
{
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = ___lhs0;
		float L_1 = L_0.___y_3;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_2 = ___rhs1;
		float L_3 = L_2.___z_4;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_4 = ___lhs0;
		float L_5 = L_4.___z_4;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_6 = ___rhs1;
		float L_7 = L_6.___y_3;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_8 = ___lhs0;
		float L_9 = L_8.___z_4;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_10 = ___rhs1;
		float L_11 = L_10.___x_2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_12 = ___lhs0;
		float L_13 = L_12.___x_2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_14 = ___rhs1;
		float L_15 = L_14.___z_4;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_16 = ___lhs0;
		float L_17 = L_16.___x_2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_18 = ___rhs1;
		float L_19 = L_18.___y_3;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_20 = ___lhs0;
		float L_21 = L_20.___y_3;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_22 = ___rhs1;
		float L_23 = L_22.___x_2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_24;
		memset((&L_24), 0, sizeof(L_24));
		Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline((&L_24), ((float)il2cpp_codegen_subtract(((float)il2cpp_codegen_multiply(L_1, L_3)), ((float)il2cpp_codegen_multiply(L_5, L_7)))), ((float)il2cpp_codegen_subtract(((float)il2cpp_codegen_multiply(L_9, L_11)), ((float)il2cpp_codegen_multiply(L_13, L_15)))), ((float)il2cpp_codegen_subtract(((float)il2cpp_codegen_multiply(L_17, L_19)), ((float)il2cpp_codegen_multiply(L_21, L_23)))), /*hidden argument*/NULL);
		V_0 = L_24;
		goto IL_005a;
	}

IL_005a:
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_25 = V_0;
		return L_25;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Vector3_get_magnitude_mF0D6017E90B345F1F52D1CC564C640F1A847AF2D_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Math_tEB65DE7CA8B083C412C969C92981C030865486CE_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		float L_0 = __this->___x_2;
		float L_1 = __this->___x_2;
		float L_2 = __this->___y_3;
		float L_3 = __this->___y_3;
		float L_4 = __this->___z_4;
		float L_5 = __this->___z_4;
		il2cpp_codegen_runtime_class_init_inline(Math_tEB65DE7CA8B083C412C969C92981C030865486CE_il2cpp_TypeInfo_var);
		double L_6;
		L_6 = sqrt(((double)((float)il2cpp_codegen_add(((float)il2cpp_codegen_add(((float)il2cpp_codegen_multiply(L_0, L_1)), ((float)il2cpp_codegen_multiply(L_2, L_3)))), ((float)il2cpp_codegen_multiply(L_4, L_5))))));
		V_0 = ((float)L_6);
		goto IL_0034;
	}

IL_0034:
	{
		float L_7 = V_0;
		return L_7;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool PointGrassCommon_get_PropertyIDsInitialized_m9958FC8398CDF68DA0FFF72DAA3C35469B07D1A1_inline (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static bool PropertyIDsInitialized { get; private set; }
		bool L_0 = ((PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_StaticFields*)il2cpp_codegen_static_fields_for(PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_il2cpp_TypeInfo_var))->___U3CPropertyIDsInitializedU3Ek__BackingField_7;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void PointGrassCommon_set_PropertyIDsInitialized_mAC9F656EF7F0EF2F47B26CD89EC6BE6CD235245D_inline (bool ___value0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static bool PropertyIDsInitialized { get; private set; }
		bool L_0 = ___value0;
		((PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_StaticFields*)il2cpp_codegen_static_fields_for(PointGrassCommon_t68D327C62EA51CB4C640E6F7B298138DB61D559A_il2cpp_TypeInfo_var))->___U3CPropertyIDsInitializedU3Ek__BackingField_7 = L_0;
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t Mathf_Max_m7FA442918DE37E3A00106D1F2E789D65829792B8_inline (int32_t ___a0, int32_t ___b1, const RuntimeMethod* method) 
{
	int32_t V_0 = 0;
	int32_t G_B3_0 = 0;
	{
		int32_t L_0 = ___a0;
		int32_t L_1 = ___b1;
		if ((((int32_t)L_0) > ((int32_t)L_1)))
		{
			goto IL_0008;
		}
	}
	{
		int32_t L_2 = ___b1;
		G_B3_0 = L_2;
		goto IL_0009;
	}

IL_0008:
	{
		int32_t L_3 = ___a0;
		G_B3_0 = L_3;
	}

IL_0009:
	{
		V_0 = G_B3_0;
		goto IL_000c;
	}

IL_000c:
	{
		int32_t L_4 = V_0;
		return L_4;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline (Color_tD001788D726C3A7F1379BEED0260B9591F440C1F* __this, float ___r0, float ___g1, float ___b2, float ___a3, const RuntimeMethod* method) 
{
	{
		float L_0 = ___r0;
		__this->___r_0 = L_0;
		float L_1 = ___g1;
		__this->___g_1 = L_1;
		float L_2 = ___b2;
		__this->___b_2 = L_2;
		float L_3 = ___a3;
		__this->___a_3 = L_3;
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_op_UnaryNegation_m5450829F333BD2A88AF9A592C4EE331661225915_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___a0, const RuntimeMethod* method) 
{
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = ___a0;
		float L_1 = L_0.___x_2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_2 = ___a0;
		float L_3 = L_2.___y_3;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_4 = ___a0;
		float L_5 = L_4.___z_4;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_6;
		memset((&L_6), 0, sizeof(L_6));
		Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline((&L_6), ((-L_1)), ((-L_3)), ((-L_5)), /*hidden argument*/NULL);
		V_0 = L_6;
		goto IL_001e;
	}

IL_001e:
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_7 = V_0;
		return L_7;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 Vector2_get_zero_m32506C40EC2EE7D5D4410BF40D3EE683A3D5F32C_inline (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_0 = ((Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_StaticFields*)il2cpp_codegen_static_fields_for(Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_il2cpp_TypeInfo_var))->___zeroVector_2;
		V_0 = L_0;
		goto IL_0009;
	}

IL_0009:
	{
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_1 = V_0;
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Mathf_Clamp01_mA7E048DBDA832D399A581BE4D6DED9FA44CE0F14_inline (float ___value0, const RuntimeMethod* method) 
{
	bool V_0 = false;
	float V_1 = 0.0f;
	bool V_2 = false;
	{
		float L_0 = ___value0;
		V_0 = (bool)((((float)L_0) < ((float)(0.0f)))? 1 : 0);
		bool L_1 = V_0;
		if (!L_1)
		{
			goto IL_0015;
		}
	}
	{
		V_1 = (0.0f);
		goto IL_002d;
	}

IL_0015:
	{
		float L_2 = ___value0;
		V_2 = (bool)((((float)L_2) > ((float)(1.0f)))? 1 : 0);
		bool L_3 = V_2;
		if (!L_3)
		{
			goto IL_0029;
		}
	}
	{
		V_1 = (1.0f);
		goto IL_002d;
	}

IL_0029:
	{
		float L_4 = ___value0;
		V_1 = L_4;
		goto IL_002d;
	}

IL_002d:
	{
		float L_5 = V_1;
		return L_5;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Color_tD001788D726C3A7F1379BEED0260B9591F440C1F Color_get_black_mB50217951591A045844C61E7FF31EEE3FEF16737_inline (const RuntimeMethod* method) 
{
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_0;
		memset((&L_0), 0, sizeof(L_0));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_0), (0.0f), (0.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_001d;
	}

IL_001d:
	{
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_1 = V_0;
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 Vector2_get_one_m9097EB8DC23C26118A591AF16702796C3EF51DFB_inline (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_0 = ((Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_StaticFields*)il2cpp_codegen_static_fields_for(Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_il2cpp_TypeInfo_var))->___oneVector_3;
		V_0 = L_0;
		goto IL_0009;
	}

IL_0009:
	{
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_1 = V_0;
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_Normalize_mEF8349CC39674236CFC694189AFD36E31F89AC8F_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___value0, const RuntimeMethod* method) 
{
	float V_0 = 0.0f;
	bool V_1 = false;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_2;
	memset((&V_2), 0, sizeof(V_2));
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = ___value0;
		float L_1;
		L_1 = Vector3_Magnitude_m21652D951393A3D7CE92CE40049A0E7F76544D1B_inline(L_0, NULL);
		V_0 = L_1;
		float L_2 = V_0;
		V_1 = (bool)((((float)L_2) > ((float)(9.99999975E-06f)))? 1 : 0);
		bool L_3 = V_1;
		if (!L_3)
		{
			goto IL_001e;
		}
	}
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_4 = ___value0;
		float L_5 = V_0;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_6;
		L_6 = Vector3_op_Division_mCC6BB24E372AB96B8380D1678446EF6A8BAE13BB_inline(L_4, L_5, NULL);
		V_2 = L_6;
		goto IL_0026;
	}

IL_001e:
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_7;
		L_7 = Vector3_get_zero_m0C1249C3F25B1C70EAD3CC8B31259975A457AE39_inline(NULL);
		V_2 = L_7;
		goto IL_0026;
	}

IL_0026:
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_8 = V_2;
		return L_8;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3_Normalize_mC749B887A4C74BA0A2E13E6377F17CCAEB0AADA8_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* __this, const RuntimeMethod* method) 
{
	float V_0 = 0.0f;
	bool V_1 = false;
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = (*(Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2*)__this);
		float L_1;
		L_1 = Vector3_Magnitude_m21652D951393A3D7CE92CE40049A0E7F76544D1B_inline(L_0, NULL);
		V_0 = L_1;
		float L_2 = V_0;
		V_1 = (bool)((((float)L_2) > ((float)(9.99999975E-06f)))? 1 : 0);
		bool L_3 = V_1;
		if (!L_3)
		{
			goto IL_002d;
		}
	}
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_4 = (*(Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2*)__this);
		float L_5 = V_0;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_6;
		L_6 = Vector3_op_Division_mCC6BB24E372AB96B8380D1678446EF6A8BAE13BB_inline(L_4, L_5, NULL);
		*(Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2*)__this = L_6;
		goto IL_0038;
	}

IL_002d:
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_7;
		L_7 = Vector3_get_zero_m0C1249C3F25B1C70EAD3CC8B31259975A457AE39_inline(NULL);
		*(Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2*)__this = L_7;
	}

IL_0038:
	{
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Mathf_Lerp_m47EF2FFB7647BD0A1FDC26DC03E28B19812139B5_inline (float ___a0, float ___b1, float ___t2, const RuntimeMethod* method) 
{
	float V_0 = 0.0f;
	{
		float L_0 = ___a0;
		float L_1 = ___b1;
		float L_2 = ___a0;
		float L_3 = ___t2;
		float L_4;
		L_4 = Mathf_Clamp01_mA7E048DBDA832D399A581BE4D6DED9FA44CE0F14_inline(L_3, NULL);
		V_0 = ((float)il2cpp_codegen_add(L_0, ((float)il2cpp_codegen_multiply(((float)il2cpp_codegen_subtract(L_1, L_2)), L_4))));
		goto IL_0010;
	}

IL_0010:
	{
		float L_5 = V_0;
		return L_5;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t SingleLayer_get_LayerIndex_mB5C8A986682EAAA66CD1371500373D28A93B727E_inline (SingleLayer_tA74696727907F9D2CD336CD4C7AA5C5C8C8FB4ED* __this, const RuntimeMethod* method) 
{
	{
		// public int LayerIndex => m_LayerIndex;
		int32_t L_0 = __this->___m_LayerIndex_0;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void SingleLayer__ctor_mB56173673F914A84F0A215F602C28EC44017CE31_inline (SingleLayer_tA74696727907F9D2CD336CD4C7AA5C5C8C8FB4ED* __this, int32_t ___layerIndex0, const RuntimeMethod* method) 
{
	{
		// public SingleLayer(int layerIndex) { m_LayerIndex = layerIndex; }
		int32_t L_0 = ___layerIndex0;
		__this->___m_LayerIndex_0 = L_0;
		// public SingleLayer(int layerIndex) { m_LayerIndex = layerIndex; }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t Mathf_Min_m888083F74FF5655778F0403BB5E9608BEFDEA8CB_inline (int32_t ___a0, int32_t ___b1, const RuntimeMethod* method) 
{
	int32_t V_0 = 0;
	int32_t G_B3_0 = 0;
	{
		int32_t L_0 = ___a0;
		int32_t L_1 = ___b1;
		if ((((int32_t)L_0) < ((int32_t)L_1)))
		{
			goto IL_0008;
		}
	}
	{
		int32_t L_2 = ___b1;
		G_B3_0 = L_2;
		goto IL_0009;
	}

IL_0008:
	{
		int32_t L_3 = ___a0;
		G_B3_0 = L_3;
	}

IL_0009:
	{
		V_0 = G_B3_0;
		goto IL_000c;
	}

IL_000c:
	{
		int32_t L_4 = V_0;
		return L_4;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void DisplacementDelegate_Invoke_m5D544DE149EA8535E783D20BD994198770497B8D_inline (DisplacementDelegate_t4C836C2D37D9FABEC9514A574F9425C4B15B981A* __this, PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900* ___manager0, const RuntimeMethod* method) 
{
	typedef void (*FunctionPointerType) (RuntimeObject*, PointGrassDisplacementManager_tFEF7E4E44E97BBC3C2D7513ED76BF5AC3AF81900*, const RuntimeMethod*);
	((FunctionPointerType)__this->___invoke_impl_1)((Il2CppObject*)__this->___method_code_6, ___manager0, reinterpret_cast<RuntimeMethod*>(__this->___method_3));
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 Vector4_op_Implicit_m2ECA73F345A7AD84144133E9E51657204002B12D_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___v0, const RuntimeMethod* method) 
{
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = ___v0;
		float L_1 = L_0.___x_2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_2 = ___v0;
		float L_3 = L_2.___y_3;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_4 = ___v0;
		float L_5 = L_4.___z_4;
		Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 L_6;
		memset((&L_6), 0, sizeof(L_6));
		Vector4__ctor_m96B2CD8B862B271F513AF0BDC2EABD58E4DBC813_inline((&L_6), L_1, L_3, L_5, (0.0f), /*hidden argument*/NULL);
		V_0 = L_6;
		goto IL_0020;
	}

IL_0020:
	{
		Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 L_7 = V_0;
		return L_7;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_get_normalized_m736BBF65D5CDA7A18414370D15B4DFCC1E466F07_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* __this, const RuntimeMethod* method) 
{
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = (*(Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2*)__this);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_1;
		L_1 = Vector3_Normalize_mEF8349CC39674236CFC694189AFD36E31F89AC8F_inline(L_0, NULL);
		V_0 = L_1;
		goto IL_000f;
	}

IL_000f:
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_2 = V_0;
		return L_2;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t Vector2Int_get_x_mA2CACB1B6E6B5AD0CCC32B2CD2EDCE3ECEB50576_inline (Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A* __this, const RuntimeMethod* method) 
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->___m_X_0;
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t Vector2Int_get_y_m48454163ECF0B463FB5A16A0C4FC4B14DB0768B3_inline (Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A* __this, const RuntimeMethod* method) 
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->___m_Y_1;
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector2Int__ctor_mC20D1312133EB8CB63EC11067088B043660F11CE_inline (Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A* __this, int32_t ___x0, int32_t ___y1, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = ___x0;
		__this->___m_X_0 = L_0;
		int32_t L_1 = ___y1;
		__this->___m_Y_1 = L_1;
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A Vector2Int_get_zero_mF92C338E9CB9434105090E675E04D20A29649553_inline (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A L_0 = ((Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A_StaticFields*)il2cpp_codegen_static_fields_for(Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A_il2cpp_TypeInfo_var))->___s_Zero_2;
		V_0 = L_0;
		goto IL_0009;
	}

IL_0009:
	{
		Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A L_1 = V_0;
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t Mathf_CeilToInt_mF2BF9F4261B3431DC20E10A46CFEEED103C48963_inline (float ___f0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Math_tEB65DE7CA8B083C412C969C92981C030865486CE_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		float L_0 = ___f0;
		il2cpp_codegen_runtime_class_init_inline(Math_tEB65DE7CA8B083C412C969C92981C030865486CE_il2cpp_TypeInfo_var);
		double L_1;
		L_1 = ceil(((double)L_0));
		V_0 = il2cpp_codegen_cast_double_to_int<int32_t>(L_1);
		goto IL_000c;
	}

IL_000c:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t Mathf_RoundToInt_m60F8B66CF27F1FA75AA219342BD184B75771EB4B_inline (float ___f0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Math_tEB65DE7CA8B083C412C969C92981C030865486CE_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		float L_0 = ___f0;
		il2cpp_codegen_runtime_class_init_inline(Math_tEB65DE7CA8B083C412C969C92981C030865486CE_il2cpp_TypeInfo_var);
		double L_1;
		L_1 = bankers_round(((double)L_0));
		V_0 = il2cpp_codegen_cast_double_to_int<int32_t>(L_1);
		goto IL_000c;
	}

IL_000c:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_get_one_mC9B289F1E15C42C597180C9FE6FB492495B51D02_inline (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = ((Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_StaticFields*)il2cpp_codegen_static_fields_for(Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_il2cpp_TypeInfo_var))->___oneVector_6;
		V_0 = L_0;
		goto IL_0009;
	}

IL_0009:
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_1 = V_0;
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void PointGrassRenderer_SetBladeType_mF4BC8DC77A9FC34419A19830C292A4C415A6C805_inline (PointGrassRenderer_tCAB9C932B5AEF5960C04E43451965E31C6A471C2* __this, int32_t ___type0, const RuntimeMethod* method) 
{
	{
		// public void SetBladeType(BladeType type) { bladeType = type; }
		int32_t L_0 = ___type0;
		__this->___bladeType_11 = L_0;
		// public void SetBladeType(BladeType type) { bladeType = type; }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool Nullable_1_get_HasValue_m6B76086B0E863AB1D634FD03E30154F230070435_gshared_inline (Nullable_1_t9C51B084784B716FFF4ED4575C63CFD8A71A86FE* __this, const RuntimeMethod* method) 
{
	{
		bool L_0 = (bool)__this->___hasValue_0;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m4407E4C389F22B8CEC282C15D56516658746C383_gshared_inline (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = (int32_t)__this->____size_2;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void List_1_Clear_m16C1F2C61FED5955F10EB36BC1CB2DF34B128994_gshared_inline (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, const RuntimeMethod* method) 
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->____version_3;
		__this->____version_3 = ((int32_t)il2cpp_codegen_add(L_0, 1));
		if (!true)
		{
			goto IL_0035;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->____size_2;
		V_0 = L_1;
		__this->____size_2 = 0;
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) <= ((int32_t)0)))
		{
			goto IL_003c;
		}
	}
	{
		ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* L_3 = (ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918*)__this->____items_1;
		int32_t L_4 = V_0;
		Array_Clear_m50BAA3751899858B097D3FF2ED31F284703FE5CB((RuntimeArray*)L_3, 0, L_4, NULL);
		return;
	}

IL_0035:
	{
		__this->____size_2 = 0;
	}

IL_003c:
	{
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void List_1_Add_mEBCF994CC3814631017F46A387B1A192ED6C85C7_gshared_inline (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, RuntimeObject* ___item0, const RuntimeMethod* method) 
{
	ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* V_0 = NULL;
	int32_t V_1 = 0;
	{
		int32_t L_0 = (int32_t)__this->____version_3;
		__this->____version_3 = ((int32_t)il2cpp_codegen_add(L_0, 1));
		ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* L_1 = (ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918*)__this->____items_1;
		V_0 = L_1;
		int32_t L_2 = (int32_t)__this->____size_2;
		V_1 = L_2;
		int32_t L_3 = V_1;
		ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* L_4 = V_0;
		NullCheck(L_4);
		if ((!(((uint32_t)L_3) < ((uint32_t)((int32_t)(((RuntimeArray*)L_4)->max_length))))))
		{
			goto IL_0034;
		}
	}
	{
		int32_t L_5 = V_1;
		__this->____size_2 = ((int32_t)il2cpp_codegen_add(L_5, 1));
		ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* L_6 = V_0;
		int32_t L_7 = V_1;
		RuntimeObject* L_8 = ___item0;
		NullCheck(L_6);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(L_7), (RuntimeObject*)L_8);
		return;
	}

IL_0034:
	{
		RuntimeObject* L_9 = ___item0;
		((  void (*) (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D*, RuntimeObject*, const RuntimeMethod*))il2cpp_codegen_get_method_pointer(il2cpp_rgctx_method(method->klass->rgctx_data, 11)))(__this, L_9, il2cpp_rgctx_method(method->klass->rgctx_data, 11));
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void List_1_Add_m930CA3F1A016B820967E8D53B5D8B2EEB9F10DAC_gshared_inline (List_1_t7D73463EA17F4D29A37E8110AB60DAD5C80EBF65* __this, Bounds_t367E830C64BBF235ED8C3B2F8CF6254FDCAD39C3 ___item0, const RuntimeMethod* method) 
{
	BoundsU5BU5D_t45563ED55B62FA0536E8117454C56C0CFA5B25F5* V_0 = NULL;
	int32_t V_1 = 0;
	{
		int32_t L_0 = (int32_t)__this->____version_3;
		__this->____version_3 = ((int32_t)il2cpp_codegen_add(L_0, 1));
		BoundsU5BU5D_t45563ED55B62FA0536E8117454C56C0CFA5B25F5* L_1 = (BoundsU5BU5D_t45563ED55B62FA0536E8117454C56C0CFA5B25F5*)__this->____items_1;
		V_0 = L_1;
		int32_t L_2 = (int32_t)__this->____size_2;
		V_1 = L_2;
		int32_t L_3 = V_1;
		BoundsU5BU5D_t45563ED55B62FA0536E8117454C56C0CFA5B25F5* L_4 = V_0;
		NullCheck(L_4);
		if ((!(((uint32_t)L_3) < ((uint32_t)((int32_t)(((RuntimeArray*)L_4)->max_length))))))
		{
			goto IL_0034;
		}
	}
	{
		int32_t L_5 = V_1;
		__this->____size_2 = ((int32_t)il2cpp_codegen_add(L_5, 1));
		BoundsU5BU5D_t45563ED55B62FA0536E8117454C56C0CFA5B25F5* L_6 = V_0;
		int32_t L_7 = V_1;
		Bounds_t367E830C64BBF235ED8C3B2F8CF6254FDCAD39C3 L_8 = ___item0;
		NullCheck(L_6);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(L_7), (Bounds_t367E830C64BBF235ED8C3B2F8CF6254FDCAD39C3)L_8);
		return;
	}

IL_0034:
	{
		Bounds_t367E830C64BBF235ED8C3B2F8CF6254FDCAD39C3 L_9 = ___item0;
		((  void (*) (List_1_t7D73463EA17F4D29A37E8110AB60DAD5C80EBF65*, Bounds_t367E830C64BBF235ED8C3B2F8CF6254FDCAD39C3, const RuntimeMethod*))il2cpp_codegen_get_method_pointer(il2cpp_rgctx_method(method->klass->rgctx_data, 11)))(__this, L_9, il2cpp_rgctx_method(method->klass->rgctx_data, 11));
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Quaternion__ctor_m868FD60AA65DD5A8AC0C5DEB0608381A8D85FCD8_inline (Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974* __this, float ___x0, float ___y1, float ___z2, float ___w3, const RuntimeMethod* method) 
{
	{
		float L_0 = ___x0;
		__this->___x_0 = L_0;
		float L_1 = ___y1;
		__this->___y_1 = L_1;
		float L_2 = ___z2;
		__this->___z_2 = L_2;
		float L_3 = ___w3;
		__this->___w_3 = L_3;
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector4__ctor_m96B2CD8B862B271F513AF0BDC2EABD58E4DBC813_inline (Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3* __this, float ___x0, float ___y1, float ___z2, float ___w3, const RuntimeMethod* method) 
{
	{
		float L_0 = ___x0;
		__this->___x_1 = L_0;
		float L_1 = ___y1;
		__this->___y_2 = L_1;
		float L_2 = ___z2;
		__this->___z_3 = L_2;
		float L_3 = ___w3;
		__this->___w_4 = L_3;
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Vector3_Magnitude_m21652D951393A3D7CE92CE40049A0E7F76544D1B_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___vector0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Math_tEB65DE7CA8B083C412C969C92981C030865486CE_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = ___vector0;
		float L_1 = L_0.___x_2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_2 = ___vector0;
		float L_3 = L_2.___x_2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_4 = ___vector0;
		float L_5 = L_4.___y_3;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_6 = ___vector0;
		float L_7 = L_6.___y_3;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_8 = ___vector0;
		float L_9 = L_8.___z_4;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_10 = ___vector0;
		float L_11 = L_10.___z_4;
		il2cpp_codegen_runtime_class_init_inline(Math_tEB65DE7CA8B083C412C969C92981C030865486CE_il2cpp_TypeInfo_var);
		double L_12;
		L_12 = sqrt(((double)((float)il2cpp_codegen_add(((float)il2cpp_codegen_add(((float)il2cpp_codegen_multiply(L_1, L_3)), ((float)il2cpp_codegen_multiply(L_5, L_7)))), ((float)il2cpp_codegen_multiply(L_9, L_11))))));
		V_0 = ((float)L_12);
		goto IL_0034;
	}

IL_0034:
	{
		float L_13 = V_0;
		return L_13;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_op_Division_mCC6BB24E372AB96B8380D1678446EF6A8BAE13BB_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___a0, float ___d1, const RuntimeMethod* method) 
{
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = ___a0;
		float L_1 = L_0.___x_2;
		float L_2 = ___d1;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_3 = ___a0;
		float L_4 = L_3.___y_3;
		float L_5 = ___d1;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_6 = ___a0;
		float L_7 = L_6.___z_4;
		float L_8 = ___d1;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_9;
		memset((&L_9), 0, sizeof(L_9));
		Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline((&L_9), ((float)(L_1/L_2)), ((float)(L_4/L_5)), ((float)(L_7/L_8)), /*hidden argument*/NULL);
		V_0 = L_9;
		goto IL_0021;
	}

IL_0021:
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_10 = V_0;
		return L_10;
	}
}
