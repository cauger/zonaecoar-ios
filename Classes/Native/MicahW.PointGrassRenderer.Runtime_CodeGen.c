﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void MicahW.PointGrass.FreeCam::Start()
extern void FreeCam_Start_m3F115537F67F9ECAEDFC485B8C64E5984ED93348 (void);
// 0x00000002 System.Void MicahW.PointGrass.FreeCam::Update()
extern void FreeCam_Update_m87790A0FA44364BB417773C383A66444BF47B72C (void);
// 0x00000003 System.Void MicahW.PointGrass.FreeCam::.ctor()
extern void FreeCam__ctor_m48EBAA195DB00803549D380950ABCDB9CC9433B2 (void);
// 0x00000004 System.Void MicahW.PointGrass.ObjectOscillator::Start()
extern void ObjectOscillator_Start_mF5D4EF734128FE6DCBDA83C5AC82865A1523F859 (void);
// 0x00000005 System.Void MicahW.PointGrass.ObjectOscillator::Update()
extern void ObjectOscillator_Update_mC3721C9C8D8241120B5FFB7A287C6FD3F13DC815 (void);
// 0x00000006 System.Void MicahW.PointGrass.ObjectOscillator::.ctor()
extern void ObjectOscillator__ctor_m5E852F90A3D1DBDD07D28F92624934F31CDF9AD7 (void);
// 0x00000007 MicahW.PointGrass.PointGrassCommon/MeshPoint[] MicahW.PointGrass.DistributePointsAlongMesh::DistributePoints(MicahW.PointGrass.PointGrassCommon/MeshData,UnityEngine.Vector3,System.Single,System.Int32,System.Boolean,System.Nullable`1<UnityEngine.Vector3>,System.Boolean,System.Boolean,System.Boolean,System.Single,UnityEngine.Vector2)
extern void DistributePointsAlongMesh_DistributePoints_m697A346959B6378FF0BF569C2DD2C8189E81AAF4 (void);
// 0x00000008 MicahW.PointGrass.PointGrassCommon/MeshPoint[] MicahW.PointGrass.DistributePointsAlongMesh::DistributePoints_CPU(MicahW.PointGrass.PointGrassCommon/MeshData,System.Single[],System.Int32,System.Int32,System.Single,System.Nullable`1<UnityEngine.Vector3>,System.Boolean,System.Boolean)
extern void DistributePointsAlongMesh_DistributePoints_CPU_m7EDF68975B721F8590DB32548BFCA21B9593805D (void);
// 0x00000009 System.Single[] MicahW.PointGrass.DistributePointsAlongMesh::GetCumulativeTriSizes(System.Int32[],UnityEngine.Vector3[],UnityEngine.Vector2[],UnityEngine.Vector3,System.Boolean,System.Single&)
extern void DistributePointsAlongMesh_GetCumulativeTriSizes_m64D783A59608A96F2FABF19CA7D0BB72F399B18C (void);
// 0x0000000A System.Single[] MicahW.PointGrass.DistributePointsAlongMesh::GetTriSizes(System.Int32[],UnityEngine.Vector3[],UnityEngine.Vector3)
extern void DistributePointsAlongMesh_GetTriSizes_m8FCA660CA122706DD76F206211626C4421F8439B (void);
// 0x0000000B System.Single[] MicahW.PointGrass.DistributePointsAlongMesh::GetWeightedTriSizes(System.Int32[],UnityEngine.Vector3[],UnityEngine.Vector2[],UnityEngine.Vector3)
extern void DistributePointsAlongMesh_GetWeightedTriSizes_m7C97E84C6C9894F39FBE0A9E10D6581E58EF2C97 (void);
// 0x0000000C System.Int32 MicahW.PointGrass.DistributePointsAlongMesh::FindTriangleIndex(System.Single[],System.Single)
extern void DistributePointsAlongMesh_FindTriangleIndex_mFA277A2DD05D747D627B4504C44E8BED5F0F8DFA (void);
// 0x0000000D System.Boolean MicahW.PointGrass.PointGrassCommon::get_BladeMeshesGenerated()
extern void PointGrassCommon_get_BladeMeshesGenerated_m4E21A4A4AC4828F974670684D93241428FCBD935 (void);
// 0x0000000E System.Boolean MicahW.PointGrass.PointGrassCommon::get_PropertyIDsInitialized()
extern void PointGrassCommon_get_PropertyIDsInitialized_m9958FC8398CDF68DA0FFF72DAA3C35469B07D1A1 (void);
// 0x0000000F System.Void MicahW.PointGrass.PointGrassCommon::set_PropertyIDsInitialized(System.Boolean)
extern void PointGrassCommon_set_PropertyIDsInitialized_mAC9F656EF7F0EF2F47B26CD89EC6BE6CD235245D (void);
// 0x00000010 System.Void MicahW.PointGrass.PointGrassCommon::FindPropertyIDs()
extern void PointGrassCommon_FindPropertyIDs_m4B83779C3F568CC8BFEEFEC4FB0A50B525997711 (void);
// 0x00000011 System.Void MicahW.PointGrass.PointGrassCommon::UpdateMaterialPropertyBlock(UnityEngine.MaterialPropertyBlock&,UnityEngine.Transform)
extern void PointGrassCommon_UpdateMaterialPropertyBlock_mC29A761CEB43323C5150BA2BBFDB228ABA7427C6 (void);
// 0x00000012 System.Void MicahW.PointGrass.PointGrassCommon::GenerateGrassMeshes()
extern void PointGrassCommon_GenerateGrassMeshes_m1187A0027530C7B935B788DF2975503346224C28 (void);
// 0x00000013 UnityEngine.Mesh MicahW.PointGrass.PointGrassCommon::GenerateGrassMesh_Flat(System.Int32)
extern void PointGrassCommon_GenerateGrassMesh_Flat_m0B8138A7DAEFC9028E47E9D68ACCCC39A3B2E23B (void);
// 0x00000014 UnityEngine.Mesh MicahW.PointGrass.PointGrassCommon::GenerateGrassMesh_Cylinder(System.Int32,System.Int32)
extern void PointGrassCommon_GenerateGrassMesh_Cylinder_m3290C77744AA3E4E2C99868E8D082E81EC4A9D0F (void);
// 0x00000015 System.Void MicahW.PointGrass.PointGrassCommon::ProjectBaseMesh(MicahW.PointGrass.PointGrassCommon/MeshData&,UnityEngine.LayerMask,UnityEngine.Transform)
extern void PointGrassCommon_ProjectBaseMesh_m5562159B69B1FD110DEC077043E7DE5D4020F74B (void);
// 0x00000016 UnityEngine.Texture2D MicahW.PointGrass.PointGrassCommon::CreateTextureCopy(UnityEngine.Texture2D,System.Int32,System.Int32)
extern void PointGrassCommon_CreateTextureCopy_mADE8309152AFE527D5756F91C7491A9B0C2A8C1F (void);
// 0x00000017 System.Void MicahW.PointGrass.PointGrassCommon::CacheTerrainData(UnityEngine.TerrainData,UnityEngine.TerrainLayer[])
extern void PointGrassCommon_CacheTerrainData_mE54FEFBA4451B4B69F63A2617DA43149225A3EAE (void);
// 0x00000018 MicahW.PointGrass.PointGrassCommon/MeshData MicahW.PointGrass.PointGrassCommon::CreateMeshFromTerrainData(UnityEngine.TerrainData,System.Single,System.Int32,System.Int32,System.Int32,System.Int32)
extern void PointGrassCommon_CreateMeshFromTerrainData_mA5C0F6BD781BEB33C68AF5BF697E1FAE23002E55 (void);
// 0x00000019 MicahW.PointGrass.PointGrassCommon/MeshData MicahW.PointGrass.PointGrassCommon::CreateMeshFromFilters(UnityEngine.Transform,UnityEngine.MeshFilter[])
extern void PointGrassCommon_CreateMeshFromFilters_mD660AD50FC9DD73F259A0D447B2C689EF1A437B3 (void);
// 0x0000001A System.Void MicahW.PointGrass.PointGrassCommon::<GenerateGrassMesh_Cylinder>g__SetVert|17_0(System.Int32,UnityEngine.Vector3,MicahW.PointGrass.PointGrassCommon/<>c__DisplayClass17_0&)
extern void PointGrassCommon_U3CGenerateGrassMesh_CylinderU3Eg__SetVertU7C17_0_m9E44405726796B377C797C8B52794EF850E59ABD (void);
// 0x0000001B System.Void MicahW.PointGrass.PointGrassCommon/MeshPoint::.ctor(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Color,UnityEngine.Vector4)
extern void MeshPoint__ctor_m6C2D6745D2F6CAC65B54433FBFAB990C649DC31E (void);
// 0x0000001C System.Void MicahW.PointGrass.PointGrassCommon/ObjectData::.ctor(UnityEngine.Vector3,System.Single,System.Single)
extern void ObjectData__ctor_mB2378318DDD47DCB6AEC9D788C27A66A94AE84CC (void);
// 0x0000001D System.Void MicahW.PointGrass.PointGrassCommon/MeshData::.ctor(UnityEngine.Vector3[],UnityEngine.Vector3[],UnityEngine.Vector2[],UnityEngine.Color[],System.Int32[],UnityEngine.Vector2[])
extern void MeshData__ctor_m70ACE57271D2DB0D80D16027646899AFC5035A50 (void);
// 0x0000001E System.Void MicahW.PointGrass.PointGrassCommon/MeshData::.ctor(UnityEngine.Vector3[],UnityEngine.Vector3[],UnityEngine.Vector2[],System.Int32[],UnityEngine.Vector2[])
extern void MeshData__ctor_m4DDE83D4B889F4A8E8CB34522845BB26A776D752 (void);
// 0x0000001F System.Void MicahW.PointGrass.PointGrassCommon/MeshData::.ctor(UnityEngine.Vector3[],UnityEngine.Vector3[],UnityEngine.Vector2[],System.Int32[])
extern void MeshData__ctor_mA84B1EAEC99B47A4A6F11607DB3AA143B334926C (void);
// 0x00000020 System.Boolean MicahW.PointGrass.PointGrassCommon/MeshData::get_HasColours()
extern void MeshData_get_HasColours_m67226DD3676EBC3004861960060933A3EFB23FA2 (void);
// 0x00000021 System.Boolean MicahW.PointGrass.PointGrassCommon/MeshData::get_HasAttributes()
extern void MeshData_get_HasAttributes_m4F13A5EE919CA43A34BBE872AC66756434B30060 (void);
// 0x00000022 System.Void MicahW.PointGrass.PointGrassCommon/MeshData::RecalculateBounds()
extern void MeshData_RecalculateBounds_m23CC32EAA976F74CAD691ACF55DEDE635C504655 (void);
// 0x00000023 System.Void MicahW.PointGrass.PointGrassCommon/MeshData::RecalculateNormals()
extern void MeshData_RecalculateNormals_m3DB8055C317DE19049DCC6647BCCB199FAF40917 (void);
// 0x00000024 System.Void MicahW.PointGrass.PointGrassCommon/MeshData::ApplyDensityCutoff(System.Single)
extern void MeshData_ApplyDensityCutoff_m8F76018CA26B3621D4B38ED51A0EF5101D35B1E4 (void);
// 0x00000025 System.Void MicahW.PointGrass.PointGrassCommon/MeshData::ApplyLengthMapping(UnityEngine.Vector2)
extern void MeshData_ApplyLengthMapping_mCD709AEF3230ECE96D914FBDF433B3D1DF97C183 (void);
// 0x00000026 System.Void MicahW.PointGrass.PointGrassCommon/MeshData::.cctor()
extern void MeshData__cctor_m44C85DCB2151B6174F6AAEA659563486E628DC19 (void);
// 0x00000027 System.Int32 MicahW.PointGrass.SingleLayer::get_LayerIndex()
extern void SingleLayer_get_LayerIndex_mB5C8A986682EAAA66CD1371500373D28A93B727E (void);
// 0x00000028 System.Int32 MicahW.PointGrass.SingleLayer::get_Mask()
extern void SingleLayer_get_Mask_m46E3E593C1213BB974ABBCDD3A0FD09791DC1473 (void);
// 0x00000029 System.Void MicahW.PointGrass.SingleLayer::.ctor(System.Int32)
extern void SingleLayer__ctor_mB56173673F914A84F0A215F602C28EC44017CE31 (void);
// 0x0000002A System.Void MicahW.PointGrass.SingleLayer::Set(System.Int32)
extern void SingleLayer_Set_mBBB701BA6BC74136FD35A221A1341CFECEB93BEB (void);
// 0x0000002B System.Int32 MicahW.PointGrass.PointGrassDisplacementManager::get_DisplacerCount()
extern void PointGrassDisplacementManager_get_DisplacerCount_m0526FE46A67C96DC0238F4E9281DB7DAEFF83E66 (void);
// 0x0000002C System.Void MicahW.PointGrass.PointGrassDisplacementManager::add_OnInitialize(MicahW.PointGrass.PointGrassDisplacementManager/DisplacementDelegate)
extern void PointGrassDisplacementManager_add_OnInitialize_mD1CDC92403A311CF3BBA4C8913EF1BE50C48DD54 (void);
// 0x0000002D System.Void MicahW.PointGrass.PointGrassDisplacementManager::remove_OnInitialize(MicahW.PointGrass.PointGrassDisplacementManager/DisplacementDelegate)
extern void PointGrassDisplacementManager_remove_OnInitialize_m5F94865C071988D7AF7EF522E3ABFAFFABF7E6B0 (void);
// 0x0000002E System.Void MicahW.PointGrass.PointGrassDisplacementManager::Awake()
extern void PointGrassDisplacementManager_Awake_mAF13AF01B1DED72D347730F1F9B43FD0DC1A7E01 (void);
// 0x0000002F System.Void MicahW.PointGrass.PointGrassDisplacementManager::OnDisable()
extern void PointGrassDisplacementManager_OnDisable_m898149390DFAF08C40B552FAB668A5803BC82107 (void);
// 0x00000030 System.Void MicahW.PointGrass.PointGrassDisplacementManager::LateUpdate()
extern void PointGrassDisplacementManager_LateUpdate_m158729EC12F6BCD84E77ED5264A436B0E8341C7E (void);
// 0x00000031 System.Void MicahW.PointGrass.PointGrassDisplacementManager::UpdateBuffer()
extern void PointGrassDisplacementManager_UpdateBuffer_m852C3B19D6D618139E8B3913E875A1DA1A48996F (void);
// 0x00000032 System.Void MicahW.PointGrass.PointGrassDisplacementManager::AddDisplacer(MicahW.PointGrass.PointGrassDisplacer)
extern void PointGrassDisplacementManager_AddDisplacer_m34B41A7480D16398EC5BF58ECFB2A73F92EFEE6F (void);
// 0x00000033 System.Void MicahW.PointGrass.PointGrassDisplacementManager::RemoveDisplacer(MicahW.PointGrass.PointGrassDisplacer)
extern void PointGrassDisplacementManager_RemoveDisplacer_m610EACCC6E76F1C21D580178793521035C34B32A (void);
// 0x00000034 System.Void MicahW.PointGrass.PointGrassDisplacementManager::UpdatePropertyBlock(UnityEngine.MaterialPropertyBlock&)
extern void PointGrassDisplacementManager_UpdatePropertyBlock_mE705BEB14C825F65DBF87F0DA864B492DFBAC365 (void);
// 0x00000035 System.Void MicahW.PointGrass.PointGrassDisplacementManager::.ctor()
extern void PointGrassDisplacementManager__ctor_m613DF958029CBB7AE2CCBA0A61A7A9A07012EC7F (void);
// 0x00000036 System.Void MicahW.PointGrass.PointGrassDisplacementManager::.cctor()
extern void PointGrassDisplacementManager__cctor_m36A658B29ED41A6CF72524B532CF9A7CE0B03509 (void);
// 0x00000037 System.Void MicahW.PointGrass.PointGrassDisplacementManager/DisplacementDelegate::.ctor(System.Object,System.IntPtr)
extern void DisplacementDelegate__ctor_m5FC18C77D43CFAAC46030EE3782AB012C1852A2B (void);
// 0x00000038 System.Void MicahW.PointGrass.PointGrassDisplacementManager/DisplacementDelegate::Invoke(MicahW.PointGrass.PointGrassDisplacementManager)
extern void DisplacementDelegate_Invoke_m5D544DE149EA8535E783D20BD994198770497B8D (void);
// 0x00000039 System.IAsyncResult MicahW.PointGrass.PointGrassDisplacementManager/DisplacementDelegate::BeginInvoke(MicahW.PointGrass.PointGrassDisplacementManager,System.AsyncCallback,System.Object)
extern void DisplacementDelegate_BeginInvoke_m0E606E6CE383DFF7108CFBF1E37B6066114ABEBA (void);
// 0x0000003A System.Void MicahW.PointGrass.PointGrassDisplacementManager/DisplacementDelegate::EndInvoke(System.IAsyncResult)
extern void DisplacementDelegate_EndInvoke_m4269C29D065D5E2622B2F23184EF82132D62D8C5 (void);
// 0x0000003B System.Void MicahW.PointGrass.PointGrassDisplacementManager/<>c::.cctor()
extern void U3CU3Ec__cctor_m1164B4575D6E45ED9E21975B33041E40006D5D18 (void);
// 0x0000003C System.Void MicahW.PointGrass.PointGrassDisplacementManager/<>c::.ctor()
extern void U3CU3Ec__ctor_m81B88999DD9498F0E72DF82C81657C2AEB967FE3 (void);
// 0x0000003D MicahW.PointGrass.PointGrassCommon/ObjectData MicahW.PointGrass.PointGrassDisplacementManager/<>c::<UpdateBuffer>b__13_0(MicahW.PointGrass.PointGrassDisplacer)
extern void U3CU3Ec_U3CUpdateBufferU3Eb__13_0_m28DE49B834780F799169B47BA3327FE3FCFF9CFA (void);
// 0x0000003E System.Void MicahW.PointGrass.PointGrassDisplacer::Reset()
extern void PointGrassDisplacer_Reset_m372EADAF5A72A9C79F8B31C93AEDC70A66716764 (void);
// 0x0000003F System.Void MicahW.PointGrass.PointGrassDisplacer::OnEnable()
extern void PointGrassDisplacer_OnEnable_m24FE571048E2F0EB323702C02DD0C33EF143E448 (void);
// 0x00000040 System.Void MicahW.PointGrass.PointGrassDisplacer::OnDisable()
extern void PointGrassDisplacer_OnDisable_m0AA403F7A98845FEC1603359586985C16CEAF61E (void);
// 0x00000041 System.Void MicahW.PointGrass.PointGrassDisplacer::Initialize(MicahW.PointGrass.PointGrassDisplacementManager)
extern void PointGrassDisplacer_Initialize_m45044718CDB24241F017732A234C381653A0650A (void);
// 0x00000042 MicahW.PointGrass.PointGrassCommon/ObjectData MicahW.PointGrass.PointGrassDisplacer::GetObjectData()
extern void PointGrassDisplacer_GetObjectData_m910E224560C96B24B45954527158E492A9CF4859 (void);
// 0x00000043 System.Void MicahW.PointGrass.PointGrassDisplacer::.ctor()
extern void PointGrassDisplacer__ctor_m1D4A021DA0DC02AD9821550EB8B99231E3F612C7 (void);
// 0x00000044 System.Void MicahW.PointGrass.PointGrassWind::OnEnable()
extern void PointGrassWind_OnEnable_m9D03B4E3F96CD5584E00FBA036873EA66302869C (void);
// 0x00000045 System.Void MicahW.PointGrass.PointGrassWind::LateUpdate()
extern void PointGrassWind_LateUpdate_m72593FADB6033D1FC96B9565E6AE8A669CC58895 (void);
// 0x00000046 System.Void MicahW.PointGrass.PointGrassWind::GetShaderIDs()
extern void PointGrassWind_GetShaderIDs_mF3766A5521D8AFCA5B72DD11BA5EA10B02FCC8C9 (void);
// 0x00000047 System.Void MicahW.PointGrass.PointGrassWind::RefreshValues()
extern void PointGrassWind_RefreshValues_m9AF23283B96B07EF4B748964644941DD789CB8EA (void);
// 0x00000048 MicahW.PointGrass.PointGrassWind/PackedProperties MicahW.PointGrass.PointGrassWind::PackProperties()
extern void PointGrassWind_PackProperties_mFEB1D1032F4CBADF707C35634FA8BE9169FF526B (void);
// 0x00000049 System.Void MicahW.PointGrass.PointGrassWind::.ctor()
extern void PointGrassWind__ctor_m30AEB1C86052693AFE52034CDDFE4C4ED39A0F9A (void);
// 0x0000004A System.Void MicahW.PointGrass.PointGrassWind/PackedProperties::.ctor(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector2,System.Single)
extern void PackedProperties__ctor_m1ADC969557DD708678FAFC6C730181CDAFEAC8A2 (void);
// 0x0000004B System.Boolean MicahW.PointGrass.PointGrassRenderer::get_UsingMultipleMeshes()
extern void PointGrassRenderer_get_UsingMultipleMeshes_m61A92AF3BC219534CD301983C34CECB07E6A3E59 (void);
// 0x0000004C System.Void MicahW.PointGrass.PointGrassRenderer::Reset()
extern void PointGrassRenderer_Reset_m56465633422BCAF773FAD656147169CBD7553DCA (void);
// 0x0000004D System.Void MicahW.PointGrass.PointGrassRenderer::OnEnable()
extern void PointGrassRenderer_OnEnable_mAAD03A50636AA34D588C4F740A42C7D0E0DD1083 (void);
// 0x0000004E System.Void MicahW.PointGrass.PointGrassRenderer::OnDisable()
extern void PointGrassRenderer_OnDisable_mD6B91B87B90C3A9D61CEA537026CCCFCDAC9DC28 (void);
// 0x0000004F System.Void MicahW.PointGrass.PointGrassRenderer::LateUpdate()
extern void PointGrassRenderer_LateUpdate_mDB02656F534CADAF622888481915E2866BF206E2 (void);
// 0x00000050 System.Boolean MicahW.PointGrass.PointGrassRenderer::CompatibilityCheck()
extern void PointGrassRenderer_CompatibilityCheck_mF24B3CB5752F3B46635B710E9399DC0169EDC298 (void);
// 0x00000051 System.Void MicahW.PointGrass.PointGrassRenderer::BuildPoints()
extern void PointGrassRenderer_BuildPoints_m22ADAC358BA12DDC83B6F4E9AAB9317E4A53FA13 (void);
// 0x00000052 System.Void MicahW.PointGrass.PointGrassRenderer::BuildPoints_Mesh(System.Int32,System.Nullable`1<UnityEngine.Vector3>)
extern void PointGrassRenderer_BuildPoints_Mesh_m68283F3A64997B2E73127B91105D2501F74225BB (void);
// 0x00000053 System.Void MicahW.PointGrass.PointGrassRenderer::BuildPoints_Terrain(System.Int32,System.Nullable`1<UnityEngine.Vector3>)
extern void PointGrassRenderer_BuildPoints_Terrain_mE2B10018E53754D361B63EDF70C17C8A1C706CE2 (void);
// 0x00000054 System.Boolean MicahW.PointGrass.PointGrassRenderer::GetMeshData(MicahW.PointGrass.PointGrassCommon/MeshData&)
extern void PointGrassRenderer_GetMeshData_mCF6800F541A60360ACDF687BB51CA1DD1B7B173D (void);
// 0x00000055 System.Boolean MicahW.PointGrass.PointGrassRenderer::GetTerrainMeshData(MicahW.PointGrass.PointGrassCommon/MeshData&,UnityEngine.Vector2Int)
extern void PointGrassRenderer_GetTerrainMeshData_mE1FA1BE05C1FCB35601AD79D3C1B0219ECA2BEF8 (void);
// 0x00000056 System.Void MicahW.PointGrass.PointGrassRenderer::CreateBuffers(MicahW.PointGrass.PointGrassCommon/MeshPoint[])
extern void PointGrassRenderer_CreateBuffers_mFC53982D8596D1E77FE4711A0D9FEC742A925FAB (void);
// 0x00000057 System.Void MicahW.PointGrass.PointGrassRenderer::ClearBuffers()
extern void PointGrassRenderer_ClearBuffers_mE07D764621EE41CB4ED709B578227440F728B60A (void);
// 0x00000058 System.Void MicahW.PointGrass.PointGrassRenderer::CreateBufferFromPoints(MicahW.PointGrass.PointGrassCommon/MeshPoint[],UnityEngine.ComputeBuffer&,UnityEngine.MaterialPropertyBlock&)
extern void PointGrassRenderer_CreateBufferFromPoints_mFC5340A5C557D32EAEDECCC71E3D48114E5DCF07 (void);
// 0x00000059 System.Void MicahW.PointGrass.PointGrassRenderer::CreateBuffersFromPoints_Multi(MicahW.PointGrass.PointGrassCommon/MeshPoint[],UnityEngine.ComputeBuffer[]&,UnityEngine.MaterialPropertyBlock[]&)
extern void PointGrassRenderer_CreateBuffersFromPoints_Multi_m0FB6D3F6742650ACA37DF48E03DE221ABC7EF599 (void);
// 0x0000005A System.Void MicahW.PointGrass.PointGrassRenderer::DrawGrass()
extern void PointGrassRenderer_DrawGrass_m3930D3BDAD2C3E7C6EB7DB2B1206DE93B1095288 (void);
// 0x0000005B System.Void MicahW.PointGrass.PointGrassRenderer::DrawGrassBuffer(UnityEngine.ComputeBuffer,UnityEngine.MaterialPropertyBlock&,UnityEngine.Mesh,UnityEngine.Material,UnityEngine.Bounds)
extern void PointGrassRenderer_DrawGrassBuffer_m97B71865FE29413A6BA689202968F9B24C097B88 (void);
// 0x0000005C UnityEngine.MaterialPropertyBlock MicahW.PointGrass.PointGrassRenderer::CreateMaterialPropertyBlock(UnityEngine.ComputeBuffer)
extern void PointGrassRenderer_CreateMaterialPropertyBlock_m0FC42DFE135544DE358968620AEAD8A957FC46B0 (void);
// 0x0000005D UnityEngine.Mesh MicahW.PointGrass.PointGrassRenderer::GetGrassMesh()
extern void PointGrassRenderer_GetGrassMesh_mE60BB12C500D3FF1E47167072BC6943666771124 (void);
// 0x0000005E UnityEngine.Bounds MicahW.PointGrass.PointGrassRenderer::GetLocalBounds()
extern void PointGrassRenderer_GetLocalBounds_mD32C528489677A59FBD7360789D895D7361C5E4D (void);
// 0x0000005F UnityEngine.Bounds MicahW.PointGrass.PointGrassRenderer::TransformBounds(UnityEngine.Bounds)
extern void PointGrassRenderer_TransformBounds_m4DDAF13C40FA22C3FD9D439ECE335DF21345A5A1 (void);
// 0x00000060 UnityEngine.Bounds MicahW.PointGrass.PointGrassRenderer::AddBoundsExtrusion(UnityEngine.Bounds)
extern void PointGrassRenderer_AddBoundsExtrusion_m2D5303CFC7A6FFCC3A0F3DDD2D8AE89AD7D55305 (void);
// 0x00000061 System.Void MicahW.PointGrass.PointGrassRenderer::SetDistributionSource(UnityEngine.Mesh)
extern void PointGrassRenderer_SetDistributionSource_mD92AFB9FF364F5C8B6AA62964D2F6851DAAC898E (void);
// 0x00000062 System.Void MicahW.PointGrass.PointGrassRenderer::SetDistributionSource(UnityEngine.MeshFilter)
extern void PointGrassRenderer_SetDistributionSource_mF4A9B204F14B12FCC37F992B6793992FAC5FECB5 (void);
// 0x00000063 System.Void MicahW.PointGrass.PointGrassRenderer::SetDistributionSource(UnityEngine.TerrainData)
extern void PointGrassRenderer_SetDistributionSource_m75F9D26F46EE2525591694087F2C7D38DE1470F3 (void);
// 0x00000064 System.Void MicahW.PointGrass.PointGrassRenderer::SetDistributionSource(UnityEngine.MeshFilter[])
extern void PointGrassRenderer_SetDistributionSource_m88344D60FD8578ECD8AE1B7A3A596E06C514B978 (void);
// 0x00000065 System.Void MicahW.PointGrass.PointGrassRenderer::SetBladeType(MicahW.PointGrass.PointGrassCommon/BladeType)
extern void PointGrassRenderer_SetBladeType_mF4BC8DC77A9FC34419A19830C292A4C415A6C805 (void);
// 0x00000066 System.Void MicahW.PointGrass.PointGrassRenderer::SetBladeMesh(UnityEngine.Mesh)
extern void PointGrassRenderer_SetBladeMesh_mE55D9726DCDFCAC335303BB80BDC47A5A172CC98 (void);
// 0x00000067 System.Void MicahW.PointGrass.PointGrassRenderer::SetBladeMesh(UnityEngine.Mesh[],System.Single[],UnityEngine.Material[])
extern void PointGrassRenderer_SetBladeMesh_m7EFCC1B0EA053D81B4E6A3C0AF493B50C7240E7C (void);
// 0x00000068 System.Void MicahW.PointGrass.PointGrassRenderer::SetBladeDensities(System.Single[])
extern void PointGrassRenderer_SetBladeDensities_mDDCC9D21547179AC46182BC9C554F127571BED69 (void);
// 0x00000069 System.Void MicahW.PointGrass.PointGrassRenderer::SetMaterial(UnityEngine.Material)
extern void PointGrassRenderer_SetMaterial_mF9A26D70C3BC4E8BDE7F1E2D8BDF0E796EDB6BCF (void);
// 0x0000006A System.Void MicahW.PointGrass.PointGrassRenderer::SetMaterials(UnityEngine.Material[])
extern void PointGrassRenderer_SetMaterials_mF165DA9F1096738EBA94283B8AF3C7BDA4F02DAC (void);
// 0x0000006B System.Void MicahW.PointGrass.PointGrassRenderer::SetShadowMode(UnityEngine.Rendering.ShadowCastingMode)
extern void PointGrassRenderer_SetShadowMode_mCA8965F6C3EF23B8E9298EBFBAB7E37642840709 (void);
// 0x0000006C System.Void MicahW.PointGrass.PointGrassRenderer::SetRenderLayer(System.Int32)
extern void PointGrassRenderer_SetRenderLayer_m8D795E6EC60BD400A501392384C25FBB52D15AA6 (void);
// 0x0000006D System.Void MicahW.PointGrass.PointGrassRenderer::SetRenderLayer(MicahW.PointGrass.SingleLayer)
extern void PointGrassRenderer_SetRenderLayer_m5E5C5358264C9EE01F09B2FDC581ED0396B14D03 (void);
// 0x0000006E System.Void MicahW.PointGrass.PointGrassRenderer::SetPointCount(System.Single,System.Boolean)
extern void PointGrassRenderer_SetPointCount_mB91F62C99EF3847582EA4A991660AF9EF85909A6 (void);
// 0x0000006F System.Void MicahW.PointGrass.PointGrassRenderer::SetPointLODFactor(System.Single)
extern void PointGrassRenderer_SetPointLODFactor_m6B56E6698E218C3A8EF8EBF54A6E5BA39F01974B (void);
// 0x00000070 System.Void MicahW.PointGrass.PointGrassRenderer::SetSeed(System.Int32)
extern void PointGrassRenderer_SetSeed_m1E7BFC08547D043358AC9B37853C86B71F16A5AB (void);
// 0x00000071 System.Void MicahW.PointGrass.PointGrassRenderer::SetSeed(System.Boolean)
extern void PointGrassRenderer_SetSeed_m7006831D12C690BB89E3CAE12641BA53806EEB99 (void);
// 0x00000072 System.Void MicahW.PointGrass.PointGrassRenderer::SetOverwriteNormal(UnityEngine.Vector3)
extern void PointGrassRenderer_SetOverwriteNormal_m886584547687DBF3796D81D48CAFE6DD101B1DED (void);
// 0x00000073 System.Void MicahW.PointGrass.PointGrassRenderer::SetOverwriteNormal(System.Boolean)
extern void PointGrassRenderer_SetOverwriteNormal_m90C4E02215600133BB9BE781320E40F6BF0C8432 (void);
// 0x00000074 System.Void MicahW.PointGrass.PointGrassRenderer::SetDensity(System.Boolean,System.Single)
extern void PointGrassRenderer_SetDensity_mEE9B214C95DD40BA047238BFF26605923DF0A922 (void);
// 0x00000075 System.Void MicahW.PointGrass.PointGrassRenderer::SetLength(System.Boolean,System.Single,System.Single)
extern void PointGrassRenderer_SetLength_m60833ECF4D40582FCC58115819F40FFEB1B048E4 (void);
// 0x00000076 System.Void MicahW.PointGrass.PointGrassRenderer::SetProjection(MicahW.PointGrass.PointGrassCommon/ProjectionType,UnityEngine.LayerMask)
extern void PointGrassRenderer_SetProjection_m3658FABFD119D3E8F6883AA55378A2270FC03F68 (void);
// 0x00000077 System.Void MicahW.PointGrass.PointGrassRenderer::SetBoundingBoxOffset(UnityEngine.Bounds)
extern void PointGrassRenderer_SetBoundingBoxOffset_m343441B544ABFF460275045A9DC2FDFD753CF66A (void);
// 0x00000078 MicahW.PointGrass.PointGrassRenderer/DebugInformation MicahW.PointGrass.PointGrassRenderer::GetDebugInfo()
extern void PointGrassRenderer_GetDebugInfo_m03C7213070DB1796513F105275D6250450C9500E (void);
// 0x00000079 System.Void MicahW.PointGrass.PointGrassRenderer::.ctor()
extern void PointGrassRenderer__ctor_mEE8F13109E74534D3943C869A8C52BB4AECF23FC (void);
// 0x0000007A System.Void MicahW.PointGrass.PointGrassRenderer::<CompatibilityCheck>g__Disable|43_0()
extern void PointGrassRenderer_U3CCompatibilityCheckU3Eg__DisableU7C43_0_mAD8E4C2F34F3754FDA2660945EF8B86F40B9AF3C (void);
static Il2CppMethodPointer s_methodPointers[122] = 
{
	FreeCam_Start_m3F115537F67F9ECAEDFC485B8C64E5984ED93348,
	FreeCam_Update_m87790A0FA44364BB417773C383A66444BF47B72C,
	FreeCam__ctor_m48EBAA195DB00803549D380950ABCDB9CC9433B2,
	ObjectOscillator_Start_mF5D4EF734128FE6DCBDA83C5AC82865A1523F859,
	ObjectOscillator_Update_mC3721C9C8D8241120B5FFB7A287C6FD3F13DC815,
	ObjectOscillator__ctor_m5E852F90A3D1DBDD07D28F92624934F31CDF9AD7,
	DistributePointsAlongMesh_DistributePoints_m697A346959B6378FF0BF569C2DD2C8189E81AAF4,
	DistributePointsAlongMesh_DistributePoints_CPU_m7EDF68975B721F8590DB32548BFCA21B9593805D,
	DistributePointsAlongMesh_GetCumulativeTriSizes_m64D783A59608A96F2FABF19CA7D0BB72F399B18C,
	DistributePointsAlongMesh_GetTriSizes_m8FCA660CA122706DD76F206211626C4421F8439B,
	DistributePointsAlongMesh_GetWeightedTriSizes_m7C97E84C6C9894F39FBE0A9E10D6581E58EF2C97,
	DistributePointsAlongMesh_FindTriangleIndex_mFA277A2DD05D747D627B4504C44E8BED5F0F8DFA,
	PointGrassCommon_get_BladeMeshesGenerated_m4E21A4A4AC4828F974670684D93241428FCBD935,
	PointGrassCommon_get_PropertyIDsInitialized_m9958FC8398CDF68DA0FFF72DAA3C35469B07D1A1,
	PointGrassCommon_set_PropertyIDsInitialized_mAC9F656EF7F0EF2F47B26CD89EC6BE6CD235245D,
	PointGrassCommon_FindPropertyIDs_m4B83779C3F568CC8BFEEFEC4FB0A50B525997711,
	PointGrassCommon_UpdateMaterialPropertyBlock_mC29A761CEB43323C5150BA2BBFDB228ABA7427C6,
	PointGrassCommon_GenerateGrassMeshes_m1187A0027530C7B935B788DF2975503346224C28,
	PointGrassCommon_GenerateGrassMesh_Flat_m0B8138A7DAEFC9028E47E9D68ACCCC39A3B2E23B,
	PointGrassCommon_GenerateGrassMesh_Cylinder_m3290C77744AA3E4E2C99868E8D082E81EC4A9D0F,
	PointGrassCommon_ProjectBaseMesh_m5562159B69B1FD110DEC077043E7DE5D4020F74B,
	PointGrassCommon_CreateTextureCopy_mADE8309152AFE527D5756F91C7491A9B0C2A8C1F,
	PointGrassCommon_CacheTerrainData_mE54FEFBA4451B4B69F63A2617DA43149225A3EAE,
	PointGrassCommon_CreateMeshFromTerrainData_mA5C0F6BD781BEB33C68AF5BF697E1FAE23002E55,
	PointGrassCommon_CreateMeshFromFilters_mD660AD50FC9DD73F259A0D447B2C689EF1A437B3,
	PointGrassCommon_U3CGenerateGrassMesh_CylinderU3Eg__SetVertU7C17_0_m9E44405726796B377C797C8B52794EF850E59ABD,
	MeshPoint__ctor_m6C2D6745D2F6CAC65B54433FBFAB990C649DC31E,
	ObjectData__ctor_mB2378318DDD47DCB6AEC9D788C27A66A94AE84CC,
	MeshData__ctor_m70ACE57271D2DB0D80D16027646899AFC5035A50,
	MeshData__ctor_m4DDE83D4B889F4A8E8CB34522845BB26A776D752,
	MeshData__ctor_mA84B1EAEC99B47A4A6F11607DB3AA143B334926C,
	MeshData_get_HasColours_m67226DD3676EBC3004861960060933A3EFB23FA2,
	MeshData_get_HasAttributes_m4F13A5EE919CA43A34BBE872AC66756434B30060,
	MeshData_RecalculateBounds_m23CC32EAA976F74CAD691ACF55DEDE635C504655,
	MeshData_RecalculateNormals_m3DB8055C317DE19049DCC6647BCCB199FAF40917,
	MeshData_ApplyDensityCutoff_m8F76018CA26B3621D4B38ED51A0EF5101D35B1E4,
	MeshData_ApplyLengthMapping_mCD709AEF3230ECE96D914FBDF433B3D1DF97C183,
	MeshData__cctor_m44C85DCB2151B6174F6AAEA659563486E628DC19,
	SingleLayer_get_LayerIndex_mB5C8A986682EAAA66CD1371500373D28A93B727E,
	SingleLayer_get_Mask_m46E3E593C1213BB974ABBCDD3A0FD09791DC1473,
	SingleLayer__ctor_mB56173673F914A84F0A215F602C28EC44017CE31,
	SingleLayer_Set_mBBB701BA6BC74136FD35A221A1341CFECEB93BEB,
	PointGrassDisplacementManager_get_DisplacerCount_m0526FE46A67C96DC0238F4E9281DB7DAEFF83E66,
	PointGrassDisplacementManager_add_OnInitialize_mD1CDC92403A311CF3BBA4C8913EF1BE50C48DD54,
	PointGrassDisplacementManager_remove_OnInitialize_m5F94865C071988D7AF7EF522E3ABFAFFABF7E6B0,
	PointGrassDisplacementManager_Awake_mAF13AF01B1DED72D347730F1F9B43FD0DC1A7E01,
	PointGrassDisplacementManager_OnDisable_m898149390DFAF08C40B552FAB668A5803BC82107,
	PointGrassDisplacementManager_LateUpdate_m158729EC12F6BCD84E77ED5264A436B0E8341C7E,
	PointGrassDisplacementManager_UpdateBuffer_m852C3B19D6D618139E8B3913E875A1DA1A48996F,
	PointGrassDisplacementManager_AddDisplacer_m34B41A7480D16398EC5BF58ECFB2A73F92EFEE6F,
	PointGrassDisplacementManager_RemoveDisplacer_m610EACCC6E76F1C21D580178793521035C34B32A,
	PointGrassDisplacementManager_UpdatePropertyBlock_mE705BEB14C825F65DBF87F0DA864B492DFBAC365,
	PointGrassDisplacementManager__ctor_m613DF958029CBB7AE2CCBA0A61A7A9A07012EC7F,
	PointGrassDisplacementManager__cctor_m36A658B29ED41A6CF72524B532CF9A7CE0B03509,
	DisplacementDelegate__ctor_m5FC18C77D43CFAAC46030EE3782AB012C1852A2B,
	DisplacementDelegate_Invoke_m5D544DE149EA8535E783D20BD994198770497B8D,
	DisplacementDelegate_BeginInvoke_m0E606E6CE383DFF7108CFBF1E37B6066114ABEBA,
	DisplacementDelegate_EndInvoke_m4269C29D065D5E2622B2F23184EF82132D62D8C5,
	U3CU3Ec__cctor_m1164B4575D6E45ED9E21975B33041E40006D5D18,
	U3CU3Ec__ctor_m81B88999DD9498F0E72DF82C81657C2AEB967FE3,
	U3CU3Ec_U3CUpdateBufferU3Eb__13_0_m28DE49B834780F799169B47BA3327FE3FCFF9CFA,
	PointGrassDisplacer_Reset_m372EADAF5A72A9C79F8B31C93AEDC70A66716764,
	PointGrassDisplacer_OnEnable_m24FE571048E2F0EB323702C02DD0C33EF143E448,
	PointGrassDisplacer_OnDisable_m0AA403F7A98845FEC1603359586985C16CEAF61E,
	PointGrassDisplacer_Initialize_m45044718CDB24241F017732A234C381653A0650A,
	PointGrassDisplacer_GetObjectData_m910E224560C96B24B45954527158E492A9CF4859,
	PointGrassDisplacer__ctor_m1D4A021DA0DC02AD9821550EB8B99231E3F612C7,
	PointGrassWind_OnEnable_m9D03B4E3F96CD5584E00FBA036873EA66302869C,
	PointGrassWind_LateUpdate_m72593FADB6033D1FC96B9565E6AE8A669CC58895,
	PointGrassWind_GetShaderIDs_mF3766A5521D8AFCA5B72DD11BA5EA10B02FCC8C9,
	PointGrassWind_RefreshValues_m9AF23283B96B07EF4B748964644941DD789CB8EA,
	PointGrassWind_PackProperties_mFEB1D1032F4CBADF707C35634FA8BE9169FF526B,
	PointGrassWind__ctor_m30AEB1C86052693AFE52034CDDFE4C4ED39A0F9A,
	PackedProperties__ctor_m1ADC969557DD708678FAFC6C730181CDAFEAC8A2,
	PointGrassRenderer_get_UsingMultipleMeshes_m61A92AF3BC219534CD301983C34CECB07E6A3E59,
	PointGrassRenderer_Reset_m56465633422BCAF773FAD656147169CBD7553DCA,
	PointGrassRenderer_OnEnable_mAAD03A50636AA34D588C4F740A42C7D0E0DD1083,
	PointGrassRenderer_OnDisable_mD6B91B87B90C3A9D61CEA537026CCCFCDAC9DC28,
	PointGrassRenderer_LateUpdate_mDB02656F534CADAF622888481915E2866BF206E2,
	PointGrassRenderer_CompatibilityCheck_mF24B3CB5752F3B46635B710E9399DC0169EDC298,
	PointGrassRenderer_BuildPoints_m22ADAC358BA12DDC83B6F4E9AAB9317E4A53FA13,
	PointGrassRenderer_BuildPoints_Mesh_m68283F3A64997B2E73127B91105D2501F74225BB,
	PointGrassRenderer_BuildPoints_Terrain_mE2B10018E53754D361B63EDF70C17C8A1C706CE2,
	PointGrassRenderer_GetMeshData_mCF6800F541A60360ACDF687BB51CA1DD1B7B173D,
	PointGrassRenderer_GetTerrainMeshData_mE1FA1BE05C1FCB35601AD79D3C1B0219ECA2BEF8,
	PointGrassRenderer_CreateBuffers_mFC53982D8596D1E77FE4711A0D9FEC742A925FAB,
	PointGrassRenderer_ClearBuffers_mE07D764621EE41CB4ED709B578227440F728B60A,
	PointGrassRenderer_CreateBufferFromPoints_mFC5340A5C557D32EAEDECCC71E3D48114E5DCF07,
	PointGrassRenderer_CreateBuffersFromPoints_Multi_m0FB6D3F6742650ACA37DF48E03DE221ABC7EF599,
	PointGrassRenderer_DrawGrass_m3930D3BDAD2C3E7C6EB7DB2B1206DE93B1095288,
	PointGrassRenderer_DrawGrassBuffer_m97B71865FE29413A6BA689202968F9B24C097B88,
	PointGrassRenderer_CreateMaterialPropertyBlock_m0FC42DFE135544DE358968620AEAD8A957FC46B0,
	PointGrassRenderer_GetGrassMesh_mE60BB12C500D3FF1E47167072BC6943666771124,
	PointGrassRenderer_GetLocalBounds_mD32C528489677A59FBD7360789D895D7361C5E4D,
	PointGrassRenderer_TransformBounds_m4DDAF13C40FA22C3FD9D439ECE335DF21345A5A1,
	PointGrassRenderer_AddBoundsExtrusion_m2D5303CFC7A6FFCC3A0F3DDD2D8AE89AD7D55305,
	PointGrassRenderer_SetDistributionSource_mD92AFB9FF364F5C8B6AA62964D2F6851DAAC898E,
	PointGrassRenderer_SetDistributionSource_mF4A9B204F14B12FCC37F992B6793992FAC5FECB5,
	PointGrassRenderer_SetDistributionSource_m75F9D26F46EE2525591694087F2C7D38DE1470F3,
	PointGrassRenderer_SetDistributionSource_m88344D60FD8578ECD8AE1B7A3A596E06C514B978,
	PointGrassRenderer_SetBladeType_mF4BC8DC77A9FC34419A19830C292A4C415A6C805,
	PointGrassRenderer_SetBladeMesh_mE55D9726DCDFCAC335303BB80BDC47A5A172CC98,
	PointGrassRenderer_SetBladeMesh_m7EFCC1B0EA053D81B4E6A3C0AF493B50C7240E7C,
	PointGrassRenderer_SetBladeDensities_mDDCC9D21547179AC46182BC9C554F127571BED69,
	PointGrassRenderer_SetMaterial_mF9A26D70C3BC4E8BDE7F1E2D8BDF0E796EDB6BCF,
	PointGrassRenderer_SetMaterials_mF165DA9F1096738EBA94283B8AF3C7BDA4F02DAC,
	PointGrassRenderer_SetShadowMode_mCA8965F6C3EF23B8E9298EBFBAB7E37642840709,
	PointGrassRenderer_SetRenderLayer_m8D795E6EC60BD400A501392384C25FBB52D15AA6,
	PointGrassRenderer_SetRenderLayer_m5E5C5358264C9EE01F09B2FDC581ED0396B14D03,
	PointGrassRenderer_SetPointCount_mB91F62C99EF3847582EA4A991660AF9EF85909A6,
	PointGrassRenderer_SetPointLODFactor_m6B56E6698E218C3A8EF8EBF54A6E5BA39F01974B,
	PointGrassRenderer_SetSeed_m1E7BFC08547D043358AC9B37853C86B71F16A5AB,
	PointGrassRenderer_SetSeed_m7006831D12C690BB89E3CAE12641BA53806EEB99,
	PointGrassRenderer_SetOverwriteNormal_m886584547687DBF3796D81D48CAFE6DD101B1DED,
	PointGrassRenderer_SetOverwriteNormal_m90C4E02215600133BB9BE781320E40F6BF0C8432,
	PointGrassRenderer_SetDensity_mEE9B214C95DD40BA047238BFF26605923DF0A922,
	PointGrassRenderer_SetLength_m60833ECF4D40582FCC58115819F40FFEB1B048E4,
	PointGrassRenderer_SetProjection_m3658FABFD119D3E8F6883AA55378A2270FC03F68,
	PointGrassRenderer_SetBoundingBoxOffset_m343441B544ABFF460275045A9DC2FDFD753CF66A,
	PointGrassRenderer_GetDebugInfo_m03C7213070DB1796513F105275D6250450C9500E,
	PointGrassRenderer__ctor_mEE8F13109E74534D3943C869A8C52BB4AECF23FC,
	PointGrassRenderer_U3CCompatibilityCheckU3Eg__DisableU7C43_0_mAD8E4C2F34F3754FDA2660945EF8B86F40B9AF3C,
};
extern void MeshPoint__ctor_m6C2D6745D2F6CAC65B54433FBFAB990C649DC31E_AdjustorThunk (void);
extern void ObjectData__ctor_mB2378318DDD47DCB6AEC9D788C27A66A94AE84CC_AdjustorThunk (void);
extern void SingleLayer_get_LayerIndex_mB5C8A986682EAAA66CD1371500373D28A93B727E_AdjustorThunk (void);
extern void SingleLayer_get_Mask_m46E3E593C1213BB974ABBCDD3A0FD09791DC1473_AdjustorThunk (void);
extern void SingleLayer__ctor_mB56173673F914A84F0A215F602C28EC44017CE31_AdjustorThunk (void);
extern void SingleLayer_Set_mBBB701BA6BC74136FD35A221A1341CFECEB93BEB_AdjustorThunk (void);
extern void PackedProperties__ctor_m1ADC969557DD708678FAFC6C730181CDAFEAC8A2_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[7] = 
{
	{ 0x0600001B, MeshPoint__ctor_m6C2D6745D2F6CAC65B54433FBFAB990C649DC31E_AdjustorThunk },
	{ 0x0600001C, ObjectData__ctor_mB2378318DDD47DCB6AEC9D788C27A66A94AE84CC_AdjustorThunk },
	{ 0x06000027, SingleLayer_get_LayerIndex_mB5C8A986682EAAA66CD1371500373D28A93B727E_AdjustorThunk },
	{ 0x06000028, SingleLayer_get_Mask_m46E3E593C1213BB974ABBCDD3A0FD09791DC1473_AdjustorThunk },
	{ 0x06000029, SingleLayer__ctor_mB56173673F914A84F0A215F602C28EC44017CE31_AdjustorThunk },
	{ 0x0600002A, SingleLayer_Set_mBBB701BA6BC74136FD35A221A1341CFECEB93BEB_AdjustorThunk },
	{ 0x0600004A, PackedProperties__ctor_m1ADC969557DD708678FAFC6C730181CDAFEAC8A2_AdjustorThunk },
};
static const int32_t s_InvokerIndices[122] = 
{
	8550,
	8550,
	8550,
	8550,
	8550,
	8550,
	8849,
	8917,
	9102,
	10806,
	10158,
	11747,
	14685,
	14685,
	13818,
	14771,
	12182,
	14771,
	13494,
	11838,
	11073,
	10797,
	12283,
	9105,
	11861,
	11112,
	1474,
	2094,
	373,
	722,
	1427,
	8283,
	8283,
	8550,
	8550,
	6767,
	6828,
	14771,
	8366,
	8366,
	6669,
	6669,
	8366,
	13829,
	13829,
	8550,
	8550,
	8550,
	8550,
	6702,
	6702,
	6571,
	8550,
	14771,
	3761,
	6702,
	1743,
	6702,
	14771,
	8550,
	7349,
	8550,
	8550,
	8550,
	6702,
	8717,
	8550,
	8550,
	8550,
	14771,
	8550,
	8719,
	8550,
	1475,
	8283,
	8550,
	8550,
	8550,
	8550,
	8283,
	8550,
	3311,
	3311,
	4664,
	2243,
	6702,
	8550,
	1954,
	1954,
	8550,
	685,
	5969,
	8400,
	8282,
	4332,
	4332,
	6702,
	6702,
	6702,
	6702,
	6669,
	6702,
	2006,
	6702,
	6702,
	6702,
	6669,
	6669,
	6768,
	3837,
	6767,
	6669,
	6583,
	6830,
	6583,
	3106,
	1857,
	3431,
	6582,
	8718,
	8550,
	8550,
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_MicahW_PointGrassRenderer_Runtime_CodeGenModule;
const Il2CppCodeGenModule g_MicahW_PointGrassRenderer_Runtime_CodeGenModule = 
{
	"MicahW.PointGrassRenderer.Runtime.dll",
	122,
	s_methodPointers,
	7,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
